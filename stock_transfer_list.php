<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: stock_transfer_list.php

CREATED ON	: 09-Dec-2016

CREATED BY	: Lakshmi

PURPOSE     : List of stock for customer withdrawals

*/



/*

TBD: 

*/



/* DEFINES - START */

define('STOCK_TRANSFER_FUNC_ID','175');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



	$alert_type = -1;

	$alert = "";

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list    = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'2','1');

	$edit_perms_list    = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'3','1');

	$delete_perms_list  = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'4','1');

	$add_perms_list     = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'1','1');
	
	$approve_perms_list = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'6','1');
     
	 //Get Location List

	$stock_location_master_search_data = array();

	$location_list = i_get_stock_location_master_list($stock_location_master_search_data);

	if($location_list["status"] == SUCCESS)

	{

		$location_list_data = $location_list["data"];

	}

	else

	{ 

		$alert = $location_list["data"];

		$alert_type = 0;

	}

	

	

	 // Get Stock Transfer modes already added

	$stock_transfer_search_data = array("status"=>"Pending"	);

	$stock_transfer_list = i_get_stock_transfer($stock_transfer_search_data);

	if($stock_transfer_list['status'] == SUCCESS)

	{

		$stock_transfer_list_data = $stock_transfer_list['data'];

	}	

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Stock Transfer List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Stock Transfer Request List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="stock_add_transfer.php"> Request Stock Transfer</a></span><?php } ?>

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			

              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

				    <th width="6%">SL No</th>

					<th style="word-wrap:break-word;">Material Name</th>

					<th width="10%">Quantity</th>

					<th style="word-wrap:break-word;">Source Project</th>

					<th style="word-wrap:break-word;">Destination Project</th>

					<th style="word-wrap:break-word;">Remarks</th>

					<th style="word-wrap:break-word;">Requested By</th>					

					<th style="word-wrap:break-word;">Requested On</th>									

					<th colspan= "2" style="word-wrap:break-word">Actions</th>

    					

				</tr>

				</thead>

				<tbody>							

				<?php

				if($stock_transfer_list["status"] == SUCCESS)

				{

					$sl_no = 0;

					for($count = 0; $count < count($stock_transfer_list_data); $count++)

					{

						$sl_no++;
						$added_by = $stock_transfer_list_data[$count]["added_by_id"];
						
					?>

					<tr>

					<td width="6%"><?php echo $sl_no; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["stock_material_name"]; ?> <br><?php echo $stock_transfer_list_data[$count]["stock_material_code"]; ?></td>

					<td width="10%"><?php echo $stock_transfer_list_data[$count]["stock_transfer_quantity"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["source"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["dest"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["stock_transfer_remarks"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["added_by"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($stock_transfer_list_data[$count][

					"stock_transfer_added_on"])); ?></td>					

					<td style="word-wrap:break-word;"><?php if(($stock_transfer_list_data[$count]["stock_transfer_active"] == "1")){?><a href="#" onclick="return stock_delete_transfer(<?php echo $stock_transfer_list_data[$count]["stock_transfer_id"]; ?>);">Delete</a><?php } ?></td>
					
					<td style="word-wrap:break-word;"><?php if(($user != $added_by) && ($approve_perms_list['status'] == SUCCESS)) { ?><?php if(($stock_transfer_list_data[$count]["stock_transfer_status"] == "Pending")){?><a href="#" onclick="return stock_approve_transfer(<?php echo $stock_transfer_list_data[$count]["stock_transfer_id"]; ?>);">Approve</a><?php } else if($stock_transfer_list_data[$count]["stock_transfer_status"] == "Approved") { echo "Approved" ;} } else
						{
							echo "";
						}?></td>
					

					</tr>

					<?php

					}

					

				}

				else

				{

				?>

				<td colspan="9">No Stock Transfer Request yet!</td>

				

				<?php

				}

				 ?>	



                </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function stock_delete_transfer(transfer_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

						alert(xmlhttp.responseText)

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_transfer_list.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_transfer.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("transfer_id=" + transfer_id + "&action=0");

		}

	}	

}

function stock_approve_transfer(transfer_id)

{

	var ok = confirm("Are you sure you want to Approve?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

						alert(xmlhttp.responseText)

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_transfer_list.php";

					}

				}

			}



			xmlhttp.open("POST", "ajax/stock_approve_stock_transfer.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("transfer_id=" + transfer_id + "&action=0");

		}

	}	

}

function go_to_stock_edit_transfer(transfer_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "get");

    form.setAttribute("action", "stock_edit_transfer.php");

	

	var hiddenField1 = document.createElement("input");

	hiddenField1.setAttribute("type","hidden");

	hiddenField1.setAttribute("name","transfer_id");

	hiddenField1.setAttribute("value",transfer_id);

	

	form.appendChild(hiddenField1);

	

	document.body.appendChild(form);

    form.submit();

}

</script>



  </body>



</html>