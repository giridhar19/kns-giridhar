<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_REQUEST['plan_id']))
	{
		$plan_id = $_REQUEST['plan_id'];
	}
	else
	{
		// Do nothing
	}
	
	
	// Capture the form data
	if(isset($_POST["add_project_plan_process_submit"]))
	{

		$plan_id              = $_POST["hd_plan_id"];
		$name 	              = $_POST["ddl_name"];
		$method 	          = $_POST["txt_method"];
		$process_start_date   = $_POST["date_process_start_date"];
		$process_end_date 	  = $_POST["date_process_end_date"];
		$remarks 	          = $_POST["txt_remarks"];
		$assigned_user 	      = $_POST["ddl_assigned_user"];
		
		// Check for mandatory fields
		if(($plan_id != "") && ($name != "") && ($method != "") && ($process_start_date != "") && ($process_end_date != "") && ($assigned_user != ""))
		{
			if(((strtotime($process_start_date)) <= (strtotime($process_end_date))) && ((strtotime($process_start_date)) >= strtotime(date('Y-m-d'))))
			{
				$project_plan_process_iresult = i_add_project_plan_process($plan_id,$name,$method,$process_start_date,$process_end_date,$remarks,$user,'',$assigned_user);
				
				if($project_plan_process_iresult["status"] == SUCCESS)
				{	
					$project_plan_process_id = $project_plan_process_iresult["data"];
					header("location:project_add_task.php?project_plan_process_id=$project_plan_process_id");
				}
				
				$alert = $project_plan_process_iresult["data"];
			}
			else
			{
				$alert_type = 0;
				$alert      = 'Start Date and End Date cannot be earlier than today. End Date cannot be earlier than start date';
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

   // Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}

// Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

	// Get Project Plan Process modes already added
		$project_plan_process_search_data = array("active"=>'1');
		$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
		if($project_plan_process_list['status'] == SUCCESS)
		{
			$project_plan_process_list_data = $project_plan_process_list['data'];
		}	
	}
	else
	{
		header("location:login.php");
	}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Project Plan Process</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Project Plan Process</h3><span style="float:right; padding-right:20px;"><a href="project_plan_process_list.php">Project Plan Process List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Project Plan Process</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_add_project_plan_process_form" class="form-horizontal" method="post" action="project_add_project_plan_process.php">
								<input type="hidden" name="hd_plan_id" value="<?php echo $plan_id; ?>" />
									<fieldset>										
																
											<div class="control-group">											
											<label class="control-label" for="ddl_name">Process*</label>
											<div class="controls">
												<select name="ddl_name" required>
												<option>- - Select Process - -</option>
												<?php
												for($count = 0; $count < count($project_process_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_process_master_list_data[$count]["project_process_master_id"]; ?>"><?php echo $project_process_master_list_data[$count]["project_process_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_method">Work Description*</label>
											<div class="controls">
												<textarea rows="4" cols="50" class="span6" name="txt_method" placeholder="Method" required="required"></textarea>
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="date_process_start_date">Plan Start Date</label>
											<div class="controls">
												<input type="date" class="span6" name="date_process_start_date" placeholder="Start Date">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="date_process_end_date">Plan End Date</label>
											<div class="controls">
												<input type="date" class="span6" name="date_process_end_date" placeholder="End Date">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_assigned_user">Assigned User*</label>
											<div class="controls">
												<select name="ddl_assigned_user" required>
												<option>- - Select User - -</option>
												<?php
												for($count = 0; $count < count($user_list_data); $count++)
												{
												?>
												<option value="<?php echo $user_list_data[$count]["user_id"]; ?>"><?php echo $user_list_data[$count]["user_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_project_plan_process_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
