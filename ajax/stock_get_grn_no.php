<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$search  = $_POST["search"];
	$stock_grn_search_data = array("grn_no"=>$search,"grn_no_check"=>'2');
	$grn_list_result = i_get_stock_grn_list($stock_grn_search_data);
	if($grn_list_result["status"] == SUCCESS)
	{		
		$grn_list = '';

		for($count = 0; $count < count($grn_list_result['data']); $count++)
		{
			$grn_list = $grn_list.'<a style="display:block; width:100%; border-bottom:1px solid #efefef; text-decoration:none; cursor:pointer; padding:5px;" onclick="return select_grn_no(\''.$grn_list_result['data'][$count]['stock_grn_id'].'\',\''.$grn_list_result['data'][$count]['stock_grn_no'].'\');">'.$grn_list_result['data'][$count]['stock_grn_no'].'</a>';
		}
	}
	else
	{
		$grn_list = 'FAILURE';
	}
	
	echo $grn_list;
}
else
{
	header("location:login.php");
}
?>