<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$machine_id      = $_POST["machine_id"];
	// Get stock quantity
	$project_machine_rate_master_search_data = array("machine_type"=>$machine_id,"order"=>'rate_desc',"active"=>'1');
	$project_machine_rate_master_data =  i_get_project_machine_rate_master($project_machine_rate_master_search_data);
	if($project_machine_rate_master_data["status"] == SUCCESS)
	{
		if($machine_rate = $project_machine_rate_master_data["data"][0]["project_machine_vendor_fuel"] > $machine_rate = $project_machine_rate_master_data["data"][0]["project_machine_kns_fuel"])
		{
			$machine_rate = $project_machine_rate_master_data["data"][0]["project_machine_vendor_fuel"];
		}
		else
		{
			$machine_rate = $project_machine_rate_master_data["data"][0]["project_machine_kns_fuel"];
		}
	}
	else
	{
		$machine_rate = "0";
	}
	
	// Get machine vendor
	$project_machine_master_search_data = array("machine_id"=>$machine_id);
	$project_machine_rate_master_data =  i_get_project_machine_master($project_machine_master_search_data);
	if($project_machine_rate_master_data["status"] == SUCCESS)
	{
		$machine_vendor = $project_machine_rate_master_data["data"][0]["project_machine_vendor_master_name"];
	}
	else
	{
		$machine_vendor = "";
	}
	
	$result = array("rate"=>$machine_rate,"vendor"=>$machine_vendor);
	
	echo json_encode($result);
}
else
{
	header("location:login.php");
}
?>