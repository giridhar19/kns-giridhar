<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'katha_transfer'.DIRECTORY_SEPARATOR.'kt_buy_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$request_id        = $_POST["request_id"];
	$active            = $_POST["action"];
	
	$kt_buy_request_update_data = array("active"=>$active);
	$kt_buy_request_result = i_update_kt_buy_request($request_id,$kt_buy_request_update_data);
	
	if($kt_buy_request_result["status"] == FAILURE)
	{
		echo $kt_buy_request_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>