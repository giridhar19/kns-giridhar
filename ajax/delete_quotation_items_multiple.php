<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
 
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$item_list = $_POST["quote_items"];
	$item_list_trimmed = trim($item_list,',');
	
	$quote_item_array = explode(',',$item_list_trimmed);
	$status = 1;
	for($count = 0; $count < count($quote_item_array); $count++)
	{
		
		$quotation_compare_update_data = array("active"=>'0');
		$delete_files_result = i_update_quotation_compare($quote_item_array[$count],'',$quotation_compare_update_data);	
		if($delete_files_result["status"] == FAILURE)
		{
			$status = $status & 0;
		}
		else
		{			
			
		}
	}
	
	echo $status;
}
else
{
	header("location:login.php");
}
?>