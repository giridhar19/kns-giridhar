<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

TBD:

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Update attendance details

	$transfer_id   		 = $_POST["transfer_id"];
	$status        		 = $_POST["action"];
	$s_project     		 = $_POST["s_project"];
	$d_project     		 = $_POST["d_project"];
	$material_id   		 = $_POST["material_id"];
	$qty		   		 = $_POST["qty"];
	$accepted_qty		 = $_POST["accepted_qty"];
	$remarks		   	 = $_POST["remarks"];
	$accepted_on    	 = date("Y-m-d H:i:s");
	$accepted_by   		 = $user;

	$stock_transfer_update_data = array("status"=>'Accepted',"accepted_on"=>$accepted_on,"accepted_by"=>$accepted_by,"source_project"=>$s_project,"destination_project"=>$d_project,"material_id"=>$material_id,"quantity"=>$qty,"accepted_qty"=>$accepted_qty,"accepted_remarks"=>$remarks);

	$accept_files_result = i_transfer_stock($transfer_id,$stock_transfer_update_data);

	if($accept_files_result["status"] == FAILURE)

	{

		echo $accept_files_result["data"];

	}

	else

	{

		echo "SUCCESS";

	}

}

else

{

	header("location:login.php");

}

?>