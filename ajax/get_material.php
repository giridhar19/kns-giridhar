<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$search  = $_POST["search"];
	$stock_material_search_data = array("material_name_code"=>$search,"material_active"=>'1');
	$material_list_result = i_get_stock_material_master_list($stock_material_search_data);
	if($material_list_result["status"] == SUCCESS)
	{		
		$material_list = '';

		for($count = 0; $count < count($material_list_result['data']); $count++)
		{
			$material_list = $material_list.'<a style="display:block; width:100%; border-bottom:1px solid #efefef; text-decoration:none; cursor:pointer; padding:5px;" onclick="return select_material(\''.$material_list_result['data'][$count]['stock_material_id'].'\',\''.$material_list_result['data'][$count]['stock_material_name'].'\',\''.$material_list_result['data'][$count]['stock_material_code'].'\',\''.$material_list_result['data'][$count]['stock_unit_name'].'\')">'.$material_list_result['data'][$count]['stock_material_name'].' - '.$material_list_result['data'][$count]['stock_material_code'].'</a>';
		}
	}
	else
	{
		$material_list = 'FAILURE';
	}
	
	echo $material_list;
}
else
{
	header("location:login.php");
}
?>