<?php

/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock_vendor_master.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'transaction_config.php');
/*
PURPOSE : To add Stock Vendor Master
INPUT 	: Vendor Code,Address,Contact Person,Contact Number,Email Id,Type of Service,Country,Pan No,Service Contact No,Approved List No,Remarks,Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_stock_vendor_master($address,$vendor_name,$contact_person,$contact_number,$email_id,$type_of_service,$country,$pan_no,$service_contact_no,$approved_list_no,$remarks,$added_by)
{
	$vendor_code =  p_generate_vendor_no();
	$vendor_master_iresult = db_add_stock_vendor_master($vendor_code,$address,$vendor_name,$contact_person,$contact_number,$email_id,$type_of_service,$country,$pan_no,$service_contact_no,$approved_list_no,$remarks,$added_by);
	
	if($vendor_master_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Vendor Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To get Vendor Master list
INPUT 	: Vendor ID, Active
OUTPUT 	: Vendor Master List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_stock_vendor_master_list($stock_vendor_master_search_data)
{
	$vendor_master_sresult = db_get_stock_vendor_master_list($stock_vendor_master_search_data);
	
	if($vendor_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $vendor_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No vendor master added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Vendor Master
INPUT 	: Vendor ID, Vendor Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_vendor_master($vendor_id,$vendor_name,$vendor_master_update_data)
{
	$vendor_master_sresult = db_update_vendor_master($vendor_id,$vendor_master_update_data);
	
	if($vendor_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Vendor Successfully updated";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To delete Vendor Master
INPUT 	: Vendor ID, Vendor Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_vendor_master($vendor_id,$vendor_master_update_data)
{
	$vendor_master_sresult = db_update_vendor_master($vendor_id,$vendor_master_update_data);
	
	if($vendor_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Vendor Successfully deleted";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To add Stock Vendor Type Of Service Master
INPUT 	: Service Name, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_type_of_service($service_name,$remarks,$added_by)
{
	$stock_type_of_service_search_data = array("service_name"=>$service_name,"service_name_check"=>'1','active'=>'1');
	$type_of_service_sresult = db_get_stock_type_of_service_list($stock_type_of_service_search_data);
	
	if($type_of_service_sresult["status"] == DB_NO_RECORD)
	{
		$type_of_service_iresult = db_add_stock_type_of_service_master($service_name,$remarks,$added_by);
		
		if($type_of_service_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Type of Service Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This Service Type already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Type Of Service Master list
INPUT 	: Service Name, Active
OUTPUT 	: Type Of Service List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_type_of_service_list($stock_type_of_service_search_data)
{
	$type_of_service_sresult = db_get_stock_type_of_service_list($stock_type_of_service_search_data);
	
	if($type_of_service_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$type_of_service_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No service added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Stock Type OF Service Master
INPUT 	: Service ID,Stock Type OF Service Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_stock_type_of_service($service_id,$stock_type_of_service_update_data)
{
	$stock_service_type_search_data = array("service_name"=>$stock_type_of_service_update_data['service_name'],"service_name_check"=>'1',"active"=>'1');
	$service_type_master_sresult = db_get_stock_type_of_service_list($stock_service_type_search_data);
	
	$allow_update = false;
	if($service_type_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($service_type_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($service_type_master_sresult['data'][0]['stock_type_of_service_id'] == $service_id)
		{
			$allow_update = true;
		}
	}
	
	$type_of_service_sresult = db_update_stock_type_of_service($service_id,$stock_type_of_service_update_data);
		
	if($type_of_service_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Type of Service Successfully updated";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}
/*
PURPOSE : To add Stock Vendor Item Mapping
INPUT 	: Vendor ID, Item ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_stock_vendor_item_mapping($vendor_id,$item_id,$remarks,$added_by)
{
	$stock_vendor_item_mapping_search_data = array("vendor_id"=>$vendor_id,"item_id"=>$item_id);
	$type_of_service_sresult =  db_get_stock_vendor_item_mapping($stock_vendor_item_mapping_search_data);
	
	if($type_of_service_sresult["status"] == DB_NO_RECORD)
	{
	
		$stock_vendor_item_mapping_iresult =  db_add_stock_vendor_item_mapping($vendor_id,$item_id,$remarks,$added_by);
		
		if($stock_vendor_item_mapping_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "stock vendor item mapping Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}					
	}
	else
	{
		$return["data"]   = "";
		$return["status"] = FAILURE;
	}
	
	return $return;
}
	
/*
PURPOSE : To get Stock Vendor Item Mapping list
INPUT 	: Mapping ID, Vendor ID, Item ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Stock Vendor Item Mapping
BY 		: Lakshmi
*/
function i_get_stock_vendor_item_mapping($stock_vendor_item_mapping_search_data)
{
	$stock_vendor_item_mapping_sresult = db_get_stock_vendor_item_mapping($stock_vendor_item_mapping_search_data);
	
	if($stock_vendor_item_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $stock_vendor_item_mapping_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No item has been mapped to vendors yet. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Stock Vendor Item Mapping
INPUT 	: Mapping ID, Stock Vendor Item Mapping Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_stock_vendor_item_mapping($mapping_id,$stock_vendor_item_mapping_update_data)
{
	$stock_vendor_item_mapping_sresult = db_update_stock_vendor_item_mapping($mapping_id,$stock_vendor_item_mapping_update_data);
		
	if($stock_vendor_item_mapping_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Vendor Item Mapping Successfully updated";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

function p_generate_vendor_no()
{
	// Get the last invoice no
	$stock_vendor_master_search_data = array("sort"=>'1');
	$indent_no_data = db_get_stock_vendor_master_list($stock_vendor_master_search_data);
	
	if($indent_no_data["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$invoice_numeric_value_data = $indent_no_data["data"][0]["stock_vendor_code"];
		$invoice_numeric_value_array = explode(INV_NO_DELIMITER,$invoice_numeric_value_data);
		
		$invoice_numeric_value = ($invoice_numeric_value_array[2]) + 1;
	}
	else
	{
		$invoice_numeric_value = 1;
	}
	
	return INV_NO_ROOT.INV_NO_DELIMITER.INV_NO_VENDOR.INV_NO_DELIMITER.$invoice_numeric_value;
}
?>