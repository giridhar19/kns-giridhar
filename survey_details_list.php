<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: survey_details_list.php
CREATED ON	: 05-Jan-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('SURVEY_DETAILS_LIST_FUNC_ID','201');
/* DEFINES - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',SURVEY_DETAILS_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',SURVEY_DETAILS_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',SURVEY_DETAILS_LIST_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',SURVEY_DETAILS_LIST_FUNC_ID,'1','1');

	// Query String Data
	// Nothing
	
	if(isset($_GET['survey_id']))
	{
		$survey_id = $_GET['survey_id'];
	}
	else
	{
		$survey_id = "";
	}

	if(isset($_GET["survey_process_id"]))
	{
		$process_id = $_GET["survey_process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	$search_village    = "";
	$search_project    = "";
	
	if(isset($_POST['file_search_submit']))
	{
		$search_village        = $_POST["search_village"];
		$search_project        = $_POST["search_project"];
		$survey_id             = $_POST["hd_survey_id"];
	}
	
	// Get Survey Process Master modes already added
	$survey_process_master_search_data = array("process_id"=>$process_id);
	$survey_process_master_list = i_get_survey_process_master($survey_process_master_search_data);
	if($survey_process_master_list['status'] == SUCCESS)
	{
		$survey_process_master_list_data = $survey_process_master_list['data'];
	}	

	else
	{
		$alert = $survey_process_master_list["data"];
		$alert_type = 0;
	}
	
	 // Get Village Master modes already added
	$village_master_list = i_get_village_list('');
	if($village_master_list['status'] == SUCCESS)
	{
		$village_master_list_data = $village_master_list["data"];
	}
	else
	{
		$alert = $village_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Survey already added
	$survey_project_master_search_data = array("active"=>'1');
	$survey_project_master_list = i_get_survey_project_master($survey_project_master_search_data);
	if($survey_project_master_list['status'] == SUCCESS)
	{
		$survey_project_master_list_data = $survey_project_master_list['data'];
	}
	else
	{
		$alert = $survey_project_master_list["data"];
		$alert_type = 0;
	}

	// Get Survey  Details already added
	$survey_details_search_data = array("active"=>'1',"survey_id"=>$survey_id,"process_id"=>$process_id,"village"=>$search_village,"project"=>$search_project);
	$survey_details_list = i_get_survey_details($survey_details_search_data);
	if($survey_details_list['status'] == SUCCESS)
	{
		$survey_details_list_data = $survey_details_list['data'];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey Details List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Survey Details List</h3><?php if($add_perms_list["status"] == SUCCESS) {?><span style="float:right; padding-right:20px;"><a href="survey_add_details.php">Survey Add Details</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
			<?php 
			if($survey_id == '')
			{?>
			<div class="widget-header" style="height:50px; padding-top:10px;">
			<form method="post" id="file_search_form" action="survey_details_list.php">
			<input type="hidden" name="hd_survey_id" value="<?php echo $survey_id; ?>" />
			
			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($survey_project_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $survey_project_master_list_data[$project_count]["survey_project_master_id"]; ?>" <?php if($search_project == $survey_project_master_list_data[$project_count]["survey_project_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $survey_project_master_list_data[$project_count]["survey_project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_village">
			  <option value="">- - Select Village - -</option>
			  <?php
			  for($village_count = 0; $village_count < count($village_master_list_data); $village_count++)
			  {
			  ?>
			  <option value="<?php echo $village_master_list_data[$village_count]["village_id"]; ?>" <?php if($search_village == $village_master_list_data[$village_count]["village_id"]) { ?> selected="selected" <?php } ?>><?php echo $village_master_list_data[$village_count]["village_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
			  </div>
			<?php
			}
			?>
            <div class="widget-content">
			<?php 
			if($view_perms_list["status"] == SUCCESS)
			{
		    ?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
					<th>Village</th>
					<th>Extent</th>
					<th>Planned Start Date</th>
					<th>Planned End Date</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="5" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($survey_details_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($survey_details_list_data); $count++)
					{
						$sl_no++;
						
						    //Get  BD project Master
							//$survey_project_master_search_data = array("active"=>'1',"name"=>$survey_details_list_data[$count]["survey_details_project"]);
	                        //$survey_project_master_list = i_get_survey_project_master($survey_project_master_search_data);
							//if($survey_project_master_list["status"] == SUCCESS)
							//{
								//$survey_project_master_list_data = $survey_project_master_list["data"];
								//$project_name = $survey_project_master_list_data[0]["survey_project_master_name"];
							//}
							
							// Get Survey File already added
							$survey_file_search_data = array("active"=>'1',"survey_id"=>$survey_details_list_data[$count]["survey_details_id"]);
							$survey_file_list = i_get_survey_file($survey_file_search_data);
							if($survey_file_list['status'] == SUCCESS)
							{
								$survey_file_list_data = $survey_file_list["data"];
								$extent = $survey_file_list_data[0]["survey_master_extent"];
							}
							else
							{
								$extent = '';
							}
						
									
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $survey_details_list_data[$count]["survey_project_master_name"]; ?></td>
					<td><?php echo $survey_details_list_data[$count]["village_name"]; ?></td>
					<td><?php echo $extent; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($survey_details_list_data[$count]["survey_details_planned_start_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($survey_details_list_data[$count]["survey_details_planned_end_date"])); ?></td>
					<td><?php echo $survey_details_list_data[$count]["survey_details_remarks"]; ?></td>
					<td><?php echo $survey_details_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($survey_details_list_data[$count]["survey_details_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_survey_edit_details('<?php echo $survey_details_list_data[$count]["survey_details_id"]; ?>');">Edit</a><?php } ?></td>
					
					<td><?php if(($survey_details_list_data[$count]["survey_details_active"] == "1") && ($delete_perms_list["status"] == SUCCESS)){ ?><a href="#" onclick="return survey_delete_details(<?php echo $survey_details_list_data[$count]["survey_details_id"]; ?>);">Delete</a><?php } ?></td>
					
					<td><?php if(($survey_details_list_data[$count]["survey_details_status"] == "Pending") && ($edit_perms_list["status"] == SUCCESS)){ ?><a href="#" onclick="return survey_completed_details(<?php echo $survey_details_list_data[$count]["survey_details_id"]; ?>);">Complete</a><?php }  else { echo "Completed" ; } ?></td>
					
					<td style="word-wrap:break-word;"><?php if($view_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_survey_process('<?php echo $survey_details_list_data[$count]["survey_details_id"]; ?>','view');">Survey Process</a><?php } ?></td>
					
					<td style="word-wrap:break-word;"><?php if($view_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_survey_file('<?php echo $survey_details_list_data[$count]["survey_details_id"]; ?>','view','<?php echo $survey_details_list_data[$count]["survey_details_village"]; ?>','<?php echo $survey_details_list_data[$count]["survey_details_project"]; ?>');">Survey File</a><?php } ?></td>
					
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Survey Details added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			   <?php
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function survey_delete_details(survey_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "survey_details_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/survey_delete_details.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("survey_id=" + survey_id + "&action=0");
		}
	}	
}

function survey_completed_details(survey_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "survey_details_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/survey_complete_details.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("survey_id=" + survey_id + "&action=Completed");
		}
	}	
}

function go_to_survey_edit_details(survey_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_edit_details.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","survey_id");
	hiddenField1.setAttribute("value",survey_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_survey_process(survey_id,source)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_process_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","survey_id");
	hiddenField1.setAttribute("value",survey_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","source");
	hiddenField2.setAttribute("value",source);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_survey_file(survey_id,source,village,project)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_file_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","survey_id");
	hiddenField1.setAttribute("value",survey_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","source");
	hiddenField2.setAttribute("value",source);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","village");
	hiddenField3.setAttribute("value",village);
	
	var hiddenField4 = document.createElement("input");
	hiddenField4.setAttribute("type","hidden");
	hiddenField4.setAttribute("name","project");
	hiddenField4.setAttribute("value",project);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	form.appendChild(hiddenField4);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_survey_assign_project(survey_id,village)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_assign_project.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","survey_id");
	hiddenField1.setAttribute("value",survey_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","village");
	hiddenField2.setAttribute("value",village);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

</script>

  </body>

</html>