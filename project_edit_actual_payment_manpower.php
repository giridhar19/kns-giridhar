<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['manpower_id']))
	{
		$manpower_id = $_GET['manpower_id'];
	}
	else
	{
		$manpower_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["edit_project_actual_payment_manpower_submit"]))
	{
		$manpower_id	     = $_POST["hd_manpower_id"];
		$task_manpower_id	 = $_POST["hd_task_manpower_id"];
		$vendor_id	         = $_POST["ddl_vendor_id"];
		$amount	             = $_POST["amount"];
		$from_date	         = $_POST["from_date"];
		$to_date             = $_POST["to_date"];
		$remarks 	         = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($vendor_id != "") && ($amount != "") && ($from_date != "") && ($to_date != ""))
		{
			$project_actual_payment_manpower_update_data = array("vendor_id"=>$vendor_id,"amount"=>$amount,"from_date"=>$from_date,"to_date"=>$to_date,"remarks"=>$remarks);
			$project_actual_payment_manpower_iresult = i_update_project_actual_payment_manpower($manpower_id,$project_actual_payment_manpower_update_data);
			
			if($project_actual_payment_manpower_iresult["status"] == SUCCESS)
				
			{	
			  $alert_type = 1;
			  header("location:project_actual_payment_manpower_list.php");
			}
			else
			{
			   $alert = "Project Already Exists";
			  $alert_type = 0;	
			}
			
			$alert = $project_actual_payment_manpower_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			
		}
	}

   // Get Machine Vendor master modes already added
	$project_machine_vendor_master_search_data = array("active"=>'1');
	$project_machine_vendor_master_list = i_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);
	if($project_machine_vendor_master_list['status'] == SUCCESS)
	{
		$project_machine_vendor_master_list_data = $project_machine_vendor_master_list['data'];
	}
	else
	{
		$alert = $project_machine_vendor_master_list["data"];
		$alert_type = 0;
	}

    // Get Project Actual Payment Manpower modes already added
	$project_actual_payment_manpower_search_data = array("active"=>'1',"manpower_id"=>$manpower_id,"start"=>'-1');
	$project_actual_payment_manpower_list = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
	if($project_actual_payment_manpower_list['status'] == SUCCESS)
	{
		$project_actual_payment_manpower_list_data = $project_actual_payment_manpower_list['data'];
	}	
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Project Actual Payment Manpower</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Project Project Actual Payment Manpower</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Project Actual Payment Manpower</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_edit_actual_payment_manpower_form" class="form-horizontal" method="post" action="project_edit_actual_payment_manpower.php">
								<input type="hidden" name="hd_manpower_id" value="<?php echo $manpower_id; ?>" />
								<input type="hidden" name="hd_task_manpower_id" value="<?php echo $task_manpower_id; ?>" />
									<fieldset>										
										
										<div class="control-group">											
											<label class="control-label" for="ddl_vendor_id">Vendor*</label>
											<div class="controls">
												<select name="ddl_vendor_id" required>
												<option value="">- - Select Vendor - -</option>
												<?php
												for($count = 0; $count < count($project_machine_vendor_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_id"]; ?>" <?php if($project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_id"] == $project_actual_payment_manpower_list_data[0]["project_actual_payment_manpower_vendor_id"]){ ?> selected="selected" <?php } ?>><?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
											</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="amount">Amount</label>
											<div class="controls">
												<input type="number" name="amount" placeholder="Amount" value="<?php echo $project_actual_payment_manpower_list_data[0]
												["project_actual_payment_manpower_amount"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="from_date">From Date</label>
											<div class="controls">
												<input type="date" name="from_date" placeholder="Amount" value="<?php echo $project_actual_payment_manpower_list_data[0]
												["project_actual_payment_manpower_from_date"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="to_date">To Date</label>
											<div class="controls">
												<input type="date" name="to_date" placeholder="Amount" value="<?php echo $project_actual_payment_manpower_list_data[0]
												["project_actual_payment_manpower_to_date"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" name="txt_remarks" placeholder="Remarks" value="<?php echo $project_actual_payment_manpower_list_data[0]["project_actual_payment_manpower_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_project_actual_payment_manpower_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
