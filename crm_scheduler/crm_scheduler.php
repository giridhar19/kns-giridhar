<?php
/**
 * @author Nitin Kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');

// What is the date today
$today = date("Y-m-d");

// What was the date yesterday
$yesterday = date("Y-m-d",strtotime($today." -1 days"));

// What was the date day before yesterday
$day_before_yesterday = date("Y-m-d",strtotime($today." -2 days"));

// Get user list so that mail gets generated for everyone
$user_list = i_get_user_list('','','','');
		
$sl_no = 0;
// Loop for all users
for($user_count = 0; $user_count < count($user_list["data"]); $user_count++)
{
	$role = $user_list["data"][$user_count]["user_role"];
	// Check for users belonging to sales department
	if(($role == 5) || ($role == 6) || ($role == 7) || ($role == 8) || ($role == 9))
	{
		// Get scheduled follow ups until day before yesterday
		$fup_list_sch_day_before_yesterday = i_get_enquiry_fup_latest('','','4','exclude','',$user_list["data"][$user_count]["user_id"],'','','',"assignee_only",'',$day_before_yesterday);
		if($fup_list_sch_day_before_yesterday["status"] == SUCCESS)
		{
			$until_day_before_yesterday_sch_fup_count = count($fup_list_sch_day_before_yesterday["data"]);
		}	
		else
		{
			$until_day_before_yesterday_sch_fup_count = 0;
		}
		
		// Get yesterday's scheduled follow ups
		$fup_list_sch_yesterday =  i_get_enquiry_fup_latest('',$yesterday,'4','exclude','',$user_list["data"][$user_count]["user_id"],'','','',"assignee_only",'','');
		if($fup_list_sch_yesterday["status"] == SUCCESS)
		{
			$yesterday_sch_fup_count = count($fup_list_sch_yesterday["data"]);
		}	
		else
		{
			$yesterday_sch_fup_count = 0;
		}
	
		// Get yesterday's performed follow ups
		$start_date = $yesterday." 00:00:00";
		$end_date   = $yesterday." 23:59:59";
		$fup_list_com_yesterday = i_get_enquiry_fup_latest('','','4','exclude','',$user_list["data"][$user_count]["user_id"],$start_date,$end_date,'',"assigner_only",'','');
		if($fup_list_com_yesterday["status"] == SUCCESS)
		{
			$yesterday_com_fup_count = count($fup_list_com_yesterday["data"]);
		}	
		else
		{
			$yesterday_com_fup_count = 0;
		}
	
		// Get today's scheduled follow ups
		$fup_list_sch_today = i_get_enquiry_fup_latest('',$today,'4','exclude','',$user_list["data"][$user_count]["user_id"],'','','',"assignee_only",'','');
		if($fup_list_sch_today["status"] == SUCCESS)
		{
			$today_sch_fup_count = count($fup_list_sch_today["data"]);
		}	
		else
		{
			$today_sch_fup_count = 0;
		}
		
		if($yesterday_sch_fup_count == 0)
		{
			$performance = "N.A";
		}
		else
		{
			$performance = round(((($yesterday_com_fup_count)/($yesterday_sch_fup_count + $until_day_before_yesterday_sch_fup_count))*100),2)."%";
			
			if(round(((($yesterday_com_fup_count)/($yesterday_sch_fup_count + $until_day_before_yesterday_sch_fup_count))*100),2) > 100)
			{
				$performance = "100%";
			}
		}
		
		// Compose 
		$message = "Dear ".$user_list["data"][$user_count]["user_name"].",<br /><br />
		Please find below the follow up performance report of yesterday:<br /><br />
		<table border=\"1\" style=\"border-collapse:collapse;\">
		<tr>
		<td><strong>Pending Follow Ups until day before yesterday</td>	
		<td>".$until_day_before_yesterday_sch_fup_count."</td>
		</tr>
		<tr>
		<td><strong>Follow Ups Planned for Yesterday</strong></td>
		<td>".$yesterday_sch_fup_count."</td>
		</tr>
		<tr>
		<td><strong>Follow Ups Done Yesterday</strong></td>
		<td>".$yesterday_com_fup_count."</td>
		</tr>
		<tr>
		<td><strong>Yesterday's Performance</strong></td>
		<td><span style=\"color:red;\">".$performance."</span></td>
		</tr>
		<tr>
		<td><strong>Follows Ups Planned for Today</strong></td>	
		<td>".$today_sch_fup_count."</td>		
		</tr>";
		
		$message = $message."</table>";
		// Send Mail
		$to = $user_list["data"][$user_count]["user_email_id"]."||"."prabhakar@knsgroup.in||jagannath@knsgroup.in";
		$cc = "venkataramanaiah@knsgroup.in";
		$subject = "Daily Follow Up Summary ";
		
		$mail_status = send_email($to,$cc,$subject,$message,"");
		if($mail_status == SUCCESS)
		{
			$status_mail = "SUCCESS";
		}
		else
		{
			$status_mail = "FAILURE";
		}	
		echo ("Status of Mail sent: $status_mail <br />");	*/	
	}	
}
?>