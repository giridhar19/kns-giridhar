<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: na_task_list.php
CREATED ON	: 30-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans which are marked as not applicable
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["process"]))
	{
		$process_plan_id = $_GET["process"];
	}
	else	
	{
		$process_plan_id = "";
	}

	if(isset($_GET["task_type"]))
	{
		$task_type = $_GET["task_type"];
	}
	else
	{
		$task_type = "";
	}

	// Temp data
	$alert = "";

	if($role == 1)
	{
		$assigned_to = "";
	}
	else
	{
		$assigned_to = $user;
	}

	if(isset($_POST["task_update_button"]))
	{	
		$record_count    = $_POST["count"];
		$process_plan_id = $_POST["process_plan"];
		
		for($count = 0; $count < $record_count; $count++)
		{
			$task                = $_POST["task_data"][$count]['task_id'];
			$process_plan        = $_POST["task_data"][$count]['process_id'];
			$planned_end_date    = $_POST["task_data"][$count]['planned_date'];
			$start_date          = $_POST["task_data"][$count]['start_date'];
			$actual_end_date     = $_POST["task_data"][$count]['actual_date'];
			$document            = $_POST["task_data"][$count]['document'];
			$reason				 = $_POST["task_data"][$count]['delay_reason'];
			if(isset($_POST["task_data"][$count]['not_applicable']))
			{
				$applicable = $_POST["task_data"][$count]['not_applicable'];
			}
			else
			{
				$applicable = "1";
			}
			$approved_on         = $_POST["task_data"][$count]['approved_on'];
			
			if($actual_end_date <= date("Y-m-d"))
			{
				$task_plan_update_result = i_update_task_plan($task,$planned_end_date,$start_date,$actual_end_date,$reason,$document,$applicable,$approved_on);		
				
				if($task_plan_update_result["status"] == SUCCESS)
				{
					$alert = $task_plan_update_result["data"];
					$disp_class = "";
					
					if(($actual_end_date != "0000-00-00") && ($actual_end_date != "1969-12-31") && ($actual_end_date != "1970-01-01"))
					{
						// Get file details
						$file_sdetails = i_get_legal_process_plan_details($process_plan);
					
						// Update process and task details in the file
						$update_result = i_update_file_details($task,$process_plan,$file_sdetails["data"]["process_plan_legal_file_id"]);
						
						if($update_result["status"] == SUCCESS)
						{
							$alert = "Task Plan Updated Successfully!";
							$disp_class = "green";
						}
						else
						{
							$alert = "Internal Error. please try again later!";
							$disp_class = "red";
						}
					}
				}
				else
				{
					$alert = $task_plan_update_result["data"];
					$disp_class = "red";
				}
			}
			else
			{
				$alert = "Actual Date cannot be later than today!";
				$disp_class = "red";
			}
		}
	}

	// Get list of task plans for this process plan
	if($role == 3)
	{
		$access_user = $user;
	}
	else
	{
		$access_user = "";
	}
	$legal_task_plan_list = i_get_task_plan_list('',$task_type,$process_plan_id,'','0000-00-00','0',$access_user,'slno');
	if($legal_task_plan_list["status"] == SUCCESS)
	{
		$legal_task_plan_list_data = $legal_task_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_task_plan_list["data"];
	}

	if($process_plan_id != "")
	{
		// Get specific project details
		$process_plan_details = i_get_legal_process_plan_details($process_plan_id);
	}
	else
	{
		$process_plan_details = "";
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Not Applicable Task List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Not Applicable Tasks List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<form action="pending_task_list.php" method="post" id="task_update_form">
			<input type="hidden" name="process_plan" value="<?php echo $process_plan_id; ?>" />
			<input type="hidden" name="count" value="<?php echo count($legal_task_plan_list_data); ?>" />
			<span style="padding-left:50px;">
			<?php if($process_plan_details != "")
			{
			?>
			Survey Number: <strong><?php echo $process_plan_details["data"]["file_survey_number"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Village: <strong><?php echo $process_plan_details["data"]["file_village"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Party Name: <strong><?php echo $process_plan_details["data"]["file_land_owner"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Process: <strong><?php echo $process_plan_details["data"]["process_name"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			<?php
			}
			if($alert != "")
			{
				?>
				<span style="color:<?php echo $disp_class; ?>"><?php echo $alert; ?></span>
				<?php
			}
			else
			{
			?>
			All project tasks
			<?php
			}
			?>
			</span>
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>Work Description</th>
					<th>Planned End Date</th>
					<th>Actual Start Date</th>
					<th>Actual End Date</th>
					<th>Variance (in days)</th>
					<th>Reason</th>
					<th>Document</th>
					<th>Assigned To</th>								
				</tr>
				</thead>
				<tbody>
				 <?php
				if($legal_task_plan_list["status"] == SUCCESS)
				{				
					$sl_count = 0;
					for($count = 0; $count < count($legal_task_plan_list_data); $count++)
					{
						$task_process_data = i_get_legal_process_plan_details($legal_task_plan_list_data[$count]["task_plan_legal_process_plan_id"]);
						$sl_count++;
						if(($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "0000-00-00") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "1969-12-31") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "1970-01-01") && ($role == 3))
						{
							// Do nothing
						}					
						else
						{
						if(get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00")
						{
							$end_date = date("Y-m-d");
						}
						else
						{
							$end_date = $legal_task_plan_list_data[$count]["task_plan_actual_end_date"];
						}
						$start_date = $legal_task_plan_list_data[$count]["task_plan_planned_end_date"];
						
						$variance = get_date_diff($start_date,$end_date);
						if($variance["status"] == 1)
						{
							if((get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#FF0000";
								$font_class = "#000000";
							}
							else						
							{
								$css_class = "#FFA500";
								$font_class = "#000000";
							}
						}
						else
						{
							if((get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#FFFFFF";
								$font_class = "#000000";
							}
							else
							{	$css_class = "#00FF00";
								$font_class = "#000000";
							}
						}
					?>
					<input type="hidden" name="task_data[<?php echo $count; ?>][task_id]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_id"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][process_id]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_process_plan_id"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][document]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_document_path"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][approved_by]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_approved_by"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][approved_on]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_planned_approved_on"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][delay_reason]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_delay_reason"]; ?>" />
					<tr bgcolor="<?php echo $css_class; ?>">
						<td style="word-wrap:break-word;"><?php echo $sl_count; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $legal_task_plan_list_data[$count]["task_type_name"]; ?><br /><br /><strong><?php echo $task_process_data["data"]["file_number"]; ?></strong><br /><br />
						Sy No: <?php echo $task_process_data["data"]["file_survey_number"]; ?></td>						
						<td><input type="date" name="task_data[<?php echo $count; ?>][planned_date]" value="<?php echo get_formatted_date($legal_task_plan_list_data[$count]["task_plan_planned_end_date"],"Y-m-d"); ?>" <?php if(($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "0000-00-00") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "1969-12-31") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "1970-01-01") && (($role == 3) || ($role == 2))){?> readOnly="true" <?php } ?> /></td>
						
						<td><input type="date" name="task_data[<?php echo $count; ?>][start_date]" value="<?php echo get_formatted_date($legal_task_plan_list_data[$count]["task_plan_start_date"],"Y-m-d"); ?>" <?php if(($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "0000-00-00") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "1969-12-31") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "1970-01-01") && (($role == 3) || ($role == 2))) { ?> readOnly="true" <?php } ?> /></td>
						
						<td><input type="date" name="task_data[<?php echo $count; ?>][actual_date]" value="<?php echo get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d"); ?>" <?php if(($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "0000-00-00") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "1969-12-31") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "1970-01-01") && (($role == 3) || ($role == 2))) { ?> readOnly="true" <?php } ?> /></td>
						
						<td><?php echo $variance["data"];?></td>
						
						<td><a href="reason_list.php?task=<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_id"]; ?>"><span style="color:black; text-decoration: underline;"><?php echo $legal_task_plan_list_data[$count]["reason"]; ?></span></a><br/><br/>
						<a href="update_delay_reason.php?task=<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_id"]; ?>&process=<?php echo $process_plan_id; ?>"><span style="color:black; text-decoration: underline;">Update Delay Reason</span></a></td>
						
						<td><?php if($legal_task_plan_list_data[$count]["task_plan_document_path"] != "") { ?><a href="documents/<?php echo $legal_task_plan_list_data[$count]["task_plan_document_path"]; ?>" target="_blank"><span style="color:black; text-decoration: underline;">Download</span></a><?php } ?><br /><br /><a href="upload_document.php?task=<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_id"]; ?>"><span style="color:black; text-decoration: underline;">Upload Document</span></a></td>
						
						<td><?php echo $legal_task_plan_list_data[$count]["user_name"]; ?><br /><br />
						<input type="checkbox" name="task_data[<?php echo $count; ?>][not_applicable]" value="0" <?php if($legal_task_plan_list_data[$count]["task_plan_applicable"] == "0")
						{
						?>
						checked
						<?php
						}?>>&nbsp;&nbsp;&nbsp;NA</td>															
						
					</tr>
					<?php 
						}
					}
				}
				else
				{
				?>
				<td colspan="9">No tasks added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <br />
			<input type="submit" class="btn btn-primary" name="task_update_button" value="Save" />
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>