<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'post_legal_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here
	
	// Initialization
	$alert_type = -1;
	$alert      = '';
	
	if(isset($_POST['assigned_file_search_submit']))
	{
		$project = $_POST['ddl_project'];
	}
	else
	{
		$project = '';
	}	

	// Get list of Users Login
	$assigned_file_list = i_get_site_approval_list('','','','','','',$project);
	if($assigned_file_list["status"] == SUCCESS)
	{
		$assigned_file_list_data = $assigned_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$assigned_file_list["data"];
	}
	
	// Get CRM project list
	$project_list = i_get_project_list('','');
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = -1;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Assigned File Mapping List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
			<h3>Survey Numbers Sent for Approval&nbsp;&nbsp;&nbsp;Total Extent: <span id="total_extent_span"><i>Calculating</i></span></h3>
			<span style="float:right; padding-right:30px;"><a href="create_file_project_plan.php">Initiate new approvals</a></span>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="assigned_file_search_form" action="assigned_files_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($count = 0; $count < count($project_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php if($project == $project_list_data[$count]["project_id"]){ ?> selected <?php } ?>><?php echo $project_list_data[$count]["project_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>				  
			  <input type="submit" name="assigned_file_search_submit" />
			  </form>			  
            </div>
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>File ID</th>
					<th>Survey Number</th>					
					<th>Extent</th>	
					<th>Village</th>	
					<th>Authority</th>	
					<th>Status</th>
					<th>Remarks</th>					
					<th>Added On</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$total_extent = 0;
				if($assigned_file_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($assigned_file_list_data); $count++)
					{				
						$total_extent = $total_extent + $assigned_file_list_data[$count]["bd_file_extent"]; 
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $assigned_file_list_data[$count]["site_approval_file_id"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $assigned_file_list_data[$count]["bd_file_survey_no"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $assigned_file_list_data[$count]["bd_file_extent"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $assigned_file_list_data[$count]["village_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $assigned_file_list_data[$count]["site_approval_approving_authority"]; ?></td>
						<?php if($assigned_file_list_data[$count]["site_approval_status"] == 0)
						{
							$status = "Not Approved";
						}
						else
						{
							$status = "Approved";
						}?>
						<td style="word-wrap:break-word;"><?php echo $status; ?></td>
						<td style="word-wrap:break-word;"><?php echo $assigned_file_list_data[$count]["site_approval_remarks"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($assigned_file_list_data[$count]["site_approval_added_on"])); ?></td>
						<td><?php if($assigned_file_list_data[$count]["site_approval_status"] == 0) {?><a href="#" onclick="return approve_file(<?php echo $assigned_file_list_data[$count]["site_approval_id"]; ?>);">Approve</a><?php } ?></td>
					</tr>
					<span id="span_msg"></span>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="8">No Files sent for approval yet</td>
				</tr>
				<?php
				}
				?>	
                </tbody>
				<script>
				document.getElementById('total_extent_span').innerHTML = <?php echo $total_extent; ?> + ' guntas';
				</script>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function approve_file(site_approval_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{         
		if (ok)
		{
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("action", "update_site_approval.php");
			
			var hiddenField2 = document.createElement("input");
			hiddenField2.setAttribute("type","hidden");
			hiddenField2.setAttribute("name","site_approval_id");
			hiddenField2.setAttribute("value",site_approval_id);
				
			form.appendChild(hiddenField2);	
			
			document.body.appendChild(form);
			form.submit();
		}
	}	
}</script>

  </body>

</html>
