<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock_grn.php');



/*

PURPOSE : To add Stock Grn

INPUT 	: Purchase Order Id,Invoice Number,Invoice Date,Location,DC Number,DC Date,Vehicle Number,Remarks,Added By,Added On

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/
function i_add_stock_grn($purchase_order_id,$invoice_number,$invoice_date,$location,$project,$doc,$dc_number,$dc_date,$vehicle_number,$remarks,$added_by)
{

	$grn_no = p_generate_grn_no();
	$grn_iresult = db_add_stock_grn($grn_no,$purchase_order_id,$invoice_number,$invoice_date,$location,$project,$doc,$dc_number,$dc_date,$vehicle_number,$remarks,$added_by);
	

	if($grn_iresult['status'] == SUCCESS)

	{

		$return["data"]   = $grn_iresult['data'];

		$return["status"] = SUCCESS;	

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

			

	return $return;

}



/*

PURPOSE : To get stock Grn list

INPUT 	: Stock Grn Search Data

OUTPUT 	: Grn List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_grn_list($stock_grn_search_data)

{

	$grn_sresult = db_get_stock_grn_list($stock_grn_search_data);

	

	if($grn_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$grn_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No grn added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Grn

INPUT 	: Grn ID, Grn Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_update_grn($grn_id,$grn_update_data)

{

	$grn_sresult = db_update_grn($grn_id,$grn_update_data);

		

	if($grn_sresult['status'] == SUCCESS)

	{

		$return["data"]   = "grn Successfully added";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To add Stock Grn Items

INPUT 	: Grn Id,Item,Quantity,Uom,remarks,Added By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_add_stock_grn_items($grn_id,$item,$quantity,$inward_qty,$uom,$remarks,$added_by)

{	

	$grn_items_iresult = db_add_stock_grn_items($grn_id,$item,$quantity,$inward_qty,$uom,$remarks,$added_by);

	

	if($grn_items_iresult['status'] == SUCCESS)

	{

		$return["data"]   = "GRN items Successfully Added";

		$return["status"] = SUCCESS;	

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get Grn Item list

INPUT 	: Grn Id, Active

OUTPUT 	: Grn Item List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_grn_items_list($stock_grn_items_search_data)

{

	$grn_items_sresult = db_get_stock_grn_items_list($stock_grn_items_search_data);

	

	if($grn_items_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$grn_items_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No grn added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Grn Items

INPUT 	: Grn Item ID, Grn Items Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_grn_items($item_id,$grn_items_update_data)

{

		$grn_items_sresult = db_update_grn_items($item_id,$grn_items_update_data);

		

		if($grn_items_sresult['status'] == SUCCESS)

		{

			$return["data"]   = "grn items Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}



/*

PURPOSE : To add Stock Grn Engineer Inspection

INPUT 	: Grn Item Id,Quantity,Remarks,Remarks to Account,Added by

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_add_stock_grn_engineer_inspection($grn_item_id,$quantity,$rejected_quantity,$additional_quantity,$remarks,$remarks_to_account,$added_by)

{

	$grn_engineer_inspection_iresult = db_add_stock_grn_engineer_inspection($grn_item_id,$quantity,$rejected_quantity,$additional_quantity,$remarks,$remarks_to_account,$added_by);

		

	if($grn_engineer_inspection_iresult['status'] == SUCCESS)

	{

		$return["data"]   = $grn_engineer_inspection_iresult['data'];

		$return["status"] = SUCCESS;			

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get Grn Engineer Inspection list

INPUT 	: Engineer Inspection Search Data

OUTPUT 	: Grn Engineer Inspection List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data)

{

	$grn_engineer_inspection_sresult = db_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);

	

	if($grn_engineer_inspection_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$grn_engineer_inspection_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No grn engineer inspection added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Grn Engineer Inspection

INPUT 	: Inspection ID, Grn Items Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_stock_grn_engineer_inspection($inspection_id,$stock_grn_engineer_inspection_update_data)

{

		$grn_engineer_inspection_sresult = db_update_stock_grn_engineer_inspection($inspection_id,$stock_grn_engineer_inspection_update_data);

		

		if($grn_engineer_inspection_sresult['status'] == SUCCESS)

		{

			$return["data"]   = "grn engineer inspection Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}



/*

PURPOSE : To add Stock Grn payments

INPUT 	: Grn Id,Amount,Mode,Date,Bank,Branch,Added By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_add_stock_grn_payments($grn_id,$amount,$mode,$date,$bank,$branch,$added_by)

{

	

		$grn_payments_iresult = db_add_stock_grn_payments($grn_id,$amount,$mode,$date,$bank,$branch,$added_by);

		

		if($grn_payments_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "grn payments Successfully Added";

			$return["status"] = SUCCESS;	

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	

	return $return;

}



/*

PURPOSE : To get Grn Payments list

INPUT 	: Payment Id,Active

OUTPUT 	: Grn Payments List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_grn_payments_list($stock_grn_payments_search_data)

{

	$grn_payments_sresult = db_get_stock_grn_payments_list($stock_grn_payments_search_data);

	

	if($grn_payments_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$grn_payments_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No grn payments added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Grn Payments

INPUT 	: Payment ID, Grn Payments Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_grn_payments($payment_id,$grn_payments_update_data)

{

		$grn_payments_sresult = db_update_grn_payments($payment_id,$grn_payments_update_data);

		

		if($grn_payments_sresult['status'] == SUCCESS)

		{

			$return["data"]   = "grn payments Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}



/*

PURPOSE : To add Stock Grn Debit Note

INPUT 	: Grn Id,Amount,Active,Remarks,Added By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_add_stock_grn_debit_note($grn_id,$amount,$remarks,$added_by)

{

	$stock_grn_debit_note_search_data = array("stock_grn_id"=>$grn_id);

	$grn_debit_note_sresult = db_get_stock_grn_debit_note_list($stock_grn_debit_note_search_data);

	

	if($grn_debit_note_sresult["status"] == DB_NO_RECORD)

	{

		$grn_debit_note_iresult = db_add_stock_grn_debit_note($grn_id,$amount,$remarks,$added_by);

		

		if($grn_debit_note_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "grn debit note Successfully Added";

			$return["status"] = SUCCESS;	

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "This grn debit note already exists!";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get Grn Debit Note list

INPUT 	: Debit Note Id,Active

OUTPUT 	: Grn Debit Note List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_grn_debit_note_list($stock_grn_debit_note_search_data)

{

	$grn_debit_note_sresult = db_get_stock_grn_debit_note_list($stock_grn_debit_note_search_data);

	

	if($grn_debit_note_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$grn_debit_note_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No grn debit note added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Grn Debit Note

INPUT 	: Debit Note ID, Grn Debit Note Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_grn_debit_note($debit_note_id,$grn_debit_note_update_data)

{

		$grn_debit_note_sresult = db_update_grn_debit_note($debit_note_id,$grn_debit_note_update_data);

		

		if($grn_debit_note_sresult['status'] == SUCCESS)

		{

			$return["data"]   = "grn debit note Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}



/*

PURPOSE : To add Stock Quantity

INPUT 	: Material Id,Quantity,Uom,Updated By,Updated On,Added By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_add_stock_quantity($material_id,$quantity,$uom,$updated_by,$updated_on,$added_by)

{

	$stock_quantity_search_data = array("stock_material_id"=>$material_id);

	$quantity_sresult = db_get_stock_quantity_list($stock_quantity_search_data);

	

	if($quantity_sresult["status"] == DB_NO_RECORD)

	{

		$quantity_iresult = db_add_stock_quantity($material_id,$quantity,$uom,$updated_by,$updated_on,$added_by);

		

		if($quantity_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "quantity Successfully Added";

			$return["status"] = SUCCESS;	

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "This quantity already exists!";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get Quantity list

INPUT 	: Stock Quantity Search Data

OUTPUT 	: Quantity List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_quantity_list($stock_quantity_search_data)

{

	$quantity_sresult = db_get_stock_quantity_list($stock_quantity_search_data);

	

	if($quantity_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$quantity_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No quantity added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update Quantity

INPUT 	: Quantity ID, Quantity Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_quantity($quantity_id,$quantity_update_data)

{

		$quantity_sresult = db_update_quantity($quantity_id,$quantity_update_data);

		

		if($quantity_sresult['status'] == SUCCESS)

		{

			$return["data"]   = "quantity Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

		

	return $return;

}



/*

PURPOSE : To add stock grn accounts approval

INPUT 	: Item, Amount, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Punith

*/

function i_add_stock_grn_accounts($item_id,$amount,$remarks,$added_by)

{

	$stock_grn_accounts_iresult = db_add_stock_grn_accounts($item_id,$amount,$remarks,$added_by);

		

	if($stock_grn_accounts_iresult['status'] == SUCCESS)

	{

		$return["data"]   = "Accounts Approval Successfully added";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}	

		

	return $return;

}



/*

PURPOSE : To get Stock Account Approvals

INPUT 	: Account Search Array

OUTPUT 	: Account Search List

BY 		: Punith

*/

function i_get_stock_grn_accounts($stock_grn_accounts_search_data)

{

	$stock_grn_accounts_sresult = db_get_stock_grn_accounts_list($stock_grn_accounts_search_data);

	

	if($stock_grn_accounts_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $stock_grn_accounts_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No Account Approval added yet!"; 

    }

	

	return $return;

}



/*
PURPOSE : To get Stock Account Approvals
INPUT 	: Account Search Array
OUTPUT 	: Account Search List
BY 		: Punith
*/
function i_get_stock_sum_grn_accounts($stock_grn_accounts_search_data)
{
	$stock_grn_accounts_sresult = db_get_sum_stock_grn_accounts_list($stock_grn_accounts_search_data);
	
	if($stock_grn_accounts_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $stock_grn_accounts_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Account Approval added yet!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update stock account approvals

INPUT 	: Accounts Approval ID, Accounts Update Data

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_update_stock_grn_accounts($stock_grn_accounts_id,$stock_grn_accounts_update_data)

{

	$stock_grn_accounts_uresult = db_update_stock_grn_accounts($stock_grn_accounts_id,$stock_grn_update_data);

	

	if($stock_grn_accounts_uresult['status'] == SUCCESS)

	{

		$return["data"]   = "GRN Account Approval Successfully Added";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

			

	return $return;

}

/*
PURPOSE : To add new Stock Management Payment
INPUT 	: PO ID, Total Value, Additional Cost, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_stock_management_payment($po_id,$total_value,$additional_cost,$remarks,$added_by)
{   
	$stock_management_payment_iresult =  db_add_stock_management_payment($po_id,$total_value,$additional_cost,$remarks,$added_by);
	
	if($stock_management_payment_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Stock Management Payment Successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Stock Management Payment List
INPUT 	: Payment ID, PO ID, Total Value, Additional Cost, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Stock Management Payment, success or failure message
BY 		: Lakshmi
*/
function i_get_stock_management_payment($stock_management_payment_search_data)
{
	$stock_management_payment_sresult = db_get_stock_management_payment($stock_management_payment_search_data);
	
	if($stock_management_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$stock_management_payment_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Stock Management Payment Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Stock Management Payment
INPUT 	: Payment ID, Stock Management Payment Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_stock_management_payment($payment_id,$stock_management_payment_update_data)
{       
		$stock_management_payment_sresult = db_update_stock_management_payment($payment_id,$stock_management_payment_update_data);
		
		if($stock_management_payment_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Stock Management Payment Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}
/*
PURPOSE : To add Stock Payment Release
INPUT 	: Payment Release Id,Amount,Active,Added By Added on
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_stock_payment_release($po_id,$amount,$remarks,$added_by)
{
	$payment_release_iresult = db_add_stock_release_payment($po_id,$amount,$remarks,$added_by);
	
	if($payment_release_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $payment_release_iresult['data'];
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
			
	return $return;
}

/*
PURPOSE : To get stock Grn Payment Release list
INPUT 	: Stock Payment Release Search Data
OUTPUT 	: Payment Release  List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_stock_payment_release_list($stock_payment_release_search_data)
{
	$payment_release_sresult = db_get_stock_payment_release_list($stock_payment_release_search_data);
	
	if($payment_release_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $payment_release_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = ""; 
    }
	
	return $return;
}

/*
PURPOSE : To update Released Payment
INPUT 	: Release Payment ID, Release Payment Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_payment_release($payment_release_id,$grn_payment_release_update_data)
{
	$payment_release_sresult = db_update_payment_release($payment_release_id,$grn_payment_release_update_data);  
		
	if($payment_release_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Payment Successfully Updated";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}
/*
PURPOSE : To add new Stock Issue Payment
INPUT 	: release ID, vendor ID, amount, instrument details, remarks, added_by
OUTPUT 	: payment ID, success or failure message
BY 		: Ashwini
*/
function i_add_stock_issue_payment($release_id,$vendor_id,$amount,$mode,$instrument_details,$remarks,$added_by)
{
	$stock_issue_payment_iresult = db_add_stock_issue_payment($release_id,$vendor_id,$amount,$mode,$instrument_details,$remarks,$added_by);
	
	if($stock_issue_payment_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "release_id Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	return $return;
}


/*
PURPOSE : To get new Stock Issue Payment
INPUT 	: stock_issue_payment Search Data Array
OUTPUT 	: stock_issue_payment list or Error Details, success or failure message
BY 		: Ashwini
*/
function i_get_stock_issue_payment($stock_issue_payment_search_data)
{
	$stock_issue_payment_sresult = db_get_stock_issue_payment($stock_issue_payment_search_data);
	
	if($stock_issue_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $stock_issue_payment_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No stock_issue_payment added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Stock Issue Payment 
INPUT 	: payment ID,Stock Issue Payment Update Array
OUTPUT 	: payment ID; Message of success or failure
BY 		: Ashwini
*/
function i_update_stock_issue_payment($payment_id,$stock_issue_payment_update_data)
{
	$stock_issue_payment_sresult = db_update_stock_issue_payment($payment_id,$stock_issue_payment_update_data);
	
	if($stock_issue_payment_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "payment_id Successfully added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To delete Stock Issue Payment List
INPUT 	: payment ID,Stock Issue Payment Update Array
OUTPUT 	: payment ID; Message of success or failure
BY 		: Ashwini
*/

function i_delete_stock_issue_payment($payment_id,$stock_issue_payment_update_data)
{
	$stock_issue_payment_update_data = array('active'=>'0');
	$stock_issue_payment_sresult = db_update_stock_issue_payment($payment_id,$stock_issue_payment_update_data);
	
	if($stock_issue_payment_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "stock_issue_payment Successfully Deleted";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Stock Additional Payment Master
INPUT 	:  payment type, remarks, added_by
OUTPUT 	: master ID, success or failure message
BY 		: Ashwini
*/
function i_add_stock_additional_payment_master($payment_type,$remarks,$added_by,$added_on)
{
	$stock_additional_payment_master_search_data = array('payment_type'=>$payment_type);
	$stock_additional_payment_master_sresult = db_get_stock_additional_payment_master($stock_additional_payment_master_search_data);
	
	if($stock_additional_payment_master_sresult["status"] == DB_NO_RECORD)
	{
		$stock_additional_payment_master_iresult = db_add_stock_additional_payment_master($payment_type,$remarks,$added_by,$added_on);
		
		if($stock_additional_payment_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "payment_type Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = " payment_type already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}


/*
PURPOSE : To get new Stock Additional Payment Master
INPUT 	: stock_additional_payment_master Search Data Array
OUTPUT 	: stock_additional_payment_master list or Error Details, success or failure message
BY 		: Ashwini
*/
function i_get_stock_additional_payment_master($stock_additional_payment_master_search_data)
{
	$stock_additional_payment_master_sresult = db_get_stock_additional_payment_master($stock_additional_payment_master_search_data);
	
	if($stock_additional_payment_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $stock_additional_payment_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No stock_additional_payment_master added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update  Stock Additional Payment Master
INPUT 	: master ID,Stock Issue Payment Update Array
OUTPUT 	: master ID; Message of success or failure
BY 		: Ashwini
*/
function i_update_stock_additional_payment_master($master_id,$stock_additional_payment_master_update_data)
{
		$stock_additional_payment_master_sresult = db_update_stock_additional_payment_master($master_id,$stock_additional_payment_master_update_data);
		
		if($stock_additional_payment_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "master_id Successfully added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
		
	return $return;
}

/*
PURPOSE : To delete  Stock Additional Payment Master List
INPUT 	: master ID, Stock Additional Payment Master Update Array
OUTPUT 	: master ID; Message of success or failure
BY 		: Ashwini
*/

function i_delete_stock_additional_payment_master($master_id,$stock_additional_payment_master_update_data)
{
	$stock_additional_payment_master_update_data = array('active'=>'0');
	$stock_additional_payment_master_sresult = db_update_stock_additional_payment_master($master_id,$stock_additional_payment_master_update_data);
	
	if($stock_additional_payment_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "stock_additional_payment_master Successfully Deleted";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}
/*
PURPOSE : To get Grn Engineer Inspection list
INPUT 	: Engineer Inspection Search Data
OUTPUT 	: Grn Engineer Inspection List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_sum_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data)
{
	$grn_engineer_inspection_sresult = db_get_sum_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);
	
	if($grn_engineer_inspection_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$grn_engineer_inspection_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No grn engineer inspection added. Please contact the system admin"; 
    }
	
	return $return;
}



/* PRIVATE FUNCTIONS - START */

function p_generate_grn_no()

{

	// Get the last invoice no

	$stock_grn_search_data = array("sort"=>'1');

	$grn_no_data = db_get_stock_grn_list($stock_grn_search_data);

	

	if($grn_no_data["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$grn_numeric_value_data = $grn_no_data["data"][0]["stock_grn_no"];

		$grn_numeric_value_array = explode(INV_NO_DELIMITER,$grn_numeric_value_data);

		

		$grn_numeric_value = ($grn_numeric_value_array[2]) + 1;

	}

	else

	{

		$grn_numeric_value = 1;

	}

	

	return INV_NO_ROOT.INV_NO_DELIMITER.INV_NO_GRN.INV_NO_DELIMITER.$grn_numeric_value;

}
/*
PURPOSE : To add Stock Grn Document
INPUT 	: Document ID, Grn ID, Name, Active, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/
function i_add_stock_grn_document($grn_id,$name,$remarks,$added_by)
{
	
		$stock_grn_document_iresult = db_add_stock_grn_document($grn_id,$name,$remarks,$added_by);
		
		if($stock_grn_document_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Stock Grn Document Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
		
	return $return;
}

/*
PURPOSE : To get Stock Grn Document list
INPUT 	: Document ID, Active
OUTPUT 	: Stock Grn Document List or Error Details, success or failure message
BY 		: Ashwini
*/
function i_get_stock_grn_document_list($stock_grn_document_search_data)
{
	$stock_grn_document_sresult = db_get_stock_grn_document_list($stock_grn_document_search_data);
	
	if($stock_grn_document_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$stock_grn_document_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Stock Grn Document added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Stock Grn Document
INPUT 	: Document ID, Stock Grn Document Update Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_update_stock_grn_document($document_id,$stock_grn_document_update_data)
{
		$stock_grn_document_sresult = db_update_stock_grn_document($document_id,$stock_grn_document_update_data);
		
		if($stock_grn_document_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Stock Grn Document Successfully added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
		
	return $return;
}
 

?>