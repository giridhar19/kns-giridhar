<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 18th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string data
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}
	if(isset($_GET["follow"]))
	{
		$fup_id = $_GET["follow"];
	}
	else
	{
		$fup_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["edit_payment_fup_submit"]))
	{
		$fup_id     = $_POST["hd_fup_id"];				
		$remarks    = $_POST["txt_remarks"];
		$booking_id = $_POST["hd_booking_id"];
		
		// Check for mandatory fields
		if(($booking_id !="") && ($remarks !="") && ($user !=""))
		{
			$payment_fup_uresult = i_edit_payment_fup($fup_id,$remarks,$user);
			
			if($payment_fup_uresult["status"] == SUCCESS)
			{
				header("location: crm_add_payment_fup.php?booking=".$booking_id);
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $payment_fup_uresult["data"];			
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Payment Follow Up</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header" style="height:70px;">
	      				<i class="icon-user"></i>
	      				<h3>Project: <?php if($cust_details["status"] == SUCCESS){
							echo $cust_details_list[0]["project_name"]; 
						}
						else
						{
							echo '<i>NOT ADDED</i>';
						}?>
						&nbsp;&nbsp;&nbsp;&nbsp;Site No: <?php if($cust_details["status"] == SUCCESS)
						{
							echo $cust_details_list[0]["crm_site_no"]; 
						}
						else
						{
							echo '<i>NOT ADDED</i>';
						}?>
						&nbsp;&nbsp;&nbsp;&nbsp;Customer: <?php if($cust_details["status"] == SUCCESS)
						{						
							echo $cust_details_list[0]["crm_customer_name_one"]; ?> (<?php echo $cust_details_list[0]["crm_customer_contact_no_one"]; ?>)
						<?php
						}
						else
						{
							echo '<i>NOT ADDED</i>';
						}?></h3>					
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    Edit Payment Follow Up							
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_payment_fup" class="form-horizontal" method="post" action="crm_edit_payment_fup.php">
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />
								<input type="hidden" name="hd_fup_id" value="<?php echo $fup_id; ?>" />
									<fieldset>
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Customer Remarks*</label>
											<div class="controls">
												<textarea name="txt_remarks" class="span6" required></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_payment_fup_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
