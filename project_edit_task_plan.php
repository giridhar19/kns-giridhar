<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	$task_planning = $_SESSION['task_planning_id'];
	$task_planning = explode(",", $_SESSION['task_planning_id']);
	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	$task_data = $_POST['task_data'];
	$task_data_array = json_decode($task_data,true);	
	$data_value = $task_data_array["data"];
	for($task_count = 0 ; $task_count < (count($data_value) - 1) ; $task_count++)
	{
		$process_name   = $data_value[$task_count][0];
		$task_name    	= $data_value[$task_count][1];
		$task_id    	= $data_value[$task_count][2];
		$uom	    	= $data_value[$task_count][3];
		$measurment 	= $data_value[$task_count][4];
		$road_name	 	= $data_value[$task_count][5];
		$object_type 	= $data_value[$task_count][6];
		$object_name 	= $data_value[$task_count][7];
		$no_of_objects 	= $data_value[$task_count][8];
		$per_day_output = $data_value[$task_count][9];
		$total_days	    = $data_value[$task_count][10];
		$rate			= $data_value[$task_count][11];
		$start_date 	= $data_value[$task_count][12];
		$end_date	 	= $data_value[$task_count][13];
		$planning_id 	= $data_value[$task_count][14];
		  if (in_array($planning_id, $task_planning))
		  {
			$task_planning_id = $planning_id ;
			// Get list of task plans for this process plan
			$project_task_planning_search_data = array("active"=>'1',"planning_id"=>$task_planning_id);
			$project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);
			if($project_task_planning_list["status"] == SUCCESS)
			{
				$project_task_planning_list_data = $project_task_planning_list["data"];
				$old_measurment = $project_task_planning_list_data[0]["project_task_planning_measurment"];
				$old_end_date  = $project_task_planning_list_data[0]["project_task_planning_end_date"];
			}
		  }
		  else
		  {
			$task_planning = "";
		  }
		
		//Get Loaction Master data
		if($road_name != NULL)
		{
			// Get Projects Site Location Mapping added
			$project_site_location_mapping_master_search_data = array("active"=>'1',"name"=>$road_name);
			$project_site_location_mapping_master_list = i_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);
			if($project_site_location_mapping_master_list['status'] == SUCCESS)
			{
				$project_site_location_mapping_master_list_data = $project_site_location_mapping_master_list['data'];
				$location_id = $project_site_location_mapping_master_list_data[0]["project_site_location_mapping_master_id"];
			}	
			else
			{
				$location_id = "No Roads";
			}
		}
		else
		{
			$location_id = "No Roads";
		}
		//Get Machine type Master
		// Machine Type List
		$project_machine_type_master_search_data = array("active"=>'1',"name"=>$object_name);
		$project_machine_type_master_list = i_get_project_machine_type_master($project_machine_type_master_search_data);
		if($project_machine_type_master_list["status"] == SUCCESS)
		{
			$project_machine_type_master_list_data = $project_machine_type_master_list["data"];
			$object_name_id = $project_machine_type_master_list_data[0]["project_machine_type_master_id"];
		}
		else
		{
			// Get Project CW Master List
			$project_cw_master_search_data = array("active"=>'1',"name"=>$object_name);
			$project_cw_master_list = i_get_project_cw_master($project_cw_master_search_data);
			if($project_cw_master_list['status'] == SUCCESS)
			{
				$project_cw_master_list_data = $project_cw_master_list["data"];
				$object_name_id = $project_cw_master_list_data[0]["project_cw_master_id"];
			}
		}
		
		//Get Unit Measure Master
		// Get Unit Measure modes already added
		$project_uom_master_search_data = array("active"=>'1',"name"=>$uom);
		$unit_list = i_get_project_uom_master($project_uom_master_search_data);
		if($unit_list['status'] == SUCCESS)
		{
			$unit_list_data = $unit_list['data'];
			$uom_id = $unit_list_data[0]["project_uom_id"];
		}
		else
		{
			$uom_id = "";
		}
		$start_date_formatted = get_formatted_date($start_date,"Y-m-d");
		$end_date_formatted = get_formatted_date($end_date,"Y-m-d");
		
		//Data Insertion
		if($measurment != 0)
		{
			$project_task_planning_update_data= array("measurment"=>$measurment,"no_of_roads"=>$location_id,"object_type"=>$object_type,
			"machine_type"=>$object_name_id,"uom"=>$uom_id,"no_of_object"=>$no_of_objects,"per_day_out"=>$per_day_output,"total_days"=>$total_days,"plan_start_date"=>$start_date_formatted,"plan_end_date"=>$end_date_formatted);
			$project_object_output_master_iresult = i_update_project_task_planning($planning_id,'',$project_task_planning_update_data);
			if($project_object_output_master_iresult["status"] == SUCCESS)
			{
				$task_plan_history_iresults = i_add_project_task_planning_shadow($planning_id,$total_days,$start_date_formatted,$old_measurment,$measurment,$old_end_date,$end_date_formatted,'',$user);
			}
		}
	}
}
else
{
	header("location:login.php");
}	
?>
