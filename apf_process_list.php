<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: apf_details_list.php
CREATED ON	: 10-Jan-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('APF_PROCESS_LIST_FUNC_ID','320');
/* DEFINES - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',APF_PROCESS_LIST_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',APF_PROCESS_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',APF_PROCESS_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',APF_PROCESS_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing
	
	if(isset($_GET['apf_id']))
	{
		$apf_id = $_GET['apf_id'];
	}
	else
	{
		$apf_id = "";
	}
	
	if(isset($_GET['project']))
	{
		$project = $_GET['project'];
	}
	else
	{
		$project = "";
	}
	
	if(isset($_GET['apf_process_id']))
	{
		$apf_process_id = $_GET['apf_process_id'];
	}
	else
	{
		$apf_process_id = "";
	}
	
	if(isset($_REQUEST['source']))
	{
		$source = $_REQUEST['source'];
	}
	else
	{
		$source = "";
	}

	// Get process details
	$apf_details_search_data = array("apf_id"=>$apf_id);
	$apf_details_list = i_get_apf_details($apf_details_search_data);
	if($apf_details_list['status'] == SUCCESS)
	{
		
		$apf_details_list_data = $apf_details_list["data"];
		// Get process details
		$apf_file_search_data = array("apf_id"=>$apf_details_list_data[0]["apf_details_id"]);
		$apf_file_list = i_get_apf_file($apf_file_search_data);
		if($apf_file_list['status'] == SUCCESS)
		{			
			$project_name  = $apf_file_list["data"][0]["apf_project_master_name"];
			$bank_name     = $apf_file_list["data"][0]["apf_bank_master_name"];
			$survey_no     = $apf_file_list["data"][0]["apf_survey_master_survey_no"];
		}		
		else
		{
			$project_name  = "";
			$bank_name     = "";
			$survey_no     = "";
		}
	}	
	else
	{
		$project_name  = "";
		$bank_name     = "";
		$survey_no     = "";
	}
	
	if($apf_id == '')
	{
		$search_project = "-1";
	}
	else
	{
		$search_project = '';
	}
	$search_process   = "";
	$search_bank      = "";
	
	if(isset($_POST['file_search_submit']))
	{
		$search_project    = $_POST["search_project"];
		$search_process    = $_POST["search_process"];
		$search_bank       = $_POST["search_bank"];
		$apf_id            = $_POST["hd_apf_id"];
	}
	
	
	// Get APF Process Master modes already added
	$apf_process_master_search_data = array("active"=>'1');
	$apf_process_master_list = i_get_apf_process_master($apf_process_master_search_data);
	if($apf_process_master_list['status'] == SUCCESS)
	{
		$apf_process_master_list_data = $apf_process_master_list['data'];
	}	

	else
	{
		$alert = $apf_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Bank Master modes already added
	$apf_bank_master_search_data = array("active"=>'1');
	$bank_master_list = i_get_apf_bank_master($apf_bank_master_search_data);
	if($bank_master_list['status'] == SUCCESS)
	{
		$bank_master_list_data = $bank_master_list["data"];
	}
	else
	{
		$alert = $bank_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Master already added
	$apf_project_master_search_data = array("active"=>'1');
	$apf_project_master_list = i_get_apf_project_master($apf_project_master_search_data);
	if($apf_project_master_list['status'] == SUCCESS)
	{
		$apf_project_master_list_data = $apf_project_master_list['data'];
	}
	else
	{
		$alert = $apf_project_master_list["data"];
		$alert_type = 0;
	}

	// Get APF Process already added
	$apf_process_search_data = array("active"=>'1',"apf_id"=>$apf_id,"apf_process_id"=>$apf_process_id,"project_name"=>$search_project,"bank_name"=>$search_bank,
	"process_id"=>$search_process);
	$apf_process_list = i_get_apf_process($apf_process_search_data);
	if($apf_process_list['status'] == SUCCESS)
	{
		$apf_process_list_data = $apf_process_list['data'];
	}	
		
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>APF Process List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:80px"> <i class="icon-th-list"></i>			
              <h3>APF Process List</h3><?php if($add_perms_list['status'] == SUCCESS){?><span style="float:right; padding-right:20px;">
			  <?php if($source == "view") { ?> <a href="apf_add_process.php?apf_id=<?php echo $apf_id ;?> ">Add Process</a><?php } ?></span><?php } ?><?php if($apf_id != "") { ?>Project:&nbsp;&nbsp;&nbsp;<?php echo $project_name; ?>&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;Bank:&nbsp;&nbsp;&nbsp;<?php echo $bank_name; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</br>Survey No:&nbsp;&nbsp;&nbsp;<?php echo $survey_no; ?><?php } ?>
            </div>
            <!-- /widget-header -->
            <?php if($apf_id == "") { ?>
			<div class="widget-header" style="height:50px; padding-top:10px;">		
			<form method="post" id="file_search_form" action="apf_process_list.php">
			<input type="hidden" name="hd_apf_id" value="<?php echo $apf_id; ?>" />
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($apf_count = 0; $apf_count < count($apf_project_master_list_data); $apf_count++)
			  {
			  ?>
			  <option value="<?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_id"]; ?>" <?php if($search_project == $apf_project_master_list_data[$apf_count]["apf_project_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($apf_count = 0; $apf_count < count($apf_process_master_list_data); $apf_count++)
			  {
			  ?>
			  <option value="<?php echo $apf_process_master_list_data[$apf_count]["apf_process_master_id"]; ?>" <?php if($search_process == $apf_process_master_list_data[$apf_count]["apf_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $apf_process_master_list_data[$apf_count]["apf_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_bank">
			  <option value="">- - Select Bank - -</option>
			  <?php
			  for($bank_count = 0; $bank_count < count($bank_master_list_data); $bank_count++)
			  {
			  ?>
			  <option value="<?php echo $bank_master_list_data[$bank_count]["apf_bank_master_id"]; ?>" <?php if($search_bank == $bank_master_list_data[$bank_count]["apf_bank_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $bank_master_list_data[$bank_count]["apf_bank_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
			  </div>
			   <?php
			  }
			  ?>
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
					<th>Process</th>
					<th>Bank</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Leadtime</th>				
					<th>Added On</th>
					<th colspan="4" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($apf_process_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($apf_process_list_data); $count++)
					{
						$sl_no++;
						
						// Get APF Process Master modes already added
						$apf_process_master_search_data = array("process_id"=>$apf_process_list_data[$count]["process_id"]);
						$apf_process_master_list = i_get_apf_process_master($apf_process_master_search_data);
						if($apf_process_master_list['status'] == SUCCESS)
						{
							$apf_process_master_list_data = $apf_process_master_list['data'];
							$process_name = $apf_process_master_list_data[0]["apf_process_master_name"];
						}	

						else
						{
							$alert = $apf_process_master_list["data"];
							$alert_type = 0;
						}
						
						//Get Leadtime
						 $process_start_date = $apf_process_list_data[$count]["apf_process_start_date"];
						 $process_end_date = $apf_process_list_data[$count]["apf_process_end_date"];
						 $leadtime = get_date_diff($process_start_date,$process_end_date);
	
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $apf_process_list_data[$count]["apf_project_master_name"]; ?></td>
					<td><?php echo $process_name; ?></td>
					<td><?php echo $apf_process_list_data[$count]["apf_bank_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($process_start_date,"d-M-Y"); ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($process_end_date,"d-M-Y"); ?></td>
					<td><?php echo $leadtime["data"] ;?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($apf_process_list_data[$count][
					"apf_process_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_apf_edit_process('<?php echo $apf_process_list_data[$count]["apf_process_id"]; ?>','<?php echo $apf_process_list_data[$count]["apf_process_apf_id"]; ?>');">Edit</a><?php } ?></td>
					<td><?php if(($apf_process_list_data[$count]["apf_process_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return apf_delete_process_list('<?php echo $apf_process_list_data[$count]["apf_process_id"]; ?>','<?php echo $apf_process_list_data[$count]["apf_process_apf_id"]; ?>');">Delete</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_query_response('<?php echo $apf_process_list_data[$count]["apf_process_id"]; ?>','view');">Add Query</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_apf_documents('<?php echo $apf_process_list_data[$count]["process_id"]; ?>','<?php echo $apf_process_list_data[$count]["apf_process_id"]; ?>');">Upload Documents</a><br /><br /><a style="padding-right:10px" href="#" onclick="return go_to_apf_download('<?php echo $apf_process_list_data[$count]["process_id"]; ?>','<?php echo $apf_process_list_data[$count]["apf_process_id"]; ?>');">Download</a><br /><br /><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			 <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function apf_delete_process_list(apf_process_id,apf_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "apf_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/apf_delete_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("apf_process_id=" + apf_process_id + "&action=0");
		}
	}	
}
function go_to_apf_edit_process(apf_process_id,apf_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_edit_process.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","apf_process_id");
	hiddenField1.setAttribute("value",apf_process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","apf_id");
	hiddenField2.setAttribute("value",apf_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_query_response(process_id,source)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_add_query.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","source");
	hiddenField2.setAttribute("value",source);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_apf_documents(process_id,apf_process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_upload_documents.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","apf_process_id");
	hiddenField2.setAttribute("value",apf_process_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_apf_download(process_id,apf_process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "apf_document_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","apf_process_id");
	hiddenField2.setAttribute("value",apf_process_id);
	
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>