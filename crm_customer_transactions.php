<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 29th Dec 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END *//* DEFINES - START */define('CRM_CUSTOMER_TRANS_FUNC_ID','338');/* DEFINES - END */
/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list    = i_get_user_perms($user,'',CRM_CUSTOMER_TRANS_FUNC_ID,'1','1');	$view_perms_list   = i_get_user_perms($user,'',CRM_CUSTOMER_TRANS_FUNC_ID,'2','1');	$edit_perms_list   = i_get_user_perms($user,'',CRM_CUSTOMER_TRANS_FUNC_ID,'3','1');	$delete_perms_list = i_get_user_perms($user,'',CRM_CUSTOMER_TRANS_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}				if(isset($_GET["status"]))	{		$status = $_GET["status"];	}	else	{		$status = "";	}		
	/* QUERY STRING - END */
	
	// Get the booking details for this booking ID
	$booking_details = i_get_site_booking($booking_id,'','','','','','','','','','','','','');
	if($booking_details["status"] == SUCCESS)
	{
		$booking_details_list = $booking_details["data"];
	}
	else
	{
		$alert = $booking_details["data"];
		$alert_type = 0;
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>CRM Customer Transactions</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Customer Transactions</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">						
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">							
								<div class="tab-pane active" id="formcontrols">
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Customer Details</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>	
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>			  
										  <tr>
										  <td width="25%">Enquiry No</td>
										  <td width="70%"><?php echo $booking_details_list[0]["enquiry_number"]; ?></td>
										  </tr>
										  <tr>
										  <td>Project</td>
										  <td><?php echo $booking_details_list[0]["project_name"]; ?></td>
										  </tr>
										  <tr>
										  <td>Site No</td>										  
										  <td><?php echo $booking_details_list[0]["crm_site_no"]; ?></td>
										  </tr>
										  <tr>
										  <td>Actual Site Area (Sq. ft)</td>										  
										  <td><?php echo $booking_details_list[0]["crm_site_area"]; ?></td>	
										  </tr>
										  <tr>
										  <td>Sale Consideration Area (Sq. ft)</td>										  
										  <td><?php echo $booking_details_list[0]["crm_booking_consideration_area"]; ?></td>
										  </tr>
										  <tr>
										  <td>Rate per sq. ft.</td>										  
										  <td><?php echo $booking_details_list[0]["crm_booking_rate_per_sq_ft"]; ?></td>
										  </tr>
										  <tr>
										  <td>Sale Consideration value</td>										  
										  <td><?php echo ($booking_details_list[0]["crm_booking_consideration_area"]*$booking_details_list[0]["crm_booking_rate_per_sq_ft"]); ?></td>
										  </tr>
										  <tr>
										  <td>Booked By</td>										  
										  <td><?php echo $booking_details_list[0]["user_name"]; ?></td>
										  </tr>
										  <tr>
										  <td>Booked On</td>										  
										  <td><?php if($booking_details_list[0]["crm_booking_date"] != "0000-00-00")
										  {
											  echo date("d-M-Y",strtotime($booking_details_list[0]["crm_booking_date"]));
										  }
										  else
										  {
											  echo "Booking Date Not Entered";
										  }
										  ?></td>
										  </tr>
										  <tr>
										  <td>Client Name</td>										  
										  <td><?php
										  if($cust_details["status"] == SUCCESS)
										  {
											  echo $cust_details_list[0]["crm_customer_name_one"];
										  }
										  else
										  {
											  echo $booking_details_list[0]["name"];
										  }
										  ?></td>
										  </tr>
										  <tr>
										  <td>Mobile Number</td>										  
										  <td><?php
										  if($cust_details["status"] == SUCCESS)
										  {
											  echo $cust_details_list[0]["crm_customer_contact_no_one"];
										  }
										  else
										  {
											  echo $booking_details_list[0]["cell"];
										  }
										  ?></td>
										  </tr>
										  <tr>
										  <td>Email</td>										  
										  <td><?php
										  if($cust_details["status"] == SUCCESS)
										  {
											  echo $cust_details_list[0]["crm_customer_email_id_one"];
										  }
										  else
										  {
											  echo $booking_details_list[0]["email"];
										  }
										  ?></td>
										  </tr>
										  <tr>
										  <td>Source</td>										  
										  <td><?php
										  echo $booking_details_list[0]["enquiry_source_master_name"];
										  ?></td>
										  </tr>
										  </tbody>
										</table>
										<table class="table table-bordered" style="table-layout: fixed;">
										  <tbody>
										  <tr>
										  <?php
										  if($cust_details["status"] == SUCCESS)
										  {
										  ?>
										  <td><?php if($view_perms_list['status'] == SUCCESS){?><a href="crm_view_customer_profile.php?profile=<?php echo $cust_details_list[0]["crm_customer_details_id"]; ?>">View Customer Profile</a><?php } ?></td>
										  <td><?php if($edit_perms_list['status'] == SUCCESS){?><a href="crm_edit_customer_profile.php?profile=<?php echo $cust_details_list[0]["crm_customer_details_id"]; ?>">Edit Customer Profile</a><?php } ?></td>
										  <?php
										  }
										  else
										  {
										  ?>
										  <td colspan="2"><?php if($add_perms_list['status'] == SUCCESS){?><a href="crm_add_customer_profile.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Add Customer Profile</a><?php } ?></td>
										  <?php
										  }
										  ?>
										  <td><?php if($edit_perms_list['status'] == SUCCESS){?><a href="crm_update_booking_area.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Update Booking Area</a><?php } ?></td>
										  </tr>
										  <tr>
										  <td><?php if($add_perms_list['status'] == SUCCESS){?><a href="crm_add_payment_schedule.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Add Payment Schedule</a><?php } ?></td>
										  <td><?php if($view_perms_list['status'] == SUCCESS){?><a href="crm_view_payment_schedule.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">View Payment Schedule</a><?php } ?></td>
										  <td><?php if($add_perms_list['status'] == SUCCESS){?><a href="crm_add_payment_terms.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Add Other Payment Terms</a><?php } ?></td>
										  </tr>
										  <tr>
										  <td><?php if($add_perms_list['status'] == SUCCESS){?><a href="crm_add_payment.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Add Payment</a><?php } ?></td>
										  <td><?php if($view_perms_list['status'] == SUCCESS){?><a href="crm_view_payments.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">View Payments</a><?php } ?></td>
										  <td><?php if($add_perms_list['status'] == SUCCESS){?><a href="crm_add_payment_fup.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Add Payment Follow Up</a><?php } ?></td>
										  </tr>
										  <tr>
										  <td><?php if($add_perms_list['status'] == SUCCESS){?><a href="crm_add_others_payment.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Add Other Payment</a><?php } ?></td>
										  <td><?php if($view_perms_list['status'] == SUCCESS){?><a href="crm_view_other_payments.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">View Other Payments</a><?php } ?></td>
										  <td>&nbsp;</td>
										  </tr>
										  <tr>
										  <td><?php if($add_perms_list['status'] == SUCCESS){?><a href="crm_add_payment_delay_reason.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>&status=<?php echo '0'; ?>">Update Delay Reason</a><?php } ?></td>
										  <td><?php if($view_perms_list['status'] == SUCCESS){?><a href="#" onclick="return untag_delay(<?php echo $booking_details_list[0]["crm_booking_id"]; ?>);">Release from delay</a><?php } ?></td>
										  <td>&nbsp;</td>
										  </tr>
											  <tr>
											  <td><?php if($add_perms_list['status'] == SUCCESS){?><a href="crm_add_booking_process_data.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Add Dates</a><?php } ?></td>
											  <td><?php if($view_perms_list['status'] == SUCCESS){?><a href="crm_cancel_booking.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Cancel Booking</a><?php } ?></td>
											  <td><?php if($view_perms_list['status'] == SUCCESS){?><a href="crm_change_rate.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Change Rate</a><?php } ?></td>
											  </tr>
											  <?php
											  if($edit_perms_list['status'] == SUCCESS)
											  {
											  ?>
											  <tr>
											  <td><a href="crm_edit_booking.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>&status=<?php echo $status ; ?>">Edit Booking</a></td>
											  <td>&nbsp;</td>
											  <td>&nbsp;</td>
											  </tr>
											  <?php
											  }
										  ?>
										  </tbody>
										</table>										
									</fieldset>									
								</div>																
								
							</div>
						  						  
						</div>
	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function untag_delay(booking_id)
{
	var ok = confirm("Are you sure you want to remove delay reason?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{				
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					    //Success
					}
				}
			}

			xmlhttp.open("POST", "crm_untag_delay_reason.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("booking=" + booking_id);
		}
	}	
}
</script>
  </body>

</html>
