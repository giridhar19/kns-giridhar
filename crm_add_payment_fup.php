<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 18th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string data
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["add_payment_fup_submit"]))
	{
		$booking_id      = $_POST["hd_booking_id"];		
		$follow_up_date  = $_POST["dt_follow_up"];
		$notes           = $_POST["txt_notes"];
		
		// Check for mandatory fields
		if(($booking_id !="") && ($follow_up_date !="") && ($notes !="") && ($user !=""))
		{
			if(strtotime($follow_up_date) < strtotime(date("Y-m-d H:i:s")))
			{
				$alert_type = 0;
				$alert      = "Follow Up date cannot be lesser than current date time";
			}
			else
			{
				$payment_fup_iresult = i_add_payment_fup($booking_id,$follow_up_date,$notes,$user);
				
				if($payment_fup_iresult["status"] == SUCCESS)
				{
					header("location: crm_add_payment_fup.php?booking=".$booking_id);
				}
				else
				{
					$alert_type = 0;
				}
				
				$alert = $payment_fup_iresult["data"];
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Payment Follow Up List
	$payment_fup_list = i_get_payment_fup_list('',$booking_id,'','','','','');
	if($payment_fup_list["status"] == SUCCESS)
	{
		$payment_fup_list_data = $payment_fup_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$payment_fup_list["data"];
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Payment Follow Up</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header" style="height:70px;">
	      				<i class="icon-user"></i>
						<?php
						if($cust_details['status'] == SUCCESS)
						{
						?>
	      				<h3>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Customer: <?php echo $cust_details_list[0]["crm_customer_name_one"]; ?> (<?php echo $cust_details_list[0]["crm_customer_contact_no_one"]; ?>)</h3>	
						<?php
						}
						else
						{
							?>
							<h3>Project: <i>Profile Not entered</i>&nbsp;&nbsp;&nbsp;&nbsp;Site No: <i>Profile Not entered</i>&nbsp;&nbsp;&nbsp;&nbsp;Customer: <i>Profile Not entered</i></h3>	
							<?php
						}
						?>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    Add Payment Follow Up	
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								<span style="float:right;"><a href="crm_customer_transactions.php?booking=<?php echo $booking_id; ?>">Back to customer profile</a><br /><br /><a href="crm_payment_follow_up_report.php">Back to Payment Follow Up Report</a></span>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_payment_fup" class="form-horizontal" method="post" action="crm_add_payment_fup.php">
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />
									<fieldset>																																																							
										<div class="control-group">											
											<label class="control-label" for="dt_follow_up">Follow Up Date*</label>
											<div class="controls">
												<input type="datetime-local" class="span6" name="dt_follow_up" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_notes">Notes*</label>
											<div class="controls">
												<textarea name="txt_notes" class="span6" required></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
											
										<div class="form-actions">
										<?php
										if($payment_fup_list['status'] != SUCCESS)
										{
											?>
											<input type="submit" class="btn btn-primary" name="add_payment_fup_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
											<?php
										}
										else if($payment_fup_list_data[0]["crm_payment_follow_up_updated_by"] != '')
										{
										?>										
											<input type="submit" class="btn btn-primary" name="add_payment_fup_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										<?php
										}
										else
										{
											echo 'Please update status of previous follow up before adding a new follow up';
										}
										?>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								<table class="table table-bordered" style="table-layout: fixed;" id="fup_table">
								<thead>
								  <tr>
									<th>Follow Up Date</th>					
									<th>Notes (To discuss with client)</th>	
									<th>Added By</th>
									<th>Updated On</th>
									<th>Customer Remarks</th>									
									<th>Updated By</th>									
									<th>&nbsp;</th>			
								</tr>
								</thead>
								<tbody>							
								<?php
								if($payment_fup_list["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($payment_fup_list_data); $count++)
									{																	
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i",strtotime($payment_fup_list_data[$count]["crm_payment_follow_up_date_time"])); ?></td>
									<td style="word-wrap:break-word;"><?php echo $payment_fup_list_data[$count]["crm_payment_follow_up_notes"]; ?></td>		
									<td style="word-wrap:break-word;"><?php echo $payment_fup_list_data[$count]["adder"]; ?></td>
									<td style="word-wrap:break-word;"><?php if(($payment_fup_list_data[$count]["crm_payment_follow_up_updated_on"] != "0000-00-00 00:00:00") && ($payment_fup_list_data[$count]["crm_payment_follow_up_updated_on"] != "1970-01-01 01:00:00"))
									{
										echo date("d-M-Y H:i",strtotime($payment_fup_list_data[$count]["crm_payment_follow_up_updated_on"]));
										$lock_updation = true;
									}
									else
									{
										echo "";
										$lock_updation = false;
									}?></td>
									<td style="word-wrap:break-word;"><?php echo $payment_fup_list_data[$count]["crm_payment_follow_up_cust_remarks"]; ?></td>									
									<td style="word-wrap:break-word;"><?php echo $payment_fup_list_data[$count]["updater"]; ?></td>							
									<td style="word-wrap:break-word;"><?php if($lock_updation == false) { ?><a href="crm_edit_payment_fup.php?booking=<?php echo $payment_fup_list_data[$count]["crm_payment_follow_up_booking_id"]; ?>&follow=<?php echo $payment_fup_list_data[$count]["crm_payment_follow_up_id"]; ?>">Update</a><?php } else{ echo 'ALREADY UPDATED'; } ?></td>
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="7">No follow ups yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
