<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 23rd Sep 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* DEFINES - START */
define('WAITING_PO_FUNC_ID','189');
/* DEFINES - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page		
	$edit_perms_list = i_get_user_perms($user,'',WAITING_PO_FUNC_ID,'3','1');
	
	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";	
	
	if(isset($_POST["po_search_submit"]))
	{
		$search_user = $_POST["search_user"];
	}
	else
	{
		$search_user = $user;
	}
	// Get Users modes already added
	$user_list = i_get_user_list('','','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}
	
	// Get Purchase Item Details
	$stock_purchase_order_items_search_data = array("order_id"=>$order_id,"active"=>'1',"status"=>'Waiting',"added_by"=>$search_user);
	$purchase_order_items_list = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);	
	if($purchase_order_items_list["status"] == SUCCESS)
	{
		$purchase_order_items_list_data = $purchase_order_items_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$purchase_order_items_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Waiting Purchase Orders</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Work In Progress Purchase Orders</h3>
	  				</div> <!-- /widget-header -->
					<?php
					if($edit_perms_list['status'] == SUCCESS)
					{
					?>
					<div class="widget-header" style="height:50px; padding-top:10px;">               
					  <form method="post" id="file_search_form" action="stock_po_waiting_list.php">
					  <input type="hidden" name="hd_vendor_id" id="hd_vendor_id" value="<?php echo $search_vendor; ?>" />	
					  <span style="padding-right:20px; padding-left:20px; float:left;">	
					  <select name="search_user">
					  <option value="">- - Select Assigned To - -</option>
					  <?php
					  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
					  {
					  ?>
					  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
					  <?php
					  }
					  ?>
					  </select>
					  </span>
					  <input type="submit" name="po_search_submit" />
					  </form>			  
					</div>
					<?php
					}
					?>
					<div class="widget-content">
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>					
										</div>
									</div>
								
								 <div class="widget widget-table action-table">
           
            <!-- /widget-header -->
            <div class="widget-content">
			<form action="task_list.php" method="post" id="task_update_form">		
			<input type="hidden" name="hd_order_id" value="<?php echo $order_id; ?>" />
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>Item</th>
					<th>Purchase Order No</th>
					<th>Quantity</th>
					<th>Tax Type</th>
					<th>Vendor</th>
					<th>Rate</th>
					<th>Value</th>					
					<th>Added On</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?php
				if($purchase_order_items_list["status"] == SUCCESS)
				{					
					for($count = 0; $count < count($purchase_order_items_list_data); $count++)
					{		
						$po_item_status = $purchase_order_items_list_data[$count]["stock_purchase_order_item_status"];
					?>				
					<tr>
						
						<td><?php echo $purchase_order_items_list_data[$count]["stock_material_name"]; ?></td>
						<td><?php echo $purchase_order_items_list_data[$count]["stock_purchase_order_number"]; ?></td>
						<td><?php echo $item_qty = $purchase_order_items_list_data[$count]["stock_purchase_order_item_quantity"]; ?></td>
						<td><?php echo $purchase_order_items_list_data[$count]["stock_tax_type_master_name"]; ?></td>
						<td><?php echo $purchase_order_items_list_data[$count]["stock_vendor_name"]; ?></td>
						<td><?php echo $rate = $purchase_order_items_list_data[$count]["stock_purchase_order_item_cost"]; ?></td>
						<td><?php echo $total_value = ($item_qty * $rate) ; ?></td>
						<td><?php echo date("d-M-Y",strtotime($purchase_order_items_list_data[$count]
						["stock_purchase_order_item_added_on"])); ?></td>
						<td><a href="stock_purchase_order_items.php?po_id=<?php echo $purchase_order_items_list_data[$count]["stock_purchase_order_id"]; ?>">View PO</a></td>												
					</tr>
					<?php 		
					}
				}
				else
				{
				?>
				<td colspan="10">No Items added</td>
				<?php
				}
				 ?>	

                </tbody>
				</table>
				<br/>	
				
				<div class="modal-body">
			    <div class="row">
 
				  </div>
				  </div>			  
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
               <br><br>
			   
						  
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->  
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_purchase_order_items(item_id,po_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "stock_purchase_order_items.php";
					}
				}
			}

			xmlhttp.open("POST", "stock_delete_purchase_order_items.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("item_id=" + item_id + "po_id=" + po_id +  "&action=0");
		}
	}	
}
function go_to_grn(order_id,item_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "stock_add_grn.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","item_id");
	hiddenField1.setAttribute("value",item_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","order_id");
	hiddenField2.setAttribute("value",order_id);
	
	
	form.appendChild(hiddenField1);	
	form.appendChild(hiddenField2);	
	
	document.body.appendChild(form);
    form.submit();
}
function edit_purchase_order_items(item_id,order_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "stock_edit_po_items.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","item_id");
	hiddenField1.setAttribute("value",item_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","order_id");
	hiddenField2.setAttribute("value",order_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
function go_to_quotation_list(indent_item_id,mode)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "stock_quotation_compare_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","indent_item_id");
	hiddenField1.setAttribute("value",indent_item_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","mode");
	hiddenField2.setAttribute("value",mode);
	
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function item_for_approval(order_id)
{
	var ok = confirm("Are you sure you want to send for approvals?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{						
						document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
						document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
						window.location = "stock_po_items_approval.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/stock_po_items_for_approval.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("order_id=" + order_id + "&action=0");
		}
	}	
}

</script>


  </body>

</html>
