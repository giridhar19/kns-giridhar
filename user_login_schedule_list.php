<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/* DEFINES - START */
define('USER_LOGIN_SCHEDULE_FUNC_ID','3');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list = i_get_user_perms($user,'',USER_LOGIN_SCHEDULE_FUNC_ID,'2','1');

	// Query String Data
	// Nothing here
	
	// Form processing
	if(isset($_POST['schedule_search_submit']))
	{
		if((isset($_POST['dt_login_date_start'])) && ($_POST['dt_login_date_start'] != ''))
		{
			$login_date_start_value = $_POST['dt_login_date_start'];
			$login_date_start = $login_date_start_value." 00:00:00";
		}
		else
		{
			$login_date_start = '';			
		}
		if((isset($_POST['dt_login_date_end'])) && ($_POST['dt_login_date_end'] != ''))
		{
			$login_date_end_value = $_POST['dt_login_date_end'];
			$login_date_end = $login_date_end_value." 23:59:59";
		}		
		else
		{
			$login_date_end   = '';
		}
		$search_user = $_POST['search_user'];
	}
	else
	{
		$login_date_start = '';
		$login_date_end   = '';
		$search_user      = $user;
	}

	// Get list of Users Login
	$user_login_list = i_get_user_login_schedule($search_user,$login_date_start,$login_date_end,'');
	if($user_login_list["status"] == SUCCESS)
	{
		$user_login_list_data = $user_login_list["data"];
	}
	
	// Get list of users
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Users Login List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
			<h3>User Login Schedule</h3>
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="login_schedule_search" action="user_login_schedule_list.php">			  			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select User - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_login_date_start" value="<?php echo $login_date_start_value; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_login_date_end" value="<?php echo $login_date_end_value; ?>" />
			  </span>
			  <input type="submit" name="schedule_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>User Name</th>
					<th>Date And Time</th>					
					<th>IP Address</th>
				</tr>
				</thead>
				<tbody>
				<?php
				if($user_login_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($user_login_list_data); $count++)
					{						
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $user_login_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($user_login_list_data[$count]["user_login_schedule_login_date_time"])); ?></td>		
						<td style="word-wrap:break-word;"><?php echo $user_login_list_data[$count]["user_login_schedule_ip_address"]; ?></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="3">No login schedule for your search!</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
			<?php
			}
			else
			{
				echo 'You are not authorized to view user\'s login details';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
