<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: stock_vendor_list.php

CREATED ON	: 29-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of vendor for customer withdrawals

*/



/*

TBD: 

*/
$_SESSION['module'] = 'Stock Masters';


/* DEFINES - START */

define('VENDOR_ITEM_MAPPING_FUNC_ID','155');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',VENDOR_ITEM_MAPPING_FUNC_ID,'2','1');	

	$add_perms_list    = i_get_user_perms($user,'',VENDOR_ITEM_MAPPING_FUNC_ID,'1','1');



	// Query String Data

	if(isset($_REQUEST["indent_item_id"]))

	{

		$item_id = $_REQUEST["indent_item_id"];

	}

	else

	{

		$item_id = "";

	}

	

	if(isset($_POST['vendor_material_search_submit']))

	{		

		$search_vendor_name = $_POST["stxt_vendor"];

		if($search_vendor_name != '')

		{

			$search_vendor = $_POST["hd_vendor_id"];

		}

		else

		{

			$search_vendor = '';

		}

		

		$search_material_name = $_POST["stxt_material"];

		if($search_material_name != '')

		{

			$search_material = $_POST["hd_material_id"];		

		}

		else

		{

			$search_material = '';

		}

	}

	else if($item_id != '')

	{

		$search_material = $item_id;

	}

	else

	{

		$search_vendor        = '';

		$search_vendor_name   = '';

		$search_material      = '';

		$search_material_name = '';

	}



	// Temp data

	//Get Venor Type of Service List

	$stock_vendor_item_mapping_search_data = array("item_id"=>$search_material,"vendor_id"=>$search_vendor);

	$vendor_item_list = i_get_stock_vendor_item_mapping($stock_vendor_item_mapping_search_data);

	if($vendor_item_list["status"] == SUCCESS)

	{

		$vendor_item_list_data = $vendor_item_list["data"];

	}

	else

	{

		$alert = $vendor_item_list["data"];

		$alert_type = 0;

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Vendor Item Mapping List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

    <link href="css/style1.css" rel="stylesheet">





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Vendor List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="stock_master_add_vendor_item_mapping.php?material=<?php echo $item_id; ?>">Add Vendor Item Mapping</a></span><?php } ?>

            </div>

            <!-- /widget-header -->

			<div class="widget-header" style="height:50px; padding-top:10px;">               

			  <form method="post" id="vendor_item_search_form" action="stock_master_vendor_item_mapping_list.php">

			  <input type="hidden" name="hd_material_id" id="hd_material_id" value="<?php echo $search_material; ?>" />

			  <input type="hidden" name="hd_vendor_id" id="hd_vendor_id" value="<?php echo $search_vendor; ?>" />			  

			  <div style="padding-right:20px; width:200px; float:left;	">										

					<input type="text" name="stxt_material" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" placeholder="Search by material name or code" value="<?php echo $search_material_name; ?>" />

					<div id="search_results_material" class="dropdown-content"></div>

			  </div>			

			  <div style="padding-right:20px; width:200px; float:left;">										

					<input type="text" name="stxt_vendor" autocomplete="off" id="stxt_vendor" onkeyup="return get_vendor_list();" placeholder="Search by vendor name" value="<?php echo $search_vendor_name; ?>" />

					<div id="search_results_vendor" class="dropdown-content"></div>

			  </div>

			  <input type="submit" name="vendor_material_search_submit" />

			  </form>			  

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			

              <table class="table table-bordered">

                <thead>

                  <tr>

				    <th>SL No</th>

				    <th>Vendor Name</th>

				    <th>Item</th>
					
				    <th>Item Code</th>

				    <th>Contact Person</th>

					<th>Contact Number</th>

					<th>Email Id</th>

					<th>Address</th>							

				</tr>

				</thead>

				<tbody>							

				<?php

				if($vendor_item_list["status"] == SUCCESS)

								{	

                                    $sl_no = 0;							

									for($count = 0; $count < count($vendor_item_list_data); $count++)

									{

										$sl_no++;

									?>

									<tr>

									<td><?php echo $sl_no; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_name"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_material_name"]; ?></td>
									
									<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_material_code"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_contact_person"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_contact_number"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_email_id"]; ?></td>

                                    <td style="word-wrap:break-word;"><?php echo $vendor_item_list_data[$count]["stock_vendor_address"]; ?></td>

									</tr>

									<?php									

									}

								}

								else

								{

								?>

								<td colspan="5">No Vendor data added yet!</td>

				                <?php

				                }

				               ?>	



                  </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function get_material_list()

{ 

	var searchstring = document.getElementById('stxt_material').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{		

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results_material').style.display = 'block';

					document.getElementById('search_results_material').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results_material').style.display = 'none';

	}

}



function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;

	

	document.getElementById('search_results_material').style.display = 'none';

}



function get_vendor_list()

{ 

	var searchstring = document.getElementById('stxt_vendor').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{					

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results_vendor').style.display = 'block';

					document.getElementById('search_results_vendor').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/stock_get_vendor.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results_vendor').style.display = 'none';

	}

}



function select_vendor(vendor_id,search_vendor)

{

	document.getElementById('hd_vendor_id').value = vendor_id;

	document.getElementById('stxt_vendor').value = search_vendor;

	

	document.getElementById('search_results_vendor').style.display = 'none';

}

</script>



</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

</body>



</html>