<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

/*
PURPOSE : To get formatted date
INPUT 	: Date in whatever format, most generally yyyy-mm-dd; Format expected
OUTPUT 	: Formatted date in dd-mmm-yyyy
BY 		: Nitin Kashyap
*/
function get_formatted_date($ip_date,$format)
{
	switch($format)
	{
		case "Y-m-d":
		if(($ip_date == "0000-00-00") || ($ip_date == ""))
		{
			$return = "0000-00-00";
		}
		else
		{
			$return = date('Y-m-d', strtotime($ip_date));
		}
		break;
		
		case "d-M-Y":
		if(($ip_date == "0000-00-00") || ($ip_date == "0000-00-00 00:00:00") || ($ip_date == "") || ($ip_date == "1970-01-01"))
		{
			$return = "";
		}
		else
		{
			$return = date('d-M-Y', strtotime($ip_date));
		}
		break;
		
		default:
		if(($ip_date == "0000-00-00") || ($ip_date == ""))
		{
			$return = "0000-00-00";
		}
		else
		{
			$return = date('Y-m-d', strtotime($ip_date));
		}
		break;
	}	
	
	return $return;
}

/*
PURPOSE : To get date difference
INPUT 	: Date 1 in yyyy-mm-dd format, Date 2 in yyyy-mm-dd format
OUTPUT 	: Difference in Number of days, -1 if there is an error
BY 		: Nitin Kashyap
*/
function get_date_diff($start_date,$end_date)
{
    if(($start_date == "") || ($end_date == "") || ($start_date == "0000-00-00") || ($end_date == "0000-00-00"))
	{
		$return["data"] = "";
		$return["status"] = 2; // No fixed date for one of the 2 dates
	}
	else
	{
		$date1 = new DateTime($start_date);
		$date2 = new DateTime($end_date);
		$interval = $date1->diff($date2);
		
		if($date1 < $date2)
		{
			$return["data"] = $interval->days;
			$return["status"] = 1; // Overshot
		}
		else
		{			
			if($date1 > $date2)
			{
				$return["data"] = '-'.$interval->days;
				$return["status"] = 0; // Ahead
			}
			else
			{
				$return["data"] = $interval->days;
				$return["status"] = 0; // On track
			}
		}
	}	
	
	return $return;
}

/*
PURPOSE : To generate unique ID based on time stamp
INPUT 	: None
OUTPUT 	: Unique ID
BY 		: Nitin Kashyap
*/
function generate_unique_id()
{
	$time_array = explode(" ",microtime());
	
	$time_microseconds = ($time_array[0] * 100000000);
	
	$time_unix_epoch   = $time_array[1];
	
	$unique_id = $time_unix_epoch.$time_microseconds;
	
	return $unique_id;
}

/*
PURPOSE : To generate a random string
INPUT 	: Length
OUTPUT 	: Random String
BY 		: Nitin Kashyap
*/
function generate_random_string($length) 
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	
    $random_string = '';
	
    for ($count = 0; $count < $length; $count++) 
	{
        $random_string .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $random_string;
}

/*
PURPOSE : To get acres from guntas
INPUT 	: Area in guntas
OUTPUT 	: array with acre and gunta details
BY 		: Nitin Kashyap
*/
function get_acre_from_guntas($area_in_guntas)
{
	$acres  = $area_in_guntas/40;
	$guntas = fmod($area_in_guntas,40.00);
	
	$area_array["acres"]  = floor($acres);
	$area_array["guntas"] = $guntas;
	
	return $area_array;
}

/*
PURPOSE : To get amount in required format
INPUT 	: Amount, Format
OUTPUT 	: Formatted amount value
BY 		: Nitin Kashyap
*/
function get_formatted_amount($ip_amount,$format)
{
	switch($format)
	{
	case "INDIA":
	$money_array = explode('.',$ip_amount);	
	$strlen = strlen($money_array[0]);	
	
	if($strlen <= 3) // Below Thousand
	{
		$rupees = $money_array[0];
	}
	else if(($strlen > 3) && ($strlen <= 5)) // Below Lakh
	{
		$hundreds = substr($money_array[0],-3,3);
		$thousands = substr($money_array[0],0,$strlen - 3);
		
		$rupees = $thousands.','.$hundreds;
	}
	else if(($strlen > 5) && ($strlen <= 7)) // Below crore
	{
		$hundreds = substr($money_array[0],-3,3);
		$thousands = substr($money_array[0],-5,2);
		$lakhs = substr($money_array[0],0,$strlen - 5);
		
		$rupees = $lakhs.','.$thousands.','.$hundreds;
	}
	else if($strlen > 7)
	{
		$hundreds = substr($money_array[0],-3,3);
		$thousands = substr($money_array[0],-5,2);
		$lakhs = substr($money_array[0],-7,2);
		$crores = substr($money_array[0],0,$strlen - 7);
		
		$rupees = $crores.','.$lakhs.','.$thousands.','.$hundreds;
	}
	
	if(count($money_array) > 1)
	{
		$return = $rupees.'.'.$money_array[1];
	}
	else
	{
		$return = $rupees.'.'.'00';
	}
	break;
	
	default:
	$money_array = explode('.',$ip_amount);
	$strlen = strlen($money_array[0]);
	
	if($strlen <= 3) // Below Thousand
	{
		$rupees = $money_array[0];
	}
	else if(($strlen > 3) && ($strlen <= 5)) // Below Lakh
	{
		$hundreds = substr($money_array[0],-3,3);
		$thousands = substr($money_array[0],-5,2);
		
		$rupees = $thousands.','.$hundreds;
	}
	else if(($strlen > 5) && ($strlen <= 7)) // Below crore
	{
		$hundreds = substr($money_array[0],-3,3);
		$thousands = substr($money_array[0],-5,2);
		$lakhs = substr($money_array[0],-7,2);
		
		$rupees = $lakhs.','.$thousands.','.$hundreds;
	}
	else if($strlen > 7)
	{
		$hundreds = substr($money_array[0],-3,3);
		$thousands = substr($money_array[0],-5,2);
		$lakhs = substr($money_array[0],-7,2);
		$crores = substr($money_array[0],0,$strlen - 7);
		
		$rupees = $crores.','.$lakhs.','.$thousands.','.$hundreds;
	}
	
	$return = $rupees.'.'.$money_array[0];	
	break;
	}
	
	return $return;
}

function upload_file($file_id,$user_id) 
{
	if($_FILES[$file_id]["name"] != "")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			$return["status"] = FAILURE;
			$return["data"]   = $_FILES[$file_id]["error"];
		}
		else
		{
			$not_allowed =  array('exe','cmd','com','exm','sql');
			$filename = $_FILES[$file_id]['name'];
			$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
			if(in_array($ext,$not_allowed)) 
			{
				$return["status"] = FAILURE;
				$return["data"]   = "Files with file type $ext not allowed!";
			}
			else
			{
				$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
				move_uploaded_file($_FILES[$file_id]["tmp_name"], "docs".DIRECTORY_SEPARATOR.$_FILES[$file_id]["name"]);	

				$return["status"] = SUCCESS;
				$return["data"]   = $_FILES[$file_id]["name"];
			}	
		}
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "Please upload a valid file!";
	}
	
	return $return;
}

/*
PURPOSE : To get time difference
INPUT 	: Input time in hh:mm format, Output time in hh:mm format
OUTPUT 	: Difference time as array (array[0] - hours, array[1] - mins)
BY 		: Nitin Kashyap
*/
function get_time_difference($start_time,$end_time)
{
	$start_time_array = explode(':',$start_time);
	$end_time_array   = explode(':',$end_time);	
	
	$start_time_in_mins = ($start_time_array[0]*60) + $start_time_array[1];
	$end_time_in_mins   = ($end_time_array[0]*60) + $end_time_array[1];
	
	$time_difference_in_mins = abs($start_time_in_mins - $end_time_in_mins);
	
	$time_difference['hours'] = str_pad((floor($time_difference_in_mins/60)),2,"0",STR_PAD_LEFT);
	$time_difference['mins']  = str_pad(($time_difference_in_mins%60),2,"0",STR_PAD_LEFT);
	
	if($start_time_in_mins > $end_time_in_mins)
	{
		$return['status'] = FAILURE;
		$return['data']   = $time_difference;
	}
	else
	{
		$return['status'] = SUCCESS;
		$return['data']   = $time_difference;
	}
	
	return $return;
}

/*
PURPOSE : To get time in hh:mm format
INPUT 	: Input time in minutes
OUTPUT 	: Output time in hh:mm format
BY 		: Nitin Kashyap
*/
function get_time_in_hours_mins($ip_time)
{
	$hours = floor($ip_time/60);
	$mins  = $ip_time%60;
	
	$return = str_pad($hours,2,"0",STR_PAD_LEFT).':'.str_pad($mins,2,"0",STR_PAD_LEFT);
	
	return $return;
}

/*
PURPOSE : To get number in ordinal format
INPUT 	: Input number
OUTPUT 	: Output number in ordinal format. Ex: 1st, 2nd, 3rd etc.
BY 		: Sonakshi
*/
function get_ordinal_number($number) 
{
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($number % 100) >= 11) && (($number%100) <= 13))
        return $number. 'th';
    else
        return $number. $ends[$number % 10];
}

/*
PURPOSE : To get IP address of accessing user
INPUT 	: None
OUTPUT 	: IP Address
BY 		: Nitin Kashyap
*/
function GetIP()
{
    if (getenv("HTTP_CLIENT_IP")) 
	{
        $ip = getenv("HTTP_CLIENT_IP");
    } 
	elseif(getenv("HTTP_X_FORWARDED_FOR")) 
	{
        $ip = getenv("HTTP_X_FORWARDED_FOR");
        if(strstr($ip, ',')) 
		{
            $tmp = explode(',', $ip);
            $ip = trim($tmp[0]);
        }
    } 
	else 
	{
        $ip = getenv("REMOTE_ADDR");
    }
	
    return $ip;
}

function get_day($number)
{
	switch ($number) 
	{
    case 0:
        $return = "Sunday";
        break;
    case 1:
        $return = "Monday";
        break;
    case 2:
        $return = "Tuesday";
        break;
	case 3:
        $return = "Wednesday";
        break;
	case 4:
        $return = "Thursday";
        break;
	case 5:
        $return = "Friday";
        break;
	case 6:
        $return = "Saturday";
        break;
    }
	return $return;
	
}

/*
PURPOSE : To sort an array based on a column
INPUT 	: Array Reference, Column Name, Order
OUTPUT 	: IP Address
BY 		: Nitin Kashyap
*/
function array_sort_conditional(&$array,$sort_technique) 
{
    if(!is_array($array)) 
	{
		$return = FAILURE;
	}
	else
	{
		usort($array,$sort_technique); 
		reset($array);
	}
}

function sort_on_site_no($first_site,$second_site)
{
    return compare_text_nos($first_site["crm_site_no"], $second_site["crm_site_no"]);
}

function sort_on_fup_dates($first_date,$second_date)
{
	if(strtotime($first_date["fup_date"]) < strtotime($second_date["fup_date"]))
	{
		$return = -1;
	}
	else if(strtotime($first_date["fup_date"]) == strtotime($second_date["fup_date"]))
	{
		$return = 0;
	}
	else if(strtotime($first_date["fup_date"]) > strtotime($second_date["fup_date"]))
	{
		$return = 1;
	}
	
	return $return;
}

function compare_text_nos($text_num_1,$text_num_2)
{
	if((int)($text_num_1) < (int)($text_num_2))
	{
		$return = -1;
	}
	else if((int)($text_num_1) > (int)($text_num_2))
	{
		$return = 1;
	}
	else if((int)($text_num_1) == (int)($text_num_2))
	{
		if(strlen($text_num_1) > strlen($text_num_2))
		{
			$return = 1;
		}
		else if(strlen($text_num_1) < strlen($text_num_2))
		{
			$return = -1;
		}
		else
		{
			$first_last_char  = substr($text_num_1, -1);
			$second_last_char = substr($text_num_2, -1);
			
			if($first_last_char > $second_last_char)
			{
				$return = 1;
			}
			else if($first_last_char < $second_last_char)
			{
				$return = -1;
			}
			else
			{
				$return = 0;
			}
		}
	}
	
	return $return;
}function convert_num_to_words($number){	// Split into rupees and paise	$amount = explode('.',$number);		$number = $amount[0];		if(array_key_exists(1,$amount))	{		$paise  = $amount[1];				if(($paise == 00) || ($paise == "00"))		{			$paise_words = "zero";			$paise = 101;		}	}	else	{		$paise_words = "zero";		$paise = 101;	}		$removal_chars = array(' ',',','-',':',';','/');	$number = str_replace($removal_chars,"",$number);		$hyphen      = '-';    $conjunction = ' and ';    $separator   = ' ';    $negative    = 'negative ';    $decimal     = ' point ';    $dictionary  = array(        0                   => 'zero',        1                   => 'one',        2                   => 'two',        3                   => 'three',        4                   => 'four',        5                   => 'five',        6                   => 'six',        7                   => 'seven',        8                   => 'eight',        9                   => 'nine',        10                  => 'ten',        11                  => 'eleven',        12                  => 'twelve',        13                  => 'thirteen',        14                  => 'fourteen',        15                  => 'fifteen',        16                  => 'sixteen',        17                  => 'seventeen',        18                  => 'eighteen',        19                  => 'nineteen',        20                  => 'twenty',		21                  => 'twenty one',		22                  => 'twenty two',		23                  => 'twenty three',		24                  => 'twenty four',		25                  => 'twenty five',		26                  => 'twenty six',		27                  => 'twenty seven',		28                  => 'twenty eight',		29                  => 'twenty nine',        30                  => 'thirty',		31                  => 'thirty one',		32                  => 'thirty two',		33                  => 'thirty three',		34                  => 'thirty four',		35                  => 'thirty five',		36                  => 'thirty six',		37                  => 'thirty seven',		38                  => 'thirty eight',		39                  => 'thirty nine',        40                  => 'forty',		41                  => 'forty one',		42                  => 'forty two',		43                  => 'forty three',		44                  => 'forty four',		45                  => 'forty five',		46                  => 'forty six',		47                  => 'forty seven',		48                  => 'forty eight',		49                  => 'forty nine',        50                  => 'fifty',		51                  => 'fifty one',		52                  => 'fifty two',		53                  => 'fifty three',		54                  => 'fifty four',		55                  => 'fifty five',		56                  => 'fifty six',		57                  => 'fifty seven',		58                  => 'fifty eight',		59                  => 'fifty nine',        60                  => 'sixty',		61                  => 'sixty one',		62                  => 'sixty two',		63                  => 'sixty three',		64                  => 'sixty four',		65                  => 'sixty five',		66                  => 'sixty six',		67                  => 'sixty seven',		68                  => 'sixty eight',		69                  => 'sixty nine',        70                  => 'seventy',		71                  => 'seventy one',		72                  => 'seventy two',		73                  => 'seventy three',		74                  => 'seventy four',		75                  => 'seventy five',		76                  => 'seventy six',		77                  => 'seventy seven',		78                  => 'seventy eight',		79                  => 'seventy nine',        80                  => 'eighty',		81                  => 'eighty one',		82                  => 'eighty two',		83                  => 'eighty three',		84                  => 'eighty four',		85                  => 'eighty five',		86                  => 'eighty six',		87                  => 'eighty seven',		88                  => 'eighty eight',		89                  => 'eighty nine',        90                  => 'ninety',		91                  => 'ninety one',		92                  => 'ninety two',		93                  => 'ninety three',		94                  => 'ninety four',		95                  => 'ninety five',		96                  => 'ninety six',		97                  => 'ninety seven',		98                  => 'ninety eight',		99                  => 'ninety nine',        100                 => 'hundred',        1000                => 'thousand',        100000              => 'lakh',        10000000            => 'crore'    );        if (!is_numeric($number)) {        return "not number";    }        if($number < 21)	{		$string = $dictionary[$number];	}	else if($number < 100)	{		$tens = intval(($number/10));		$units = ($number%10);				$string = $dictionary[$tens*10];				if($units != 0)		{			$string = $string.$separator.$dictionary[$units];		}	}	else if($number < 1000)	{		$num_residue = $number%100;		$number = intval(($number/100));		$hundreds = $number;				$tens = intval(($num_residue/10));		$units = ($num_residue%10);				$string = $dictionary[$hundreds].$separator.$dictionary[100];		if($tens != 0)		{			$string = $string." ".$dictionary[$tens*10];		}			if($units != 0)		{			$string = $string.$separator.$dictionary[$units];		}		}	else if($number < 100000)	{		$num_residue = $number%1000;		$number = intval(($number/1000));		$thousands = $number;				$num_residue_one = $num_residue%100;		$number = intval(($num_residue/100));		$hundreds = $number;				$tens = intval(($num_residue_one/10));		$units = ($num_residue_one%10);				$string = $dictionary[$thousands].$separator.$dictionary[1000];		if($hundreds != 0)		{			$string = $string." ".$dictionary[$hundreds].$separator.$dictionary[100];		}		if($tens != 0)		{			$string = $string." ".$dictionary[$tens*10];		}		if($units != 0)		{			$string = $string.$separator.$dictionary[$units];		}		}	else if($number < 10000000)	{		$num_residue = $number%100000;		$number = intval(($number/100000));		$lakhs = $number;				$num_residue_one = $num_residue%1000;		$number = intval(($num_residue/1000));		$thousands = $number;				$num_residue_two = $num_residue_one%100;		$number = intval(($num_residue_one/100));		$hundreds = $number;				$tens = intval(($num_residue_two/10));		$units = ($num_residue_two%10);				$string = $dictionary[$lakhs].$separator.$dictionary[100000];		if($thousands != 0)		{			$string = $string." ".$dictionary[$thousands].$separator.$dictionary[1000];		}		if($hundreds != 0)		{			$string = $string." ".$dictionary[$hundreds].$separator.$dictionary[100];		}		if($tens != 0)		{			$string = $string." ".$dictionary[$tens*10];		}		if($units != 0)		{			$string = $string.$separator.$dictionary[$units];		}		}	else	{		$num_residue_cr = $number%10000000;		$number = intval(($number/10000000));		$crores = $number;			$num_residue = $num_residue_cr%100000;		$number = intval(($num_residue_cr/100000));		$lakhs = $number;				$num_residue_one = $num_residue%1000;		$number = intval(($num_residue/1000));		$thousands = $number;				$num_residue_two = $num_residue_one%100;		$number = intval(($num_residue_one/100));		$hundreds = $number;				$tens = intval(($num_residue_two/10));		$units = ($num_residue_two%10);				$string = $dictionary[$crores].$separator.$dictionary[10000000];		if($lakhs != 0)		{			$string = $string." ".$dictionary[$lakhs].$separator.$dictionary[100000];		}				if($thousands != 0)		{			$string = $string." ".$dictionary[$thousands].$separator.$dictionary[1000];		}		if($hundreds != 0)		{			$string = $string." ".$dictionary[$hundreds].$separator.$dictionary[100];		}		if($tens != 0)		{			$string = $string." ".$dictionary[$tens*10];		}		if($units != 0)		{			$string = $string.$separator.$dictionary[$units];		}	}		if($paise < 21)	{		$paise_words = $dictionary[$paise];	}	else if($paise < 100)	{		$tens = intval(($paise/10));		$units = ($paise%10);				$paise_words = $dictionary[$tens*10];				if($units != 0)		{			$paise_words = $paise_words.$separator.$dictionary[$units];		}	}	else	{		// Do nothing		$paise_words = '';	}	if($paise_words != '')	{		$string = $string.$separator.'RUPEES'.$separator.$paise_words.$separator.'PAISE';	}        return strtoupper($separator.$string);}function get_month_start_date($date,$count){	$cur_month = date('m',strtotime($date));		if(($cur_month - $count) > 12) // Next year	{		$month = ($cur_month - $count)%12;				$month = str_pad($month, 2, "0", STR_PAD_LEFT);		$year  = date('Y',strtotime($date)) + floor((($cur_month - $count)/12));				$start_date = $year.'-'.$month.'-01';	}	else if(($cur_month - $count) > 0) // This year only	{		$month = $cur_month - $count;		$month = str_pad($month, 2, "0", STR_PAD_LEFT);		$year  = date('Y',strtotime($date));				$start_date = $year.'-'.$month.'-01';	}	else // Previous year	{				$month = 12 - (abs($cur_month - $count));		$month = str_pad($month, 2, "0", STR_PAD_LEFT);		$year  = date('Y',strtotime($date)) - 1;				$start_date = $year.'-'.$month.'-01';	}				return $start_date;}function sort_on_number($first_no,$second_no){	if($first_no["pcount"] < $second_no["pcount"])	{		$return = 1;	}	else if($first_no["pcount"] == $second_no["pcount"])	{		$return = 0;	}	else if($first_no["pcount"] > $second_no["pcount"])	{		$return = -1;	}		return $return;}function get_financial_year_start($date){	$current_month = date('m',strtotime($date));	$current_year  = date('Y',strtotime($date));		if($current_month < 4)	{		$fy_start_date = ($current_year - 1).'-04-01';	}	else	{		$fy_start_date = ($current_year).'-04-01';	}		return $fy_start_date;}
?>