<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 7th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string
	if(isset($_GET["enquiry"]))
	{
		$enquiry_id = $_GET["enquiry"];
	}
	else	
	{
		$enquiry_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["add_assign_enquiry_submit"]))
	{
		$enquiry_id = $_POST["hd_enquiry"];
		$assignee   = $_POST["ddl_assigned_to"];
		
		// Check for mandatory fields
		if(($enquiry_id !="") && ($assignee !=""))
		{
			$enquiry_uresult = i_update_enquiry_assignee($enquiry_id,$assignee);
			
			if($enquiry_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				
				if($role == 1)
				{
					header("location:crm_enquiry_list.php");
				}
				else if($role == 5)
				{
					header("location:crm_enquiry_list_unq.php");
				}
				else					
				{
					header("location:crm_enquiry_fup_latest.php");
				}
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $enquiry_uresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Enquiry List
	$enquiry_list = i_get_enquiry_list($enquiry_id,'','','','','','','','','','','','','','','','','','','');
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Assign Enquiry</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Assign Enquiry</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="assign_enquiry" class="form-horizontal" method="post" action="crm_assign_enquiry.php">
								<input type="hidden" name="hd_enquiry" value="<?php echo $enquiry_id; ?>" />
									<fieldset>										
												
										<div class="control-group">											
											<label class="control-label" for="ddl_project">Enquiry Details</label>
											<div class="controls">
												Enquiry Number: <strong><?php echo $enquiry_list_data[0]["enquiry_number"]; ?></strong><br/>
												Customer Name: <strong><?php echo $enquiry_list_data[0]["name"]; ?></strong><br/>
												Customer Mobile: <strong><?php echo $enquiry_list_data[0]["cell"]; ?></strong><br/>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_assigned_to">Assigned To*</label>
											<div class="controls">
												<select name="ddl_assigned_to" required>
												<?php
												for($count = 0; $count < count($user_list_data); $count++)
												{
													
													?>
													<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
													if($user == $user_list_data[$count]["user_id"])
													{
													?>												
													selected="selected"
													<?php
													}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
													<?php													
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_assign_enquiry_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
