<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 07th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "") && ($_SESSION["loggedin_role"] == "1"))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["site"]))
	{
		$site_id = $_GET["site"];
	}
	else
	{
		$site_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_mgmt_blocking_submit"]))
	{		
		$site_id   = $_POST["hd_site_id"];
		$mgmt_user = $_POST["ddl_user"];
		$reason    = $_POST["ddl_blk_reason"];
		$remarks   = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($mgmt_user !="") && ($reason !="") && ($site_id !=""))
		{
			$site_block_mgmt_iresult = i_add_site_mgmt_blocking($site_id,$mgmt_user,$reason,$remarks,$user);
			
			if($site_block_mgmt_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				
				$site_status_update_result = i_update_site_status($site_id,MGMT_BLOCKED_STATUS,$user);
				
				if($site_status_update_result["status"] == SUCCESS)
				{
					$alert_type = 1;
					$alert      = $site_block_mgmt_iresult["data"];
				}
				else
				{
					$alert_type = 0;
					$alert      = "Site blocking was unsuccessful. Please inform the admin!";
				}
			}
			else
			{
				$alert_type = 0;
				$alert      = $site_block_mgmt_iresult["data"];
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// User List
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Mgmnt BLocking Reason List
	$blk_reason_list = i_get_mgmnt_blk_reason_list('');
	if($blk_reason_list["status"] == SUCCESS)
	{
		$blk_reason_list_data = $blk_reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$blk_reason_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get site details
	$allow_mgmt_block = false;
	$site_details = i_get_site_list('','','','','','','',$site_id);
	if($site_details["status"] == SUCCESS)
	{	
		if($site_details["data"][0]['crm_site_status'] == AVAILABLE_STATUS)
		{
			$allow_mgmt_block = true;			
		}
		else if($site_details["data"][0]['crm_site_status'] == BLOCKED_STATUS)
		{
			$allow_mgmt_block = false;
			// Provide link to blocked list
			$alert      = $alert."Alert: This site is blocked by someone. Click <a href=\"crm_blocked_site_list.php\">here</a> to go to blocked site list";
			$alert_type = 0; // Failure
		}
		else if($site_details["data"][0]['crm_site_status'] == MGMT_BLOCKED_STATUS)
		{	
			$allow_mgmt_block = false;
			// Provide link to management blocked list
			$alert      = $alert."Alert: This site is blocked by someone from the management. Click <a href=\"crm_mgmnt_blocked_site_list.php\">here</a> to go to management blocked site list";
			$alert_type = 0; // Failure
		}
		else
		{
			$allow_mgmt_block = false;
			// Provide link to booked list
			$alert      = $alert."Alert: This site is blocked by someone from the management. Click <a href=\"crm_all_booking_list.php\">here</a> to go to booked site list";
			$alert_type = 0; // Failure
		}
	}
	else
	{
		$alert      = $alert."Alert: No site with this ID";
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Management Blocking</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Management Blocking</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Management Block Details</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<?php
								if($allow_mgmt_block == true)
								{
								?>
								<form id="add_mgmt_blocking" class="form-horizontal" method="post" action="crm_add_mgmt_blocking.php">								
								<input type="hidden" name="hd_site_id" value="<?php echo $site_id; ?>" />
									<fieldset>										
																														
										<div class="control-group">											
											<label class="control-label" for="ddl_user">Management User*</label>
											<div class="controls">
												<select name="ddl_user">
												<option value="">- - Select User - -</option>
												<?php
												for($count = 0; $count < count($user_list_data); $count++)
												{
												?>
												<option value="<?php echo $user_list_data[$count]["user_id"]; ?>"><?php echo $user_list_data[$count]["user_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_blk_reason">Blocking Reason*</label>
											<div class="controls">
												<select name="ddl_blk_reason">
												<option value="">- - Select blocking reason - -</option>
												<?php
												for($count = 0; $count < count($blk_reason_list_data); $count++)
												{
												?>
												<option value="<?php echo $blk_reason_list_data[$count]["crm_management_block_reason_id"]; ?>"><?php echo $blk_reason_list_data[$count]["crm_management_block_reason_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" required></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_mgmt_blocking_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								<?php
								}
								?>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
