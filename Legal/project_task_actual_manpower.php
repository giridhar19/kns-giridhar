<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing
	
	if(isset($_GET['project_process_task_id']))
	{
		$task_id = $_GET['project_process_task_id'];
	}
	else
	{
		$task_id = $_POST["hd_task_id"];
	}
	
	//Get Man power List 
	$man_power_search_data = array("task_id"=>$task_id,"active"=>'1',"work_type"=>"Regular");
	$man_power_actual_data = i_get_man_power_list($man_power_search_data);
	if($man_power_actual_data["status"] != SUCCESS)
	{
		$manpower_status = "NotStarted";
		$manpower_percent = "";		
	}
	else
	{
		$manpower_percent = $man_power_actual_data["data"][0]["project_task_actual_manpower_completion_percentage"];
		$manpower_status ="";
	}
	if(isset($_POST["add_tasks_submit"]))
	{
		// Capture all form data
		$task_id                = $_POST["hd_task_id"];
		$date                   = $_POST['date'];
		$men                    = $_POST['num_men'];
		$women                  = $_POST['num_women'];
		$mason                  = $_POST['num_mason'];
		$others                 = $_POST['num_others'];
		$agency                 = $_POST['ddl_agency'];
		$men_rate               = $_POST['hd_men_rate'];
		$women_rate             = $_POST['hd_women_rate'];
		$mason_rate             = $_POST['hd_mason_rate'];
		$work_type              = $_POST['work_type'];
		$remarks 	            = $_POST['txt_remarks'];
	
		
		// Get Project Task Actual Contract modes already added
		$project_task_actual_boq_search_data = array("task_id"=>$task_id,"active"=>'1',"work_type"=>"Regular");
		$contract_latest_list = i_get_project_task_boq_actual($project_task_actual_boq_search_data);
		if($contract_latest_list['status'] != SUCCESS)
		{
			$contract_status = "NotStarted";
		}
		else
		{
			// Do Nothing
		}
		
		// Get Project Task Actual Machine modes already added
		$actual_machine_plan_search_data = array("task_id"=>$task_id,"active"=>'1',"work_type"=>"Regular");
		$machine_latest_list = i_get_machine_planning_list($actual_machine_plan_search_data);
		if($machine_latest_list['status'] != SUCCESS)
		{
			$machine_status = "NotStarted";
		}
		else
		{
			// Do Nothing
		}
		var_dump($manpower_status);
		var_dump($contract_status);
		var_dump($machine_status);
		if(($manpower_status == "NotStarted") && ($machine_status == "NotStarted") && ($contract_status == "NotStarted"))
		{
			
			$project_process_task_update_data = array("actual_start_date"=>$date);
			$task_update_start_date_list = i_update_project_process_task($task_id,$project_process_task_update_data);
		}
		else
		{
			//Do nothing
		}
		
		if($work_type == 1)
		{
			$task_iresult =  i_add_man_power($task_id,$date,$men,$women,$mason,$others,$agency,$men_rate,$women_rate,$mason_rate,'','',$remarks,$user,'Regular','');
			if($task_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert = $task_iresult["data"];
			}
			else
			{
				//Do nothing
			}
		}
		elseif($work_type == 2)
		{
			$manpower_iresult =  i_add_man_power($task_id,$date,$men,$women,$mason,$others,$agency,$men_rate,$women_rate,$mason_rate,'','',$remarks,$user,'Rework','');
			if($manpower_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert = $manpower_iresult["data"];
			}
			else
		
			{
				//Do nothing
			}
			$rework_iresult =  i_add_project_manpower_rework($task_id,$date,$men,$women,$mason,$agency,'','','','','',$remarks,$user);
		}
	}
	else
	{
		$alert_type = 0;
		$alert = "Actual Man Power for this date is already exist";
	}
    //Get Project Task Master modes already added
	$project_task_master_search_data = array("active"=>'1',"task_id"=>$task_id);
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list['status'] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list['data'];
		$task_name = $project_task_master_list_data[0]["project_task_master_name"];
	}
	else
	{
		$task_name = "";
	}
	
	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}	
	 else
	{
		$alert = $project_manpower_agency_list["data"];
		$alert_type = 0;
	}
	
	// Temp data
	$project_process_task_search_data = array("task_id"=>$task_id,"active"=>'1');
	$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
	if($project_plan_process_task_list["status"] == SUCCESS)
	{
		$project_plan_process_task_list_data = $project_plan_process_task_list["data"];
		$task_name = $project_plan_process_task_list_data[0]["project_task_master_name"];
		$project = $project_plan_process_task_list_data[0]["project_master_name"];
		$process = $project_plan_process_task_list_data[0]["project_process_master_name"];
	}
	else
	{
		$alert = $project_plan_process_task_list["data"];
		$alert_type = 0;
		$task_name = "";
		$project = "";
		$process = "";
	}

	// Temp data
	$man_power_search_data = array("task_id"=>$task_id,"active"=>'1');
	$man_power_list = i_get_man_power_list($man_power_search_data);
	if($man_power_list["status"] == SUCCESS)
	{
		$man_power_list_data = $man_power_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$man_power_list["data"];
	}
	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Task Actual Man Power List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
			<h3>&nbsp;&nbsp; Project : <?php echo $project ;?> &nbsp;&nbsp;Process :
						<?php echo  $process;?>&nbsp;&nbsp; Task:  &nbsp;  &nbsp;<?php echo $task_name; ?>&nbsp;&nbsp;&nbsp;</h3>
            </div>
            <!-- /widget-header -->
			<div class="tab-content">
							
			<div class="tab-pane active" id="formcontrols">
			<form id="add_tasks" class="form-horizontal" method="post" action="project_task_actual_manpower.php">
			
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th style="width:20%">Date</th>
					<th style="width:10%">Agency</th>
					<th style="width:10%">Men</th>
					<th style="width:10%">Women</th>
					<th style="width:10%">Mason</th>
					<th style="width:10%">Others</th>
                    <th style="width:15%">Remarks</th>							
                    <th style="width:15%">Work Type</th>							
    					
				</tr>
				</thead>
				<tbody>							
				<?php
					?>
					<input type="hidden" name="hd_task_master_count" id="hd_task_master_count" value="<?php echo count($project_plan_process_task_list_data); ?> /">
					<input type="hidden" name="hd_men_rate" id="hd_men_rate" value="">
					<input type="hidden" name="hd_women_rate" id="hd_women_rate" value="">
					<input type="hidden" name="hd_mason_rate" id="hd_mason_rate" value="">
					<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
					<?php
					?>
					<tr>
					<td><input type="date" name="date" style="width:80%" id="date" value="<?php echo date('Y-m-d'); ?>"></td>
					<td><select name="ddl_agency" id="ddl_agency" onchange="return manpower_rate()" required>
					<option>--Select vendor--</option>
					<?php
					for($count = 0; $count < count($project_manpower_agency_list_data); $count++)
					{
					?>
					<option value="<?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_id"]; ?>"><?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_name"]; ?></option>
					<?php
					}
					?></select></td>
					<td><input type="number" name="num_men" style="width:80%" id="num_men" onkeyup='return display_man_days();'>
					<br /><span id="num_men_days"></span>
					<span id="hd_male_rate"></span></td>
					<td><input type="number" name="num_women" style="width:80%" id="num_women" onkeyup='return display_woman_days();'>
					<br /><span id="num_women_days"></span>
					<span id="hd_female_rate"></</td>
					<td><input type="number" name="num_mason" style="width:80%" id="num_mason" onkeyup='return display_mason_days();'>
					<br /><span id="num_mason_days"></span>
					<span id="hd_mason1_rate"></span></td>
					<td><input type="number" name="num_others" style="width:80%" id="num_others"></td>
					<td><input type="text" name="txt_remarks" style="width:80%" id="txt_remarks"></td>
					
					<td style="font-size:20px;"> <input type="radio" style="color:red" name="work_type" id="work_type"  <?php if($manpower_percent >= 100) { ?> disabled <?php } ?> value="1">Regular Work<br>
					<input type="radio" name="work_type" id="work_type" <?php if($manpower_percent >= 100) { echo 'checked="checked"'; } ?> value="2"> Rework<br> </td>
					</tr>
				</tbody>
              </table>
			  <div class="form-actions">
						<button type="submit" class="btn btn-primary" name="add_tasks_submit" id="add_tasks_submit">Submit</button> 
						<button type="reset" class="btn">Cancel</button>
					</div> <!-- /form-actions -->
					
				</fieldset>
			</form>
			</div>
			<table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Date</th>
					<th>Task Name</th>
					<th>Agency</th>
					<th>Men</th>
					<th>Women</th>
					<th>Mason</th>
					<th>Others</th>
					<th>Work Type</th>
					<th>Completion Percent</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($man_power_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($man_power_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($man_power_list_data[$count][
					"project_task_actual_manpower_date"])); ?></td>
					<td><?php echo $man_power_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $man_power_list_data[$count]["project_manpower_agency_name"]; ?></td>
					<td><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"]; ?></td>
					<td><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"]; ?></td>
					<td><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"]; ?></td>
					<td><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_man_hours"]; ?></td>
					<td><?php echo $man_power_list_data[$count]["project_task_actual_manpower_work_type"]; ?></td>
					<td><?php echo $man_power_list_data[$count]["project_task_actual_manpower_completion_percentage"]; ?>%</td>
					<td><?php echo $man_power_list_data[$count]["project_task_actual_manpower_remarks"]; ?></td>
					<td><?php echo $man_power_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($man_power_list_data[$count][
					"project_task_actual_manpower_added_on"])); ?></td>
					<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_task_actual_manpower('<?php echo $man_power_list_data[$count]["project_task_actual_manpower_id"]; ?>','<?php echo $task_id; ?>');">Edit </a></div></td>
					<td><?php if(($man_power_list_data[$count]["project_task_actual_manpower_active"] == "1")){?><a href="#" onclick="return project_delete_task_actual_manpower(<?php echo $man_power_list_data[$count]["project_task_actual_manpower_id"]; ?>);">Delete</a><?php } ?></td>-->
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_actual_manpower(man_power_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_task_actual_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_task_actual_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&action=0");
		}
	}	
}
function go_to_project_edit_task_actual_manpower(man_power_id,task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_actual_manpower.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","man_power_id");
	hiddenField1.setAttribute("value",man_power_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","task_id");
	hiddenField2.setAttribute("value",task_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
function check_validity(total_payment_value,released_amount)
{
	var amount = parseInt(document.getElementById('amount').value);
	
	var cur_amount = parseInt(amount);
	var total_payment_value  = parseInt(total_payment_value);
	var released_amount      = parseInt(released_amount);
	if((cur_amount + released_amount) > total_payment_value)
	{
		document.getElementById('amount').value = 0;
		alert('Cannot release greater than total value');
	}
}

function display_man_days()
{
	var man_hrs  = document.getElementById('num_men').value;
	var men_rate = document.getElementById('hd_men_rate').value;
	
	if(man_hrs != '')
	{
		document.getElementById('num_men_days').innerHTML = (man_hrs/8) + ' - men';
		document.getElementById('hd_male_rate').innerHTML = 'Men Rate - ' + men_rate;
		if(men_rate == 0)
		{
			document.getElementById("add_tasks_submit").disabled = true;	
		}
		else
		{
			document.getElementById("add_tasks_submit").disabled = false;
		}
	}
	else
	{
		document.getElementById('num_men_days').innerHTML = '';
	}
}

function display_woman_days()
{
	
	var woman_hrs  = document.getElementById('num_women').value;
	
	if(woman_hrs != '')
	{
		var women_rate = document.getElementById('hd_women_rate').value;
		document.getElementById('num_women_days').innerHTML = (woman_hrs/8) + ' - women';
		document.getElementById('hd_women_rate').value = women_rate;
		document.getElementById('hd_female_rate').innerHTML  = 'Women Rate - ' + women_rate;
		if(women_rate == 0)
		{
			document.getElementById("add_tasks_submit").disabled = true;	
		}
		else
		{
			document.getElementById("add_tasks_submit").disabled = false;
		}
	}
	else
	{
		document.getElementById('num_women_days').innerHTML = '';
	}
}

function display_mason_days()
{
	var mason_hrs = document.getElementById('num_mason').value;
	
	if(mason_hrs > 0)
	{
		var mason_rate = document.getElementById('hd_mason_rate').value;
		document.getElementById('num_mason_days').innerHTML = (mason_hrs/8) + ' - mason';
		document.getElementById("hd_mason1_rate").innerHTML     = 'Mason Rate - ' + mason_rate;
		if(mason_rate == 0)
		{
			document.getElementById("add_tasks_submit").disabled = true;	
		}
		else
		{
			document.getElementById("add_tasks_submit").disabled = false;
		}
	}
	else
	{
		document.getElementById('num_mason_days').innerHTML = '';
	}
}

function manpower_rate()
{        

	var vendor_id = document.getElementById("ddl_agency").value;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange = function()
	{
		
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{			
			var object = JSON.parse(xmlhttp.responseText);
			document.getElementById('hd_men_rate').value = object.men_rate;
			document.getElementById('hd_women_rate').value  = object.women_rate;
			document.getElementById("hd_mason_rate").value     = object.mason_rate;
			men_rate    = object.men_rate;
			woman_rate  = object.women_rate;
			mason_rate  = object.mason_rate;
		}
	}

	xmlhttp.open("POST", "ajax/project_get_manpower_rate.php");   //
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("vendor_id=" + vendor_id);
}
</script>

  </body>

</html>