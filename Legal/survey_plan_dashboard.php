<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 16-Oct-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('DASHBOARD_SURVEY_FUNC_ID','348');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list = i_get_user_perms($user,'',DASHBOARD_SURVEY_FUNC_ID,'2','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Get all survey nos
	$survey_master_search_data = array("active"=>'1');
	$survey_master_list = i_get_survey_master($survey_master_search_data);
	if($survey_master_list['status'] == SUCCESS)
	{
		$total_survey_ext_plan = 0;
		for($count = 0; $count < count($survey_master_list['data']); $count++)
		{
			$survey_master_extent = $survey_master_list["data"][$count]["extent"];
			$total_survey_ext_plan = $total_survey_ext_plan + $survey_master_extent;
		}
	}
	 else
	{
		$total_survey_ext_plan = 0;
	}

	// Get all survey nos
	$survey_master_search_data = array("active"=>'1',"parent_survey_id"=>'0');
	$survey_master_list = i_get_survey_master($survey_master_search_data);
	if($survey_master_list['status'] == SUCCESS)
	{	    
		$total_survey_ext_parent_plan = 0;
		for($count = 0; $count < count($survey_master_list['data']); $count++)
		{
			$survey_master_extent = $survey_master_list["data"][$count]["extent"];
			$total_survey_ext_parent_plan = $total_survey_ext_parent_plan + $survey_master_extent;
		}
	}
	 else
	{
		$total_survey_ext_parent_plan = 0;
	}
	
	$total_survey_ext_dependent_plan= 0;
	$total_survey_ext_dependent_plan = $total_survey_ext_plan - $total_survey_ext_parent_plan;
	
	
	// Get Survey File
	$survey_file_search_data = array("active"=>'1',"status"=>'Completed');
	$survey_file_list = i_get_survey_file($survey_file_search_data);
	if($survey_file_list['status'] == SUCCESS)
	{
		$total_survey_ext_actual = 0;
		for($count = 0; $count < count($survey_file_list['data']); $count++)
		{
			$survey_master_ext = $survey_file_list["data"][$count]["survey_master_extent"];
			$total_survey_ext_actual = $total_survey_ext_actual + $survey_master_ext;
		}
	}
	 else
	{
		$total_survey_ext_actual = 0;
	}
	
	// Get Survey File
	$survey_file_search_data = array("active"=>'1',"status"=>'Completed',"parent_survey_id"=>'0');
	$survey_file_list = i_get_survey_file($survey_file_search_data);
	if($survey_file_list['status'] == SUCCESS)
	{
		$total_survey_ext_parent_actual = 0;
		for($count = 0; $count < count($survey_file_list['data']); $count++)
		{
			$survey_master_ext = $survey_file_list["data"][$count]["survey_master_extent"];
			$total_survey_ext_parent_actual = $total_survey_ext_parent_actual + $survey_master_ext;
		}
	}
	 else
	{
		$total_survey_ext_parent_actual = 0;
	}
	
	$total_survey_ext_dependent_actual= 0;
	$total_survey_ext_dependent_actual = $total_survey_ext_plan - $total_survey_ext_parent_actual;	
	
	 $total_survey_per            =  ($total_survey_ext_actual/$total_survey_ext_plan)*100;
	 $total_survey_parent_per     =  ($total_survey_ext_parent_actual/$total_survey_ext_parent_plan)*100;
	 $total_survey_dependent_per  =  ($total_survey_ext_dependent_actual/$total_survey_ext_dependent_plan)*100;
	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey - Dashboard</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Survey Dashboard List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th></th>
					<th>Planned</th>
					<th>Actual</th>	
					<th>%</th>	
					</tr>
                    
				</tr>
				</thead>
				<tbody>
				
					<tr>
					<td>Total Survey Ext</td>
					<td style="word-wrap:break-word;"><?php  echo $total_survey_ext_plan; ?></td>
					<td style="word-wrap:break-word;"><?php echo $total_survey_ext_actual; ?></td>
					<td style="word-wrap:break-word;"><?php echo $total_survey_per; ?></td>
					</tr>
					
					<tr>
					<td>Total Survey Parent</td>
					<td style="word-wrap:break-word;"><?php  echo $total_survey_ext_parent_plan; ?></td>
					<td style="word-wrap:break-word;"><?php  echo $total_survey_ext_parent_actual; ?></td>
					<td style="word-wrap:break-word;"><?php  echo $total_survey_parent_per; ?></td>
					</tr>
					<tr>
					<td>Total Survey Dependent</td>
						<td style="word-wrap:break-word;"><?php  echo $total_survey_ext_dependent_plan; ?></td>
						<td style="word-wrap:break-word;"><?php  echo $total_survey_ext_dependent_actual; ?></td>
						<td style="word-wrap:break-word;"><?php  echo $total_survey_dependent_per; ?></td>
					</tr>
					

                </tbody>
              </table>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
