<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_actual_payment_manpower_list.php
CREATED ON	: 07-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	// Nothing

	// Get Project  Payment ManPower modes already added
	$project_actual_payment_manpower_search_data = array("active"=>'1',"status"=>"Approved");
	$project_actual_payment_manpower_list = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
	if($project_actual_payment_manpower_list['status'] == SUCCESS)
	{
		$project_actual_payment_manpower_list_data = $project_actual_payment_manpower_list['data'];
	}	
	
	// Get Project  Payment ManPower modes already added
	$project_man_power_issue_payment_search_data = array();
	$project_actual_payment_issue_list = i_get_project_man_power_issue_payment($project_man_power_issue_payment_search_data);
	if($project_actual_payment_issue_list['status'] == SUCCESS)
	{
		$project_actual_payment_issue_list_data = $project_actual_payment_issue_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Payment ManPower List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Payment ManPower List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>From Date</th>
					<th>To Date</th>
					<th>Delay</th>
					<th>Bill No</th>
					<th>Vendor Name</th>
					<th>Amount</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th>Action</th>									
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_actual_payment_manpower_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_actual_payment_manpower_list_data); $count++)
					{
						$sl_no++;
						
						$start_date = date("Y-m-d");
						$end_date = $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_approved_on"];
						$delay = get_date_diff($end_date,$start_date);
						
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo date("d-M-Y",strtotime($project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_from_date"])); ?></td>
					<td><?php echo date("d-M-Y",strtotime($project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_to_date"])); ?></td>
					<td><?php echo $delay["data"] ; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_bill_no"]; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_manpower_agency_name"]; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_amount"]; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_remarks"]; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_actual_payment_manpower_list_data[$count][
					"project_actual_payment_manpower_added_on"])); ?></td>
					<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_payment_manpower(<?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]; ?>);">Edit</a></div></td>
					<td><?php if(($project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_active"] == "1")){?><a href="#" onclick="return project_delete_payment_manpower(<?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]; ?>);">Delete</a><?php } ?></td>-->
					<td><a href="#" onclick="return go_to_project_issue_payment_manpower('<?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]; ?>','<?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_vendor_id"]; ?>');">Issue Payment</a></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_payment_manpower(manpower_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_actual_payment_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_delete_actual_payment_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("manpower_id=" + manpower_id + "&action=0");
		}
	}	
}
function project_approve_payment_manpower(manpower_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_actual_payment_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_actual_payment_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("manpower_id=" + manpower_id + "&action=Approved");
		}
	}	
}
function go_to_project_edit_payment_manpower(manpower_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_actual_payment_manpower.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","manpower_id");
	hiddenField1.setAttribute("value",manpower_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_project_issue_payment_manpower(payment_manpower_id,vendor_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_add_man_power_issue_payment.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","payment_manpower_id");
	hiddenField1.setAttribute("value",payment_manpower_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","vendor_id");
	hiddenField2.setAttribute("value",vendor_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>