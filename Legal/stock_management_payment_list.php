<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: kns_grn_list.php
CREATED ON	: 29-Sep-2016
CREATED BY	: Lakshmi
PURPOSE     : List of grn for customer withdrawals
*/

/*
TBD: 
*/

/* DEFINES - START */
define('STOCK_MANAGEMENT_PAYMENT_LIST_FUNC_ID','210');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',STOCK_MANAGEMENT_PAYMENT_LIST_FUNC_ID,'2','1');
	$add_perms_list    = i_get_user_perms($user,'',STOCK_MANAGEMENT_PAYMENT_LIST_FUNC_ID,'1','1');

	// Query String Data
	if(isset($_REQUEST["order_id"]))
	{
		$po_id = $_REQUEST["order_id"];
	}
	else
	{
		$po_id = "-1";
	}
	if(isset($_REQUEST["grn_id"]))
	{
		$grn_id = $_REQUEST["grn_id"];
	}
	else
	{
		$grn_id = "-1";
	}
	
	if(isset($_POST['grn_search_submit']))
	{
		$location = $_POST['ddl_location'];
	}
	else
	{
		$location = '';
	}

	// Get Management Payment List
	$stock_management_payment_search_data = array();
	$management_payment_list =  i_get_stock_management_payment($stock_management_payment_search_data);
	if($management_payment_list['status'] == SUCCESS)
	{
		$management_payment_list_data = $management_payment_list['data'];		
	}	
	else
	{
		$alert = $management_payment_list["data"];
		$alert_type = 0;		
	}
	// Get Released Payment List
	$stock_payment_release_search_data = array('po_id'=>$po_id);
	$released_payment_list =  i_get_stock_payment_release_list($stock_payment_release_search_data);
	if($released_payment_list['status'] == SUCCESS)
	{
		$released_payment_list_data = $released_payment_list['data'];	
		$total_released_amount = 0;
		for($released_count = 0 ; $released_count < count($released_payment_list_data) ; $released_count++)
		{
			$released_amount = $released_payment_list_data[$released_count]["stock_release_payment_amount"];	
			$total_released_amount = $total_released_amount + $released_amount;
		}
	}	
	else
	{
		$total_released_amount =0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Management Payment List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Managment Payment List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Po No</th>
					<th>Grn No</th>
					<th>Value</th>
					<th>Additional Payment</th>
					<th>Total Amount</th>																							
					<th>Action</th>												
				</tr>
				</thead>
				<tbody>							
				<?php
				if($management_payment_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($management_payment_list_data); $count++)
					{
						$sl_no++;
						$total_grn_value    = $management_payment_list_data[$count]["stock_management_payment_total_value"];
						$additional_payment = $management_payment_list_data[$count]["stock_management_payment_additional_cost"];
						$grand_total = $total_grn_value + $additional_payment ;
						$stock_grn_search_data = array("purchase_order_id"=>$management_payment_list_data[$count]["stock_management_payment_po_id"]);
						$stock_grn_list = i_get_stock_grn_list($stock_grn_search_data);
						if($stock_grn_list["status"] == SUCCESS)
						{
							$stock_grn_no = $stock_grn_list["data"][0]["stock_grn_no"];
						}
						if($grand_total != $total_released_amount)
						{
						?>
						<tr>
						<td><?php echo $sl_no; ?></td>
						<td><?php echo $management_payment_list_data[$count]["stock_purchase_order_number"]; ?></td>
						<td><?php echo $stock_grn_no; ?></td>
						<td><?php echo $total_grn_value ; ?></td>
						<td><?php echo $additional_payment; ?></td>
						<td><?php echo $grand_total ;?></td>
						<td style="word-wrap:break-word;"><?php if($add_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px"
						href="stock_release_payments.php?po_id=<?php echo $management_payment_list_data[$count]["stock_purchase_order_id"]; ?>">Release Payment</a><?php } ?></td>
						
						</tr>
						<?php
						}						
					}
				}
				else
				{
				?>
				<td colspan="14">No GRN added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>	
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_grn(grn_id,order_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "stock_grn_list.php";
					}
				}
			}

			xmlhttp.open("POST", "stock_delete_grn.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("grn_id=" + grn_id + "&order_id=" + order_id + "&action=0");
		}
	}	
}

function go_to_grn_edit(grn_id,order_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "stock_edit_grn.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","grn_id");
	hiddenField1.setAttribute("value",grn_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","order_id");
	hiddenField2.setAttribute("value",order_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>


  </body>

</html>