<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert	

	// Query String
	if(isset($_GET["survey_id"]))
	{
		$survey_id = $_GET["survey_id"];		
	}	
	else
	{
		$survey_id = "";
	}

	// Get process details
	$survey_details_search_data = array("active"=>'1',"survey_id"=>$survey_id);
	$survey_details_list = i_get_survey_details($survey_details_search_data);
	if($survey_details_list['status'] == SUCCESS)
	{
		$survey_details_list_data = $survey_details_list["data"];
		// Get process details
		$survey_file_search_data = array("survey_id"=>$survey_details_list_data[0]["survey_details_id"]);
		$survey_file_list = i_get_survey_file($survey_file_search_data);
		if($survey_file_list['status'] == SUCCESS)
		{
		
		$village_name  = $survey_file_list["data"][0]["village_name"];
		$survey_no     = $survey_file_list["data"][0]["survey_master_survey_no"];
	    }		
		else
		{
			$village_name  = "<em>PLEASE ADD FILE</em>";
			$survey_no  = "<em>PLEASE ADD FILE</em>";
			
	    }
	}

	if(isset($_POST["add_process_submit"]))
	{
		// Capture all form data
		$project                = $_POST["hd_project"];
		$survey_id              = $_POST["hd_survey_id"];
		//$project_process_id   = $_POST["hd_process_id"];
		
		$process_type_array = $_POST["cb_process_type"];
		for($process_type_count = 0; $process_type_count < count($process_type_array); $process_type_count++)
		{
			$process_type = $process_type_array[$process_type_count];
			// Add task
			$process_iresult = i_add_survey_process($survey_id,$process_type,'','','',$user);
			if($process_iresult["status"] == SUCCESS)
			{
				header("location:survey_details_list.php?survey_id=$survey_id");
				$alert =  "Process Successfully Added";
				$alert_type = 1;
			}
		}
	}

	// Get list of process for this process type	
	$survey_process_master_search_data = array("active"=>'1',"process_type"=>'1');
	$survey_process_master_list = i_get_survey_process_master($survey_process_master_search_data);
	if($survey_process_master_list["status"] == SUCCESS)
	{
		$survey_process_master_list_data = $survey_process_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$survey_process_master_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Process</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Village:  &nbsp;  &nbsp;<?php echo $village_name; ?>&nbsp;&nbsp;&nbsp;&nbsp;Survey No:&nbsp;&nbsp;&nbsp;<?php echo $survey_no; ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Process to this Project</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
							
								<div class="tab-pane active" id="formcontrols">
								<form id="add_process" class="form-horizontal" method="post" action="survey_add_process.php">
								<input type="hidden" name="hd_survey_id" value="<?php echo $survey_id; ?>" />
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
								<input type="hidden" name="hd_project" value="<?php echo $project; ?>" />
									<fieldset>
										
										<div class="control-group">											
											<label class="control-label" for="process_type">Choose Process</label>
											<div class="controls">
												<?php 
												for($count = 0; $count < count($survey_process_master_list_data); $count++)
												{
											   // Get already added process for this project
												$survey_process_search_data = array("survey_id"=>$survey_id,"process_id"=>$survey_process_master_list_data[$count]["survey_process_master_id"]);
												$project_process = i_get_survey_process($survey_process_search_data);
												if($project_process['status'] != SUCCESS)
												
												{?>
												<input type="checkbox" name="cb_process_type[]" value="<?php echo $survey_process_master_list_data[$count]["survey_process_master_id"]; ?>" />&nbsp;&nbsp;&nbsp;<?php echo $survey_process_master_list_data[$count]["survey_process_master_name"]; ?>&nbsp;&nbsp;&nbsp;<br /><br />
												<?php
												 }
												}?>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->                                                                                                                       <br />										
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="add_process_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
                                								
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
