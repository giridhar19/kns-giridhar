<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 7th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */
$_SESSION['module'] = 'Legal Reports';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$complete_count         = 0;
	$bw_zero_and_thirty     = 0;
	$greater_than_thirty    = 0;
	$greater_than_sixty     = 0;
	$greater_than_ninety    = 0;
	$greater_than_onetwenty = 0;
	/* DATA INITIALIZATION - END */

	/* Get file count for active/inactive */
	$file_list = i_get_file_list('','','','','','','','','','');

	if($file_list["status"] == SUCCESS)
	{
		for($count = 0; $count < count($file_list["data"]); $count++)
		{
			$project_list = i_get_legal_process_plan_list('',$file_list["data"][$count]["file_id"],'','','','','','','','','');
			if($project_list["status"] == SUCCESS)
			{
				$incomplete    = false;
				$process_count = 0;
				while(($process_count < count($project_list["data"])) && ($incomplete == false))
				{										
					if($project_list["data"][$process_count]["process_plan_legal_completed"] == '0')
					{
						$incomplete = true;
					}
					$process_count++;
				}	
			}
			else
			{
				$incomplete = true;
			}			
			
			if($incomplete == true)
			{
				$file_start_date = $file_list["data"][$count]["file_start_date"];
				$today = date("Y-m-d");
				
				$difference = get_date_diff($file_start_date,$today);
				
				if($difference["data"] < 30)
				{
					$bw_zero_and_thirty++;
				}
				if(($difference["data"] > 30) && ($difference["data"] <= 60))
				{
					$greater_than_thirty++;
				}
				else if(($difference["data"] > 60) && ($difference["data"] <= 90))
				{
					$greater_than_sixty++;
				}
				else if(($difference["data"] > 90) && ($difference["data"] <= 120))
				{
					$greater_than_ninety++;
				}
				else if($difference["data"] > 120)
				{
					$greater_than_onetwenty++;
				}
				else
				{
					// Nothing here
				}
			}
			else
			{
				$complete_count++;
			}
		}
	}
	else
	{
		// Do nothing. There are no files
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>KNS Legal - Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
	
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span6">                        
                    </div>
                    <!-- /span6 -->
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>
                                    File Summary<span style="padding-left:20px;">TOTAL FILES: <?php echo ($complete_count + $bw_zero_and_thirty + $greater_than_thirty + $greater_than_sixty + $greater_than_ninety + $greater_than_onetwenty); ?></span></h3><br />
									<div style="color:#7FFF00;">Completed (<?php echo $complete_count; ?>)</div>
									<div style="color:#FF0000;">Between 0 and 30 days (<?php echo $bw_zero_and_thirty; ?>)</div>
									<div style="color:#00FFFF;">Greater than 30 days (<?php echo $greater_than_thirty; ?>)</div>
									<div style="color:#FFE4C4;">Greater than 60 days (<?php echo $greater_than_sixty; ?>)</div>
									<div style="color:#0000FF;">Greater than 90 days (<?php echo $greater_than_ninety; ?>)</div>
									<div style="color:#DC143C;">Greater than 120 days (<?php echo $greater_than_onetwenty; ?>)</div>
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                                <canvas id="donut-chart" class="chart-holder" width="538" height="250">
                                </canvas>
                                <!-- /bar-chart -->
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
    <div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/excanvas.min.js"></script>
    <script src="js/chart.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/base.js"></script>
    <script>
        var doughnutData = [
				{
				    value: <?php echo $complete_count; ?>,
				    color: "#7FFF00"
				},
				{
				    value: <?php echo $bw_zero_and_thirty; ?>,
				    color: "#FF0000"
				},
				{
				    value: <?php echo $greater_than_thirty; ?>,
				    color: "#00FFFF"
				},
				{
				    value: <?php echo $greater_than_sixty; ?>,
				    color: "#FFE4C4"
				},
				{
				    value: <?php echo $greater_than_ninety; ?>,
				    color: "#0000FF"
				},
				{
				    value: <?php echo $greater_than_onetwenty; ?>,
				    color: "#DC143C"
				}
			];

        var myDoughnut = new Chart(document.getElementById("donut-chart").getContext("2d")).Doughnut(doughnutData);
	</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
</body>
</html>
