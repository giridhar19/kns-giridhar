<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 17-April-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['contract_rate_id']))
	{
		$contract_rate_id = $_GET['contract_rate_id'];
	}
	else
	{
		$contract_rate_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["edit_project_master_contract_rate_submit"]))
	{
		$contract_rate_id     = $_POST["hd_contract_rate_id"];
		//$process              = $_POST["ddl_process"];
		//$work_task            = $_POST["work_task"];
		$aplicable_date       = $_POST["aplicable_date"];
		$uom                  = $_POST["ddl_unit"];
		$rate                 = $_POST["contract_rate"];
		$remarks 	          = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($aplicable_date != "")&& ($uom != ""))
		{
			$project_contract_rate_master_update_data = array("aplicable_date"=>$aplicable_date,"uom"=>$uom,"rate"=>$rate,
			"remarks"=>$remarks);
			$project_contract_rate_master_iresult = i_update_project_contract_rate_master($contract_rate_id,'','','',$project_contract_rate_master_update_data);
			
			if($project_contract_rate_master_iresult["status"] == SUCCESS)
				
			{	
			  $alert_type = 1;
			  header("location:project_master_contract_rate_list.php");
			}
			else
			{
			   $alert = "Project Already Exists";
			  $alert_type = 0;	
			}
			
			$alert = $project_contract_rate_master_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			
		}
	}
	
	// Get Stock master uom modes already added
	$stock_unit_measure_search_data = array("active"=>'1');
	$stock_unit_measure_list = i_get_stock_unit_measure_list($stock_unit_measure_search_data);
	if($stock_unit_measure_list['status'] == SUCCESS)
	{
		$stock_unit_measure_list_data = $stock_unit_measure_list['data'];
		
    }
     else
    {
		$alert = $stock_unit_measure_list["data"];
		$alert_type = 0;
	}
	

    // Get project Contract Process modes already added
	$project_contract_process_search_data = array("active"=>'1');
	$project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
	if($project_contract_process_list['status'] == SUCCESS)
	{
		$project_contract_process_list_data = $project_contract_process_list['data'];
	}
	else
	{
		$alert = $project_contract_process_list["data"];
		$alert_type = 0;
	}

    // Get Project Contract Rate modes already added
	$project_contract_rate_master_search_data = array("active"=>'1',"contract_rate_id"=>$contract_rate_id);
	$project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
	if($project_contract_rate_master_list['status'] == SUCCESS)
	{
		$project_contract_rate_master_list_data = $project_contract_rate_master_list['data'];
	}	
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Project - Contract rate</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Project Contract Rate</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Project Contract Rate</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_master_edit_contract_rate_form" class="form-horizontal" method="post" action="project_master_edit_contract_rate.php">
								<input type="hidden" name="hd_contract_rate_id" value="<?php echo $contract_rate_id; ?>" />
									<fieldset>										
										
										<div class="control-group">											
											<label class="control-label" for="ddl_process">Contract Process*</label>
											<div class="controls">
												<select name="ddl_process" disabled>
												<option value="">- - Select Process - -</option>
												<?php
												for($count = 0; $count < count($project_contract_process_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_contract_process_list_data[$count]["project_contract_process_id"]; ?>" <?php if($project_contract_process_list_data[$count]["project_contract_process_id"] == $project_contract_rate_master_list_data[0]["project_contract_rate_master_process"]){ ?> selected="selected" <?php } ?>><?php echo $project_contract_process_list_data[$count]["project_contract_process_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
											</div> <!-- /control-group -->
											
											<div class="control-group">											
											<label class="control-label" for="work_task">Work Task</label>
											<div class="controls">
												<input type="text" name="work_task" placeholder="Work Task" value="<?php echo $project_contract_rate_master_list_data[0]
												["project_contract_rate_master_work_task"] ;?>" disabled>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="aplicable_date">Aplicable Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="aplicable_date" placeholder="Aplicable Date" value="<?php echo $project_contract_rate_master_list_data[0]["project_contract_rate_master_aplicable_date"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
											
											<div class="control-group">											
											<label class="control-label" for="ddl_unit">UOM*</label>
											<div class="controls">
												<select name="ddl_unit" required>
												<option value="">- - Select UOM - -</option>
												<?php
												for($count = 0; $count < count($stock_unit_measure_list_data); $count++)
												{
												?>
												<option value="<?php echo $stock_unit_measure_list_data[$count]["stock_unit_id"]; ?>" <?php if($stock_unit_measure_list_data[$count]["stock_unit_id"] == $project_contract_rate_master_list_data[0]["project_contract_rate_master_uom"]){ ?> selected="selected" <?php } ?>><?php echo $stock_unit_measure_list_data[$count]["stock_unit_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
											</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="contract_rate">Contract Rate</label>
											<div class="controls">
												<input type="number" name="contract_rate" step="0.01" placeholder="Contract Rate" value="<?php echo $project_contract_rate_master_list_data[0]
												["project_contract_rate_master_rate"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" name="txt_remarks" placeholder="Remarks" value="<?php echo $project_contract_rate_master_list_data[0]["project_contract_rate_master_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_project_master_contract_rate_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
