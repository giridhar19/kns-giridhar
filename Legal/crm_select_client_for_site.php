<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_select_client_for_site.php
CREATED ON	: 23-Dec-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of prospective profiles
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String
	if(isset($_GET["site"]))
	{
		$site = $_GET["site"];
	}
	else
	{
		$site = "";
	}
	// Nothing here
	
	if(isset($_POST["prospect_search_submit"]))
	{
		$site              = $_POST["hd_site"];
		$added_by          = $_POST["ddl_search_user"];
		$filter_start_date = $_POST["dt_start_date"];				
		$filter_end_date   = $_POST["dt_end_date"];		
	}
	else
	{
		if(($role == 1) || ($role == 5))
		{
			$added_by = "";
		}
		else
		{
			$added_by = $user;
		}
		
		$filter_start_date = "";
		$filter_end_date   = "";
	}
	
	// Temp data
	$alert = "";
	
	$prospective_client_list = i_get_prospective_clients('','',$added_by,$filter_start_date,$filter_end_date);
	if($prospective_client_list["status"] == SUCCESS)
	{
		$prospective_client_list_data = $prospective_client_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$prospective_client_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Prospective Profile List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Prospective Profile List (Total Prospects = <span id="total_count_section">
			  <?php
			  if($prospective_client_list["status"] == SUCCESS)
			  {
				$preload_count = count($prospective_client_list_data);			
			  }
			  else
			  {
			    $preload_count = 0;
			  }
			  
			  echo $preload_count;
			  ?></span>)</h3>
            </div>
			
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="prospect_search" action="crm_select_client_for_site.php">
			  <input type="hidden" name="hd_site" value="<?php echo $site; ?>" />
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_user">
			  <option value="">- - Select User - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					if(($user_list_data[$count]["user_role"] == "1") || ($user_list_data[$count]["user_role"] == "5") || ($user_list_data[$count]["user_role"] == "6") || ($user_list_data[$count]["user_role"] == "7") || ($user_list_data[$count]["user_role"] == "8"))
					{
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($added_by == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
					}
				}
      		  ?>
			  </select>
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_start_date" value="<?php echo $filter_start_date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_end_date" value="<?php echo $filter_end_date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="prospect_search_submit" />
			  </span>			  
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Name</th>
					<th>Mobile</th>
					<th>Email</th>
					<th>Company</th>
					<th>Source</th>
					<th>Walk In</th>					
					<th>Enquiry Date</th>
					<th>Prospective Date</th>					
					<th>Profile Added By</th>	
					<th>Assignee</th>									
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($prospective_client_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$unq_count = 0;
					$booked_count = 0;
					$int_filter_out_count = 0;
					for($count = 0; $count < count($prospective_client_list_data); $count++)
					{
						// Check if there is an active booking for this customer
						$cp_is_booking_exists = i_get_site_booking('','','',$prospective_client_list_data[$count]["crm_prospective_profile_id"],'','','','','','','','','','');
						
						if($cp_is_booking_exists["status"] == SUCCESS)
						{
							$booked_count++;
						}
						else
						{																			
							// Is the enquiry unqualified
							$unqualified_data = i_get_unqualified_leads('',$prospective_client_list_data[$count]["enquiry_id"],'','','','','');
							if($unqualified_data["status"] != SUCCESS)
							{
								$sl_no++;															
							?>
							<tr>
							<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["enquiry_number"]; ?></td>					
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["project_name"]; ?><br /><br />
							<a href="crm_prospective_client_profile.php?profile=<?php echo $prospective_client_list_data[$count]["crm_prospective_profile_id"]; ?>">Client Profile</a></td>					
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["cell"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["email"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["company"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["enquiry_source_master_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php if($prospective_client_list_data[$count]["walk_in"] == "1")
							{
								echo "Yes";
							}
							else
							{
								echo "No";
							}?></td>							
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($prospective_client_list_data[$count]["added_on"])); ?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($prospective_client_list_data[$count]["crm_prospective_profile_added_on"])); ?></td>																					
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["added_by"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["stm"]; ?></td>											
							<td style="word-wrap:break-word;"><a href="crm_add_booking.php?client=<?php echo $prospective_client_list_data[$count]["crm_prospective_profile_id"]; ?>&site=<?php echo $site; ?>">Book</a></td>
							</tr>
							<?php
							}
							else
							{
								$unq_count++;
							}
						}	
					}
				}
				else
				{
				?>
				<tr><td colspan="15">No prospective clients yet!</td></tr>
				<?php
				}	
				if($prospective_client_list["status"] == SUCCESS)
				{
					$final_count = count($prospective_client_list_data) - $unq_count - $int_filter_out_count - $booked_count;			
				}
				else
				{
					$final_count = 0;
				}
				 ?>	
				<script>
				document.getElementById("total_count_section").innerHTML = <?php echo $final_count; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>