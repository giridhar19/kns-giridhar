<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$file_user_id        = $_POST["file_user_id"];
	$active              = $_POST["action"];
	
	$survey_file_process_user_update_data = array("active"=>$active);
	$survey_file_process_user_result = i_update_survey_file_process_user($file_user_id,$survey_file_process_user_update_data);
	
	if($survey_file_process_user_result["status"] == FAILURE)
	{
		echo $survey_file_process_user_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>