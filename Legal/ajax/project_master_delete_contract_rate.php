<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$contract_rate_id          = $_POST["contract_rate_id"];
	$active                    = $_POST["action"];
	
	$project_contract_rate_master_update_data = array("active"=>$active);
	$delete_project_rate_result = i_delete_project_contract_rate_master($contract_rate_id,$project_contract_rate_master_update_data);
	
	if($delete_project_rate_result["status"] == FAILURE)
	{
		echo $delete_project_rate_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>