<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$contract_task_id      = $_POST["contract_task_id"];
	$contract_vendor_id    = $_POST["contract_vendor_id"];
	$project_contract_rate_master_search_data = array("vendor_id"=>$contract_vendor_id,"contract_rate_id"=>$contract_task_id,"active"=>'1');
	$project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
	
	if($project_contract_rate_master_list['status'] == SUCCESS)
	{
		$project_contract_rate_master_list_data = $project_contract_rate_master_list['data'];
		$contract_rate = $project_contract_rate_master_list_data[0]["project_contract_rate_master_rate"];
		$uom = $project_contract_rate_master_list_data[0]["stock_unit_name"];
		$uom_id = $project_contract_rate_master_list_data[0]["project_contract_rate_master_uom"];
	}
	else
	{
		$contract_rate = "0";
		$uom = 'NOT SURE';
		$uom_id = NULL;
	}
	
	$result = array("contract_rate"=>$contract_rate,"uom"=>$uom,"uom_id"=>$uom_id);	
	//echo json_encode($result);
}
else
{
	header("location:login.php");
}
?>