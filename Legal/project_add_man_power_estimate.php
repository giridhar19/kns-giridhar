<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 07_Dec-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["task_id"]))
	{
		$task_id = $_GET["task_id"];
	}
	else if(isset($_POST["edit_man_power_estimate_submit"]))
	{
		$task_id  = $_POST["hd_task_id"];
	}
	else
	{
		$task_id = "-1";
	}
	// Get Project Man Power Estimate modes already added
	$project_process_task_search_data = array("task_id"=>$task_id);
	$project_man_power_estimate_list = i_get_project_process_task($project_process_task_search_data);
	if($project_man_power_estimate_list['status'] == SUCCESS)
	{
		$project_man_power_estimate_list_data = $project_man_power_estimate_list['data'];
		$project = $project_man_power_estimate_list_data[0]["project_master_name"];
		$process = $project_man_power_estimate_list_data[0]["project_process_master_name"];
		$task = $project_man_power_estimate_list_data[0]["project_task_master_name"];
	}	
	else
		
	{
		$project ="";
		$process = "";
		$task = "";
	}
	// Capture the form data
	if(isset($_POST["edit_man_power_estimate_submit"]))
	{
		$task_id                = $_POST["hd_task_id"];
		$no_of_men              = $_POST["no_of_men"];
		$no_of_women            = $_POST["no_of_women"];
		$no_of_mason            = $_POST["no_of_mason"];
		$no_of_others           = $_POST["no_of_others"];
		$remarks 	            = $_POST["txt_remarks"];
		// Check for mandatory fields
		if(($no_of_men != "") && ($no_of_women != "") && ($no_of_mason != "") && ($no_of_others != ""))
		{
			$project_man_power_estimate_iresult = i_add_project_man_power_estimate($task_id,'',$no_of_men,$no_of_women,$no_of_mason,$no_of_others,'',$remarks,$user);
			
			if($project_man_power_estimate_iresult["status"] == SUCCESS)
				
			{	
			$alert_type = 1;
		    
				//header("location:project_man_power_estimate_list.php");
			}
			else
			{
			   $alert = "Project Already Exists";
			  $alert_type = 0;	
			}
			
			$alert = $project_man_power_estimate_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	
	// Get Project Man Power Agency Master modes already added
	$project_manpower_agency_search_data = array();
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
		
    }
     else
    {
		$alert = $project_manpower_agency_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Man Power Estimate modes already added
	$project_man_power_estimate_search_data = array("active"=>'1');
	$project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
	if($project_man_power_estimate_list['status'] == SUCCESS)
	{
		$project_man_power_estimate_list_data = $project_man_power_estimate_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Project Man Power Estimate</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Project Man Power Estimate  &nbsp;&nbsp; Project : <?php echo $project ;?> &nbsp;&nbsp;Process :
						<?php echo  $process;?>&nbsp;&nbsp;Task : <?php echo $task;?></h3><span style="float:right; padding-right:20px;"><a href="project_man_power_estimate_list.php">Project Man Power Estimate List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Project Man Power Estimate</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_add_man_power_estimate_form" class="form-horizontal" method="post" action="project_add_man_power_estimate.php">
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
									<fieldset>										
																
										
										
										
										<div class="control-group">											
											<label class="control-label" for="no_of_men">Men Hrs</label>
											<div class="controls">
												<input type="number" class="span6" name="no_of_men" id="no_of_men" onkeyup="return men_numbers();" placeholder="No of Men">Men : <span id="men" style="width:10%" ></span>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="no_of_women">Women Hrs</label>
											<div class="controls">
												<input type="number" class="span6" name="no_of_women"  id="no_of_women" onkeyup="return women_numbers();" placeholder="No of Women">Women : <span id="women" style="width:10%" ></span>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="no_of_mason">Mason Hrs</label>
											<div class="controls">
												<input type="number" class="span6" name="no_of_mason"  id="no_of_mason" onkeyup="return mason_numbers();"  placeholder="No of Mason">Mason : <span id="mason" style="width:10%" ></span>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="no_of_others">No of Others</label>
											<div class="controls">
												<input type="number" class="span6" name="no_of_others" id="no_of_others" onkeyup="return others_numbers();"  placeholder="No of Others">others : <span id="others" style="width:10%" ></span>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
								
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_man_power_estimate_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_man_power_estimate(estimate_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_man_power_estimate_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_man_power_estimate.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("estimate_id=" + estimate_id + "&action=0");
		}
	}	
}
function go_to_project_edit_man_power_estimate(estimate_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_man_power_estimate.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","estimate_id");
	hiddenField1.setAttribute("value",estimate_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function men_numbers()
{
	
	var men_hrs = (document.getElementById('no_of_men').value);
	
	
	var men_hrs = parseInt(men_hrs);
	
	var men = men_hrs/8;
	
	
	 document.getElementById("men").innerHTML = men;
		
}

function women_numbers()
{
	
	var women_hrs = (document.getElementById('no_of_women').value);
	
	
	var women_hrs = parseInt(women_hrs);
	
	var men = women_hrs/8;
	
	
	 document.getElementById("women").innerHTML = men;
		
}
function mason_numbers()
{
	
	var mason_hrs = (document.getElementById('no_of_mason').value);
	
	
	var mason_hrs = parseInt(mason_hrs);
	
	var men = mason_hrs/8;
	
	
	 document.getElementById("mason").innerHTML = men;
		
}
function others_numbers()
{
	
	var others_hrs = (document.getElementById('no_of_others').value);
	
	
	var others_hrs = parseInt(others_hrs);
	
	var men = others_hrs/8;
	
	
	 document.getElementById("others").innerHTML = men;
		
}
</script>

  </body>

</html>
