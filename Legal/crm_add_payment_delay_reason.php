<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 18th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string data
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}
	
	if(isset($_GET["status"]))
	{
		$status = $_GET["status"];
	}
	else
	{
		$status = "";
	}
	
	// Capture the form data
	if(isset($_POST["add_payment_delay_reason_submit"]))
	{
		$booking_id = $_POST["hd_booking_id"];
		$status     = $_POST["hd_status"];		
		$reason_id  = $_POST["ddl_reason"];
		$remarks    = $_POST["txt_remarks"];		
		
		// Check for mandatory fields
		if(($booking_id !="") && ($status !="") && ($reason_id !="") && ($remarks !=""))
		{
			$delay_reason_iresult = i_add_payment_delay_reason($booking_id,$reason_id,$remarks,$user);
			
			if($delay_reason_iresult["status"] == SUCCESS)
			{
				switch($status)
				{
					case BOOKED_STATUS:
					header("location: crm_approved_booking_list.php");
					break;
					
					case AGREEMENT_STATUS:
					header("location: crm_agreement_list.php");
					break;
					
					case REGISTERED_STATUS:
					header("location: crm_registration_list.php");
					break;
					
					case KATHA_TRANSFER_STATUS:
					header("location: crm_katha_transfer_list.php");
					break;
					
					default:
					header("location: crm_customer_transactions.php?booking=".$booking_id);
					break;
				}				
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $delay_reason_iresult["data"];			
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Delay Reason List - Master
	$delay_reason_list = i_get_crm_delay_reason_list('','1');
	if($delay_reason_list["status"] == SUCCESS)
	{
		$delay_reason_list_data = $delay_reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$delay_reason_list["data"];
	}
	
	// Delay Reason for this booking
	$delay_reason_details = i_get_delay_reason_list($booking_id,'');
	if($delay_reason_details["status"] == SUCCESS)
	{
		$delay_reason_details_list = $delay_reason_details["data"];
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Delay Reason</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header" style="height:70px;">
	      				<i class="icon-user"></i>
	      				<h3>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Customer: <?php echo $cust_details_list[0]["crm_customer_name_one"]; ?> (<?php echo $cust_details_list[0]["crm_customer_contact_no_one"]; ?>)</h3>					
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    Add Delay Reason
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>								
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_delay_reason" class="form-horizontal" method="post" action="crm_add_payment_delay_reason.php">
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />
								<input type="hidden" name="hd_status" value="<?php echo $status; ?>" />
									<fieldset>			
										<div class="control-group">											
											<label class="control-label" for="ddl_reason">Delay Reason*</label>
											<div class="controls">
												<select class="span6" name="ddl_reason">
												<option value="">- - Select Delay Reason - -</option>
												<?php
												for($count = 0; $count < count($delay_reason_list_data); $count++)
												{
												?>												
												<option value="<?php echo $delay_reason_list_data[$count]["crm_delay_reason_master_id"]; ?>"><?php echo $delay_reason_list_data[$count]["crm_delay_reason_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks*</label>
											<div class="controls">
												<textarea name="txt_remarks" class="span6" required></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_payment_delay_reason_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>Reason</th>					
									<th>Remarks</th>	
									<th>Added By</th>
									<th>Updated On</th>												
								</tr>
								</thead>
								<tbody>							
								<?php
								if($delay_reason_details["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($delay_reason_details_list); $count++)
									{																	
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo $delay_reason_details_list[$count]["crm_delay_reason_master_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $delay_reason_details_list[$count]["crm_delay_reason_remarks"]; ?></td>		
									<td style="word-wrap:break-word;"><?php echo $delay_reason_details_list[$count]["user_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($delay_reason_details_list[$count]["crm_delay_reason_added_on"])); ?></td>									
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="3">No updates yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
