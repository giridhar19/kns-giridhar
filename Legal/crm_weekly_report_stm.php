<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 23rd Oct 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */
$_SESSION['module'] = 'CRM Reports';
/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	if(isset($_POST["report_submit"]))
	{
		$assigned_to          = $_POST["ddl_user"];
		$follow_up_start_date = $_POST["dt_start_date"];
		$follow_up_end_date   = $_POST["dt_end_date"];

		// Get the total number of enquiries for which there are scheduled follow ups between dates
		$enquiry_list = i_get_distinct_enquiry_list('',$assigned_to,$follow_up_start_date,$follow_up_end_date,'','','added_date_desc','','');

		// Initialization
		$total_enquiry_count          = 0;
		$walk_in_count                = 0;
		$walk_in_prospective_count    = 0;
		$walk_in_hot_count            = 0;
		$walk_in_warm_count           = 0;
		$walk_in_cold_count           = 0;
		$walk_in_unqualified_count    = 0;
		$entered_on_time_count        = 0;
		$assigned_count               = 0;
		$assigned_prospective_count   = 0;
		$assigned_hot_count           = 0;
		$assigned_warm_count          = 0;
		$assigned_cold_count          = 0;
		$assigned_unqualified_count   = 0;
		$self_count                   = 0;
		$self_prospective_count       = 0;
		$self_hot_count               = 0;
		$self_warm_count              = 0;
		$self_cold_count              = 0;
		$self_unqualified_count       = 0;
		$site_visit_planned_count     = 0;
		$site_visit_actual_count      = 0;

		if($enquiry_list["status"] == SUCCESS)
		{
			$total_enquiry_count = count($enquiry_list["data"]);
			for($enquiry_count = 0; $enquiry_count < $total_enquiry_count; $enquiry_count++)
			{		
				// Number of enquiries entered on time
				$start_date = $enquiry_list["data"][$enquiry_count]["enquiry_follow_up_date"];
				$end_date   = date("Y-m-d H:i:s",strtotime($start_date."+1 days"));
				$on_time_entry_list = i_get_enquiry_fup_list($enquiry_list["data"][$enquiry_count]["enquiry_id"],'',$assigned_to,'','','',$start_date,$end_date,"","","");
				if($on_time_entry_list["status"] == SUCCESS)
				{
					$entered_on_time_count++;
				}		
			}
		}
		else
		{
			// Fill the appropriate values with 0
		}

		// Total enquiries received during this duration
		$total_enquiry_list = i_get_enquiry_list('','','','','','','','','','','',$assigned_to,'','','',$follow_up_start_date." 00:00:00",$follow_up_end_date." 23:59:59",'','','');
		if($total_enquiry_list["status"] == SUCCESS)
		{
			for($count = 0; $count < count($total_enquiry_list["data"]); $count++)
			{
				$enquiry_fup_latest = i_get_enquiry_fup_latest($total_enquiry_list["data"][$count]["enquiry_id"],'','','','','','','','asc','','','');
				
				// Check for walk in
				if($total_enquiry_list["data"][$count]["walk_in"] == "1")
				{
					$walk_in_count++;
					
					switch($enquiry_fup_latest["data"][0]["enquiry_follow_up_int_status"])
					{
						case "1";
						$walk_in_hot_count++;
						break;
						
						case "2";
						$walk_in_warm_count++;
						break;
						
						case "3";
						$walk_in_cold_count++;
						break;
						
						case "4";
						$walk_in_unqualified_count++;
						break;
						
						case "5";
						$walk_in_prospective_count++;
						break;
					}
				}
				else
				{
					// Number of enquiries received by tele-callers
					if($total_enquiry_list["data"][$count]["added_by"] != $total_enquiry_list["data"][$count]["assigned_to"])
					{
						$assigned_count++;

						switch($enquiry_fup_latest["data"][0]["enquiry_follow_up_int_status"])
						{
							case "1";
							$assigned_hot_count++;
							break;
							
							case "2";
							$assigned_warm_count++;
							break;
							
							case "3";
							$assigned_cold_count++;
							break;
							
							case "4";
							$assigned_unqualified_count++;
							break;
							
							case "5";
							$assigned_prospective_count++;
							break;
						}
					}		
					// Self-Entry leads
					else
					{			
						$self_count++;
						switch($enquiry_fup_latest["data"][0]["enquiry_follow_up_int_status"])
						{
							case "1";
							$self_hot_count++;
							break;
							
							case "2";
							$self_warm_count++;
							break;
							
							case "3";
							$self_cold_count++;
							break;
							
							case "4";
							$self_unqualified_count++;
							break;
							
							case "5";
							$self_prospective_count++;
							break;
						}			
					}						
				}
			}
		}	
		else
		{
			// Initialize all to 0
		}

		// Number of site visits fixed between dates
		$site_visit_plans = i_get_site_visit_plan_filtered_list('',$follow_up_start_date,$follow_up_end_date,'',$assigned_to,'','','');
		if($site_visit_plans["status"] == SUCCESS)
		{
			for($sv_count = 0; $sv_count < count($site_visit_plans["data"]); $sv_count++)
			{
				// Actual number of site visits completed for visits planned by this user
				$sv_completed = i_get_site_travel_plan_list('',$site_visit_plans["data"][$sv_count]["crm_site_visit_plan_id"],'','','','','2','','');
				if($sv_completed["status"] == SUCCESS)
				{
					$site_visit_actual_count++;
				}								
			}
			$site_visit_planned_count = count($site_visit_plans["data"]);
		}
		else
		{
			// Do nothing. No site visit plan was fixed
		}
	}
	else
	{
		$assigned_to          = -1;
		$follow_up_start_date = "";
		$follow_up_end_date   = "";
		
		$total_enquiry_count          = 0;
		$walk_in_count                = 0;
		$walk_in_prospective_count    = 0;
		$walk_in_hot_count            = 0;
		$walk_in_warm_count           = 0;
		$walk_in_cold_count           = 0;
		$walk_in_unqualified_count    = 0;
		$entered_on_time_count        = 0;
		$assigned_count               = 0;
		$assigned_prospective_count   = 0;
		$assigned_hot_count           = 0;
		$assigned_warm_count          = 0;
		$assigned_cold_count          = 0;
		$assigned_unqualified_count   = 0;
		$self_count                   = 0;
		$self_prospective_count       = 0;
		$self_hot_count               = 0;
		$self_warm_count              = 0;
		$self_cold_count              = 0;
		$self_unqualified_count       = 0;
		$site_visit_planned_count     = 0;
		$site_visit_actual_count      = 0;
	}

	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Sales Report - STM</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>STM Report</h3>
            </div>
			
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="weekly_report" action="crm_weekly_report_stm.php">
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_start_date" value="<?php echo $follow_up_start_date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_end_date" value="<?php echo $follow_up_end_date; ?>" />
			  </span>		
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_user">
			  <option value="">- - Select STM - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($assigned_to == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="report_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>Parameter</th>					
					<th>Numbers</th>					
					<th>Actual %</th>					
					<th>Target %</th>
					<th>Target No</th>
					<th>Shortfall</th>
					<th>Shortfall %</th>					
				</tr>
				</thead>
				<tbody>							
				<tr>
				<td>How many leads to be followed in the CRM</td>
				<td><?php echo $total_enquiry_count; ?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Followed Up/Updated in the CRM on time</td>
				<td><?php echo $entered_on_time_count; ?></td>
				<td><?php if($total_enquiry_count > 0)
				{
					$actual = round((($entered_on_time_count/$total_enquiry_count)*100),2);
					echo $actual.'%';
				}
				else
				{
					$actual = 100;
					echo 'NA';
				}	
				?></td>
				<td><?php echo ON_TIME_UPDATE_TARGET; ?>%</td>
				<td>NA</td>
				<td>NA</td>
				<td><?php if($total_enquiry_count > 0)
				{
					echo (ON_TIME_UPDATE_TARGET - $actual).'%';
				}
				else
				{
					echo 'N.A';
				}?></td>
				</tr>
				<tr>
				<td colspan="7" style="color:blue;"><strong>LEADS RECEIVED IN THE DURATION</strong></td>
				</tr>
				<tr>
				<td>Leads Received from tele-caller</td>
				<td><?php echo $assigned_count; ?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Self Entry Leads</td>
				<td><?php echo $self_count; ?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Walk In Leads</td>
				<td><?php echo $walk_in_count; ?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>				
				<tr>
				<td><strong>Total Leads<strong></td>
				<td><strong><?php echo ($self_count + $walk_in_count + $assigned_count); ?></strong></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td colspan="7" style="color:blue;"><strong>SITE VISIT RELATED</strong></td>
				</tr>
				<tr>
				<td>Tentative Site Visits Fixed</td>
				<td><?php echo $site_visit_planned_count; ?></td>
				<td><?php /*if(($assigned_count + $self_count) > 0)
				{
					$svp_actual = round((($site_visit_planned_count/($assigned_count + $self_count))*100),2);
					echo $svp_actual.'%';
				}
				else
				{
					$svp_actual = 100;
					echo 'NA';
				} */?>NA
				</td>
				<td><?php // echo TENTATIVE_SITE_VISIT_TARGET.'%'; ?>NA</td>
				<td><?php /* $svp_target_count = ((TENTATIVE_SITE_VISIT_TARGET/100)*($assigned_count + $self_count));
				echo $svp_target_count; */ ?>NA</td>
				<td><?php // echo ($svp_target_count - $site_visit_planned_count); ?>NA</td>
				<td><?php /* if($svp_target_count > 0)
				{
					echo round(((($svp_target_count - $site_visit_planned_count)/$svp_target_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				} */?>NA</td>
				</tr>
				<tr>
				<td>Actual Site Visits (without walk in)</td>
				<td><?php echo $site_visit_actual_count; ?></td>
				<td><?php if(($assigned_count + $self_count) > 0)
				{
					$sva_actual = round((($site_visit_actual_count/($assigned_count + $self_count))*100),2);
					echo $sva_actual.'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td><?php echo ACTUAL_SITE_VISIT_TARGET; ?>%</td>
				<td><?php $sva_target_count = ((ACTUAL_SITE_VISIT_TARGET/100)*($assigned_count + $self_count));
				echo $sva_target_count; ?></td>
				<td><?php echo ($sva_target_count - $site_visit_actual_count); ?></td>
				<td><?php if($sva_target_count > 0)
				{
					echo round(((($sva_target_count - $site_visit_actual_count)/$sva_target_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				</tr>
				<tr>
				<td><strong>Total Site Visits (including walk in)</strong></td>
				<td><strong><?php echo ($site_visit_actual_count + $walk_in_count); ?></strong></td>
				<td><strong><?php /* if(($assigned_count + $self_count + $walk_in_count) > 0)
				{
					$tsv_actual = round(((($site_visit_actual_count + $walk_in_count)/($assigned_count + $self_count + $walk_in_count))*100),2);
					echo $tsv_actual.'%';
				}
				else
				{
					$tsv_actual = 100;
					echo 'NA';
				} */?>NA</strong></td>
				<td><strong><?php // echo ACTUAL_SITE_VISIT_TARGET.'%'; ?>NA</strong></td>
				<td><strong><?php /* $tsv_target_count = ((ACTUAL_SITE_VISIT_TARGET/100)*($assigned_count + $self_count + $walk_in_count));
				echo $tsv_target_count; */ ?>NA</strong></td>
				<td><strong><?php // echo ($tsv_target_count - $tsv_actual); ?>NA</strong></td>
				<td><strong><?php /* if($tsv_target_count > 0) 
				{
					echo round(((($tsv_target_count - $tsv_actual)/$tsv_target_count)*100),2).'%';
				}
				else
				{
					echo 'N.A';
				} */?>NA</strong></td>
				</tr>
				<tr>
				<td colspan="7" style="color:blue;"><strong>TELECALLER LEADS</strong></td>
				</tr>
				<tr>
				<td>Telecaller Leads - Prospective</td>
				<td><?php echo $assigned_prospective_count; ?></td>
				<td><?php if($assigned_count > 0)
				{
					echo round((($assigned_prospective_count/$assigned_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}
				?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Telecaller Leads - Hot</td>
				<td><?php echo $assigned_hot_count; ?></td>
				<td><?php if($assigned_count > 0)
				{
					echo round((($assigned_hot_count/$assigned_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}
				?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Telecaller Leads - Warm</td>
				<td><?php echo $assigned_warm_count; ?></td>
				<td><?php if($assigned_count > 0)
				{
					echo round((($assigned_warm_count/$assigned_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}
				?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Telecaller Leads - Cold</td>
				<td><?php echo $assigned_cold_count; ?></td>
				<td><?php if($assigned_count > 0)
				{
					echo round((($assigned_cold_count/$assigned_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}
				?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Telecaller Leads - Unqualified</td>
				<td><?php echo $assigned_unqualified_count; ?></td>
				<td><?php if($assigned_count > 0)
				{
					echo round((($assigned_unqualified_count/$assigned_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}
				?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td colspan="7" style="color:blue;"><strong>SELF ENTERED LEADS</strong></td>
				</tr>
				<tr>
				<td>Self Entered Leads - Prospective</td>
				<td><?php echo $self_prospective_count; ?></td>
				<td><?php if($self_count > 0)
				{
					echo round((($self_prospective_count/$self_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Self Entered Leads - Hot</td>
				<td><?php echo $self_hot_count; ?></td>
				<td><?php if($self_count > 0)
				{
					echo round((($self_hot_count/$self_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Self Entered Leads - Warm</td>
				<td><?php echo $self_warm_count; ?></td>
				<td><?php if($self_count > 0)
				{
					echo round((($self_warm_count/$self_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Self Entered Leads - Cold</td>
				<td><?php echo $self_cold_count; ?></td>
				<td><?php if($self_count > 0)
				{
					echo round((($self_cold_count/$self_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Self Entered Leads - Unqualified</td>
				<td><?php echo $self_unqualified_count; ?></td>
				<td><?php if($self_count > 0)
				{
					echo round((($self_unqualified_count/$self_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td colspan="7" style="color:blue;"><strong>WALK IN LEADS</strong></td>
				</tr>
				<tr>
				<td>Walk In Leads - Prospective</td>
				<td><?php echo $walk_in_prospective_count; ?></td>
				<td><?php if($walk_in_count > 0)
				{
					echo round((($walk_in_prospective_count/$walk_in_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Walk In Leads - Hot</td>
				<td><?php echo $walk_in_hot_count; ?></td>
				<td><?php if($walk_in_count > 0)
				{
					echo round((($walk_in_hot_count/$walk_in_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Walk In Leads - Warm</td>
				<td><?php echo $walk_in_warm_count; ?></td>
				<td><?php if($walk_in_count > 0)
				{
					echo round((($walk_in_warm_count/$walk_in_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Walk In Leads - Cold</td>
				<td><?php echo $walk_in_cold_count; ?></td>
				<td><?php if($walk_in_count > 0)
				{
					echo round((($walk_in_cold_count/$walk_in_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				</tr>
				<tr>
				<td>Walk In Leads - Unqualified</td>
				<td><?php echo $walk_in_unqualified_count; ?></td>
				<td><?php if($walk_in_count > 0)
				{
					echo round((($walk_in_unqualified_count/$walk_in_count)*100),2).'%';
				}
				else
				{
					echo 'NA';
				}?></td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td>
				<td>NA</td
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>


  </body>

</html>