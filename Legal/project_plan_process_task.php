<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 23rd Sep 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */
	
/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	
	if(isset($_REQUEST['project_plan_process_id']))
	{
		$process_id = $_REQUEST['project_plan_process_id'];
	}
	else
	{
		//Nothing
	}

	var_dump($process_id);
	/* DATA INITIALIZATION - END */
	// Capture the form data
	
	if(isset($_POST["add_project_plan_process_task_submit"]))
	{
		$process_id           = $_POST["hd_project_plan_process_id"];
		$task_type 	          = $_POST["ddl_task_type"];
		$actual_start_date 	  = $_POST["actual_start_date"];
		$actual_end_date 	  = $_POST["actual_end_date"];
		$remarks 	          = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($process_id != "") && ($task_type != "") && ($actual_start_date != "") && ($actual_end_date != ""))
		{
			if(((strtotime($actual_start_date)) <= (strtotime($actual_end_date))) && ((strtotime($actual_start_date)) >= strtotime(date('Y-m-d'))))
			{
				$project_plan_process_task_iresult = i_add_project_process_task($process_id,$task_type,$actual_start_date,$actual_end_date,$remarks,$user);
				
				if($project_plan_process_task_iresult["status"] == SUCCESS)
				
				{	
					$alert_type = 1;
				}
				
				$alert = $project_plan_process_task_iresult["data"];
			}
			else
			{
				$alert_type = 0;
				$alert      = 'Start Date and End Date cannot be earlier than today. End Date cannot be earlier than start date';
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Task Master modes already added
	$project_task_master_search_data = array("active"=>'1');
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list['status'] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list['data'];
	}

	else
	{
		$alert = $project_task_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Stock Project Plan Process Task modes already added
	$project_process_task_search_data = array("active"=>'1');
	$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
	if($project_plan_process_task_list['status'] == SUCCESS)
	{
		$project_plan_process_task_list_data = $project_plan_process_task_list['data'];
	}

	else
	{
		
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Plan Process Task</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Plan Process Task Details</h3>
						<div class="pull-right"></div>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>					
										</div>
									</div>
								
								
								
								 <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Plan Process Task List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<form action="project_plan_process_task.php" method="post" id="task_update_form">				
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Process ID</th>
					<th>Task Type</th>
					<th>Actual Start Date</th>
					<th>Actual End Date</th>
					<th>Remarks</th>
					<th>Added By</th>				
					<th>Added On</th>									
					<th colspan="2" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_plan_process_task_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_plan_process_task_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_plan_process_task_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_plan_process_task_list_data[$count]["project_process_task_type"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_plan_process_task_list_data[$count][
					"project_process_actual_start_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_plan_process_task_list_data[$count][
					"project_process_actual_end_date"])); ?></td>
					<td><?php echo $project_plan_process_task_list_data[$count]["project_process_task_remarks"]; ?></td>
					<td><?php echo $project_plan_process_task_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_plan_process_task_list_data[$count][
					"project_process_task_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_plan_process_task('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>');">Edit </a></div></td>
					<td><?php if(($project_plan_process_task_list_data[$count]["project_process_task_active"] == "1")){?><a href="#" onclick="return project_delete_plan_process_task(<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>);">Delete</a><?php } ?></td>
					
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
				<br/>		
				<div class="modal-body">
			    <div class="row">
                <center><a href="#" data-toggle="modal" data-target="#myModal"><strong>+ Add Task</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center>
				  </div>
				  </div>			  
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
               <br><br>
               <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document"  style="padding:10px; text-align:center">
                     <div class="modal-content">
                       
                           <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">Add Plan Process Task</h4>
                           </div>
                            <form method="post" action="project_plan_process_task.php">
							<input type="hidden" name="hd_project_plan_process_id" value="<?php echo $process_id; ?>" />	
									<fieldset>										
										
										
										<div class="control-group">											
											<label class="control-label" for="ddl_task_type">Task Type*</label>
											<div class="controls">
												<select name="ddl_task_type" required>
												<option>- - Select Task - -</option>
												<?php
												for($count = 0; $count < count($project_task_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_task_master_list_data[$count]["project_task_master_id"]; ?>"><?php echo $project_task_master_list_data[$count]["project_task_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="actual_start_date">Actual Start Date</label>
											<div class="controls">
												<input type="date" name="actual_start_date" placeholder="Start Date" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="actual_end_date">Actual End Date</label>
											<div class="controls">
												<input type="date" name="actual_end_date" placeholder="End Date" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
                                                                                                                                                               										 <br />
										
											
										<div class="modal-footer">   
											<input type="submit" class="btn btn-primary" name="add_project_plan_process_task_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div>
									</fieldset>
								</form>
                     </div>
                  </div>
               </div>
						  
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->  
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_plan_process_task(task_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_plan_process_task.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_plan_process_task.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("task_id=" + task_id + "&action=0");
		}
	}	
}

function go_to_project_edit_plan_process_task(task_id,process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_edit_plan_process_task.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","process_id");
	hiddenField2.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>


  </body>

</html>
