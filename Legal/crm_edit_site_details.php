<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* GET INPUT DATA - START */
	if(isset($_POST['site_id']))
	{
		$site_id = $_POST['site_id'];
	}
	else
	{
		header("location:crm_site_status_report.php");
	}
	/* GET INPUT DATA - END */

	// Capture the form data
	if(isset($_POST["edit_site_project_submit"]))
	{
		$site_id         = $_POST["site_id"];
		$site_number     = $_POST["stxt_site_no"];
		$block           = $_POST["stxt_block"];
		$wing            = $_POST["stxt_wing"];	
		$dimension       = $_POST["ddl_dimension"];
		$area            = $_POST["stxt_area"];		
		$site_type       = $_POST["ddl_site_type"];		
		$rel_status      = $_POST["ddl_release_status"];				
		
		// Check for mandatory fields
		if(($site_number !="") && ($dimension !="") && ($site_type !="") && ($rel_status !=""))
		{					
			$site_data_array = array("site_id"=>$site_id,"site_no"=>$site_number,"block"=>$block,"wing"=>$wing,"dimension"=>$dimension,"area"=>$area,"site_type"=>$site_type,"release_status"=>$rel_status);
			$site_project_uresult = i_update_site_details($site_data_array,$user);
			
			if($site_project_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = "Site Details Edited Successfully";				
			}
			else
			{
				$alert_type = 0;
				$alert      = $site_project_uresult["data"];
			}						
		}
		else
		{
			$alert      = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	else
	{
		$project_id = "";
	}
	
	// Get site list
	$site_list = i_get_site_list('','','','','','','',$site_id);
	if($site_list["status"] == SUCCESS)
	{
		$site_list_data = $site_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get site type list
	$site_type_list = i_get_site_type_list('','1');
	if($site_type_list["status"] == SUCCESS)
	{
		$site_type_list_data = $site_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_type_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get dimension list
	$dimension_list = i_get_dimension_list('','','','','1');
	if($dimension_list["status"] == SUCCESS)
	{
		$dimension_list_data = $dimension_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$dimension_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get release status list
	$rel_status_list = i_get_release_status('','1');
	if($rel_status_list["status"] == SUCCESS)
	{
		$rel_status_list_data = $rel_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$rel_status_list["data"];
		$alert_type = 0; // Failure
	}
		
	// Get bank list
	$bank_list = i_get_bank_list('','1');
	if($rel_status_list["status"] == SUCCESS)
	{
		$bank_list_data = $bank_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bank_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Site Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Site</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Site Details</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_site_status" class="form-horizontal" method="post" action="crm_edit_site_details.php">
								<input type="hidden" name="site_id" value="<?php echo $site_id; ?>" />							
									<fieldset>																												

										<div class="control-group">											
											<label class="control-label" for="stxt_site_no">Site No*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_site_no" value="<?php echo $site_list_data[0]['crm_site_no']; ?>" placeholder="Site Number of the site" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_block">Block*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_block" value="<?php echo $site_list_data[0]['crm_site_block']; ?>" placeholder="Block to which site belongs" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_wing">Wing*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_wing" value="<?php echo $site_list_data[0]['crm_site_wing']; ?>" placeholder="Wing to which site belongs" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
																				
										<div class="control-group">											
											<label class="control-label" for="ddl_dimension">Site Dimension*</label>
											<div class="controls">
												<select name="ddl_dimension" required>
												<option value="">- - Select Dimension - -</option>
												<?php
												for($count = 0; $count < count($dimension_list_data); $count++)
												{
												?>
												<option value="<?php echo $dimension_list_data[$count]["crm_dimension_id"]; ?>" <?php if($site_list_data[0]['crm_site_dimension'] == $dimension_list_data[$count]["crm_dimension_id"]) { ?> selected <?php } ?>><?php echo $dimension_list_data[$count]["crm_dimension_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_dimension">Site Area</label>
											<div class="controls">
												<input type="text" name="stxt_area" value="<?php echo $site_list_data[0]['crm_site_area']; ?>" onblur="dimensions(this.value);" placeholder="Total area of the site in sq.ft (for non-standard dimensions only)"> sq. ft&nbsp;&nbsp;&nbsp; (<span id="area_sq_m"></span> sq. m)
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																																														
										<div class="control-group">											
											<label class="control-label" for="ddl_site_type">Facing/Site Type*</label>
											<div class="controls">
												<select name="ddl_site_type" required>
												<?php
												for($count = 0; $count < count($site_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $site_type_list_data[$count]["crm_site_type_id"]; ?>" <?php if($site_list_data[0]['crm_site_type'] == $site_type_list_data[$count]["crm_site_type_id"]){ ?> selected <?php } ?>><?php echo $site_type_list_data[$count]["crm_site_type_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
										
										<div class="control-group">											
											<label class="control-label" for="ddl_release_status">Release Status*</label>
											<div class="controls">
												<select name="ddl_release_status" required>
												<?php
												for($count = 0; $count < count($rel_status_list_data); $count++)
												{
												?>
												<option value="<?php echo $rel_status_list_data[$count]["release_status_id"]; ?>" <?php if($site_list_data[0]['crm_site_release_status'] == $rel_status_list_data[$count]["release_status_id"]){ ?> selected <?php } ?>><?php echo $rel_status_list_data[$count]["release_status"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_site_project_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  </div>
	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    
<script>
function dimensions(area_sq_ft)
{	
	var area_sq_m  = area_sq_ft*(0.092903);	
	area_sq_m = area_sq_m.toFixed(2);
		
	document.getElementById("area_sq_m").innerHTML = area_sq_m;
}
</script>

<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
