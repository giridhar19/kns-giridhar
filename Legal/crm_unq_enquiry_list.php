<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_unq_enquiry_list.php
CREATED ON	: 17-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Unqualified Enquiries
*/
define('CRM_UNQ_ENQUIRY_LIST_FUNC_ID','112');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_UNQ_ENQUIRY_LIST_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_UNQ_ENQUIRY_LIST_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_UNQ_ENQUIRY_LIST_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_UNQ_ENQUIRY_LIST_FUNC_ID,'4','1');
	// Query String / Get form Data
	// Nothing here
	
	// Form data
	if(isset($_POST["unq_enquiry_search_submit"]))
	{
		$reason     = $_POST["ddl_search_reason"];
		if((isset($_POST["dt_start_date"])) && ($_POST["dt_start_date"] != ""))
		{
			$start_date_value = $_POST["dt_start_date"];
			$start_date       = $start_date_value." 00:00:00";
		}
		else
		{
			$start_date = "";
		}
		if((isset($_POST["dt_end_date"])) && ($_POST["dt_end_date"] != ""))
		{
			$end_date_value = $_POST["dt_end_date"];
			$end_date = $end_date_value." 23:59:59";
		}
		else
		{
			$end_date = "";
		}
		if((isset($_POST["dt_enq_start_date"])) && ($_POST["dt_enq_start_date"] != ""))
		{
			$enq_start_date_value = $_POST["dt_enq_start_date"];
			$enq_start_date       = $enq_start_date_value." 00:00:00";
		}
		else
		{
			$enq_start_date = "";
		}
		if((isset($_POST["dt_enq_end_date"])) && ($_POST["dt_enq_end_date"] != ""))
		{
			$enq_end_date_value = $_POST["dt_enq_end_date"];
			$enq_end_date       = $enq_end_date_value." 23:59:59";
		}
		else
		{
			$enq_end_date = "";
		}		
		$enq_added_by     = $_POST["ddl_search_enq_added_by"];
		$unq_requested_by = $_POST["ddl_search_unq_requested_by"];
		$enquiry_number   = $_POST["stxt_enquiry_num"];
		$cell_number      = $_POST["stxt_cust_cell_num"];
	}
	else
	{
		$reason               = "";
		$enq_added_by         = "";
		$unq_requested_by     = "";
		$start_date           = "";
		$end_date             = "";
		$start_date_value     = "";
		$end_date_value       = "";
		$enq_start_date       = "";
		$enq_end_date         = "";
		$enq_start_date_value = "";
		$enq_end_date_value   = "";
		$enquiry_number       = "";
		$cell_number          = "";
	}

	// Temp data
	$alert = "";
	
	$unq_enquiry_list = i_get_unqualified_leads('','',$reason,$enq_added_by,'',$start_date,$end_date,$enq_start_date,$enq_end_date,$enquiry_number,$cell_number,'',$user);
	if($unq_enquiry_list["status"] == SUCCESS)
	{
		$unq_enquiry_list_data = $unq_enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$unq_enquiry_list["data"];
	}
	
	// Reason List
	$reason_list = i_get_reason_list('','1');
	if($reason_list["status"] == SUCCESS)
	{
		$reason_list_data = $reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$reason_list["data"];
		$alert_type = 0; // Failure
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Unqualified Enquiry List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Enquiry List (Total Enquiries = <span id="total_count_section"><?php if($unq_enquiry_list["status"] == SUCCESS)
			  {
				echo count($unq_enquiry_list_data);
			  }
			  else
			  {
				echo "0";
			  }?></span>)</h3>
            </div>
			
			<div class="widget-header" style="height:120px; padding-top:10px;">               
			  <form method="post" id="unq_enquiry_search" action="crm_unq_enquiry_list.php">
			  <span style="padding-left:8px; padding-right:8px;">
			  Unqualified Date - From
			  <input type="date" name="dt_start_date" value="<?php echo $start_date_value; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Unqualified Date - To
			  <input type="date" name="dt_end_date" value="<?php echo $end_date_value; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_reason">
				<option value="">- - Select a reason - -</option>
				<?php
				for($count = 0; $count < count($reason_list_data); $count++)
				{
				?>
				<option value="<?php echo $reason_list_data[$count]["reason_id"]; ?>" <?php if($reason_list_data[$count]["reason_id"] == $reason) { ?> selected="selected" <?php } ?>><?php echo $reason_list_data[$count]["reason_name"]; ?></option>								
				<?php
				}
				?>														
				</select>
			  </span><br />
			  <span style="padding-left:8px; padding-right:8px;">
			  Enquiry Date - From
			  <input type="date" name="dt_enq_start_date" value="<?php echo $enq_start_date_value; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Enquiry Date - To
			  <input type="date" name="dt_enq_end_date" value="<?php echo $enq_end_date_value; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_enq_added_by">
				<option value="">- - Enquiry Added By - -</option>
				<?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
				
				?>
				<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($user_list_data[$count]["user_id"] == $enq_added_by) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
				<?php
				
				}
				?>														
				</select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_unq_requested_by">
				<option value="">- - Unqualification Request By - -</option>
				<?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
				if(($user_list_data[$count]["user_role"] == "1") || ($user_list_data[$count]["user_role"] == "5") || ($user_list_data[$count]["user_role"] == "6") || ($user_list_data[$count]["user_role"] == "7") || ($user_list_data[$count]["user_role"] == "8"))
				{
				?>
				<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($user_list_data[$count]["user_id"] == $unq_requested_by) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
				<?php
				}
				}
				?>														
				</select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_enquiry_num" value="<?php echo $enquiry_number; ?>" placeholder="Full Enquiry Number" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_cust_cell_num" value="<?php echo $cell_number; ?>" placeholder="Full Mobile Number of Customer" />
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="unq_enquiry_search_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Name</th>
					<th>Mobile</th>
					<th>Email</th>					
					<th>Source</th>
					<th>Walk In</th>					
					<th>Enquiry Date</th>
					<th>Follow Up Count</th>
					<th>Site Visit Count</th>
					<th>Unqual Requester</th>
					<th>Unqualified On</th>
					<th>Lead Time</th>
					<th>Reason</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($unq_enquiry_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$user_filter_out_count = 0;
					for($count = 0; $count < count($unq_enquiry_list_data); $count++)
					{												
						$follow_up_data = i_get_enquiry_fup_list($unq_enquiry_list_data[$count]["enquiry_id"],'','','','','');
						if($follow_up_data["status"] == SUCCESS)
						{
							$fup_count = count($follow_up_data["data"]);
						}
						else
						{
							$fup_count = 0;
						}
						
						// Site Visit Data
						$site_visit_data = i_get_site_travel_plan_list('',$unq_enquiry_list_data[$count]["enquiry_id"],'','','','','2','','');
						if($site_visit_data["status"] == SUCCESS)
						{
							$sv_count = count($site_visit_data["data"]);
						}
						else
						{
							$sv_count = 0;
						}
						
						if(($unq_requested_by == "") || ($unq_requested_by == $unq_enquiry_list_data[$count]["assigned_to"]))
						{
							$sl_no++;
							?>
							<tr>
							<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
							<td style="word-wrap:break-word;"><?php echo $unq_enquiry_list_data[$count]["enquiry_number"]; ?></td>					
							<td style="word-wrap:break-word;"><?php echo $unq_enquiry_list_data[$count]["project_name"]; ?></td>					
							<td style="word-wrap:break-word;"><?php echo $unq_enquiry_list_data[$count]["name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $unq_enquiry_list_data[$count]["cell"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $unq_enquiry_list_data[$count]["email"]; ?></td>							
							<td style="word-wrap:break-word;"><?php echo $unq_enquiry_list_data[$count]["enquiry_source_master_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php if($unq_enquiry_list_data[$count]["walk_in"] == "1")
							{
								echo "Yes";
							}
							else
							{
								echo "No";
							}?></td>					
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($unq_enquiry_list_data[$count]["added_on"])); ?></td>
							<td style="word-wrap:break-word;"><?php echo $fup_count; ?></td>					
							<td style="word-wrap:break-word;"><?php echo $sv_count; ?></td>
							<td style="word-wrap:break-word;"><?php $stm_name_data = i_get_user_list($unq_enquiry_list_data[$count]["assigned_to"],'','','');
							echo $stm_name_data["data"][0]["user_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($unq_enquiry_list_data[$count]["crm_unqualified_lead_added_on"])); ?></td>
							<td style="word-wrap:break-word;"><?php $date_diff = get_date_diff($unq_enquiry_list_data[$count]["added_on"],$unq_enquiry_list_data[$count]["crm_unqualified_lead_added_on"]);
							echo $date_diff["data"];?></td>							
							<td style="word-wrap:break-word;"><?php echo $unq_enquiry_list_data[$count]["reason_name"]; ?></td>
							<td><a href="#" onclick="return go_to_enquiry(<?php echo $unq_enquiry_list_data[$count]["enquiry_id"]; ?>,<?php echo $unq_enquiry_list_data[$count]["crm_unqualified_lead_id"]; ?>);">Restart</a></td>
							</tr>
							<?php
						}
						else
						{
							$user_filter_out_count++;
						}
					}
				}
				else
				{
				?>
				<td colspan="15">No enquiries!</td>
				<?php
				}
				
				$final_count = count($unq_enquiry_list_data) - $user_filter_out_count;
				 ?>	
				<script>
				document.getElementById("total_count_section").innerHTML = <?php echo $final_count; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function go_to_enquiry(enquiry_id,unqualified_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "crm_add_enquiry.php");
	
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type","hidden");
	hiddenField.setAttribute("name","hd_navigate_enquiry_id");
	hiddenField.setAttribute("value",enquiry_id);
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","hd_navigate_unqual_id");
	hiddenField1.setAttribute("value",unqualified_id);
    	
	form.appendChild(hiddenField);	
	form.appendChild(hiddenField1);	
	
	document.body.appendChild(form);
    form.submit();
}
</script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>
  </body>

</html>