<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: kns_grn_accont_list.php
CREATED ON	: 29-Sep-2016
CREATED BY	: Lakshmi
PURPOSE     : List of grn account for customer withdrawals
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_history_function.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data	
	// Nothing
	
	// Temp data
	// Temp data
	
	if(isset($_POST["issue_search_submit"]))
	{
		$search_user   		= $_POST["search_user"];				$search_issued_by   = $_POST["search_issued_by"];				$search_type  		= $_POST["search_type"];				$project      		= $_POST["ddl_project"];
		if(isset($_POST["cb_returnable_type"]))
		{
			$returnable_type   = 'Returnable';
		}
		else
		{
			$returnable_type   = '';
		}
	}
	else
	{
		$search_user 		 = '';		$search_issued_by 	 = '';
		$returnable_type = 'Returnable';
	}		$start_date = "";		$start_date_time = "";			if(isset($_REQUEST["dt_start_date"]))			{				$start_date = $_REQUEST["dt_start_date"];		if($start_date != "")		{			$start_date_time = $start_date.' '."00:00:00";		}		else		{			$start_date_time = "";		}			}		$end_date = "";			if(isset($_REQUEST["dt_end_date"]))			{				$end_date = $_REQUEST["dt_end_date"];		if($end_date !="")		{			$end_date_time = $end_date.' '."23:59:59";		}		else		{			$end_date_time = "";		}			}
	$stock_issue_item_search_data = array("indent_by"=>$user,"issued_by"=>$search_issued_by,"returnable_type"=>$search_type,"start_date"=>$start_date_time,"end_date"=>$end_date_time,"project"=>$project);	
	$issue_item_list = i_get_stock_issued_item_without_sum($stock_issue_item_search_data);
	if($issue_item_list["status"] == SUCCESS)
	{
		$issue_item_list_data = $issue_item_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$issue_item_list["data"];
	}		// Get Project List	$stock_project_search_data = array();	$project_result_list = i_get_project_list($stock_project_search_data);	if($project_result_list['status'] == SUCCESS)	{		$project_result_list_data = $project_result_list['data'];			}		else	{		$alert = $project_result_list["data"];		$alert_type = 0;			}
	
	 // Get Users modes already added
	$user_list = i_get_user_list('','','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}
	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Stock Employee Wise Issue Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
    
    
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Stock Employee Wise Issue Report</h3>Total value: <span id="total_value"><i>Calculating</i></span>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="issue_search_form" action="stock_employee_wise_issue_report.php">
			  
			 <!-- <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select Issued to- -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>-->			  			   <span style="padding-left:20px; padding-right:20px;">			  <select name="search_issued_by">			  <option value="">- - Select Issued By- -</option>			  <?php			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)			  {			  ?>			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_issued_by == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>			  <?php			  }			  ?>			  </select>			  </span>
			  
			<!--  <span style="padding-left:8px; padding-right:8px;">
			  Show Only Returnable &nbsp;&nbsp;&nbsp;<input type="checkbox" name="cb_returnable_type" <?php if($returnable_type == 'Returnable') { ?> checked <?php } ?> />
			  </span> -->			  			  <span style="padding-left:20px; padding-right:20px;">			  			  <select name="search_type">			  			  <option value="">- - Select Material Type - -</option>			  			  <option value ="Returnable">Returnable</option>			  			  <option value ="NonReturnable"> Non Returnable</option>			  			  </select>			  			  </span>			  			  <span style="padding-left:20px; padding-right:20px;">			  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />			  </span>			  			  			  <span style="padding-left:20px; padding-right:20px;">			  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />			  </span>			  			   <span style="padding-right:20px;">			  <select name="ddl_project">				<option value="">- - Select Project - -</option>				<?php				for($count = 0; $count < count($project_result_list_data); $count++)				{				?>				<option value="<?php echo $project_result_list_data[$count]["stock_project_id"]; ?>"<?php if($project_result_list_data[$count]["stock_project_id"] == $project){?> selected="selected" <?php } ?>><?php echo $project_result_list_data[$count]["stock_project_name"]; ?></option>				<?php				}				?>			  </select>			  </span>			  
			  <input type="submit" name="issue_search_submit" />
			  </form>			  
            </div>
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout:fixed;">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Item Name</th>	
					<th>Item Code</th>
					<th>Type Of Material</th>	
					<th>Indent Qty</th>											
					<th>Issue Qty</th>										
					<th>Rate</th>										
					<th>Value</th>
					<th>Indent No</th>										<th>Indent Project</th>
					<th>Indent Date</th>
					<th>Issue No</th>		
					<th>Issue Date</th>																						
					<th>Indent by</th>										
					<th>Issue by</th>										
				</tr>
				</thead>
				<tbody>							
				<?php
				if($issue_item_list["status"] == SUCCESS)
				{
					$sl_no = 0;										$total_value = 0;
					for($count = 0; $count < count($issue_item_list_data); $count++)
					{
						$sl_no ++;
						
						
					// Get Stock Po Item Data
					$stock_issue_search_data = array("issue_id"=>$issue_item_list_data[$count]["stock_issue_item_issue_id"]);
					$stock_issue_list = i_get_stock_issue($stock_issue_search_data);
					if($stock_issue_list['status'] == SUCCESS)
					{
						$stock_issue_list_data = $stock_issue_list['data'];
						$indent_id = $stock_issue_list_data[0]["stock_issue_indent_id"];
					}	
					else
					{
						$po_item_id  = "";
					}
						
					// Get Stock Po Item Data
					$stock_indent_items_search_data = array("indent_id"=>$indent_id);
					$stock_indent_items_list = i_get_indent_items_list($stock_indent_items_search_data);
					if($stock_indent_items_list['status'] == SUCCESS)
					{
						$stock_indent_items_list_data = $stock_indent_items_list['data'];
						$indent_no = $stock_indent_items_list_data[0]["stock_indent_no"];												$indent_project = $stock_indent_items_list_data[0]["stock_project_name"];						$indent_project1 = $stock_indent_items_list_data[0]["stock_indent_project"];
						$indent_date   = $stock_indent_items_list_data[0]["stock_indent_added_on"];
						$indent_qty  = $stock_indent_items_list_data[0]["stock_indent_item_quantity"];
					}	
					else
					{
						$po_item_id  = "";
						
					}
					$issue_qty = $issue_item_list_data[$count]["stock_issue_item_qty"];
					$rate = $issue_item_list_data[$count]["stock_material_price"];
					$value = ($issue_qty * $rate) ;										$total_value = $total_value + $value;					
					?>
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $issue_item_list_data[$count]["stock_material_name"] ; ?></td>
					<td style="word-wrap:break-word;"><?php echo  $issue_item_list_data[$count]["stock_material_code"] ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $issue_item_list_data[$count]["stock_material_type"] ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $indent_qty ;?></td>
					<td style="word-wrap:break-word;"><?php echo $issue_qty ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $issue_item_list_data[$count]["stock_material_price"] ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $value; ?></td>							
					<td style="word-wrap:break-word;"><?php echo $indent_no ;?></td>										<td style="word-wrap:break-word;"><?php echo $indent_project ;?></td>
					<td style="word-wrap:break-word;"><?php echo date('d-M-Y',strtotime($indent_date)) ;?></td>
					<td style="word-wrap:break-word;"><?php echo $issue_item_list_data[$count]["stock_issue_no"] ;?></td>
					<td style="word-wrap:break-word;"><?php echo date('d-M-Y',strtotime($issue_item_list_data[$count]["stock_issue_issued_on"])) ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $issue_item_list_data[$count]["issued_to"] ; ?></td>
					<td style="word-wrap:break-word;"><?php echo $issue_item_list_data[$count]["added_by"] ; ?></td>
					</tr>
					<?php									
					}
				}
				else
				{
				?>
				<td colspan="6">No items issued</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>			  <script>			   document.getElementById('total_value').innerHTML = <?php echo $total_value; ?>;			  </script>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_account(account_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "stock_grn_account_list.php";
					}
				}
			}

			xmlhttp.open("POST", "stock_delete_account.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("account_id=" + account_id + "&action=0");
		}
	}	
}
function go_to_po_item(po_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "stock_purchase_order_items.php");
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","po_id");
	hiddenField2.setAttribute("value",po_id);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function get_material_list()
{ 
	var searchstring = document.getElementById('po_number').value;
	
	if(searchstring.length >= 1)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{				
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{		
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results').style.display = 'block';
					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}
				else
				{
					document.getElementById('search_results').style.display = 'none';
				}
			}
		}

		xmlhttp.open("POST", "ajax/stock_get_po_no.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);				
	}
	else	
	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_po_no(po_id,$order_no)
{	
	document.getElementById('hd_po_id').value  = po_id;
	document.getElementById('po_number').value = $order_no;
	
	document.getElementById('search_results').style.display = 'none';
}

function get_vendor_list()
{ 
	var searchstring = document.getElementById('stxt_vendor').value;
	
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{				
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{					
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results_vendor').style.display = 'block';
					document.getElementById('search_results_vendor').innerHTML     = xmlhttp.responseText;
				}
			}
		}

		xmlhttp.open("POST", "ajax/stock_get_vendor.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);				
	}
	else	
	{
		document.getElementById('search_results_vendor').style.display = 'none';
	}
}

function select_vendor(vendor_id,search_vendor)
{
	document.getElementById('hd_vendor_id').value = vendor_id;
	document.getElementById('stxt_vendor').value = search_vendor;
	
	document.getElementById('search_results_vendor').style.display = 'none';
}

</script>

  </body>

</html>
 