<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 30-Dec-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['project_process_task_id']))
	{
		$task_id = $_GET['project_process_task_id'];
	}
	else
	{
		$task_id = '';
	}
	
	// Capture the form data
	if(isset($_POST["add_machine_planning_submit"]))
	{
		
		$task_id              = $_POST["hd_task_id"];
		$machine_id           = $_POST["ddl_machine_id"];
		$machine_rate         = $_POST["machine_rate"];
		$no_of_hours          = $_POST["no_of_hours"];
		$additional_cost      = $_POST["additional_cost"];
		$remarks 	          = $_POST["txt_remarks"];
				
		// Check for mandatory fields
		if(($task_id != "") && ($machine_id != "") && ($no_of_hours != "") && ($additional_cost != ""))
		{
			$project_machine_planning_iresult = i_add_project_machine_planning($task_id,$machine_id,$machine_rate,$no_of_hours,$additional_cost,$remarks,$user);
			
			if($project_machine_planning_iresult["status"] == SUCCESS)
			{	
				$alert_type = 1;
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $project_machine_planning_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	//Get Project Task Master modes already added
	$project_task_master_search_data = array("active"=>'1');
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list['status'] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list['data'];
	}
	else
	{
		$alert = $project_task_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Machine Master modes already added
	$project_machine_type_master_search_data = array("active"=>'1');
	$project_machine_master_list = i_get_project_machine_type_master($project_machine_type_master_search_data);
	if($project_machine_master_list['status'] == SUCCESS)
	{
		$project_machine_master_list_data = $project_machine_master_list["data"];
	}
	else
	{
		$alert = $project_machine_master_list["data"];
		$alert_type = 0;
	}
	
	// Temp data
	$project_process_task_search_data = array("task_id"=>$task_id);
	$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
	if($project_plan_process_task_list["status"] == SUCCESS)
	{
		$project_plan_process_task_list_data = $project_plan_process_task_list["data"];
		$task_name = $project_plan_process_task_list_data[0]["project_task_master_name"];
		$project_name = $project_plan_process_task_list_data[0]["project_master_name"];
		$process_name = $project_plan_process_task_list_data[0]["project_process_master_name"];
	}
	else
	{
		$alert = $project_plan_process_task_list["data"];
		$alert_type = 0;
		$task_name = "";
		$project_name = "";
		$process_name = "";
	}
	
	// Temp data
	$project_machine_planning_search_data = array("active"=>'1',"task_id"=>$task_id);
	$project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
	if($project_machine_planning_list["status"] == SUCCESS)
	{
		$project_machine_planning_list_data = $project_machine_planning_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_machine_planning_list["data"];
	}
}	
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Project Machine Planning</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Project Machine Planning&nbsp;&nbsp;&nbsp;Project: <?php echo $project_name; ?>&nbsp;&nbsp;&nbsp;Project: <?php echo $process_name; ?>&nbsp;&nbsp;&nbsp;Project: <?php echo $task_name; ?></h3><span style="float:right; padding-right:20px;"><a href="project_machine_planning_list.php">Project Machine Planning List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Project Machine Planning</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_add_machine_planning_form" class="form-horizontal" method="post" action="project_add_machine_planning.php">
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
								<input type="hidden" name="machine_rate" id="machine_rate" value="<?php echo $machine_rate; ?>" />
									<fieldset>										
																		
										<div class="control-group">											
											<label class="control-label" for="ddl_machine_id">Machine Type*</label>
											<div class="controls">
												<select name="ddl_machine_id" id="ddl_machine_id" required onchange="return get_rate()" value="1">
												<option value="">- - Select Machine Type - -</option>
												<?php
												for($count = 0; $count < count($project_machine_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_machine_master_list_data[$count]["project_machine_type_master_id"]; ?>"><?php echo $project_machine_master_list_data[$count]["project_machine_type_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="no_of_hours">No of Hours*</label>
											<div class="controls">
												<input type="number" class="span6" name="no_of_hours" placeholder="Hours" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="additional_cost">Additional Cost*</label>
											<div class="controls">
												<input type="number" class="span6" name="additional_cost" placeholder="Cost" required="required">
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_machine_planning_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							 <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Task</th>
					<th>Machine</th>
					<th>Machine Rate</th>
					<th>No of Hours</th>
					<th>Additional Cost</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>														
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_machine_planning_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_machine_planning_list_data); $count++)
					{
						$sl_no++;
						
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_type_master_name"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_planning_machine_rate"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_planning_no_of_hours"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_planning_additional_cost"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["project_machine_planning_remarks"]; ?></td>
					<td><?php echo $project_machine_planning_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_machine_planning_list_data[$count][
					"project_machine_planning_added_on"])); ?></td>					
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="9">No Project machine plan added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

function get_rate()
{        
	var machine_id = document.getElementById("ddl_machine_id").value;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{				
			var object = JSON.parse(xmlhttp.responseText);			
			document.getElementById('machine_rate').value = object.rate;	
		}
	}

	xmlhttp.open("POST", "ajax/project_get_rate.php");   // file name where delete code is written
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("machine_id=" + machine_id);
}

function go_to_project_edit_machine_planning(planning_id,task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_edit_machine_planning.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","planning_id");
	hiddenField1.setAttribute("value",planning_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","task_id");
	hiddenField2.setAttribute("value",task_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
function project_delete_machine_planning(planning_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_add_machine_planning.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_machine_planning.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("planning_id=" + planning_id + "&action=0");
		}
	}	
}
</script>


  </body>

</html>