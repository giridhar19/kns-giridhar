<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 29th Sep 2016

// LAST UPDATED BY: Lakshmi

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */
$_SESSION['module'] = 'Stock Transactions';


/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_history_function.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	$grn_items_list = array('status'=>FAILURE,'data'=>'');

	/* DATA INITIALIZATION - END */

	

	if(isset($_REQUEST['grn_item']))

	{

		$grn_item = $_REQUEST['grn_item'];

	}

	else

	{

		//Nothing

	}

	if(isset($_REQUEST['grn_item_id']))

	{

		$grn_item_id = $_REQUEST['grn_item_id'];

	}

	else

	{

		//Nothing

	}

	

	if(isset($_POST['hd_grn_id']))

	{

		$grn_id       = $_POST['hd_grn_id'];

		$grn_project = $_POST['hd_grn_project'];

		$grn_no       = $_POST['stxt_grn_no'];		



		$stock_grn_items_search_data = array("grn_id"=>$grn_id);

		$grn_items_list = i_get_stock_grn_items_list($stock_grn_items_search_data);		

	}

	else

	{

		$grn_id 	  = '-1';

		$grn_project = '-1';

	}

	

	// Capture the form data

	if(isset($_POST["add_grn_engineer_inspection_submit"]))

	{

		for($ccount = 0; $ccount < ($_POST["hd_po_item_count"]); $ccount++)

		{

			$grn_item_id          = $_POST['hd_grn_item_id_'.$ccount];

			$grn_material_id      = $_POST['hd_grn_material_id_'.$ccount];

			$grn_item	          = $_POST["hd_grn_item"];			

			$grn_project         = $_POST["hd_grn_project"];			

			$quantity             = $_POST['accepted_qty_'.$ccount];

			$rejected_quantity    = $_POST['rejected_qty_'.$ccount];

			$remarks_to_account	  = $_POST["txt_remarks_to_account"];

			$remarks 	          = $_POST["txt_remarks"];

			

			// Check for mandatory fields

			if(($grn_item_id != "") && ($quantity != "") && ($remarks_to_account != ""))

			{

				$grn_engineer_inspection_iresult = i_add_stock_grn_engineer_inspection($grn_item_id,$quantity,$rejected_quantity,'',$remarks,$remarks_to_account,$user);

				

				if($grn_engineer_inspection_iresult["status"] == SUCCESS)

				{					

					$grn_insp_id =  $grn_engineer_inspection_iresult["data"];
					$material_stock_search_data = array("material_id"=>$grn_material_id,'project'=>$grn_project);

					$material_list = i_get_material_stock($material_stock_search_data);					

					if($material_list["status"] == SUCCESS)

					{

						

						$stock_quantity = $material_list["data"][0]["material_stock_quantity"];

				

					}

					else

					{

						$stock_quantity = 0;

					}

					$total_material_qunatity = $stock_quantity + $quantity + $additional_quantity;

					$material_stock_update_data = array("quantity"=>$total_material_qunatity);					

					$material_update_data =	i_update_material_stock($grn_material_id,$grn_project,$material_stock_update_data);

					

					// Update history					

					$stock_history_data = i_add_stock_history($grn_material_id,'Purchase',$grn_project,$quantity,'',$user,$grn_insp_id);

					

					header("location:stock_grn_engineer_inspection_list.php");

				}

				

				$alert = $grn_engineer_inspection_iresult["data"];

			}

			else

			{

				$alert = "Please fill all the mandatory fields";

				$alert_type = 0;

			}

		}

	}	

	

	// Get Grn Engineer Inspection modes already added

	$stock_grn_engineer_inspection_search_data = array('grn_id'=>$grn_id);

	$grn_engineer_inspection_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);

	if($grn_engineer_inspection_list['status'] == SUCCESS)

	{

		$invoice_number = "-1";

		$invoice_date 	= "-1";

		$dc_number 		= "-1";

		$dc_date 		= "-1";

		$vehicle_number = "-1";

		$grn_project   = "-1";

		$grn_id         = "-1";

		$inspection_done = true;

	}

	else

	{

		$inspection_done = false;

		// Get Material Details

		$stock_material_search_data = array();

		$material_list = i_get_stock_material_master_list($stock_material_search_data);

		if($material_list["status"] == SUCCESS)

		{

			$material_list_data = $material_list["data"];

		}

		else

		{

			$alert = $material_list["data"];

			$alert_type = 0;

		}



		// GRN Details

		$stock_grn_search_data = array("grn_id"=>$grn_id);

		$grn_list_details = i_get_stock_grn_list($stock_grn_search_data);

		if($grn_list_details["status"] == SUCCESS)

		{

			$grn_list_details_data = $grn_list_details["data"];

			$invoice_number = $grn_list_details["data"][0]["stock_grn_invoice_number"];

			$invoice_date 	= $grn_list_details["data"][0]["stock_grn_invoice_date"];

			$dc_number 		= $grn_list_details["data"][0]["stock_grn_dc_number"];

			$dc_date 		= $grn_list_details["data"][0]["stock_grn_dc_date"];

			$vehicle_number = $grn_list_details["data"][0]["stock_grn_vehicle_number"];

			$grn_project   = $grn_list_details["data"][0]["stock_grn_project"];

		}

		else

		{

			$invoice_number = "";

			$invoice_date 	= "";

			$dc_number 		= "";

			$dc_date 		= "";

			$vehicle_number = "";

			$grn_project   = "";

		}

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Add GRN Engineer Inspection</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

    <link href="css/style1.css" rel="stylesheet">





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>

    

<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      			

	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>GRN Engineer Inspection</h3><span style="float:right; padding-right:20px;"><a href="stock_grn_engineer_inspection_list.php">GRN Engineer Inspection List</a></span>

					</div>					

					

					<div class="widget-content">

						

						

						

						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Add GRN Engineer Inspection</a>

						  </li>	

						</ul>

						<br>

							<div class="control-group">												

								<div class="controls">

								<?php 

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>  

								<?php

								}

								?>

                                

								<?php 

								if($alert_type == 1) // Success

								{

								?>								

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?><a href = "stock_add_grn_engineer_inspection<?php echo $inspection_id ;?>">Click here to</a></strong>

                                    </div>

								<?php

								}

								?>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">

							<form id="add_grn_engineer_inspection_form" class="form-horizontal" method="post" action="stock_add_grn_engineer_inspection.php">

							<input type="hidden" name="hd_grn_item_id" value="<?php echo $grn_item_id; ?>" />

							<input type="hidden" name="hd_grn_item" value="<?php echo $grn_item; ?>" />

							<input type="hidden" name="hd_grn_id" id="hd_grn_id" value="<?php echo $grn_id; ?>" />

							<input type="hidden" name="hd_grn_project" id="hd_grn_project" value="<?php echo $grn_project; ?>" />

									<fieldset>		



										<div class="control-group">											

											<label class="control-label" for="stxt_grn_no">GRN Number</label>

											<div class="controls">

												<input type="text" name="stxt_grn_no" class="span6" autocomplete="off" id="stxt_grn_no" onkeyup="return get_grn_details();" value="<?php echo $grn_no; ?>"/>&nbsp;&nbsp;&nbsp;<a href="#" onclick="return submit_form();">Get Details</a>

												<div id="search_results" class="dropdown-content"></div>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="txt_remarks">Remarks</label>

											<div class="controls">

												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="txt_remarks_to_account">Remarks to Account*</label>

											<div class="controls">

												<input type="text" class="span6" name="txt_remarks_to_account" placeholder="Remarks to Account" required>

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<table class="table table-bordered">

							<thead>

							  <tr>

								<th>GRN No</th>

								<th>Po No</th>

								<th>Item</th>
								
								<th>Item Code</th>
								
								<th>Uom</th>

								<th>Ordered Quantity</th>

								<th>Already GRN Qty</th>

								<th>Inward Qty</th>

								<th>Accepted Qty</th>

								<th>Rejected Qty</th>

							</tr>

							</thead>

							<tbody>

							<?php				

							if($inspection_done == false)

							{

								if($grn_items_list["status"] == SUCCESS)

								{

									$grn_items_list_data = $grn_items_list['data'];

									?>

									<input type="hidden" name="hd_po_item_count" value="<?php echo count($grn_items_list_data); ?>" />

									<?php																				

									for($count = 0; $count < count($grn_items_list_data); $count++)

									{	

											// Get already approved quantity

											$stock_grn_engineer_inspection_search_data = array('grn_item_id'=>$grn_items_list_data[$count]["stock_grn_item_id"]);

											$grn_items_search_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);

											

											$issued_qty = 0;

											if($grn_items_search_list['status'] == SUCCESS)

											{

												for($iss_count = 0; $iss_count < count($grn_items_search_list['data']); $iss_count++)

												{

													$issued_qty = $issued_qty + $grn_items_search_list['data'][$iss_count]['stock_grn_engineer_inspection_approved_quantity'];

												}

											}

											else

											{

												$issued_qty = 0;

											}

											

											?>	

											<input type="hidden" name="hd_grn_item_id_<?php echo $count; ?>" value= "<?php echo $grn_items_list_data[$count]["stock_grn_item_id"]; ?>">

											<input type="hidden" name="hd_grn_material_id_<?php echo $count; ?>" value= "<?php echo $grn_items_list_data[$count]["stock_grn_item"]; ?>">

											<input type="hidden" name="hd_po_qty_<?php echo $count; ?>" />

											<tr>

												<td><?php echo $grn_items_list_data[$count]["stock_grn_no"]; ?></td>

												<td><?php echo $grn_items_list_data[$count]["stock_purchase_order_number"]; ?></td>

												<td><?php echo $grn_items_list_data[$count]["stock_material_name"]; ?></td>
												
												<td><?php echo $grn_items_list_data[$count]["stock_material_code"]; ?></td>
												
												<td><?php echo $grn_items_list_data[$count]["stock_unit_name"]; ?></td>

												<td><?php echo $grn_items_list_data[$count]["stock_grn_item_quantity"]; ?></td>

												<td><?php echo $issued_qty; ?></td>

												<td><?php echo $grn_items_list_data[$count]["stock_grn_item_inward_quantity"]; ?></td>

												<td><input type="number" min="0" step="0.01" name="accepted_qty_<?php echo $count; ?>" id="accepted_qty_<?php echo $count; ?>" onkeyup="return change_acc_rej_value('acc',<?php echo $count; ?>,<?php echo $grn_items_list_data[$count]["stock_grn_item_inward_quantity"]; ?>);"></td>

												<td><input type="number" min="0" step="0.01" name="rejected_qty_<?php echo $count; ?>" id="rejected_qty_<?php echo $count; ?>" onkeyup="return change_acc_rej_value('rej',<?php echo $count; ?>,<?php echo $grn_items_list_data[$count]["stock_grn_item_inward_quantity"]; ?>);"></td>

											</tr>

										<?php 		

									}

								}

								else

								{

								?>

								<td colspan="8">No Items added</td>

								<?php

								}

							}

							else

							{

								echo 'Inspection done for this GRN already';

							}?>	



							</tbody>

								</table>

                                                                                                                                                               										 <br />

										

											

										<div class="form-actions">

										<?php

										if($inspection_done == false)

										{

										?>

											<input type="submit" class="btn btn-primary" name="add_grn_engineer_inspection_submit" value="Submit" />

											<button type="reset" class="btn">Cancel</button>

										<?php

										}

										?>

										</div> <!-- /form-actions -->

									</fieldset>

								</form>

								</div>

								

							</div> 

							

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      	

	      	

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    

<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function get_grn_details()

{ 

	var searchstring = document.getElementById('stxt_grn_no').value;

	

	if(searchstring.length >= 1)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{						

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

				else

				{

					document.getElementById('search_results').style.display = 'none';

				}

			}

		}



		xmlhttp.open("POST", "ajax/stock_get_grn_no.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_grn_no(grn_id,grn_no)

{	

	document.getElementById('hd_grn_id').value   = grn_id;

	document.getElementById('stxt_grn_no').value = grn_no;

	

	document.getElementById('search_results').style.display = 'none';

}



function submit_form()

{		

	var grn_id = document.getElementById('hd_grn_id').value;

	var grn_no = document.getElementById('stxt_grn_no').value;

	

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_grn_engineer_inspection.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","hd_grn_id");

	hiddenField2.setAttribute("value",grn_id);

	

	var hiddenField3 = document.createElement("input");

	hiddenField3.setAttribute("type","hidden");

	hiddenField3.setAttribute("name","stxt_grn_no");

	hiddenField3.setAttribute("value",grn_no);

    	

	form.appendChild(hiddenField2);	

	form.appendChild(hiddenField3);	

	

	document.body.appendChild(form);

    form.submit();

}



function change_acc_rej_value(mode,count,inqty)

{	

	if(mode == 'acc')

	{

		fvalue = document.getElementById('accepted_qty_' + count).value;

		accqty = fvalue;

		rejqty = inqty - accqty;

	}

	else

	{

		fvalue = document.getElementById('rejected_qty_' + count).value;

		accqty = inqty - fvalue;

		rejqty = fvalue;

	}

	

	var status = check_accept_validity(inqty,accqty);

	

	if(status == false)

	{

		document.getElementById('accepted_qty_' + count).value = 0;

		document.getElementById('rejected_qty_' + count).value = 0;

		alert('Cannot accept more quantity than inward quantity');

	}

	else

	{

		document.getElementById('accepted_qty_' + count).value = accqty;

		document.getElementById('rejected_qty_' + count).value = rejqty;

	}

}



function check_accept_validity(inqty,acqty)

{

	if(acqty > inqty)

	{

		return false;

	}

	else

	{

		return true;

	}

}

</script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>



</html>

