<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_court_case.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_config.php');

/*
PURPOSE : To add Legal Court Case
INPUT 	: File ID, Case Type, Case Date, Case No, Case Establishment, Case Status, Case Year, Plaintiff, Defendant, Details, Added By
OUTPUT 	: Success or Failure message,If its SUCCESS it will add new data into court.
BY 		: Sonakshi D
*/
function i_add_legal_court_case($legal_court_case_file_id,$legal_court_case_type,$legal_court_case_date,$case_number,$survey_no,$village,$case_establishment,$case_status,$case_year,$case_plaintiff,$case_diffident,$legal_court_case_details,$legal_court_case_added_by)
{
	$legal_court_case_iresult =  db_add_legal_court_case($legal_court_case_file_id,$legal_court_case_date,$legal_court_case_type,$case_number,$survey_no,$village,$case_establishment,$case_status,$case_year, $case_plaintiff,$case_diffident,$legal_court_case_details,$legal_court_case_added_by);

	if($legal_court_case_iresult["status"] == SUCCESS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = $legal_court_case_iresult['data'];
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "There was an Internal Error";
	}

	return $return;
}

/*
PURPOSE : To get legal court case list
INPUT 	: Court Case Data Array
OUTPUT 	: Court Case List, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_legal_court_case($case_search_data)
{
	$court_case_sresult = db_get_legal_court_case($case_search_data);
	
	if($court_case_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $court_case_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No court case yet!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add Legal Court Case Follow Up
INPUT 	: Case ID, Follow Up Date, Status, Remarks, Added By
OUTPUT 	: Success or Failure message,If its SUCCESS it will add new data into court.
BY 		: Nitin Kashyap
*/
function i_add_legal_court_case_fup($legal_court_case_id,$legal_court_case_fup_date,$legal_court_case_fup_status,$legal_court_case_fup_remarks,$legal_court_case_fup_added_by)
{
	$legal_court_case_fup_iresult = db_add_legal_court_case_fup($legal_court_case_id,$legal_court_case_fup_date,$legal_court_case_fup_status,$legal_court_case_fup_remarks,$legal_court_case_fup_added_by);

	if($legal_court_case_fup_iresult["status"] == SUCCESS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = $legal_court_case_fup_iresult['data'];
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "There was an Internal Error";
	}
		
	return $return;
}

/*
PURPOSE : To get legal court case follow up list
INPUT 	: Court Case Follow Up Data Array
OUTPUT 	: Court Case Follow Up List, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_court_case_fup_details($court_case_fup_data)
{	
	$court_case_fup_sresult = db_get_court_case_fup($court_case_fup_data);
	
	if($court_case_fup_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $court_case_fup_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No follow ups added for this case"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Court Case Follow Up
INPUT 	: Case Follow Up ID, Case Follow Up Data
OUTPUT 	: Case Follow Up ID or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_case_fup($case_fup_id,$case_fup_data)
{
	$case_fup_uresult = db_update_court_case_fup($case_fup_id,$case_fup_data);
	
	if($case_fup_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $case_fup_uresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add Court Case Documents
INPUT 	: File Path, Case ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Soankshi
*/
function i_add_court_case_documents($file_path,$case_id,$remarks,$added_by)
{
	$court_case_documents_iresult = db_add_legal_court_case_documents($file_path,$case_id,$remarks,$added_by);
			
	if($court_case_documents_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Document Successfully added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Court Case Document List
INPUT 	: Case ID ,File Path
OUTPUT 	: BD Project List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_court_case_documents_list($documents_search_data)
{	
	$court_case_documents_sresult = db_get_legal_court_case_documents($documents_search_data);
	
	if($court_case_documents_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $court_case_documents_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Document added yet!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add court_payment request
INPUT 	: Case ID, Amount, Reason, Remarks, Payable To, Added_by
OUTPUT 	: Message, success or failure message
BY 		: Punith
*/
function i_add_court_payment_request($case_id,$amount,$reason,$remarks,$payable_to,$added_by)
{
	$court_payment_request_iresult = db_add_court_payment_request($case_id,$amount,$reason,$remarks,$payable_to,$added_by);
		
	if($court_payment_request_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Court Payment Successfully added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get court payment request list
INPUT 	: Case ID, Amount, Reason, Remarks, Payable to, Status, Added_by, Start Date, End Date
OUTPUT 	: Court Payment List or Error Details, success or failure message
BY 		: Punith
*/
function i_get_court_payment_request_list($case_id,$amount,$reason,$remarks,$payable_to,$status,$added_by,$start_date,$end_date,$request_id='')
{
	$court_payment_request_sresult = db_get_court_payment_request($case_id,$amount,$reason,$remarks,$payable_to,$status,$added_by,$start_date,$end_date,$request_id);
	
	if($court_payment_request_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $court_payment_request_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No court_payment requests yet!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update court payment
INPUT 	: Request ID, Case ID, Amount, Reason, Remarks, Payable To, Status, Added By, Added On
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_court_payment_request($request_id,$case_id,$amount,$reason,$remarks,$payable_to,$status,$updated_by,$updated_on)
{
	$court_payment_request_uresult = db_update_court_payment_request($request_id,$case_id,$amount,$reason,$remarks,$payable_to,$status,$updated_by,$updated_on);
		
	if($court_payment_request_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Court Payment Request Successfully updated";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To add court_payment release
INPUT 	: Request ID, Amount, Mode, Remarks, Paid To, Added_by
OUTPUT 	: Message, success or failure message
BY 		: Punith
*/
function i_add_court_payment_release($request_id,$amount,$mode,$remarks,$paid_to,$added_by)
{
	// Check if there is a payment for this request already
	$pay_release_data = array('request'=>$request_id);
	$released_payment_sresult = db_get_court_payment_released_list($pay_release_data);
	if($released_payment_sresult['status'] == DB_NO_RECORD)
	{
		$court_payment_release_iresult = db_add_court_payment_release($request_id,$amount,$mode,$remarks,$paid_to,$added_by);
			
		if($court_payment_release_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Court Payment Details Successfully added";
			$return["status"] = SUCCESS;	

			$court_payment_request_uresult = i_update_court_payment_request($request_id,'','','','','','4',$added_by,date('Y-m-d H:i:s'));
			if($court_payment_request_uresult['status'] != SUCCESS)
			{
				$return["data"]   = "Payment was issued, but the request status was not updated. Please inform the admin";
				$return["status"] = FAILURE;	
			}
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else if($released_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["data"]   = "Payment has already been made for this request";
		$return["status"] = FAILURE;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get court payment issued/released list
INPUT 	: Payment Release Data - Release ID, Request ID, Mode, Added_by, Start Date, End Date
OUTPUT 	: Court Payment Issued List or Error Details, success or failure message
BY 		: Punith
*/
function i_get_court_payment_release_list($pay_release_data)
{
	$court_payment_release_sresult = db_get_court_payment_released_list($pay_release_data);
	
	if($court_payment_release_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $court_payment_release_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Court Related Payments Released yet!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Court Case Details
INPUT 	: Case ID, Case Data
OUTPUT 	: Case ID or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_court_case($case_id,$case_data)
{
	$case_uresult = db_update_court_case($case_id,$case_data);
	
	if($case_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $case_uresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get court case follow up list
INPUT 	: Court Case Follow Up Filter Data - Case ID, Fup Start Date, Fup End Date, Added By
OUTPUT 	: Court Follow Up List or Error Details, success or failure message
BY 		: Nitin
*/
function i_get_court_fup_latest($case_fup_data)
{
	$case_fup_latest_sresult = db_get_court_case_fup_latest($case_fup_data);
	
	if($case_fup_latest_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $case_fup_latest_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Court Case Related Follow Ups yet!"; 
    }
	
	return $return;
}
?>