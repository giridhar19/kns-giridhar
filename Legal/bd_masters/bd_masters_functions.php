<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_bd_masters.php');

/*
PURPOSE : To add funding source
INPUT 	: Funding Source Nmae, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_fund_source($fund_source_name,$added_by)
{
	$fund_source_sresult = db_get_fund_source_list($fund_source_name,'','','','');;
	
	if($fund_source_sresult["status"] == DB_NO_RECORD)
	{
		$fund_source_iresult = db_add_fund_source($fund_source_name,$added_by);
		
		if($fund_source_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Funding Source Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Funding Source already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get funding source list
INPUT 	: Funding Source Name, Active
OUTPUT 	: Funding Source List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_fund_source($fund_source_name,$active)
{
	$fund_source_sresult = db_get_fund_source_list($fund_source_name,$active,'','','');
	
	if($fund_source_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $fund_source_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No funding source added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add village
INPUT 	: Village Name, Landmark, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_village($village_name,$landmark,$added_by)
{
	$village_sresult = db_get_village_list($village_name,'','','');
	
	if($village_sresult["status"] == DB_NO_RECORD)
	{
		$village_iresult = db_add_village($village_name,$landmark,$added_by);
		
		if($village_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Village Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Village Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get village list
INPUT 	: Village Name
OUTPUT 	: Village List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_village_list($village_name)
{
	$village_sresult = db_get_village_list($village_name,'','','');
	
	if($village_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $village_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No village added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add owner status
INPUT 	: Owner Status Name, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_owner_status($owner_status_name,$added_by)
{
	$owner_status_sresult = db_get_owner_status_list($owner_status_name,'','','');
	
	if($owner_status_sresult["status"] == DB_NO_RECORD)
	{
		$owner_status_iresult = db_add_owner_status($owner_status_name,$added_by);
		
		if($owner_status_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Land Status Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Land Status already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get owner status list
INPUT 	: Owner Status Name
OUTPUT 	: Owner Status List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_owner_status_list($owner_status_name)
{
	$owner_status_sresult = db_get_owner_status_list($owner_status_name,'','','');
	
	if($owner_status_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $owner_status_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No land status added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add BD delay reason master details
INPUT 	: Reason Name, Priority, Added By
OUTPUT 	: BD Delay Reason ID, success or failure message
BY 		: Sonakshi D
*/
function i_add_bd_delay_reason_master($delay_reason_name,$delay_reason_priority,$delay_reason_added_by)
{
	$delay_reason_search_data = array("delay_reason_name"=>$delay_reason_name);
	$delay_master_sresult =  db_get_delay_reason_master($delay_reason_search_data);
	
	if($delay_master_sresult['status'] == DB_NO_RECORD)
	{
		$reason_iresults = db_add_delay_reason_master($delay_reason_name,$delay_reason_priority,$delay_reason_added_by);
		if($reason_iresults["status"] == SUCCESS)
			
		{
			$return["status"] = SUCCESS;
			$return["data"]   = $reason_iresults['data'];
		}
		else
		{
			$return["status"] = FAILURE;
			$return["data"]   = "There was some internal error";
		}
	}
	else
	{
		$return['status'] = FAILURE;
		$return['data']   = 'A delay reason for <strong>'.$delay_reason_name.'</strong> already exists';
	}

	return $return;
}

/*
PURPOSE : To get BD delay master details
INPUT 	: Master ID, Name, Active, Priority
OUTPUT 	: Data, success or failure message
BY 		: Sonakshi D
*/
function i_get_bd_delay_reason_master($reason_master_id,$reason_master_name,$reason_master_active,$reason_master_priority)
{
	$delay_reason_search_data = array('delay_reason_master_id'=>$reason_master_id,"delay_reason_name"=>$reason_master_name,"delay_reason_priority"=>$reason_master_priority,"delay_reason_active"=>$reason_master_active);
	$delay_master_sresult =  db_get_delay_reason_master($delay_reason_search_data);
	
	if($delay_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $delay_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No reason added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add court case type
INPUT 	: Case Type, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_court_case_type($case_type,$added_by)
{
	$case_type_sresult = db_get_court_case_type($case_type,'','','','');
	
	if($case_type_sresult["status"] == DB_NO_RECORD)
	{
		$court_case_iresult = db_add_court_case_type($case_type,$added_by);
		
		if($court_case_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Court Case Type Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Court Case Type already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Court case list
INPUT 	: Case Type, Active
OUTPUT 	: Court Case Type list or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_court_case_type($case_type,$active)
{
	$case_type_sresult = db_get_court_case_type($case_type,$active,'','','');
	
	if($case_type_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $case_type_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Court Case Type added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add establishment Details
INPUT 	: establishment id,establishment name
OUTPUT 	: Data, success or failure message
BY 		: Sonakshi D
*/
function i_add_court_establishment($establishment_name,$address,$added_by)
{
	$establishment_sresult = db_get_court_establishment($establishment_name,'','','','','');
	
	if($establishment_sresult["status"] == DB_NO_RECORD)
	{
		$establishment_iresults = db_add_bd_court_establishment($establishment_name,$address,$added_by);
		
		if($establishment_iresults['status'] == SUCCESS)
		{
			$return["data"]   = "Establishment Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Establishment Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}
/*
PURPOSE : To get Establishmentmmaster Details
INPUT 	: establishment id,establishment name
OUTPUT 	: Data, success or failure message
BY 		: Sonakshi D
*/
function i_get_court_establishment($establishment_name,$address,$active,$added_by,$start_date,$end_date)
{
	$establishment_master_sresult = db_get_court_establishment($establishment_name,$address,$active,$added_by,$start_date,$end_date);
	
	if($establishment_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $establishment_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No establishment added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add Court status
INPUT 	: Status Name, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_court_status($status_name,$added_by)
{
	$status_sresult = db_get_court_status_list($status_name,'','','','');
	
	if($status_sresult["status"] == DB_NO_RECORD)
	{
		$status_iresult = db_add_court_status($status_name,$added_by);
		
		if($status_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Status Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Status Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Status list
INPUT 	: Status Name
OUTPUT 	: Status List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_court_status_list($status_name,$active)
{
	$status_sresult = db_get_court_status_list($status_name,$active,'','','');
	
	if($status_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $status_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Status added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add Account
INPUT 	: Account Name, Landmark, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_account($account_name,$contact,$added_by)
{
	$account_sresult = db_get_account_list($account_name,'','','','');
	
	if($account_sresult["status"] == DB_NO_RECORD)
	{
		$account_iresult = db_add_account($account_name,$contact,$added_by);
		
		if($account_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Account Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Account Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Account list
INPUT 	: Account Name
OUTPUT 	: Account List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_account_list($account_name,$active)
{
	$account_sresult = db_get_account_list($account_name,$active,'','','');
	
	if($account_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $account_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No account added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add Plaintiff
INPUT 	: Plaintiff Name
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_plaintiff($plaintiff_name,$added_by)
{
	$plaintiff_sresult = db_get_plaintiff($plaintiff_name,'','','','');
	
	if($plaintiff_sresult["status"] == DB_NO_RECORD)
	{
		$plaintiff_iresult =  db_add_plaintiff($plaintiff_name,$added_by);
		
		if($plaintiff_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Plaintiff Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Plaintiff already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get plaintiff list
INPUT 	: Plaintiff Name
OUTPUT 	: plaintiff List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_plaintiff_list($plaintiff_name,$added_by)
{
	$plaintiff_sresult =  db_get_plaintiff($plaintiff_name,'',$added_by,'','');
	
	if($plaintiff_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $plaintiff_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Plaintiff added. Please contact the system admin"; 
    }
	
	return $return;
}
/*
PURPOSE : To add Diffident
INPUT 	: Diffident Name, Active, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_diffident($diffident_name,$added_by)
{
	$diffident_sresult = db_get_diffident($diffident_name,'','','','');
	
	if($diffident_sresult["status"] == DB_NO_RECORD)
	{
		$diffident_iresult =  db_add_diffident($diffident_name,$added_by);
		
		if($diffident_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Diffident Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Diffident already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Diffident list
INPUT 	: Diffident Name
OUTPUT 	: Diffident List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_diffident_list($diffident_name,$added_by)
{
	$diffident_sresult = db_get_diffident($diffident_name,'',$added_by,'','');
	
	if($diffident_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $diffident_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Diffident added. Please contact the system admin"; 
    }
	
	return $return;
}

?>