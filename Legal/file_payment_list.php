<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: file_payment_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans for a particular process ID
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}
	else	
	{
		$file_id = "";
	}
	if(isset($_GET["file_id"]))
	{
		$payment_file_id = $_GET["file_id"];
	}
	else	
	{
		$payment_file_id = "";
	}

	// Temp data
	$alert = "";

	// Get list of file payments
	$file_payment_list = i_get_file_payment_list($file_id,'');

	if($file_payment_list["status"] == SUCCESS)
	{
		// Get specific file payment details
		$file_payment_list_data = $file_payment_list["data"];
	}
	else
	{
		$alert = "Invalid File!";
	}	
	
	// Get file details
	$file_details = i_get_file_details($file_id);
	
	if($file_details["status"] == SUCCESS)
	{
		// Do nothing
	}
	else
	{
		$alert = "Invalid File!";
	}

	// Get list of files
	$bd_file_list = i_get_bd_files_list($payment_file_id,'','','','','','','1');

	if($bd_file_list["status"] == SUCCESS)
	{
		$bd_file_list_data = $bd_file_list["data"];
		$land_cost = $bd_file_list_data[0]["bd_file_land_cost"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_file_list["data"];
		$land_cost = "";
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>File Payment List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>File Payment List</h3><span style="float:right; padding-right:30px;"><?php if($file_id != ''){ ?><a href="add_file_payment_request.php?file=<?php echo $file_id; ?>&payment_file_id=<?php echo $payment_file_id ;?>">Add payment Request</a><?php } ?>&nbsp;&nbsp;&nbsp;<a href="bd_payment_request_list.php?file=<?php echo $file_details["data"]["file_bd_file_id"]; ?>">Payment Request List</a></span>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">			
			<span style="padding-left:10px;">
			<?php if(($file_details["status"] == SUCCESS) && ($file_id != ''))
			{
			?>
			File Number: <strong><?php echo $file_details["data"]["file_number"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Survey Number: <strong><?php echo $file_details["data"]["file_survey_number"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Village: <strong><?php echo $file_details["data"]["file_village"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Party Name: <strong><?php echo $file_details["data"]["file_land_owner"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Extent: <strong><?php echo $file_details["data"]["file_extent"]; ?> guntas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Land Amount: <strong><?php echo get_formatted_amount($land_cost,"INDIA"); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Brokerage: <strong><?php echo get_formatted_amount($file_details["data"]["file_brokerage_charges"],"INDIA"); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			<?php
			}
			?>
			</span>
			
              <table class="table table-bordered">
                <thead>
                  <tr>					
					<th>Payment Date</th>
					<th>Amount</th>
					<th>Payment Type</th>
					<th>Paid To</th>
					<th>Remarks</th>										
				</tr>
				</thead>
				<tbody>
				 <?php				
				if($file_payment_list["status"] == SUCCESS)
				{
					$total = 0;
					for($count = 0; $count < count($file_payment_list_data); $count++)
					{
						$total = $total + $file_payment_list_data[$count]["file_payment_amount"];
						$payment_type = $file_payment_list_data[$count]["file_payment_type"];
						
						switch($payment_type)
						{
							case "1":
								$type = "Land Lord Payement";
								break;
								
							case "2":
								$type = "Change of Land Use Fee";

								break;
							case "3":
								$type = "Conversion Fee";
								break;
								
							case "4":
								$type = "Brokerage Amount";
								break;
								
							case "5":
								$type = "Stamp Duty";
							break;
							
							case "6":
								$type = "Registration Fee";
							break;
							
							case "7":
								$type = "Other Expenses";
							break;
							
							default:
								echo "";	
						}
						
						
					?>
					<tr>
						<td><?php echo date("d-M-Y",strtotime($file_payment_list_data[$count]["file_payment_date"])); ?></td>						
						<td><?php echo get_formatted_amount($file_payment_list_data[$count]["file_payment_amount"],"INDIA"); ?></td>
						<td><?php echo $type ?></td>
						<td><?php echo $file_payment_list_data[$count]["file_payment_done_to"]; ?></td>
						<td><?php echo $file_payment_list_data[$count]["file_payment_remarks"]; ?></td>
					</tr>
					<?php 
					}
					?>
					<tr>												
						<td><strong>PAID AMOUNT</strong></td>
						<td><strong><?php echo get_formatted_amount($total,"INDIA"); ?></strong></td>
						<td>&nbsp;</td>	
						<td>&nbsp;</td>	
					</tr>
					<?php if($type != "Land Lord Payement") { ?>
					<tr>												
						<td><strong>Others</strong></td>
						<td><strong><?php echo get_formatted_amount($total,"INDIA"); ?></strong></td>
						<td>&nbsp;</td>	
						<td>&nbsp;</td>	
					</tr>
					<?php 
					}
					?>
					<tr>												
						<td><strong>BALANCE AMOUNT</strong></td>
						<td><strong><?php echo get_formatted_amount(($land_cost - $total),"INDIA"); ?><strong></td>
						<td>&nbsp;</td>	
						<td>&nbsp;</td>	
					</tr>
					<?php
				}				
				else
				{
				?>
				<td colspan="3">No payment made yet!</td>
				<?php
				}
				?>	

                </tbody>
              </table>
			  <br />
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>