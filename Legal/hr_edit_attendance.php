<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 31st March 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1;
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["attendance"]))
	{
		$attendance_id = $_GET["attendance"];
	}
	else
	{
		$attendance_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["edit_attendance_submit"]))
	{
		$shift          = $_POST["ddl_shift"];
		$in_time_hours  = $_POST["stxt_intime_hours"];
		$in_time_mins   = $_POST["stxt_intime_mins"];
		$out_time_hours = $_POST["stxt_outtime_hours"];
		$out_time_mins  = $_POST["stxt_outtime_mins"];
		$attendance_id  = $_POST["hd_attendance_id"];
		
		$total_duration_data = get_time_difference($in_time_hours.':'.$in_time_mins,$out_time_hours.':'.$out_time_mins);								
		
		// Check for mandatory fields
		if(($shift != "") && ($in_time_hours != "") && ($in_time_mins != "") && ($out_time_hours != "") && ($out_time_mins != ""))
		{
			$total_time = ($total_duration_data['data']['hours']*60) + $total_duration_data['data']['mins'];
			
			if($total_time >= 510)
			{
				$work_time = 510;
				$ot_time   = $total_time - 510;
				
				$type = 1;
			}
			else if($total_time >= 450)
			{
				$is_swipe_clean = i_get_employee_swipe($in_time_hours.':'.$in_time_mins,$out_time_hours.':'.$out_time_mins);
				if($is_swipe_clean == 0)
				{
					$type = "1";
				}
				else
				{
					$type = "2";
				}
				
				$work_time = $total_time;
				$ot_time   = 0;
			}
			else if($total_time >= 240)
			{
				$type = "2";
				
				$work_time = $total_time;
				$ot_time   = 0;
			}
			else
			{
				$is_swipe_clean = i_get_employee_swipe($in_time_hours.':'.$in_time_mins,$out_time_hours.':'.$out_time_mins);
				if($is_swipe_clean == 0)
				{
					$type = "3";
				}
				else
				{
					$type = "2";
				}
				
				$work_time = $total_time;
				$ot_time   = 0;
			}
		
			$work_duration  = get_time_in_hours_mins($work_time);
			$ot_duration    = get_time_in_hours_mins($ot_time);
			$total_duration = get_time_in_hours_mins($total_time);
			
			$attendance_data = array("shift"=>$shift,"in_time"=>$in_time_hours.':'.$in_time_mins,"out_time"=>$out_time_hours.':'.$out_time_mins,"work_duration"=>$work_duration,"ot"=>$ot_duration,"total_duration"=>$total_duration,"type"=>$type);
			$attendance_uresult = i_update_attendance($attendance_id,$attendance_data);
			
			if($attendance_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = "Attendance details updated successfully";
				
				header("location:hr_attendance_report.php");
			}
			else
			{
				$alert_type = 0;
				$alert      = $attendance_uresult['data'];
			}
		}
		else
		{
			$alert      = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get attendance details
	$attendance_filter_data = array("attendance_id"=>$attendance_id);
	$attendance_list = i_get_attendance_list($attendance_filter_data);
	if($attendance_list["status"] == SUCCESS)
	{
		$attendance_list_data = $attendance_list["data"];
		
		$in_time_db  = explode(':',$attendance_list_data[0]['hr_attendance_in_time']);
		$out_time_db = explode(':',$attendance_list_data[0]['hr_attendance_out_time']);
		
		$in_time_hours = $in_time_db[0];
		$in_time_mins  = $in_time_db[1];
		
		$out_time_hours = $out_time_db[0];
		$out_time_mins  = $out_time_db[1];
		
		$work_duration_db  = $attendance_list_data[0]['hr_attendance_work_duration'];
		$ot_duration_db    = $attendance_list_data[0]['hr_attendance_ot_duration'];
		$total_duration_db = $attendance_list_data[0]['hr_attendance_total_duration'];
	}
	else
	{
		$alert = $alert."Alert: ".$attendance_list["data"];
		header('location:hr_attendance_report.php');
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Attendance</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Employee: <?php echo $attendance_list_data[0]["hr_employee_name"]; ?>&nbsp;&nbsp;&nbsp;Date: <?php echo date("d-M-Y",strtotime($attendance_list_data[0]["hr_attendance_date"])); ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Attendance</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_att_form" class="form-horizontal" method="post" action="hr_edit_attendance.php">
								<input type="hidden" name="hd_attendance_id" value="<?php echo $attendance_id; ?>" />
									<fieldset>										
															
										<div class="control-group">											
											<label class="control-label" for="ddl_shift">Shift*</label>
											<div class="controls">
												<select name="ddl_shift" required>
												<option value="">- - Select Shift - -</option>
												<option value="GS" <?php if($attendance_list_data[0]["hr_attendance_shift"] == "GS") {?> selected <?php } ?>>GS</option>
												<option value="FS" <?php if($attendance_list_data[0]["hr_attendance_shift"] == "FS") {?> selected <?php } ?>>FS</option>
												<option value="SS" <?php if($attendance_list_data[0]["hr_attendance_shift"] == "SS") {?> selected <?php } ?>>SS</option>
												</select>												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_intime">In Time*</label>
											<div class="controls">
												<input type="text" class="span3" name="stxt_intime_hours" value="<?php echo $in_time_hours; ?>" placeholder="Ex: 9 for 9.30 AM" required="required">
												<input type="text" class="span3" name="stxt_intime_mins" value="<?php echo $in_time_mins; ?>" placeholder="Ex: 30 for 9.30 AM" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="stxt_outtime">Out Time*</label>
											<div class="controls">
												<input type="text" class="span3" name="stxt_outtime_hours" value="<?php echo $out_time_hours; ?>" placeholder="Ex: 18 for 6.30 PM" required="required">
												<input type="text" class="span3" name="stxt_outtime_mins" value="<?php echo $out_time_mins; ?>" placeholder="Ex: 18 for 6.30 PM" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_work_duration_hours">Work Duration</label>
											<div class="controls">
												<span id="span_work_duration"><?php echo $work_duration_db; ?></span>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_ot_hours">OT Duration</label>
											<div class="controls">												
												<span id="span_ot_duration"><?php echo $ot_duration_db; ?></span>
											</div> <!-- /controls -->															
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="num_total_duration_hours">Total Duration*</label>
											<div class="controls">												
												<span id="span_total_duration"><?php echo $total_duration_db; ?></span>
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->																		
                                                                                                                                                               										 <br />
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_attendance_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
<script>
function update_tot_hours()
{
	working_hours = document.getElementById("num_work_duration_hours").value;
	working_mins  = document.getElementById("num_work_duration_mins").value;
	ot_hours      = document.getElementById("num_ot_hours").value;
	ot_mins       = document.getElementById("num_ot_mins").value;
	
	total_hours = +working_hours + +ot_hours;
	
	total_mins = +working_mins + +ot_mins;
	if(total_mins > 59)
	{
		total_hours = +total_hours + 1;
		total_mins  = +total_mins - 60;
	}
	
	document.getElementById("num_total_duration_hours").value = total_hours;
	document.getElementById("num_total_duration_mins").value = total_mins;
}
</script>

<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
