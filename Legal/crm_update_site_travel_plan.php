<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 14th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["travel_plan_id"]))
	{
		$travel_plan_id = $_GET["travel_plan_id"];
	}	
	else
	{
		$travel_plan_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["update_site_travel_submit"]))
	{
		$travel_plan = $_POST["hd_travel_plan_id"];
		$status      = $_POST["ddl_status"];
		$cab_number  = $_POST["stxt_cab_number"];
		
		// Check for mandatory fields
		if(($travel_plan !="") && ($status !=""))
		{
			$travel_plan_uresult = i_update_site_travel_plan($travel_plan,$status,$cab_number);
			
			if($travel_plan_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				if($status == "2")
				{
					$site_travel_plan_list = i_get_site_travel_plan_list($travel_plan,'','','','','','','','');
					header("location:crm_add_prospective_profile.php?enquiry=".$site_travel_plan_list["data"][0]["enquiry_id"]);
				}
				else
				{
					header("location:crm_site_travel_plan_list.php");
				}	
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $travel_plan_uresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get site travel plan details
	$travel_plan_list = i_get_site_travel_plan_list($travel_plan_id,'','','','','','','','');
	if($travel_plan_list["status"] == SUCCESS)
	{
		$travel_plan_list_data = $travel_plan_list["data"];
	}
	else
	{
		$alert = $travel_plan_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Update Site Travel Plan</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Update Site Travel Plan</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="update_travel_form" class="form-horizontal" method="post" action="crm_update_site_travel_plan.php">
								<input type="hidden" name="hd_travel_plan_id" value="<?php echo $travel_plan_id; ?>" />
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="ddl_status">Status*</label>
											<div class="controls">
												<select name="ddl_status" required>
												<option value="1" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "1"){ ?> selected="selected" <?php } ?>>Pending</option>	
												<option value="2" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "2"){ ?> selected="selected" <?php } ?>>Completed</option>	
												<option value="3" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "3"){ ?> selected="selected" <?php } ?>>Customer Cancelled</option>	
												<option value="4" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "4"){ ?> selected="selected" <?php } ?>>Driver Did not turn up</option>	
												<option value="5" <?php if($travel_plan_list_data[0]["crm_site_travel_plan_status"] == "5"){ ?> selected="selected" <?php } ?>>Company Cancelled</option>	
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="stxt_cab_number">Remarks*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_cab_number" placeholder="Vehicle Number of the cab if completed or remarks if any other status" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="update_site_travel_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
