<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_mgmt_blocked_site_list.php
CREATED ON	: 13-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of management site blocking
*/
/* DEFINES - START */define('CRM_MGMNT_BLOCKED_SITE_LIST_FUNC_ID','93');define('CRM_MGMNT_UNBLOCK_LIST_FUNC_ID','312');/* DEFINES - END */
/*
TBD: 
*/$_SESSION['module'] = 'CRM Projects';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list    = i_get_user_perms($user,'',CRM_MGMNT_BLOCKED_SITE_LIST_FUNC_ID,'1','1');		$view_perms_list   = i_get_user_perms($user,'',CRM_MGMNT_BLOCKED_SITE_LIST_FUNC_ID,'2','1');	$edit_perms_list   = i_get_user_perms($user,'',CRM_MGMNT_BLOCKED_SITE_LIST_FUNC_ID,'3','1');	$delete_perms_list = i_get_user_perms($user,'',CRM_MGMNT_BLOCKED_SITE_LIST_FUNC_ID,'4','1');
// Get permission settings for this user for this page	$add_perms_unblock_list    = i_get_user_perms($user,'',CRM_MGMNT_UNBLOCK_LIST_FUNC_ID,'1','1');		$view_perms_unblock_list   = i_get_user_perms($user,'',CRM_MGMNT_UNBLOCK_LIST_FUNC_ID,'2','1');	$edit_perms_unblock_list   = i_get_user_perms($user,'',CRM_MGMNT_UNBLOCK_LIST_FUNC_ID,'3','1');	$delete_perms_unblock_list = i_get_user_perms($user,'',CRM_MGMNT_UNBLOCK_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing

	// Temp data
	$alert = "";
	
	if(isset($_POST["search_mgmnt_blocking_submit"]))
	{
		$project_id = $_POST["ddl_project"];
		$mgmt_user  = $_POST["ddl_mgmt_user"];
		$reason     = $_POST["ddl_reason"];
	}
	else
	{
		$project_id = "-1";
		$mgmt_user  = "";
		$reason     = "";
	}

	$mgmnt_blocking_list = i_get_mgmt_block_list('',$project_id,'',$mgmt_user,$reason,'1','','','',$user);
	if($mgmnt_blocking_list["status"] == SUCCESS)
	{
		$mgmnt_blocking_list_data = $mgmnt_blocking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$mgmnt_blocking_list["data"];
	}
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($project_list["status"] == SUCCESS)	{		$project_list_data = $project_list["data"];	}	else	{		$alert = $alert."Alert: ".$project_list["data"];		$alert_type = 0; // Failure	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Reason List
	$reason_list = i_get_mgmnt_blk_reason_list('');
	if($reason_list["status"] == SUCCESS)
	{
		$reason_list_data = $reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$reason_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Management Blocked Site List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Management Blocked List&nbsp;&nbsp;&nbsp;(Total Sites: <span id="total_sites"></span> )&nbsp;&nbsp;&nbsp;(Total Area: <span id="total_sq_ft"></span> sq. ft)</h3>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="mgmnt_blocked_site_display" action="crm_mgmnt_blocked_site_list.php">			  		  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($project_id == $project_list_data[$count]["project_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>						
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_mgmt_user">
			  <option value="">- - Select Mgmnt User - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{					
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($mgmt_user == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_reason">
			  <option value="">- - Select Reason - -</option>
			  <?php
				for($count = 0; $count < count($reason_list_data); $count++)
				{					
					?>
					<option value="<?php echo $reason_list_data[$count]["crm_management_block_reason_id"]; ?>" <?php 
					if($reason == $reason_list_data[$count]["crm_management_block_reason_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $reason_list_data[$count]["crm_management_block_reason_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="search_mgmnt_blocking_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>
					<th>Project</th>
					<th>Site No.</th>
					<th>Area</th>
					<th>User</th>
					<th>Reason</th>								
					<th>Days</th>
					<th>Remarks</th>
					<th>Blocked On</th>
					<th>Added by</th>									
					<th>&nbsp;</th>					
				</tr>
				</thead>
				<tbody>							
				<?php			
				$total_area = 0;
				if($mgmnt_blocking_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($mgmnt_blocking_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $mgmnt_blocking_list_data[$count]["project_name"]; ?></td>
					<td><?php echo $mgmnt_blocking_list_data[$count]["crm_site_no"]; ?></td>
					<td><?php echo $mgmnt_blocking_list_data[$count]["crm_site_area"]; 
					$total_area = $total_area + $mgmnt_blocking_list_data[$count]["crm_site_area"]; ?> sq. ft</td>					
					<td><?php echo $mgmnt_blocking_list_data[$count]["blocked_user"]; ?></td>
					<td><?php echo $mgmnt_blocking_list_data[$count]["crm_management_block_reason_name"]; ?></td>									
					<td><?php $date_diff = get_date_diff(date("Y-m-d",strtotime($mgmnt_blocking_list_data[$count]["crm_management_block_added_on"])),date("Y-m-d"));
					echo $date_diff["data"];?></td>
					<td><?php echo $mgmnt_blocking_list_data[$count]["crm_management_block_site_remarks"]; ?></td>
					<td><?php echo date("d-M-Y",strtotime($mgmnt_blocking_list_data[$count]["crm_management_block_added_on"])); ?></td>
					<td><?php echo $mgmnt_blocking_list_data[$count]["added_user"]; ?></td>
					<?php if($role == "1") 
					{?>
					<td><?php					if($edit_perms_unblock_list['status'] == SUCCESS)					{					?><a href="crm_unblock.php?blocking=<?php echo $mgmnt_blocking_list_data[$count]["crm_management_block_id"]; ?>&type=mgmnt">Unblock</a></td>					<?php					}					else					{						?>						<div class="form-actions">							You are not authorized to Unblock						</div> <!-- /form-actions -->						<?php					}					?>
					<?php
					}?>
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="7">No site blocked yet!</td>
				<?php
				}
				 ?>	
				<script>
				document.getElementById("total_sq_ft").innerHTML = <?php echo $total_area; ?>;
				document.getElementById("total_sites").innerHTML = <?php echo $sl_no; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>