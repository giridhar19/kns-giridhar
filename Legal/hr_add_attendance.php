<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25th March 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */
$_SESSION['module'] = 'HR';
/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1;
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["import_attendance_submit"]))
	{
		$attendance_date = $_POST["dt_attendance"];
		$import_file     = upload("file_attendance",date("dMY"));		
				
		// Check for mandatory fields
		if(($import_file != "") && ($attendance_date != ""))
		{			
			ini_set("max_execution_time", 0);

			$row = 1;			
			if (($handle = fopen($import_file, "r")) !== FALSE) 
			{				
				while (($attendance_data = fgetcsv($handle, 0, ",")) !== FALSE) 
				{
					if($row > 1)
					{
						// Status
						$status = 1;
					
						// Field 3 - Employee Code
						$emp_code = $attendance_data[2];						
						$employee_filter_data = array("employee_code"=>$emp_code,"user_status"=>'1');
						$employee_sdetails = i_get_employee_list($employee_filter_data);						
						if($employee_sdetails["status"] == SUCCESS)
						{
							$employee = $employee_sdetails["data"][0]["hr_employee_id"];
						}
						else
						{
							$alert = $alert."Import field at row no. ".$row." due to invalid employee code";
							$status = ($status&0);
						}
						
						// Field 4 - Employee Name
						// Not needed for us
						
						// Field 5 - Shift
						$shift = $attendance_data[4];
						
						// Field 6 - In Time
						$in_time = $attendance_data[5];
						
						// Field 7 - Out Time
						$out_time = $attendance_data[6];												
												
						if($status != 0)
						{																				
							// Calculate presence status if employee found
							$total_time_array = get_time_difference($in_time,$out_time);
							$total_time = ((intval($total_time_array['data']['hours'])) * 60) + (intval($total_time_array['data']['mins']));							

							// Check if there is an out pass
							$out_pass_filter_data = array('employee_id'=>$employee,"date"=>$attendance_date,"status"=>'1');
							$out_pass_sresult = i_get_out_pass_list($out_pass_filter_data);							
							if($out_pass_sresult['status'] == SUCCESS)
							{
								$pass_in_time  = $out_pass_sresult['data'][0]['hr_out_pass_out_time'];
								$pass_out_time = $out_pass_sresult['data'][0]['hr_out_pass_finish_time'];
								
								// Check if it is an in pass or out pass
								$inp_array  = get_time_difference($pass_in_time,$in_time);								
								$outp_array = get_time_difference($out_time,$pass_in_time);
								if($inp_array['status'] == SUCCESS) // In pass
								{
									$in_out_time_array  = get_time_difference($pass_out_time,$in_time);
									if($in_out_time_array == SUCCESS)
									{
										// complete pass time to be added
										$pass_time_array = get_time_difference($pass_in_time,$pass_out_time);
									}
									else
									{
										// only time from pass in time to swipe in time to be considered
										$pass_time_array = get_time_difference($pass_in_time,$in_time);
									}																		
								}
								else if($outp_array['status'] == SUCCESS) // Out pass
								{
									// Out pass; absolutely
									$pass_time_array = get_time_difference($pass_in_time,$pass_out_time);									
								}
								else // there is an overlap between swipe in time and pass time
								{
									$in_out_time_array  = get_time_difference($out_time,$pass_out_time);
									if($in_out_time_array['status'] == SUCCESS)
									{
										// complete pass time to be added
										$pass_time_array = $in_out_time_array;										
									}
									else
									{
										// only time from pass in time to swipe in time to be considered
										$pass_time_array['data']['hours'] = '00';
										$pass_time_array['data']['mins'] = '00';
									}
								}
								
								$total_time = $total_time + ($pass_time_array['data']['hours'] * 60) + $pass_time_array['data']['mins']; 
							}
							
							// Check for clean swipe
							$is_swipe_clean = i_get_employee_swipe($in_time,$out_time);
							
							if($total_time >= 510)
							{
								// Check for  clean swipe								
								if($is_swipe_clean == 0)
								{
									$type      = "1";
									$work_time = 510;
									$ot_time   = $total_time - 510;		
								}
								else
								{
									$type       = "2";
									$work_time  = 0;
									$ot_time    = 0;
									$total_time = 0;
								}																													
							}
							else if($total_time >= 450)
							{																
								if($is_swipe_clean == 0)
								{
									$type      = "1";
									$work_time = $total_time;
									$ot_time   = 0;
								}
								else
								{
									$type       = "2";
									$work_time  = 0;
									$ot_time    = 0;
									$total_time = 0;
								}																
							}
							else if($total_time >= 240)
							{
								if($is_swipe_clean == 0)
								{
									$type      = "2";
									$work_time = $total_time;
									$ot_time   = 0;
								}
								else
								{
									$type       = "2";
									$work_time  = 0;
									$ot_time    = 0;
									$total_time = 0;
								}																								
							}
							else
							{								
								if($is_swipe_clean == 0)
								{
									$type = "3";
									$work_time = $total_time;
									$ot_time   = 0;
								}
								else
								{
									$type       = "2";
									$work_time  = 0;
									$ot_time    = 0;
									$total_time = 0;
								}									
							}
							
							$work_duration  = get_time_in_hours_mins($work_time);
							$ot_duration    = get_time_in_hours_mins($ot_time);
							$total_hours    = get_time_in_hours_mins($total_time);
						
							$attendance_entered_by = $user;
							$attendance_iresult = i_add_attendance($attendance_date,$employee,$shift,$in_time,$out_time,$work_duration,$ot_duration,$total_hours,$type,$attendance_entered_by);
							if($attendance_iresult["status"] == SUCCESS)
							{
								$alert = "Import Successful!";
								$alert_type = 1;								
							}							
						}	
					}
					
					$row++;
				}
				fclose($handle);
			}					
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
}
else
{
	header("location:login.php");
}	

function upload($file_id,$date) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$date."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], $_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Import Attendance</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>HR</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Attendance</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_attendance" class="form-horizontal" method="post" action="hr_add_attendance.php" enctype="multipart/form-data">
									<fieldset>																									
										
										<div class="control-group">											
											<label class="control-label" for="file_attendance">Attendance Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_attendance" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="file_attendance">Attendance File*</label>
											<div class="controls">
												<input type="file" class="span6" name="file_attendance" required="required">
												<p class="help-block"><a href="import_attendance_template.csv" target="_blank">Download Template</a></p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="import_attendance_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>
