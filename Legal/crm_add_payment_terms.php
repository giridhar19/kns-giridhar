<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 16th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_payment_terms_submit"]))
	{
		$booking_id            = $_POST["hd_booking_id"];
		$main_charges          = $_POST["num_maintenance_charges"];
		$water_charges         = $_POST["num_water_charges"];
		$club_house_charges    = $_POST["num_club_house_charges"];
		$documentation_charges = $_POST["num_documentation_charges"];
		$other_charges         = $_POST["num_other_charges"];
		
		// Check for mandatory fields
		if($booking_id !="")
		{
			$add_pay_terms_result = i_add_payment_terms($booking_id,$main_charges,$water_charges,$club_house_charges,$documentation_charges,$other_charges,$user);
			
			if($add_pay_terms_result["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = $add_pay_terms_result["data"];
			}
			else
			{
				$alert_type = 0;
				$alert      = $add_pay_terms_result["data"];
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
	
	// Get booking details
	$booking_list = i_get_site_booking($booking_id,'','','','','','','','','','','');
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
	}
	
	// Get payment terms
	$payment_terms_list = i_get_payment_terms($booking_id);	
	if($payment_terms_list["status"] == SUCCESS)
	{
		$payment_terms_list_data = $payment_terms_list["data"];
		
		$pt_maint_value         = $payment_terms_list_data[0]["crm_pt_layout_maintenance_charges"];
		$pt_water_value         = $payment_terms_list_data[0]["crm_pt_water_charges"];
		$pt_clubhouse_value     = $payment_terms_list_data[0]["crm_pt_club_house_charges"];
		$pt_documentation_value = $payment_terms_list_data[0]["crm_pt_documentation_charges"];
		$pt_other_value         = $payment_terms_list_data[0]["crm_pt_other_charges"];
	}
	else
	{
		$pt_maint_value         = (MAINTENANCE_RATE*$booking_list_data[0]["crm_booking_consideration_area"]);
		$pt_water_value         = (WATER_CHARGES*$booking_list_data[0]["crm_booking_consideration_area"]);
		$pt_clubhouse_value     = "0";
		$pt_documentation_value = "0";
		$pt_other_value         = "0";
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Other Payment Terms</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
						<?php 
						if($cust_details["status"] == SUCCESS)
						{
						?>
						<h3>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $cust_details_list[0]["crm_customer_name_one"]." (".$cust_details_list[0]["crm_customer_contact_no_one"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}
						else
						{
							$alert_type = 0;
							$alert      = "Customer Profile not added!";?>
	      				<h3>Project: <?php echo $booking_list_data[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $booking_list_data[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $booking_list_data[0]["name"]." (".$booking_list_data[0]["cell"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}?>
						
						<span style="float:right; padding-right:10px;"><a href="crm_customer_transactions.php?booking=<?php echo $booking_list_data[0]["crm_booking_id"]; ?>">Back to customer profile</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Other Payment Terms</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_payment_terms" class="form-horizontal" method="post" action="crm_add_payment_terms.php">
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />								
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="num_maintenance_charges">Maintenance Charges</label>
											<div class="controls">
												<input type="number" name="num_maintenance_charges" min="0" step="0.01" class="span6" value="<?php echo $pt_maint_value; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="num_water_charges">Water Charges</label>
											<div class="controls">
												<input type="number" name="num_water_charges" min="0" step="0.01" class="span6" value="<?php echo $pt_water_value; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="num_club_house_charges">Club House Charges</label>
											<div class="controls">
												<input type="number" name="num_club_house_charges" min="0" step="0.01" class="span6" value="<?php echo $pt_clubhouse_value; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="num_documentation_charges">Documentation Charges</label>
											<div class="controls">
												<input type="number" name="num_documentation_charges" min="0" step="0.01" class="span6" value="<?php echo $pt_documentation_value; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="num_other_charges">Other Charges</label>
											<div class="controls">
												<input type="number" name="num_other_charges" min="0" step="0.01" class="span6" value="<?php echo $pt_other_value; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_payment_terms_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
