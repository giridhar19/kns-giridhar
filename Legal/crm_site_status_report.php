<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_site_status_report.php
CREATED ON	: 06-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Sites and their statuses
*/
/* DEFINES - START */define('CRM_SITE_STATUS_REPORT_FUNC_ID','90');define('CRM_SITE_BANK_FUNC_ID','309');define('CRM_SITE_MORTGAGE_FUNC_ID','310');define('CRM_SITE_KATHA_TRANSFER_FUNC_ID','311');/* DEFINES - END */
/*
TBD: 
*/$_SESSION['module'] = 'CRM Projects';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

// Defines
define('MAX_COLUMNS',5);

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list    = i_get_user_perms($user,'',CRM_SITE_STATUS_REPORT_FUNC_ID,'1','1');		$view_perms_list   = i_get_user_perms($user,'',CRM_SITE_STATUS_REPORT_FUNC_ID,'2','1');	$edit_perms_list   = i_get_user_perms($user,'',CRM_SITE_STATUS_REPORT_FUNC_ID,'3','1');		$add_perms_site_bank_list    = i_get_user_perms($user,'',CRM_SITE_BANK_FUNC_ID,'1','1');		$view_perms_site_bank_list   = i_get_user_perms($user,'',CRM_SITE_BANK_FUNC_ID,'2','1');	$edit_perms_site_bank_list   = i_get_user_perms($user,'',CRM_SITE_BANK_FUNC_ID,'3','1');
	$add_perms_site_mortgage_list    = i_get_user_perms($user,'',CRM_SITE_MORTGAGE_FUNC_ID,'1','1');		$view_perms_site_mortgage_list   = i_get_user_perms($user,'',CRM_SITE_MORTGAGE_FUNC_ID,'2','1');	$edit_perms_site_mortgage_list   = i_get_user_perms($user,'',CRM_SITE_MORTGAGE_FUNC_ID,'3','1');
	$add_perms_site_katha_list    = i_get_user_perms($user,'',CRM_SITE_KATHA_TRANSFER_FUNC_ID,'1','1');		$view_perms_site_katha_list   = i_get_user_perms($user,'',CRM_SITE_KATHA_TRANSFER_FUNC_ID,'2','1');	$edit_perms_site_katha_list   = i_get_user_perms($user,'',CRM_SITE_KATHA_TRANSFER_FUNC_ID,'3','1');
	
	if(isset($_POST["site_search_submit"]))
	{
		$client             = $_POST["hd_client"];
		$search_project     = $_POST["ddl_project"];	
		$search_site_type   = $_POST["ddl_site_type"];	
		$search_dimension   = $_POST["ddl_dimension"];
		$search_site_status = $_POST["ddl_site_status"];
		$search_rel_status  = $_POST["ddl_rel_status"];
		$site_number        = $_POST["stxt_site_num"];
	}
	else
	{		
		// Query String / Get form Data
		if(isset($_GET["client"]))
		{
			$client = $_GET["client"];
		}
		else
		{
			$client = "";
		}
	
		$search_project     = "-1";	
		$search_site_type   = "";
		$search_dimension   = "";
		$search_site_status = "1";
		$search_rel_status  = "";
		$site_number        = "";
	}

	// Temp data
	$alert = "";
	$alert_type = "";
	
	// Get site list	
	$site_list = i_get_site_list($site_number,$search_project,'',$search_site_type,$search_dimension,$search_site_status,$search_rel_status,'',$user);
	if($site_list["status"] == SUCCESS)
	{
		$site_list_data = $site_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_list["data"];
	}
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($project_list["status"] == SUCCESS)	{		$project_list_data = $project_list["data"];	}	else	{		$alert = $alert."Alert: ".$project_list["data"];		$alert_type = 0; // Failure	}
	
	// Site Type List
	$site_type_list = i_get_site_type_list('','1');
	if($site_type_list["status"] == SUCCESS)
	{
		$site_type_list_data = $site_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_type_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Dimension List
	$dimension_list = i_get_dimension_list('','','','','1');
	if($dimension_list["status"] == SUCCESS)
	{
		$dimension_list_data = $dimension_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$dimension_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Site Status List
	$site_status_list = i_get_site_status_list('','1');
	if($site_status_list["status"] == SUCCESS)
	{
		$site_status_list_data = $site_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_status_list["data"];
		$alert_type = 0; // Failure
	}
		
	// Release Status List
	$rel_status_list = i_get_release_status('','1');
	if($rel_status_list["status"] == SUCCESS)
	{
		$rel_status_list_data = $rel_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$rel_status_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Site List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Site List &nbsp;&nbsp;&nbsp;(Total Sites = <span id="total_count_section">
			  <?php
			  if($site_list["status"] == SUCCESS)
			  {
				$preload_count = count($site_list_data);			
			  }
			  else
			  {
			    $preload_count = 0;
			  }
			  
			  echo $preload_count;
			  ?></span>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Total Site Area = <span id="total_area_section">
			  </span> sq. ft)</h3>
			  <span style="float:right; padding-right:5px;"><a href="excel_crm_site_status_report.php?project=<?php echo $search_project; ?>&site_type=<?php echo $search_site_type; ?>&dimension=<?php echo $search_dimension; ?>&site_status=<?php echo $search_site_status; ?>&rel_status=<?php echo $search_rel_status; ?>&site_num=<?php echo $site_number; ?>" target="_blank">Export to excel</a></span>
            </div>
			
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="status_display" action="crm_site_status_report.php">
			  <input type="hidden" name="hd_client" value="<?php echo $client; ?>" />
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_site_num" value="<?php echo $site_number; ?>" placeholder="Site Number" />
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($search_project == $project_list_data[$count]["project_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_site_type">
			  <option value="">- - Select Type - -</option>
			  <?php
				for($count = 0; $count < count($site_type_list_data); $count++)
				{
					?>
					<option value="<?php echo $site_type_list_data[$count]["crm_site_type_id"]; ?>" <?php 
					if($search_site_type == $site_type_list_data[$count]["crm_site_type_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $site_type_list_data[$count]["crm_site_type_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_site_status">
			  <option value="">- - Select Site Status - -</option>
			  <?php
				for($count = 0; $count < count($site_status_list_data); $count++)
				{
					?>
					<option value="<?php echo $site_status_list_data[$count]["status_id"]; ?>" <?php 
					if($search_site_status == $site_status_list_data[$count]["status_id"])
					{
					?>								
					selected="selected"
					<?php
					}?>><?php echo $site_status_list_data[$count]["status_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_rel_status">
			  <option value="">- - Select Site Release Status - -</option>
			  <?php
				for($count = 0; $count < count($rel_status_list_data); $count++)
				{					
					?>
					<option value="<?php echo $rel_status_list_data[$count]["release_status_id"]; ?>" <?php 
					if($search_rel_status == $rel_status_list_data[$count]["release_status_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $rel_status_list_data[$count]["release_status"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_dimension">
			  <option value="">- - Select Dimension - -</option>
			  <?php
				for($count = 0; $count < count($dimension_list_data); $count++)
				{					
					?>
					<option value="<?php echo $dimension_list_data[$count]["crm_dimension_id"]; ?>" <?php 
					if($search_dimension == $dimension_list_data[$count]["crm_dimension_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $dimension_list_data[$count]["crm_dimension_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="site_search_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">			
              <table class="table table-bordered" style="table-layout: fixed;">
				<thead>
				<th>SL No</th>
				<th>Project</th>
				<th>Site No</th>
				<th>Block</th>
				<th>Wing</th>
				<th>Dimension</th>
				<th>Area</th>
				<th>Facing</th>
				<th>Status</th>
				<th>Release Status</th>
				<th>Mortgage</th>
				<th>Survey No</th>
				<th>Katha Details</th>
				<?php
				if($role == '1')
				{
					?>
					<th>&nbsp;</th>
					<?php
				}
				?>
				</thead>
				<tbody>							
				<?php
				$total_site_area = 0;
				if($site_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($site_count = 0; $site_count < count($site_list_data); $site_count++)
					{
						$sl_no++;
						
						$survey_sresult = i_get_site_survey('',$site_list_data[$site_count]["crm_site_id"],'');		

						// Get katha details for the site
						$kt_transaction_data = array("site_id"=>$site_list["data"][$site_count]["crm_site_id"]);
						$kt_transaction_list = i_get_kt_transactions($kt_transaction_data);
						if($kt_transaction_list["status"] == SUCCESS)
						{
							$kt_transaction_list_data = $kt_transaction_list["data"];
							$katha_status_id = $kt_transaction_list_data[count($kt_transaction_list_data) - 1]["crm_katha_transfer_transaction_type"];
							
							if($katha_status_id == "1")
							{
								$katha_status = "Applied for Katha";
								$next_step_id = "2";
								$next_step    = "Receive Katha";
							}
							else if($katha_status_id == "2")
							{
								$katha_status = "Received Katha documents";
								$next_step_id = "3";
								$next_step    = "View Katha Transfer Details";
							}
						}
						else
						{
							$katha_status_id = "0";
							$katha_status = "<i>Not Applied for Katha</i>";
							$next_step_id = "1";
							$next_step    = "Apply for Katha";
						}
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $site_list["data"][$site_count]["project_name"]; ?></td>
					<td><?php echo $site_list["data"][$site_count]["crm_site_no"]; ?></td>
					<td><?php echo $site_list["data"][$site_count]["crm_site_block"]; ?></td>
					<td><?php echo $site_list["data"][$site_count]["crm_site_wing"]; ?></td>
					<td><?php echo $site_list["data"][$site_count]["crm_dimension_name"]; ?></td>
					<td><?php 
							if($site_list["data"][$site_count]["crm_dimension_non_standard"] == "1")
							{
								$area = $site_list["data"][$site_count]["crm_site_area"];
							}
							else
							{
								$area = ($site_list["data"][$site_count]["crm_dimension_length"]*$site_list["data"][$site_count]["crm_dimension_breadth"]);
							} 
							echo $area;
							$total_site_area = $total_site_area + $area;
						?>
					</td>
					<td><?php echo $site_list["data"][$site_count]["crm_site_type_name"]; ?></td>
					<td><?php echo $site_list["data"][$site_count]["status_name"]; ?></td>
					<td><?php echo $site_list["data"][$site_count]["release_status"]; ?></td>
					<td><?php echo $site_list["data"][$site_count]["crm_bank_name"]; ?><br />					<?php					if($edit_perms_site_bank_list['status'] == SUCCESS)					{?>					<a href="crm_edit_site_bank.php?site=<?php echo $site_list["data"][$site_count]["crm_site_id"]; ?>">Change</a>					<?php 					}					else					{						?>						<div class="form-actions">							You are not authorized to Edit Site Bank						</div> <!-- /form-actions -->						<?php					}?><br /><br />					<?php					if($edit_perms_site_mortgage_list['status'] == SUCCESS)					{?><a href="crm_update_mortgage_details.php?site=<?php echo $site_list["data"][$site_count]["crm_site_id"]; ?>">Update Details</a>					<?php 					}					else					{						?>						<div class="form-actions">							You are not authorized to Update Details						</div> <!-- /form-actions -->						<?php					}?>					</td>
					<td><?php echo $survey_sresult["data"][0]["crm_survey_master_survey_no"]; ?></td>
					<td><?php echo $katha_status; ?><br />					<?php					if($edit_perms_site_katha_list['status'] == SUCCESS)					{?><a href="crm_katha_transfer_process.php?site=<?php echo $site_list["data"][$site_count]["crm_site_id"]; ?>&type=<?php echo $next_step_id; ?>"><?php echo $next_step; ?></a>					<?php 						}						else						{							?>							<div class="form-actions">								You are not authorized to Transfer Katha							</div> <!-- /form-actions -->							<?php						}?></td>
						<?php
					if(($edit_perms_list['status'] == SUCCESS) && ($site_list["data"][$site_count]["crm_site_status"] == AVAILABLE_STATUS))
					{
						?>
						<td><a href="#" onclick="return go_to_edit(<?php echo $site_list["data"][$site_count]["crm_site_id"]; ?>);">Edit</a></td>
						<?php
					}
					?>
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="6">No site matching your search criteria!</td>
				</tr>
				<?php
				}
				?>
				<script>
				document.getElementById("total_area_section").innerHTML = <?php echo $total_site_area; ?>;				
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function go_to_edit(site_id)
{		
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", "crm_edit_site_details.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","site_id");
	hiddenField1.setAttribute("value",site_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
	form.submit();
}
</script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>