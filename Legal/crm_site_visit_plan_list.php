<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_site_visit_plan_list.php
CREATED ON	: 07-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Site Visit Plans
*/
define('CRM_TENTATIVE_SITE_VISIT_PLAN_LIST_FUNC_ID','150');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_TENTATIVE_SITE_VISIT_PLAN_LIST_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_TENTATIVE_SITE_VISIT_PLAN_LIST_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_TENTATIVE_SITE_VISIT_PLAN_LIST_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_TENTATIVE_SITE_VISIT_PLAN_LIST_FUNC_ID,'4','1');

	// Query String / Get form Data	
	if(isset($_GET["enquiry"]))
	{
		$enquiry = $_GET["enquiry"];
	}
	else
	{
		$enquiry = "";
	}
	
	$project           = "";
	$date              = "";	
	$added_by          = "";
	$start_date        = "";
	$start_date_filter = "";
	$end_date          = "";
	$end_date_filter   = "";
	$search_status = "0";
	if(isset($_GET["site_plan_visit_search_submit"]))
	{
		$project    = $_GET["ddl_search_project"];
		$date       = $_GET["dt_sv_plan_date"];		
		$start_date = $_GET["dt_sv_added_on_start"];
		$end_date   = $_GET["dt_sv_added_on_end"];
		$added_by   = $_GET["ddl_search_added_by"];
		
		if($start_date != "")
		{
			$start_date_filter = $start_date." 00:00:00";
		}
		if($end_date != "")
		{
			$end_date_filter = $end_date." 23:59:59";
		}
		
		$search_status   = $_GET["ddl_search_status"];
	}

	// Temp data
	$alert = "";

	if(($role == 1) || ($role == 5))
	{
		$assigned_to = "";
	}
	else
	{
		$assigned_to = $user;
	}
	
	// Project	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"project_active"=>'1',"active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
	
	if(($role == 6) || ($role == 7) || ($role == 8) || ($role == 5) || ($role == 9))
	{
		$status = "1";
	}
	else if($role == 1)
	{
		$status = "1";
	}
	$site_visit_plan_list = i_get_site_visit_plan_list($enquiry,$date,$project,$status,$added_by,$start_date_filter,$end_date_filter,'asc','',$user);
	if($site_visit_plan_list["status"] == SUCCESS)
	{
		$site_visit_plan_list_data = $site_visit_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_visit_plan_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Site Visit List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Site Visit List&nbsp;&nbsp;&nbsp;&nbsp;(Count = <span id="total_count_section"><?php if($site_visit_plan_list["status"] == SUCCESS)
			  {
				  $preload_count = count($site_visit_plan_list);
			  }
			  else
			  {
			      $preload_count = 0;
			  }
			  
			  echo $preload_count;
			  ?></span>)</h3>
            </div>
			
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="get" id="site_visit_search_form" action="crm_site_visit_plan_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_sv_added_on_start" value="<?php echo $start_date; ?>" />
			  </span>	
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_sv_added_on_end" value="<?php echo $end_date; ?>" />			  
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($count = 0; $count < count($project_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php if($project == $project_list_data[$count]["project_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_list_data[$count]["project_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_search_added_by">
			  <option value="">- - Select User - -</option>
			  <?php
			  for($count = 0; $count < count($user_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($added_by == $user_list_data[$count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
			  <?php
			  
			  }
			  ?>
			  </select>
			  </span>			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_search_status">
			  <option value="" <?php if($search_status == "") { ?>selected="selected"<?php } ?>>- - Select Status - -</option>
			  <option value="0" <?php if($search_status == "0") { ?>selected="selected"<?php } ?>>Travel Not Yet Planned</option>
			  <option value="1" <?php if($search_status == "1") { ?>selected="selected"<?php } ?>>Pending</option>
			  <option value="2" <?php if($search_status == "2") { ?>selected="selected"<?php } ?>>Completed</option>
			  <option value="3" <?php if($search_status == "3") { ?>selected="selected"<?php } ?>>Customer Cancelled</option>
			  <option value="4" <?php if($search_status == "4") { ?>selected="selected"<?php } ?>>Driver did not come</option>
			  <option value="5" <?php if($search_status == "5") { ?>selected="selected"<?php } ?>>Company Cancelled</option>
			  <option value="6" <?php if($search_status == "6") { ?>selected="selected"<?php } ?>>Not Known</option>
			  </select>
			  </span>			  			 			  
			  <span style="padding-left:20px; padding-right:20px;">
			  Select Tentative Date From Here ->&nbsp;&nbsp;:
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_sv_plan_date" value="<?php echo $date; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="submit" name="site_plan_visit_search_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Name</th>
					<th>Mobile</th>
					<th>Pickup Point</th>
					<th>Confirmation Status</th>
					<th>Drive Type</th>
					<th>Site Visit Status</th>
					<th>Site Visit Plans</th>
					<th>Site Visit Planned By</th>
					<th>Work Date</th>
					<th>Tentative Site Visit Date</th>					
					<th>Time</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($site_visit_plan_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$discard_count = 0;
					for($count = 0; $count < count($site_visit_plan_list_data); $count++)
					{						
						$sv_plan_count_data = i_get_site_visit_plan_list($site_visit_plan_list_data[$count]["enquiry_id"],'','','','','','','');
						$sv_plan_count = count($sv_plan_count_data["data"]);
						
						$st_plan_data = i_get_site_travel_plan_list('',$site_visit_plan_list_data[$count]["crm_site_visit_plan_id"],'','','','','','','');
						if($st_plan_data["status"] == SUCCESS)
						{
							$st_status = $st_plan_data["data"][0]["crm_site_travel_plan_status"];
							
							switch($st_status)
							{
							case "1":
							$sv_status = "Pending";
							break;
							
							case "2":
							$sv_status = "Completed";
							break;
							
							case "3":
							$sv_status = "Customer Cancelled";
							break;
							
							case "4":
							$sv_status = "Driver didn't turn up";
							break;
							
							case "5":
							$sv_status = "Company Cancelled";
							break;
							
							case "6":
							$sv_status = "Not Known";
							break;
							}
						}
						else
						{
							$st_status = 0;
							$sv_status = "Travel not yet planned";
						}
						
						if($search_status == $st_status)
						{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $site_visit_plan_list_data[$count]["enquiry_number"]; ?></td>					
					<td><?php echo $site_visit_plan_list_data[$count]["project_name"]; ?></td>					
					<td><?php echo $site_visit_plan_list_data[$count]["name"]; ?></td>
					<td><?php echo $site_visit_plan_list_data[$count]["cell"]; ?></td>
					<td><?php echo $site_visit_plan_list_data[$count]["crm_site_visit_pickup_location"]; ?></td>
					<td><?php switch($site_visit_plan_list_data[$count]["crm_site_visit_plan_confirmation"])
					{
						case "0":
						echo "Tentative";
						break;
						
						case "1":
						echo "Confirmed";
						break;
						
						case "2":
						echo "Cancelled";
						break;
						
						case "3":
						echo "Postponed";
						break;
						
						default:
						echo "Tentative";
						break;
					}
					?><br />
					<?php
					if(($st_status == "1") || ($st_status == "0"))
					{
					?>
					<a href="crm_update_site_visit_status.php?visit=<?php echo $site_visit_plan_list_data[$count]["crm_site_visit_plan_id"]; ?>">Update Status</a>
					<?php
					}
					?></td>
					<td><?php if($site_visit_plan_list_data[$count]["crm_site_visit_plan_drive"] == 0)
					{
						echo "KNS";
					}
					else
					{
						echo "Self-Drive";
					}?></td>
					<td><?php echo $sv_status; ?></td>
					<td><?php echo $sv_plan_count; ?></td>
					<td><?php echo $site_visit_plan_list_data[$count]["assigner"]; ?></td>
					<td><?php echo date("d-M-Y",strtotime($site_visit_plan_list_data[$count]["crm_site_visit_plan_added_on"])); ?></td>
					<td><?php echo date("d-M-Y",strtotime($site_visit_plan_list_data[$count]["crm_site_visit_plan_date"])); ?></td>					
					<td><?php echo $site_visit_plan_list_data[$count]["crm_site_visit_plan_time"]; ?></td>
					</tr>
					<?php
					}
					else
					{
						$discard_count++;
					}
					}
				}
				else
				{
				?>
				<td colspan="7">No enquiries!</td>
				<?php
				}
      			?>	
				<script>
				document.getElementById("total_count_section").innerHTML = <?php echo (count($site_visit_plan_list_data) - $discard_count); ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>