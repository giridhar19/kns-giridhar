<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/*
TBD: 
*/$_SESSION['module'] = 'HR';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Get list of employees
	$employee_filter_data = array();
	$employee_list = i_get_employee_list($employee_filter_data);

	if($employee_list["status"] == SUCCESS)
	{
		$employee_list_data = $employee_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$employee_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Employee List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Employee List</h3><span style="float:right; padding-right:10px;"><a href="hr_add_employee.php">Add Employee</a></span>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>Employee Code</th>
					<th>Employee Name</th>					
					<th>Login ID</th>
					<th>Reporting Manager</th>
				    <th>Permanent Address</th>
					<th>Father Name</th>	
					<th>DOB</th>	
					<th>Joining Date</th>		
					<th>Week Off</th>		
					
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?php
				if($employee_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($employee_list_data); $count++)
					{						
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $employee_list_data[$count]["hr_employee_code"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $employee_list_data[$count]["hr_employee_name"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $employee_list_data[$count]["email"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $employee_list_data[$count]["manager"].' ('.$employee_list_data[$count]["manager_email"].')'; ?></td>
						<td style="word-wrap:break-word;"><?php echo $employee_list_data[$count]["hr_employee_permanent_address"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $employee_list_data[$count]["hr_employee_father_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($employee_list_data[$count]["hr_employee_dob"],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($employee_list_data[$count]["hr_employee_employment_date"],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_day($employee_list_data[$count]["hr_employee_week_off"]); ?></td>
						<td style="word-wrap:break-word;"><a href="hr_edit_employee.php?employee=<?php echo $employee_list_data[$count]["hr_employee_id"]; ?>">Edit</a></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="7">No employee added!</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>
