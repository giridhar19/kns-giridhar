<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 24-July-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query String Data
	
	if(isset($_GET["budget_id"]))
	{
		$budget_id   = $_GET["budget_id"];
	}
	else
	{
		$budget_id = "";
	}

	// Capture the form data
	if(isset($_POST["edit_project_budget_submit"]))
	{

		$budget_id        = $_POST["hd_budget_id"];
		$value            = $_POST["num_value"];
		$project_id       = $_POST["ddl_project_id"];
		$remarks 	      = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($project_id != ""))
		{
			$project_budget_update_data = array("value"=>$value,"project_id"=>$project_id,"remarks"=>$remarks);
			$project_budget_iresult = i_update_project_budget($budget_id,$project_budget_update_data);
			
			if($project_budget_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
		    
				header("location:project_budget_list.php");
			}
			else
			{
				$alert 		= $project_budget_iresult["data"];
				$alert_type = 0;	
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	//Get Project Management Master List
	$project_management_master_search_data = array("active"=>'1');
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}
	
	// Temp Project Budget List
	$project_budget_search_data = array("active"=>'1',"budget_id"=>$budget_id);
	$project_budget_list = i_get_project_budget($project_budget_search_data);
	if($project_budget_list["status"] == SUCCESS)
	{
		$project_budget_list_data = $project_budget_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Project Budget </title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Project Budget</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Project Budget</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_project_budget_form" class="form-horizontal" method="post" action="project_edit_budget.php">
								<input type="hidden" name="hd_budget_id" value="<?php echo $budget_id; ?>" />
									<fieldset>

                                            <div class="control-group">											
											<label class="control-label" for="ddl_project_id">Project*</label>
											<div class="controls">
												<select name="ddl_project_id" required>
												<option>- -Select Project- -</option>
												<?php
												for($count = 0; $count < count($project_management_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_management_master_list_data[$count]["project_management_master_id"]; ?>" <?php if($project_management_master_list_data[$count]["project_management_master_id"] == $project_budget_list_data[0]["project_budget_project_id"]){ ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$count]["project_master_name"]; ?></option>
												<?php
												}
												?>	
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->									
																
										<div class="control-group">											
											<label class="control-label" for="num_value">Value*</label>
											<div class="controls">
												<input type="number" class="span6" name="num_value" min="0.01" step="0.01" value="<?php echo $project_budget_list_data[0]["project_budget_value"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea rows="4" cols="50" class="span6" name="txt_remarks" required="required"><?php echo $project_budget_list_data[0]["project_budget_remarks"] ;?></textarea>
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->
                                        <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_project_budget_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      		
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>