<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 19th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_project_submit"]))
	{
		$stock_project_name    = $_POST["stxt_project_name"];
		$stock_project_remarks = $_POST["txt_remarks"];
				
		// Check for mandatory fields
		if($stock_project_name !="")
		{
			$project_iresult = i_add_stock_project($stock_project_name,$stock_project_remarks,$user);
			if($project_iresult["status"] == SUCCESS)
			{
				$project_id = $project_iresult["data"];
				$stock_material_search_data = array();
				$stock_material_list = i_get_stock_material_master_list($stock_material_search_data);
				for($material_count = 0 ; $material_count < count($stock_material_list["data"]) ; $material_count++)
				{
					$material_stock_search_data = array("project"=>$project_id ,
					"material_id"=>$stock_material_list["data"][$material_count]["stock_material_id"]);
					$material_stock = i_get_material_stock($material_stock_search_data);
					if($material_stock["status"] != SUCCESS)
					{
						$material_stock_idata = i_add_material_stock($stock_material_list["data"][$material_count]["stock_material_id"],0,$stock_material_list["data"][$material_count]["stock_material_unit_of_measure"],'',$project_id,'','','','','',$user);
						if($material_stock_idata["status"] == SUCCESS)
						{
							$quote_add_reset_iresults = i_add_stock_quote_reset($stock_material_list["data"][$material_count]["stock_material_id"],$project_id,date("Y-m-d H:i:s"),'','',$user);
							if($quote_add_reset_iresults["status"] == SUCCESS)
							{
								//  
						}
					}
					else
					{
						
					}
					
				}	
			}
		}
		else
		{
			$alert_type = 0;
		}
		
		$alert = $project_iresult["data"];
	}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get project modes already added
	$stock_project_search_data = array();
	$project_list = i_get_project_list($stock_project_search_data);
	if($project_list['status'] == SUCCESS)
	{
		$project_list_data = $project_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project - Stock Masters</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Project</h3><span style="float:right; padding-right:20px;"><a href="stock_master_project_list.php">Project List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Project</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_project_form" class="form-horizontal" method="post" action="stock_master_add_project.php">
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="stxt_project_name"> Project Name*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_project_name" placeholder="Name of the project" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
																					
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_project_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						</div>					
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->  
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
