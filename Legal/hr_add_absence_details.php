<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25th March 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */$_SESSION['module'] = 'HR';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1;
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_absence_submit"]))
	{
		$employee           = $_POST["hd_employee"];
		$absence_date_start = $_POST["dt_absence_date_start"];
		$absence_date_end   = $_POST["dt_absence_date_end"];
		$absence_type       = $_POST["ddl_absence_type"];
		$absence_duration   = $_POST["rd_duration"];
		$remarks            = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($absence_date_start !="") && ($absence_date_end !="") && ($absence_type !="") && ($absence_duration != ""))
		{
			// Check whether there are enough leaves
			$no_of_leave_days = get_date_diff($absence_date_start,$absence_date_end);			
			$available_leaves = p_get_pending_leaves($employee,$absence_type,$absence_date_start);
			$pending_approval_leaves = t_get_approval_pending_leaves($employee,$absence_type,$absence_date_start);
			$available_leaves = $available_leaves - $pending_approval_leaves;
			
			if($absence_duration == "1")
			{
				$dur = 1;
			}
			else if($absence_duration == "2")
			{
				$dur = 0.5;
			}
			
			if((($no_of_leave_days["data"] + 1) * $dur) <= $available_leaves)
			{
				$absence_date = $absence_date_start;
				while(strtotime($absence_date) <= strtotime($absence_date_end))
				{
					$absence_iresult = i_add_absence_details($employee,$absence_date,$absence_type,$absence_duration,$remarks,$user);
					
					if($absence_iresult["status"] == SUCCESS)
					{
						$alert_type = (-1 * abs($alert_type)) & 1;
						$alert      = $alert."Your leave request for ".date("d-M-Y",strtotime($absence_date))." has been sent for manager's approval".'<br />';
					}
					else
					{
						$alert_type = (-1 * abs($alert_type)) & 0;
						$alert      = $alert.$absence_iresult["data"].' for '.date("d-M-Y",strtotime($absence_date)).'<br />';
					}
					
					$absence_date = date("Y-m-d",strtotime($absence_date.' +1 day'));
				}
			}
			else
			{
				$alert_type = 0;
				$alert      = "Not enough leaves! You have only ".$available_leaves." leaves available, but you are trying to apply for ".($no_of_leave_days["data"] + 1)." leaves";
			}
		}
		else
		{
			$alert      = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get list of employees
	$employee_filter_data = array("employee_user"=>$user);
	$employee_list = i_get_employee_list($employee_filter_data);
	if($employee_list["status"] == SUCCESS)
	{
		$employee_list_data = $employee_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$employee_list["data"];
		$alert_type = 0;
	}
	
	// Get list of absence types
	$attendance_type_filter_data = array("leave_type"=>'1',"active"=>'1');
	$absence_type_list = i_get_attendance_type($attendance_type_filter_data);
	if($absence_type_list["status"] == SUCCESS)
	{
		$absence_type_list_data = $absence_type_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$absence_type_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Absence Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>HR</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Apply for Leave</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="apply_leave_form" class="form-horizontal" method="post" action="hr_add_absence_details.php">
								<input type="hidden" name="hd_employee" value="<?php echo $employee_list_data[0]["hr_employee_id"]; ?>" />
									<fieldset>										
													
										<div class="control-group">											
											<label class="control-label" for="dt_absence_date_start">Leave From*</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_absence_date_start" required>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="dt_absence_date_end">Leave To*</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_absence_date_end" required>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_absence_type">Leave Type*</label>
											<div class="controls">
												<select name="ddl_absence_type" required>
												<option value="">- - Select Leave Type - -</option>
												<?php
												for($count = 0; $count < count($absence_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $absence_type_list_data[$count]["hr_attendance_type_id"]; ?>"><?php echo $absence_type_list_data[$count]["hr_attendance_type_name"]; ?></option>
												<?php
												}
												?>
												</select>												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																				

										<div class="control-group">											
											<label class="control-label" for="rd_duration">Leave Duration</label>
											<div class="controls">
												<input type="radio" name="rd_duration" value="1" checked />&nbsp;&nbsp;&nbsp;Full Day
												&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_duration" value="2" />&nbsp;&nbsp;&nbsp;Half Day												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea class="span6" name="txt_remarks"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_absence_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>


  </body>

</html>
