<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_plan_process_task_list.php
CREATED ON	: 14-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query String Data
	if(isset($_GET['plan_id']))
	{
		$plan_id = $_GET['plan_id'];
	}
	else
	{
		$plan_id = '';
	}
	// Nothing
	if(isset($_GET['project_plan_process_id']))
	{
		$process_id = $_GET['project_plan_process_id'];
	}
	else
	{
		//Nothing
	}
	
	if(isset($_GET['process_master_id']))
	{
		$process_master_id = $_GET['process_master_id'];
	}
	else
	{
		//Nothing
	}

	// Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list['data'];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Temp data
	$project_process_task_search_data = array("active"=>'1',"process_id"=>$process_id,"plan_id"=>$plan_id);
	$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
	if($project_plan_process_task_list["status"] == SUCCESS)
	{
		$project_plan_process_task_list_data = $project_plan_process_task_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_plan_process_task_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Plan Process Task List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Plan Process Task List</h3><span style="float:right; padding-right:20px;"><a href="project_add_task.php?project_plan_process_id=<?php echo $process_id ;?>&process_master_id=<?php echo $process_master_id ;?>&plan_id=<?php echo $plan_id; ?>">Add Task</a></span>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Process ID</th>
					<th>Task Type</th>
					<th>Actual Start Date</th>
					<th>Actual End Date</th>
					<th>Pause Date </th>								
					<th colspan="9" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_plan_process_task_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					?><input type="hidden" name="process_id" value="<?php echo $process_id; ?>" />
					<?php
					for($count = 0; $count < count($project_plan_process_task_list_data); $count++)
					{
						//Get Project Plan Process List
						$project_plan_process_search_data = array("process_id"=>$project_plan_process_task_list_data[$count]["project_process_id"]);
						$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
						if($project_plan_process_list["status"] == SUCCESS)
						{
							$project_plan_process_list_data = $project_plan_process_list["data"];
							$plan_id = $project_plan_process_list_data[0]["project_plan_process_plan_id"];
						}
						
						//Get Project Plan
						$project_plan_search_data = array("plan_id"=>$plan_id);
						$project_plan_list = i_get_project_plan($project_plan_search_data);
						if($project_plan_list["status"] == SUCCESS)
						{
							$project_id = $project_plan_list["data"][0]["project_plan_project_id"];
						}
						
						// Get Project Delay Reason
						$delay_reason_search_data = array("task_id"=>$project_plan_process_task_list_data[$count]["project_process_task_id"]);
						$project_delay_reason_list = i_get_project_delay_reason($delay_reason_search_data);
						if($project_delay_reason_list["status"] == SUCCESS)
						{
							$project_delay_reason_list_data = $project_delay_reason_list["data"];
							$pause_date = date("d-M-Y H:i:s",strtotime($project_delay_reason_list_data[0]["project_task_delay_reason_start_date"]));
						}
						else
						{
							$pause_date = "";
						}
						if($project_plan_process_task_list_data[$count]["project_process_task_status"]== "Pause")
						{
							$status = "Release";
						}
						else
						{
							$status = "Pause";
						}
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_plan_process_task_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_plan_process_task_list_data[$count]["project_task_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php if($project_plan_process_task_list_data[$count]["project_process_actual_start_date"] != "0000-00-00")
					{ ?><?php echo date("d-M-Y",strtotime($project_plan_process_task_list_data[$count][
					"project_process_actual_start_date"])); ?><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($project_plan_process_task_list_data[$count]["project_process_actual_end_date"] != "0000-00-00")
					{ ?><?php echo date("d-M-Y",strtotime($project_plan_process_task_list_data[$count][
					"project_process_actual_end_date"])); ?><?php } ?></td>
					<td><?php if($status == "Release") { echo $pause_date ; } ?></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_plan_process_task('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>','<?php echo $process_id  ;?>');">Edit </a></div></td>
					<td><?php if(($project_plan_process_task_list_data[$count]["project_process_task_active"] == "1")){?><a href="#" onclick="return project_delete_plan_process_task('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>','<?php echo $process_id; ?>');">Delete</a><?php } ?></td>
					<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_task_details('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>');">Details </a></div></td>-->
					<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_man_power_estimate('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>');">Man Power Estimate</a></div></td>-->
					<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_add_indent('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>','<?php echo $project_id ;?>');">Material</a></div></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_add_method_planning('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>','<?php echo $project_plan_process_task_list_data[$count]["project_process_id"]; ?>','<?php echo $plan_id; ?>');">Method Planning</a></div></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_machine_planning('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>');">Add Machine Plan</a></div></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_machine_planning_list('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>');">View Machine Plan</a></div></td>-->
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_task_actual('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>');">Add Actual Man Power</a></div></td>
					
					<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_task_boq('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>');">Add BOQ</a></td>
					
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_task_actual_machine_planning('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>');">Add Actual Machine Plan</a></div></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_add_method_planning('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>','<?php echo $project_plan_process_task_list_data[$count]["project_process_id"]; ?>','<?php echo $plan_id; ?>');">Method Planning</a></div></td>-->
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_delay_reason_master('<?php echo $project_plan_process_task_list_data[$count]["project_process_task_id"]; ?>','<?php echo $project_plan_process_task_list_data[$count]["project_process_id"]; ?>','<?php echo $status ;?>');"><?php echo $status ; ?></a></div></td>
					
					
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_plan_process_task(task_id,process_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_plan_process_task_list.php?project_plan_process_id=" +process_id;
					}
				}
			}

			xmlhttp.open("POST", "project_delete_plan_process_task.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("task_id=" + task_id + "&action=0");
		}
	}	
}

function go_to_project_edit_plan_process_task(task_id,process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_plan_process_task.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","process_id");
	hiddenField2.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_project_task_details(task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_task_details.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_man_power_estimate(task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_man_power_estimate.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);
	
	form.appendChild(hiddenField1);
	
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_add_method_planning(task_id,process_id,plan_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_upload_documents.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","process_id");
	hiddenField2.setAttribute("value",process_id);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","plan_id");
	hiddenField3.setAttribute("value",plan_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_project_machine_planning(project_process_task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_machine_planning.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_project_machine_planning_list(project_process_task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_machine_planning_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_add_indent(project_process_task_id,project_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "stock_add_indent.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","project_id");
	hiddenField2.setAttribute("value",project_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_delay_reason_master(project_process_task_id,process_id,status)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_update_delay_reason_master.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","process_id");
	hiddenField2.setAttribute("value",process_id);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","status");
	hiddenField3.setAttribute("value",status);
	
	form.appendChild(hiddenField1);	
	form.appendChild(hiddenField2);	
	form.appendChild(hiddenField3);	
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_release_reason_master(project_process_task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_release_task_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);
	
	form.appendChild(hiddenField1);	
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_project_task_actual(project_process_task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_manpower.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
function go_to_project_task_actual_machine_planning(project_process_task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_machine_plan.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
function go_to_project_task_boq(project_process_task_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_task_boq.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>