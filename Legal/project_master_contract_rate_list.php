<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_master_contract_rate_list.php
CREATED ON	: 17-April-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing

	$search_process   	 = "";
	
	if(isset($_POST["file_search_submit"]))
	{
		$search_process   = $_POST["search_process"];
	}
	else
	{
		$search_process = "";
	}
	
	$search_vendor   	 = "";
	
	if(isset($_POST["file_search_submit"]))
	{
		$search_vendor   = $_POST["search_vendor"];
	}
	else
	{
		$search_vendor ="";
	}
	if(isset($_POST["file_search_submit"]))
	{
		$search_work_task   = $_POST["search_work_task"];
	}
	else
	{
		$search_work_task ="";
	}
	
	// Get Project Contract Rate modes already added
	$project_contract_rate_master_search_data = array("active"=>'1',"contract_rate_id"=>$search_work_task,"process"=>$process,"vendor_id"=>$search_vendor,"process"=>$search_process);
	$project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
	if($project_contract_rate_master_list['status'] == SUCCESS)
	{
		$project_contract_rate_master_list_data = $project_contract_rate_master_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_contract_rate_master_list["data"];
	}
	
	// Project Manpower Vendor Master List
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_vendor_master_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_vendor_master_list["status"] == SUCCESS)
	{
		$project_manpower_vendor_master_list_data = $project_manpower_vendor_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_manpower_vendor_master_list["data"];
	}
	
	// Get Already added process
	$project_contract_process_search_data = array("active"=>'1');
	$project_contract_process_master_list = i_get_project_contract_process($project_contract_process_search_data);
	if($project_contract_process_master_list["status"] == SUCCESS)
	{
		$project_contract_process_master_list_data = $project_contract_process_master_list["data"];	
	}	
	else
	{
		$alert = $project_contract_process_master_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Master Contract Rate List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Master Contract Rate List</h3><span style="float:right; padding-right:20px;"><a href="project_master_add_contract_rate.php">Project Add Master Contract Rate </a></span>
            </div>
			  <div class="widget-content">
				<?php
				if($view_perms_list['status'] == SUCCESS)
				{
				?>
				<div class="widget-header" style="height:80px; padding-top:10px;">               
				  <form method="post" id="file_search_form" action="project_master_contract_rate_list.php">
				  
				  <span style="padding-left:20px; padding-right:20px;">
				  <select name="search_process">
				  <option value="">- - Select Process - -</option>
				  <?php
				  for($process_count = 0; $process_count < count($project_contract_process_master_list_data); $process_count++)
				  {
				  ?>
				  <option value="<?php echo $project_contract_process_master_list_data[$process_count]["project_contract_process_id"]; ?>" <?php if($search_process == $project_contract_process_master_list_data[$process_count]["project_contract_process_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_contract_process_master_list_data[$process_count]["project_contract_process_name"]; ?></option>
				  <?php
				  }
				  ?>
				  </select>
				  </span>
				  
				  <span style="padding-left:20px; padding-right:20px;">
				  <select name="search_vendor">
				  <option value="">- - Select Vendor - -</option>
				  <?php
				  for($project_count = 0; $project_count < count($project_manpower_vendor_master_list_data); $project_count++)
				  {
				  ?>
				  <option value="<?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if($search_vendor == $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
				  <?php
				  }
				  ?>
				  </select>
				  </span>
				  
				  <span style="padding-left:20px; padding-right:20px;">
				  <select name="search_work_task">
				  <option value="">- - Select Work Task - -</option>
				  <?php
				  for($project_count = 0; $project_count < count($project_contract_rate_master_list_data); $project_count++)
				  {
				  ?>
				  <option value="<?php echo $project_contract_rate_master_list_data[$project_count]["project_contract_rate_master_id"]; ?>" <?php if($search_work_task == $project_contract_rate_master_list_data[$project_count]["project_contract_rate_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_contract_rate_master_list_data[$project_count]["project_contract_rate_master_work_task"]; ?></option>
				  <?php
				  }
				  ?>
				  </select>
				  </span>
				  
				  <input type="submit" name="file_search_submit" />
				  </form>			  
            </div>
			 <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Contract Process</th>
					<th>Vendor</th>
					<th>Work Task</th>
					<th>UOM</th>
					<th>Contract Rate</th>
					<th>Aplicable Date</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="2" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_contract_rate_master_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_contract_rate_master_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_contract_rate_master_list_data[$count]["project_contract_process_name"]; ?></td>
					<td><?php echo $project_contract_rate_master_list_data[$count]["project_manpower_agency_name"]; ?></td>
					<td><?php echo $project_contract_rate_master_list_data[$count]["project_contract_rate_master_work_task"]; ?></td>
					<td><?php echo $project_contract_rate_master_list_data[$count]["stock_unit_name"]; ?></td>
					<td><?php echo $project_contract_rate_master_list_data[$count]["project_contract_rate_master_rate"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_contract_rate_master_list_data[$count][
					"project_contract_rate_master_aplicable_date"])); ?></td>
					<td><?php echo $project_contract_rate_master_list_data[$count]["project_contract_rate_master_remarks"]; ?></td>
					<td><?php echo $project_contract_rate_master_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_contract_rate_master_list_data[$count][
					"project_contract_rate_master_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_contract_rate('<?php echo $project_contract_rate_master_list_data[$count]["project_contract_rate_master_id"]; ?>');">Edit </a></div></td>
					<td><?php if(($project_contract_rate_master_list_data[$count]["project_contract_rate_master_active"] == "1")){?><a href="#" onclick="return project_delete_contract_rate(<?php echo $project_contract_rate_master_list_data[$count]["project_contract_rate_master_id"]; ?>);">Delete</a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_contract_rate(contract_rate_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_master_contract_rate_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_master_delete_contract_rate.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("contract_rate_id=" + contract_rate_id + "&action=0");
		}
	}	
}
function go_to_project_edit_contract_rate(contract_rate_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_master_edit_contract_rate.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","contract_rate_id");
	hiddenField1.setAttribute("value",contract_rate_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>