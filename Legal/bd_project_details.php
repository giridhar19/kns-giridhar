<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: bd_project_list.php
CREATED ON	: 11-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of BD projects
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Temp data
	$alert = "";
	
	// Query String
	if(isset($_GET["project"]))
	{
		$bd_project = $_GET["project"];
	}
	else
	{
		$bd_project = "";
	}
	
	// Get list of projects
	$bd_project_list = i_get_bd_project_list($bd_project,'','','');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_project_list_data = $bd_project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_project_list["data"];
	}

	// Get list of files for this project
	$bd_files_list = i_get_bd_files_list('',$bd_project,'','','','','','1');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_files_list_data = $bd_files_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_files_list["data"];
	}
	
	// Get profitability calculation settings	
	$bd_prof_calc_list = i_get_bd_prof_calc_settings('',$bd_project);
	if($bd_prof_calc_list["status"] == SUCCESS)
	{
		$bd_prof_calc_list_data = $bd_prof_calc_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_prof_calc_list["data"];
	}
	
	// Get list of owner status
	$owner_status_list = i_get_owner_status_list('');
	if($owner_status_list["status"] == SUCCESS)
	{
		$owner_status_list_data = $owner_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$owner_status_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>BD Project Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:100px;"> <i class="icon-th-list"></i>
              <h3><?php echo $bd_project_list_data[0]["bd_project_name"]; ?>&nbsp;&nbsp;&nbsp;Total Extent: <span id="total_extent_span"><i>Calculating</i></span> guntas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="bd_project_details_export.php?project=<?php echo $bd_project_list_data[0]["bd_project_id"]; ?>" target="_blank">Export to Excel</a></h3>	
			  <h3>
			  <?php
			  $status_totals = array();
			  for($count = 0; $count < count($owner_status_list_data); $count++)
			  {
				  $status_totals[$owner_status_list_data[$count]['bd_file_owner_status_id']] = 0;
			  ?>
			  <span <?php if($owner_status_list_data[$count]['bd_file_owner_status_id'] == '8')
					 {
						?>							
						style="color:red;"
						<?php
					 } ?>>
			  <?php echo $owner_status_list_data[$count]['bd_file_owner_status_name']; ?></span>: <span id="extent_<?php echo $owner_status_list_data[$count]['bd_file_owner_status_id']; ?>"><i>Calculating</i></span> guntas&nbsp;&nbsp;&nbsp;
			  <?php
			  }
			  ?></h3>			  
			  <span style="color:red;"><?php echo $alert; ?></span>
            </div>
            <br />
			<h2>BASIC DETAILS</h2>
			<br />
			<div class="widget-content">               
			  <table class="table table-bordered" style="table-layout: fixed;">
			  <thead>			  
			  <tr>
			  <th>Extent (Acres)</th>
			  <th>Saleable Area</th>
			  <th>JD Share</th>
			  <th>Available Area</th>
			  </tr>
			  </thead>
			  <tbody>
			  <tr>
			  <td><span id="extent"></span></td>
			  <td><span id="saleable_area"></span></td>
			  <td><span id="jd_share"></span></td>
			  <td><span id="available_area"></span></td>
			  </tr>
			  </tbody>
			  </table>
            </div>
            <br /><br />
			<h2>PROFITABILITY CALCULATIONS</h2>
			<br />
			<div class="widget-content">               
			  <table class="table table-bordered" style="table-layout: fixed;">
			  <thead>			  
			  <tr>
			  <th>Particulars</th>
			  <th>Amount</th>
			  <th>Rate</th>
			  <th>Fund Requirement</th>
			  </tr>
			  </thead>
			  <tbody>
			  <tr>
			  <td>Total Revenue Expected</td>
			  <td><span id="total_revenue"></span></td>
			  <td><?php if($bd_prof_calc_list["status"] == SUCCESS)
			  {
				echo round($bd_prof_calc_list_data[0]["bd_profitability_calc_setting_expected_rate"],2);
			  }
			  else
			  {
				echo "<span style=\"color:red;\">Not Configured yet</span>";
			  }?></td>
			  <td>&nbsp;</td>
			  </tr>
			  <tr>
			  <td>Land Cost</td>
			  <td><span id="total_land_cost"></span></td>
			  <td>&nbsp;</td>
			  <td><span id="total_fund_required"></span></td>
			  </tr>
			  <tr>
			  <td>Registration, Stamp Duty, Plan Approval, Conversion etc.</td>
			  <td><?php if($bd_prof_calc_list["status"] == SUCCESS)
			  {
				echo round($bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"],2);
			  }
			  else
			  {
				echo "<span style=\"color:red;\">Not Configured yet</span>";
			  }
			  ?></td>
			  <td>&nbsp;</td>
			  <td><?php if($bd_prof_calc_list["status"] == SUCCESS)
			  {
				echo round($bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"],2); 
			  }
			  else
			  {
			    echo "<span style=\"color:red;\">Not configured yet</span>";
			  }?></td>
			  </tr>
			  <tr>
			  <td>Cost of Development</td>
			  <td><span id="total_dev_cost"></span></td>
			  <td><?php if($bd_prof_calc_list["status"] == SUCCESS)
			  {
				echo round($bd_prof_calc_list_data[0]["bd_profitability_calc_setting_dev_cost_rate"],2); 
			  }
			  else
			  {
				echo "<span style=\"color:red;\">Not Configured yet</span>";
			  }?></td>
			  <td>&nbsp;</td>
			  </tr>
			  <tr>
			  <td>Admin, Marketing & Others</td>
			  <td><span id="admin_marketing_cost"></span></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  </tr>
			  <tr>
			  <td>Finance Cost (@18%) for 2 years</td>
			  <td><span id="finance_cost"></span></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  </tr>
			  <tr>
			  <td>Total Cost</td>
			  <td><span id="total_cost"></span></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  </tr>
			  <tr>
			  <td>Net Income</td>
			  <td><span id="net_income"></span></td>
			  <td>&nbsp;</td>
			  <td><span id="net_fund_req"></span></td>
			  </tr>
			  <tr>
			  <td>Return On Investment</td>
			  <td><span id="roi"></span></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  </tr>
			  <tr>
			  <td>Return On Turnover</td>
			  <td><span id="rot"></span></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  </tr>
			  </tbody>
			  </table>
            </div>
            <br /><br />
            <div class="widget-content" style="height:350px; overflow:scroll">
              <table class="table table-bordered" style="table-layout: auto;">
                <thead>
                  <tr>				    
				    <th>SL No</th>
					<th>Owner Name</th>
					<th>Owner No</th>
					<th>Sy No</th>	
					<th>Extent</th>						
					<th>Status</th>
					<th>Delay</th>
					<th>Land Cost</th>					
					<th>Paid</th>					
					<th>Payable</th>
					<th>Remarks</th>
					<th>Account</th>
					<th>JDA%</th>
					<th>JDA share</th>
				</tr>
				</thead>
				<tbody>
				 <?php
				if($bd_files_list["status"] == SUCCESS)
				{				
					$sl_no           = 0;
					$total_extent    = 0;
					$total_jd_extent = 0;
					$total_land_cost = 0;
					$total_paid      = 0;
					$total_payable   = 0;
					for($count = 0; $count < count($bd_files_list_data); $count++)
					{
						$sl_no++;
						
						// Get the corresponding legal file details
						$legal_file_list = i_get_file_list('',$bd_files_list_data[$count]["bd_project_file_id"],'','','','','','','','');
						
						// See which owner status this file is in
						$status_totals[$bd_files_list_data[$count]["bd_file_owner_status"]] = $status_totals[$bd_files_list_data[$count]["bd_file_owner_status"]] + $bd_files_list_data[$count]["bd_file_extent"];
						
						$already_paid = 0;
						if($legal_file_list["status"] == SUCCESS)
						{
							// Get the payment details for the legal file
							$legal_payment_list = i_get_file_payment_list($legal_file_list["data"][0]["file_id"],'');							
							if($legal_payment_list["status"] == SUCCESS)
							{
								for($lp_count = 0; $lp_count < count($legal_payment_list["data"]); $lp_count++)
								{
									$already_paid = $already_paid + $legal_payment_list["data"][$lp_count]["file_payment_amount"];
								}
							}
							else
							{
								$already_paid = 0;
							}
						}
						
						// Delay details for the file
						$delay_sresult = i_get_delay_details($bd_files_list_data[$count]["bd_project_file_id"]);
						if($delay_sresult['status'] == SUCCESS)
						{
							$delay_reason = $delay_sresult['data'][0]['bd_delay_reason_name'];
						}
						else
						{
							$delay_reason = 'NO DELAY';
						}
					?>
					<tr <?php
					if($bd_files_list_data[$count]["bd_file_owner_status"] == '8')
					{
					?>								
					style="color:red;"
					<?php
					}
					?>>
						<td><?php echo $sl_no; ?></td>
						<td><?php echo $bd_files_list_data[$count]["bd_file_owner"]; ?></td>	
						<td><?php echo $bd_files_list_data[$count]["bd_file_owner_phone_no"]; ?></td>	
						<td><?php echo $bd_files_list_data[$count]["bd_file_survey_no"]; ?></td>
						<td><?php $extent = $bd_files_list_data[$count]["bd_file_extent"];
						echo $extent;?></td>
						<td><?php echo $bd_files_list_data[$count]["bd_file_owner_status_name"]; ?></td>
						<td><a href="bd_delay_list.php?file=<?php echo $bd_files_list_data[$count]["bd_project_file_id"]; ?>" target="_blank"><?php echo $delay_reason; ?></a></td>
						<td><?php $land_cost = round($bd_files_list_data[$count]["bd_file_land_cost"],2); 
						echo $land_cost;?></td>
						<td><?php echo round($already_paid,2); ?></td>						
						<td><?php $payable = round(($land_cost - $already_paid),2);
						echo $payable; ?></td>
						<td><?php if($legal_file_list["status"] == SUCCESS)
						{
							echo $legal_file_list["data"][0]["process_name"];
						}
						else
						{
							echo $bd_files_list_data[$count]["process_name"];
						}?></td>
						<td><?php echo $bd_files_list_data[$count]["bd_own_account_master_account_name"]; ?></td>
						<td><?php echo $bd_files_list_data[$count]["bd_project_file_jda_share_percent"].'%'; ?></td>
						<td><?php $jd_share_area = round(($bd_files_list_data[$count]["bd_file_extent"]/GUNTAS_PER_ACRE)*SALEABLE_AREA*($bd_files_list_data[$count]["bd_project_file_jda_share_percent"]/100),2); 
						echo $jd_share_area;?></td>
					</tr>
					<?php
						$total_extent    = round(($total_extent + $extent),2);
						$total_jd_extent = round(($total_jd_extent + $jd_share_area),2);
						$total_land_cost = round(($total_land_cost + $land_cost),2);
						$total_paid      = round(($total_paid + $already_paid),2);
						$total_payable   = round(($total_payable + $payable),2);
					}
					?>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><b>TOTAL</b></td>
					<td><b><?php echo $total_extent; ?> guntas (<?php echo $total_extent/GUNTAS_PER_ACRE; ?> acres)</b></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><?php echo $total_land_cost; ?></td>
					<td><?php echo $total_paid; ?></td>
					<td><?php echo $total_payable; ?></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>					
					<td><?php echo $total_jd_extent; ?></td>
					</tr>
					<?php
				}
				else
				{
				?>
				<td colspan="11">No Files added yet!</td>
				<?php
				}
				 ?>	
				<script>
				document.getElementById("extent").innerHTML = <?php $total_extent = ($total_extent/GUNTAS_PER_ACRE);
				echo round($total_extent,2); ?>;
				document.getElementById("saleable_area").innerHTML = <?php echo round(($total_extent*SALEABLE_AREA),2); ?>;
				document.getElementById("jd_share").innerHTML = <?php echo round($total_jd_extent,2); ?>;
				document.getElementById("available_area").innerHTML = <?php echo round((($total_extent*SALEABLE_AREA) - $total_jd_extent),2); ?>;
				document.getElementById("total_revenue").innerHTML = <?php if($bd_prof_calc_list["status"] == SUCCESS)
				{
					$total_revenue = (($total_extent*SALEABLE_AREA) - $total_jd_extent)*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_expected_rate"];
				}
				else
				{
					$total_revenue = "0";
				}
				echo round($total_revenue,2); ?>;
				document.getElementById("total_land_cost").innerHTML = <?php echo $total_land_cost; ?>;
				document.getElementById("total_fund_required").innerHTML = <?php echo $total_payable; ?>;
				document.getElementById("total_dev_cost").innerHTML = <?php if($bd_prof_calc_list["status"] == SUCCESS)
				{
					$total_dev_cost = ($total_extent*SALEABLE_AREA)*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_dev_cost_rate"]; 
				}
				else
				{
					$total_dev_cost = "0";
				}
				echo round($total_dev_cost,2); ?>;
				document.getElementById("admin_marketing_cost").innerHTML = <?php if($bd_prof_calc_list["status"] == SUCCESS)
				{
					$admin_mktg_cost = ((($total_extent*SALEABLE_AREA) - $total_jd_extent)*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_expected_rate"])*$bd_prof_calc_list_data[0]["bd_profitability_calc_setting_admin_charges"]/100; 
				}
				else
				{
					$admin_mktg_cost = "0";
				}
				echo round($admin_mktg_cost,2); ?>;
				document.getElementById("finance_cost").innerHTML = <?php if($bd_prof_calc_list["status"] == SUCCESS)
				{
					$finance_cost = ($total_land_cost + $total_dev_cost + $admin_mktg_cost + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"])*2*($bd_prof_calc_list_data[0]["bd_profitability_calc_setting_fianance_rate"]/100);
				}
				else
				{
					$finance_cost = "0";
				}
				echo round($finance_cost,2); ?>;
				document.getElementById("total_cost").innerHTML = <?php if($bd_prof_calc_list["status"] == SUCCESS)
				{
					echo round(($total_land_cost + $total_dev_cost + $admin_mktg_cost + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"] + $finance_cost),2); 
				}
				else
				{
					echo "0";
				}?>;
				document.getElementById("net_income").innerHTML = <?php if($bd_prof_calc_list["status"] == SUCCESS)
				{
					$net_income = $total_revenue -($total_land_cost + $total_dev_cost + $admin_mktg_cost + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"] + $finance_cost); 
				}
				else
				{
					$net_income = "0";
				}
				echo round($net_income,2); ?>;
				document.getElementById("net_fund_req").innerHTML = <?php if($bd_prof_calc_list["status"] == SUCCESS) 
				{
					echo round(($total_payable + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"]),2);
				}	
				else
				{
					echo "0";
				}?>;
				document.getElementById("roi").innerHTML = <?php if($bd_prof_calc_list["status"] == SUCCESS)
				{
					echo round(($net_income/($total_land_cost + $total_dev_cost + $admin_mktg_cost + $bd_prof_calc_list_data[0]["bd_profitability_calc_setting_stamp_duty_charges"] + $finance_cost)*100),2); 
				}
				else
				{
					echo "0";
				}?>;
				document.getElementById("rot").innerHTML = <?php if($bd_prof_calc_list["status"] == SUCCESS)
				{
					echo round(($net_income/$total_revenue*100),2); 
				}	
				else
				{
					echo "0";
				}?>;
				</script>
				<script>
				document.getElementById('total_extent_span').innerHTML = <?php echo ($total_extent * GUNTAS_PER_ACRE); ?>;
				</script>
				<script>
				 <?php
				 for($count = 0; $count < count($owner_status_list_data); $count++)
				 {
					 if($owner_status_list_data[$count]['bd_file_owner_status_id'] == '8')
					 {
						?>
						document.getElementById('extent_<?php echo $owner_status_list_data[$count]['bd_file_owner_status_id']; ?>').style.color = "red";
						<?php
					 }	
				 ?>				 
				 document.getElementById('extent_<?php echo $owner_status_list_data[$count]['bd_file_owner_status_id']; ?>').innerHTML = <?php echo ($status_totals[$owner_status_list_data[$count]['bd_file_owner_status_id']]); ?>;				 				 
				 <?php
				 }
				 ?>
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
			<br /><br />
            <div class="widget-content" style="height:400px;">
			<?php
			if($bd_project_list_data[0]["bd_project_file_path"] != '')
			{
			?>
			<iframe src="<?php echo "bd_files/".$bd_project_list_data[0]["bd_project_file_path"]; ?>" width="100%" height="100%"></iframe>
			<?php
			}
			else
			{
				echo 'No plan added';
			}
			?>
			</div>
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
