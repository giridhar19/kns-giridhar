<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/
/* DEFINES - START */define('HR_HOLIDAY_LIST_FUNC_ID','53');/* DEFINES - END */
/*
TBD: 
*/$_SESSION['module'] = 'HR';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_masters_functions.php');include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$view_perms_list   = i_get_user_perms($user,'',HR_HOLIDAY_LIST_FUNC_ID,'2','1');	$edit_perms_list   = i_get_user_perms($user,'',HR_HOLIDAY_LIST_FUNC_ID,'3','1');

	// Query String Data
	// Nothing here

	if(isset($_POST["holiday_search_submit"]))
	{
		$search_year = $_POST["ddl_year"];
		$start_date = $search_year."-01-01";
		$end_date   = $search_year."-12-31";
	}
	else
	{
		$search_year = date("Y");
		$start_date = $search_year."-01-01";
		$end_date   = $search_year."-12-31";
	}

	// Get holiday list
	$holiday_filter_data = array("date_start"=>$start_date,"date_end"=>$end_date);
	$holiday_result = i_get_holiday($holiday_filter_data);
	if($holiday_result["status"] == SUCCESS)
	{
		$holiday_list = $holiday_result["data"];
	}
	else
	{
		$alert_type = 1;
		$alert      = $holiday_result["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Holiday List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Holiday List</h3>
            </div>
            <div class="widget-header" style="height:50px; padding-top:10px;"> 						 <?php			   if($view_perms_list['status'] == SUCCESS)			   {			   ?>
			  <form method="post" id="holiday_search_form" action="hr_holiday_list.php">			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_year">
			  <option value="">- - Select Year - -</option>
			  <?php
			  for($count = date("Y"); $count > 2000; $count--)
			  {
			  ?>
			  <option value="<?php echo $count; ?>" 			  			  <?php if($search_year == $count)			  {
			  ?>
			  selected="selected"
			  <?php
			  }
			  ?>><?php echo $count; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="holiday_search_submit" />
			  </form>               <?php				}				else				{					echo 'You are not authorized to view this page';				}				?>			  
            </div>            
            <!-- /widget-header -->
            <div class="widget-content">             <?php			   if($view_perms_list['status'] == SUCCESS)			   {			   ?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>Sl No</th>
					<th>Holiday Date</th>
					<th>Holiday Name</th>						
					<th>Holiday Added By</th>
					<th>Holiday Added Date Time</th>
					<th>Holiday Type</th>
					
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					
				  </tr>
				</thead>
				<tbody>
				<?php
				$sl_no = 0;
				if($holiday_result["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($holiday_list); $count++)
					{	
						$sl_no++;
					?>
					<tr>
						<td><?php echo $sl_no; ?></td>
						<td><?php echo date("d-M-Y",strtotime($holiday_list[$count]["hr_holiday_date"])); ?></td>
						<td><?php echo $holiday_list[$count]["hr_holiday_name"]; ?></td>								
						<td><?php echo $holiday_list[$count]["user_name"]; ?></td>
						<td><?php echo date("d-M-Y",strtotime($holiday_list[$count]["hr_holiday_added_on"])); ?></td>
						<td><?php if($holiday_list[$count]["hr_holiday_type"] == "1")
							{
								$type = "Mandatory";
							}
							else if($holiday_list[$count]["hr_holiday_type"] == "2")
							{
								$type = "Optional";
							}						
							echo $type;?>
						</td>
						<td><?php if($holiday_list[$count]["hr_holiday_status"] == "1")
							{
								$action = "0";
								$text   = "Disable";
							}
							else
							{
								$action = "1";
								$text   = "Enable";
							}?>
							<?php if($edit_perms_list["status"] == SUCCESS) {?><a href="hr_holiday_enable_disable.php?holiday=<?php echo $holiday_list[$count]["hr_holiday_id"]; ?>&action=<?php echo $action; ?>"><?php echo $text; ?></a><?php } ?>
						</td>
						<td><?php if($edit_perms_list["status"] == SUCCESS) {?><a href="hr_edit_holiday.php?holiday=<?php echo $holiday_list[$count]["hr_holiday_id"]; ?>">Edit</a><?php } ?></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="8">No holidays added!</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>			  <?php				}				else				{					echo 'You are not authorized to view this page';				}				?>			  
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>
