<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 23rd Mar 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1; // No alert
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["request"]))
	{
		$request_id = $_GET["request"];
	}	
	else
	{
		$request_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["release_payment_submit"]))
	{
		$request_id    = $_POST["hd_request_id"];
		$amount        = $_POST["num_amount"];
		$mode          = $_POST["ddl_mode"];
		$remarks       = $_POST["txt_remarks"];
		$bill_sub_date = $_POST["dt_bill_date"];
		
		// Check for mandatory fields
		if(($request_id != "") && ($amount != "") && ($mode != ""))
		{
			$pay_release_result = i_add_pay_release($request_id,$amount,$mode,$remarks,$bill_sub_date,$user);
		
			if($pay_release_result["status"] == SUCCESS)
			{
				$payment_request_data = array("status"=>'4');
				$pay_request_update_result = i_update_pay_request($request_id,$payment_request_data);
				if($pay_request_update_result["status"] == SUCCESS)
				{
					$pay_request_history_result = i_add_request_history($request_id,'4',$remarks,$user);
					if($pay_request_history_result["status"] == SUCCESS)
					{
						header("location:payment_request_list.php");
					}
					else
					{
						$alert_type = 1;
						$alert      = "Payment Request status updated, but with errors. Please contact the admin!";
					}
				}
				else
				{
					$alert = "Payment Released, but with errors. Please contact the admin!";
					$alert_type = 0;
				}
			}	
			else
			{
				$alert = $pay_release_result["data"];
				$alert_type = 0;
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get list of payment requests
	$pay_request_data = array("request"=>$request_id);
	$pay_request_list = i_get_pay_request_list($pay_request_data);

	if($pay_request_list["status"] == SUCCESS)
	{
		$pay_request_list_data = $pay_request_list["data"];
		
		$amount       = $pay_request_list_data[0]["legal_payment_request_amount"];
		$task_type    = $pay_request_list_data[0]["task_type_name"];
		$process_type = $pay_request_list_data[0]["process_name"];
		$file_no      = $pay_request_list_data[0]["file_number"];
		$requested_by = $pay_request_list_data[0]["user_name"];
		
		$total_paid = 0;
		// Get total amount issued for this task
		$total_pay_request_data = array("task"=>$pay_request_list_data[0]["legal_payment_request_task"],"status"=>'4');
		$total_pay_request_list = i_get_pay_request_list($total_pay_request_data);
		if($total_pay_request_list["status"] == SUCCESS)
		{
			for($pay_count = 0; $pay_count < count ($total_pay_request_list["data"]); $pay_count++)
			{
				$total_paid = $total_paid + $total_pay_request_list["data"][$pay_count]["legal_payment_request_amount"];
			}
		}
		else
		{
			$total_paid = 0;
		}
	}
	else
	{
		$alert = $alert."Alert: ".$pay_request_list["data"];
		$amount       = "0";
		$task_type    = "<i>Invalid payment request</i>";
		$process_type = "<i>Invalid payment request</i>";
		$file_no      = "<i>Invalid payment request</i>";
		$requested_by = "<i>Invalid payment request</i>";
		$total_paid   = "<i>Invalid payment request</i>";
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Release Payment</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>  
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header" style="height:70px;padding-top:10px;">
	      				<i class="icon-user"></i>
	      				<h3>Amount Requested: <?php echo $amount; ?>&nbsp;&nbsp;&nbsp;&nbsp;Task: <?php echo $task_type; ?>&nbsp;&nbsp;&nbsp;&nbsp;Process: <?php echo $process_type; ?>&nbsp;&nbsp;&nbsp;&nbsp;File: <?php echo $file_no; ?><br /><br />Requested By: <?php echo $requested_by; ?>&nbsp;&nbsp;&nbsp;&nbsp;Amount Already Taken: <?php echo $total_paid; ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">
							You are releasing payment for this request</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="release_payment" class="form-horizontal" method="post" action="release_payment.php">
								<input type="hidden" name="hd_request_id" value="<?php echo $request_id; ?>" />								
									<fieldset>
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount</label>
											<div class="controls">
												<input type="number" name="num_amount" required class="span6" value="<?php echo $amount; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_mode">Mode</label>
											<div class="controls">
												<select name="ddl_mode" required class="span6">
												<option value="">- - Select payment Mode - -</option>
												<option value="1">Online</option>
												<option value="2">Cheque</option>
												<option value="3">DD</option>
												<option value="4">Cash</option>
												<option value="5">Other</option>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																		
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="num_amount">Bill Date</label>
											<div class="controls">
												<input type="date" name="dt_bill_date" class="span6" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="release_payment_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>