<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_payment_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Temp data
	$alert = "";

	$file_number    = "";
	$reason         = "";
	$start_date     = "";
	$end_date       = "";
	
	$start_dt_form = "";
	$end_dt_form   = "";
	
	if(($role == "1") || ($role == "2") || ($role == "12"))
	{
		$added_by = "";
	}
	else
	{
		$added_by = $user;
	}

	// Search parameters
	if(isset($_POST["pay_issued_search_submit"]))
	{
		$file_number    = $_POST["search_file_no"];							
		$reason         = $_POST["ddl_reason"];		

		// Get Legal File ID
		$legal_file_details = i_get_file_list('',$file_id,'','','','','','','','','','','','');
		if($legal_file_details['status'] == SUCCESS)
		{
			$file_id = $legal_file_details['data'][0]['file_id'];
		}
		else
		{
			$file_id = '';
		}
	}
	
	// Get list of payments done
	$pay_request_list = i_get_file_payment_list($file_id,$reason,'');

	if($pay_request_list["status"] == SUCCESS)
	{
		$pay_request_list_data = $pay_request_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$pay_request_list["data"];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Payment Issued List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Payment Issued List&nbsp;&nbsp;&nbsp;Total Cost: <span id="total_spent"><i>0</i></span></h3>			  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:100px; padding-top:10px;">               
			  <form method="post" id="pay_issued_search_form" action="bd_payment_issued_list.php">			  			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="search_file_no" value="<?php echo $file_number; ?>" placeholder="Search by BD file number" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_reason">
			  <option value="">- - Select Reason - -</option>
			  <?php
			  for($count = 0; $count < count($pay_request_reason_list); $count++)
			  {
			  ?>
			  <option value="<?php echo $pay_request_reason_list_data[$count]["payment_request_reason_id"]; ?>" <?php if($reason == $pay_request_reason_list_data[$count]["payment_request_reason_id"]) { ?> selected="selected" <?php } ?>><?php echo $pay_request_reason_list_data[$count]["payment_request_reason"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>			  		  			  
			  <input type="submit" name="pay_issued_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>File ID</th>										
					<th>Survey No</th>										
					<th>Project</th>					
					<th>Land Owner</th>
					<th>Village</th>
					<th>Extent</th>
					<th>Land Status</th>
					<th>Land Cost</th>
					<th>Process Status</th>
					<th>Paid Amount</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				 <?php
				$total_issued = 0;
				$task_array = array();
				$task_count = 0;
				if($pay_request_list["status"] == SUCCESS)
				{		
					for($count = 0; $count < count($pay_request_list_data); $count++)
					{
						if($pay_request_list_data[$count]["process_name"] != '')
						{
							$process_name_display = $pay_request_list_data[$count]["process_name"];
							$file_number_display  = $pay_request_list_data[$count]["file_number"];
							$survey_no_display    = '<b>Sy No: '.$pay_request_list_data[$count]["file_survey_number"].'</b>';
						}
						else if($pay_request_list_data[$count]["bulk_process_name"] != '')
						{
							$process_name_display = $pay_request_list_data[$count]["bulk_process_name"];
							$file_number_display = '<a href="bulk_file_list.php?bprocess='.$pay_request_list_data[$count]["legal_bulk_process_id"].'" target="_blank">Click</a>';
							$survey_no_display = '';
						}				
						
						$total_issued = $total_issued + $pay_request_list_data[$count]["legal_payment_issued_amount"];
						
						if(!(in_array($pay_request_list_data[$count]["legal_payment_request_task"],$task_array)))
						{							
							$task_array[$task_count] = $pay_request_list_data[$count]["legal_payment_request_task"];
							$task_count++;
						}
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $file_number_display; ?><br /><?php echo $survey_no_display; ?></td>
						<td style="word-wrap:break-word;"><?php echo $process_name_display; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["task_type_name"]; ?></td>								
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["legal_payment_request_amount"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["legal_payment_issued_amount"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["payment_request_reason"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["legal_payment_issued_remarks"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $pay_request_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($pay_request_list_data[$count]["legal_payment_request_added_on"])); ?></td>
						<td style="word-wrap:break-word;"><?php 
						switch($pay_request_list_data[$count]["legal_payment_request_status"])
						{
						case "1":
						$pay_status = "HOD Approval Pending";
						break;
						
						case "2":
						$pay_status = "HOD Approved. Pending with Finance";
						break;
						
						case "3":
						$pay_status = "HOD Rejected";
						break;
						
						case "4":
						$pay_status = "Payment Released";
						break;
						
						case "5":
						$pay_status = "Finance Rejected";
						break;
						
						case "6":
						$pay_status = "Finance Held";
						break;
						
						default:
						$pay_status = "HOD Approval Pending";
						break;
						}
						echo $pay_status;?></td>
						<td style="word-wrap:break-word;"><?php if(($pay_request_list_data[$count]["legal_payment_request_status"] == "1") && (($role == "1") || ($role == "2")))
						{
						?>
						<a href="update_payment_request.php?request=<?php echo $pay_request_list_data[$count]["legal_payment_request_id"]; ?>&status=2">Approve</a> / <a href="update_payment_request.php?request=<?php echo $pay_request_list_data[$count]["legal_payment_request_id"]; ?>&status=3">Reject</a>
						<?php
						}
						else if(($pay_request_list_data[$count]["legal_payment_request_status"] == "2") && (($role == "1") || ($role == "12")))						
						{
						?>
						<a href="release_payment.php?request=<?php echo $pay_request_list_data[$count]["legal_payment_request_id"]; ?>">Release</a> / <a href="update_payment_request.php?request=<?php echo $pay_request_list_data[$count]["legal_payment_request_id"]; ?>&status=5">Reject</a> / <a href="update_payment_request.php?request=<?php echo $pay_request_list_data[$count]["legal_payment_request_id"]; ?>&status=6">Hold</a>
						<?php
						}
						else
						{
							?>
							<a href="pay_request_status_list.php?request=<?php echo $pay_request_list_data[$count]["legal_payment_request_id"]; ?>" target="_blank">Details</a>
							<?php
						}
						?></td>
					</tr>
					<?php 	
					}
				}
				else
				{
				?>
				<td colspan="11">No Payment Issued yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <script>
			  document.getElementById('total_spent').innerHTML = <?php echo $total_issued; ?>;
			  document.getElementById('average_spent').innerHTML = <?php echo ($total_issued/$task_count); ?>;
			  </script>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(file_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_file_list.php";
				}
			}

			xmlhttp.open("GET", "legal_file_delete.php?file=" + file_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

</script>

  </body>

</html>
