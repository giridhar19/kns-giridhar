<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th May 2017
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	/* DATA INITIALIZATION - END */

	/* QUERY STRING DATA - START */
	// Nothing here as of now
	/* QUERY STRING DATA - END */

	// Get Project Master
	$project_management_master_search_data = array("active"=>'1');
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}	
	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Progress Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Progress Report List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>Project</th>
				    <th>Total Plan Days</th>
					<th>Actual Days </th>
					<th>Actual Percentage </th>										<th>Delay</th>
					<th>Pause Days</th>					
					<th>Remarks</th>					
				</tr>
				</thead>
				<tbody>
					<?php
					if($project_management_master_list['status'] == SUCCESS)
					{
						for($count = 0; $count < count($project_management_master_list_data); $count++)
						{
							/* Get total planned days - Start */
							$project_plan_process_search_data = array("active"=>'1',"project_id"=>$project_management_master_list_data[$count]['project_management_master_id'],"order"=>'start_date_asc',"ignore_not_started"=>'1');
							$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
							if($project_plan_process_list["status"] == SUCCESS)
							{
								$project_plan_process_list_data = $project_plan_process_list["data"];
								$project_pl_start_date = $project_plan_process_list_data[0]['project_plan_process_start_date'];
							}
							else
							{
								$project_pl_start_date = date('Y-m-d');
							}
							
							$project_plan_process_search_data = array("active"=>'1',"project_id"=>$project_management_master_list_data[$count]['project_management_master_id'],"order"=>'end_date_desc');
							$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
							if($project_plan_process_list["status"] == SUCCESS)
							{
								$project_plan_process_list_data = $project_plan_process_list["data"];
								$project_pl_end_date = $project_plan_process_list_data[0]['project_plan_process_end_date'];
							}
							else
							{
								$project_pl_end_date = date('Y-m-d');
							}
							$planned_data = get_date_diff($project_pl_start_date,$project_pl_end_date);
							$total_plan_days = $planned_data['data'] + 1;
							/* Get total planned days - End */							
							/* Get total actual days - Start */
							$project_process_task_search_data = array("active"=>'1',"project_id"=>$project_management_master_list_data[$count]['project_management_master_id'],"order"=>'start_date_asc',"ignore_not_started"=>'1');
							$project_plan_process_task_start_date_list = i_get_project_process_task($project_process_task_search_data);
							if($project_plan_process_task_start_date_list["status"] == SUCCESS)
							{
								$project_plan_process_task_list_data = $project_plan_process_task_start_date_list["data"];
								$project_actual_start_date = $project_plan_process_task_list_data[0]['project_process_actual_start_date'];
							}
							else
							{
								$project_actual_start_date = date('Y-m-d');
							}
							
							$project_process_task_search_data = array("active"=>'1',"project_id"=>$project_management_master_list_data[$count]['project_management_master_id'],"project_process_actual_end_date"=>'0000-00-00');
							$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
							if($project_plan_process_task_list["status"] == SUCCESS)
							{
								$project_actual_end_date = date('Y-m-d');
							}
							else
							{
								$project_process_task_search_data = array("active"=>'1',"project_id"=>$project_management_master_list_data[$count]['project_management_master_id'],"order"=>'end_date_desc');
								$project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
								if($project_plan_process_task_list["status"] == SUCCESS)
								{
									$project_plan_process_task_list_data = $project_plan_process_task_list["data"];
									$project_actual_end_date = $project_plan_process_task_list_data[0]['project_process_actual_end_date'];
								}
								else
								{
									$project_actual_end_date = date('Y-m-d');
								}
							}							
							$actual_data = get_date_diff($project_actual_start_date,$project_actual_end_date);
							$actual_days = $actual_data['data'] + 1;
							/* Get total actual days - End */
							
							/* Get actual percentage */							$project_process_task_search_data = array("active"=>'1',"project_id"=>$project_management_master_list_data[$count]['project_management_master_id']);							$project_plan_process_task_start_date_list = i_get_project_process_task($project_process_task_search_data);
							if($project_plan_process_task_start_date_list["status"] == SUCCESS)
							{								$total_completion_percentage = 0;
								$project_plan_process_task_list_data = $project_plan_process_task_start_date_list["data"];
								$no_of_task = count($project_plan_process_task_list_data);
								for($task_count = 0 ; $task_count < count($project_plan_process_task_list_data) ; $task_count++)
								{
									$total_completion_percentage = $total_completion_percentage + $project_plan_process_task_list_data[$task_count]["project_process_task_completion"];
								}
								$actual_percentage = $total_completion_percentage/$no_of_task;								
							}
							else
							{
								$actual_percentage = 0;
							}
							
							/* Get total pause days - Start */							// Get all tasks of this project - which are started							$no_of_paused_tasks = 0;							$total_no_of_paused_days = 0;							if($project_plan_process_task_list['status'] == SUCCESS)							{								$project_plan_process_task_list_data = $project_plan_process_task_list['data'];																for($tcount = 0; $tcount < count($project_plan_process_task_list_data); $tcount++)								{									// Get pause for each task									$delay_reason_search_data = array('task_id'=>$project_plan_process_task_list_data[$tcount]['project_process_task_id']);									$task_pause_list = i_get_project_delay_reason($delay_reason_search_data);																		if($task_pause_list['status'] == SUCCESS)									{										for($pt_count = 0; $pt_count < count($task_pause_list['data']); $pt_count++)										{											$no_of_paused_tasks++;																						if($task_pause_list['data'][$pt_count]['project_task_delay_reason_end_date'] == '0000-00-00 00:00:00')											{												// Total for still open												$tp_end_date = date('Y-m-d');											}											else											{												// Total for completed													$tp_end_date = date('Y-m-d',strtotime($task_pause_list['data'][$pt_count]['project_task_delay_reason_end_date']));											}																						$tp_start_date = date('Y-m-d',strtotime($task_pause_list['data'][$pt_count]['project_task_delay_reason_start_date']));																						$tp_diff = get_date_diff($tp_start_date,$tp_end_date);																												$total_no_of_paused_days = $total_no_of_paused_days + $tp_diff['data'] + 1;										}									}								}							}																												if($no_of_paused_tasks != 0)							{								$pause_days = round($total_no_of_paused_days/$no_of_paused_tasks,2);							}							else							{								$pause_days = '0';							}							/* Get total pause days - End */
											
							$ratio = round($actual_percentage,2)/$actual_days;
							$actual = 100/$total_plan_days;
														if(($total_plan_days -1) == 0)							{								$remarks = 'NOT PLANNED';								$delay   = '0';								$track_color = 'blue';							}
							else if($ratio < $actual) // Off track
							{
								$remarks = "OUT OF TRACK";
								$delay = $actual - $ratio;								$track_color = 'red';
							}
							else // On track
							{
								$remarks = "ON TRACK";
								$delay = 0;								$track_color = 'green';
							}
							if(($delay*$actual_days) >= 100)							{								$overshot = true;							}							else							{								$overshot = false;							}
							?>
							<tr>
								<td><?php echo $project_management_master_list_data[$count]['project_master_name']; ?></td>
								<td><?php echo $total_plan_days;?></td>
								<td><?php echo $actual_days;?></td>
								<td><?php echo round($actual_percentage,2).'%';?></td>																<td <?php if($overshot == true){ ?> style="color:red; font-weight:bold;" <?php } ?>><?php echo round((round($delay,2)*$actual_days),2).'%';?></td>
								<td><?php echo $pause_days.' days';?> (<?php echo $no_of_paused_tasks.' tasks'; ?>)</td>								
								<td style="color:<?php echo $track_color; ?>"><?php echo $remarks;?></td>
							</tr>						
					<?php
						}
					}
					else
					{
						?>
						<tr>
						<td colspan="7">No project added yet</td>
						</tr>
						<?php
					}
					?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
