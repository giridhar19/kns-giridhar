<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: receipt.php
CREATED ON	: 24-Nov-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Sales Receipt
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	if(isset($_GET['payment']))
	{
		$payment_id = $_GET['payment'];
	}
	else
	{
		header('location:crm_pending_booking_list.php');
	}
	
	// Get payment details
	$payment_sresult = i_get_payment($payment_id,'','','','');
	if($payment_sresult['status'] == SUCCESS)
	{
		$receipt_amount 	  = $payment_sresult['data'][0]['crm_payment_amount'];
		$receipt_amount_words = convert_num_to_words($receipt_amount);
		$receipt_date   	  = $payment_sresult['data'][0]['crm_payment_receipt_date'];
		if($payment_sresult['data'][0]['crm_customer_name_one'] == '')
		{
			$receipt_customer_name = '';
		}
		else
		{
			$receipt_customer_name = $payment_sresult['data'][0]['crm_customer_name_one'];
		}
		if($payment_sresult['data'][0]['crm_customer_agreement_address'] == '')
		{
			$receipt_customer_addr = '<span style="color:red;"><i>Customer Address Not Entered</i></span>';
		}
		else
		{
			$receipt_customer_addr = $payment_sresult['data'][0]['crm_customer_agreement_address'];
		}
		$receipt_towards  		= 'Site No: '.$payment_sresult['data'][0]['crm_site_no'].' , Project: '.$payment_sresult['data'][0]['project_name'];
		$receipt_pay_mode 		= $payment_sresult['data'][0]['payment_mode_name'];
		$receipt_pay_date 		= $payment_sresult['data'][0]['crm_payment_instrument_date'];
		$receipt_bank 			= $payment_sresult['data'][0]['crm_payment_bank'];
		$receipt_instrument_det = $payment_sresult['data'][0]['crm_payment_branch'];
		$receipt_remarks 		= $payment_sresult['data'][0]['crm_payment_remarks'];
		$receipt_number   		= date('Y',strtotime($payment_sresult['data'][0]['crm_payment_added_on'])).'/'.$payment_sresult['data'][0]['crm_payment_receipt_no'];
	}
	else
	{
		$receipt_amount 	    = '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_amount_words   = '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_date   	    = '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_customer_name  = '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_customer_addr  = '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_towards        = '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_pay_mode       = '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_pay_date       = '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_bank 			= '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_instrument_det = '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_remarks 		= '<span style="color:red;"><i>Invalid Payment</i></span>';
		$receipt_number         = '<span style="color:red;"><i>Invalid Payment</i></span>';
	}
}
else
{
	header('location:login.php');
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Sales Receipt</title>
</head>

<body onload="window.print();">

<div style="border:1px solid #33F; border-radius:10px; -moz-border-radius:10px; -o-border-radius:10px;">
<table width="100%" border="0" cellpadding="5">
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
    <td>CIN : U45201KA2007PTC044230</td>
  </tr>
  <tr>
    <td height="28" colspan="4" align="center"><img src="kns-logo.png" /></td>
    </tr>
  <tr>
    <td colspan="4" align="center"><h2 style="margin:0px;">K.N.S Infrastructure Pvt. Ltd.,</h2><span style="font-size:12px;">No.1125/12, 1st Floor, Service Road, Hampinagar, Vijayanagar 2nd stage, Bangalore - 560 104, Phone: 080 - 6450 5219</span><hr></td>
    </tr>
  <tr>
    <td colspan="4" align="center"></td>
    </tr>
  <tr>
    <td width="29%">No. <strong><?php echo $receipt_number; ?></strong></td>
    <td colspan="2" align="center">RECEIPT</td>
    <td width="39%"><span style="padding-left:60px;">Date <strong><?php echo date('d-M-Y',strtotime($receipt_date)); ?></strong></span></td>
  </tr>
  <tr>
    <td colspan="4">Received with thanks from Shri/Smt./M/s.&nbsp;&nbsp;&nbsp;<strong><?php echo strtoupper($receipt_customer_name); ?></strong></td>	
  </tr>  
  <tr>
    <td colspan="4">the sum of Rupees&nbsp;&nbsp;&nbsp;<strong><u><?php echo $receipt_amount_words; ?> ONLY</u></strong></td>	
  </tr>  
  <tr valign="top">
    <td colspan="4">towards&nbsp;&nbsp;&nbsp;<strong><u><?php echo strtoupper($receipt_towards); ?></u></strong></td>		
  </tr>
  <tr>
    <td colspan="4">by <strong><u><?php echo strtoupper($receipt_pay_mode); ?> (<?php echo strtoupper($receipt_bank); ?>,<?php echo $receipt_instrument_det; ?>) DATED <?php echo strtoupper(date('d-M-Y',strtotime($receipt_pay_date))); ?></u></strong></td>	
  </tr>
  <tr>
    <td colspan="4">Remarks: <?php echo $receipt_remarks; ?></td>
  </tr>
  <tr>
    <td colspan="2"><div style="border:1px solid #33F; width:90%; padding-left:10px; text-align:center;"><h2 style="font-size:1.3em;">Rs. <?php echo $receipt_amount; ?></h2></div>
	<span style="text-align:center; display:block;width:90%; font-size:12px;">(Cheque subject to realisation)</span>
	</td>
    <td width="25%" align="center">
       <br /><br /><br /><br />
      <span style="font-size:12px;">Party's Signature</span></td>
    <td align="center">
	<span style="font-size:14px;">For K.N.S Infrastructure Pvt. Ltd.,</span>
       <br /><br /><br /><br />     
      <span style="font-size:12px;">Authorised Signature</span></td>
  </tr>
</table>
</div>
<div style="height:40px; line-height:40px; text-align:center; font-size:10px;">
--------------------------------cut here------------------------------------------------cut here---------------------------------------cut here-----------------------------

</div>
<div style="border:1px solid #33F; border-radius:10px; -moz-border-radius:10px; -o-border-radius:10px;">
<table width="100%" border="0" cellpadding="5">
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
    <td>CIN : U45201KA2007PTC044230</td>
  </tr>
  <tr>
    <td height="28" colspan="4" align="center"><img src="kns-logo.png" /></td>
    </tr>
  <tr>
    <td colspan="4" align="center"><h2 style="margin:0px;">K.N.S Infrastructure Pvt. Ltd.,</h2><span style="font-size:12px;">No.1125/12, 1st Floor, Service Road, Hampinagar, Vijayanagar 2nd stage, Bangalore - 560 104, Phone: 080 - 6450 5219</span><hr></td>
    </tr>
  <tr>
    <td colspan="4" align="center"></td>
    </tr>
  <tr>
    <td width="29%">No. <strong><?php echo $receipt_number; ?></strong></td>
    <td colspan="2" align="center">RECEIPT</td>
    <td width="39%"><span style="padding-left:60px;">Date <strong><?php echo date('d-M-Y',strtotime($receipt_date)); ?></strong></span></td>
  </tr>
  <tr>
    <td colspan="4">Received with thanks from Shri/Smt./M/s. &nbsp;&nbsp;&nbsp;<strong><?php echo strtoupper($receipt_customer_name); ?></strong></td>
  </tr>
  <tr>
    <td colspan="4">the sum of Rupees&nbsp;&nbsp;&nbsp;<strong><u><?php echo $receipt_amount_words; ?> ONLY</u></strong></td>
  </tr>  
  <tr valign="top">
    <td colspan="4">towards&nbsp;&nbsp;&nbsp;<strong><u><?php echo strtoupper($receipt_towards); ?></u></strong></td>		
  </tr>
  <tr>
    <td colspan="4">by <strong><u><?php echo strtoupper($receipt_pay_mode); ?> (<?php echo strtoupper($receipt_bank); ?>,<?php echo $receipt_instrument_det; ?>) DATED <?php echo strtoupper(date('d-M-Y',strtotime($receipt_pay_date))); ?></u></strong></td>	
  </tr>
  <tr>
    <td colspan="4">Remarks: <?php echo $receipt_remarks; ?></td>
  </tr>
  <tr>
    <td colspan="2"><div style="border:1px solid #33F; width:90%; padding-left:10px; text-align:center;"><h2 style="font-size:1.3em;">Rs. <?php echo $receipt_amount; ?></h2></div>
	<span style="text-align:center; display:block;width:90%; font-size:12px;">(Cheque subject to realisation)</span>
	</td>
    <td width="25%" align="center">
       <br /><br /><br /><br />
      <span style="font-size:12px;">Party's Signature</span></td>
    <td align="center">
	<span style="font-size:14px;">For K.N.S Infrastructure Pvt. Ltd.,</span>
       <br /><br /><br /><br />     
      <span style="font-size:12px;">Authorised Signature</span></td>
  </tr>
</table>
</div>

</body>
</html>
