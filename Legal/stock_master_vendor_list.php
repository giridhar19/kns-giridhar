<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: stock_vendor_list.php

CREATED ON	: 29-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of vendor for customer withdrawals

*/



/*

TBD: 

*/
$_SESSION['module'] = 'Stock Masters';


/* DEFINES - START */
define('VENDOR_MASTER_FUNC_ID','154');
/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',VENDOR_MASTER_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',VENDOR_MASTER_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',VENDOR_MASTER_FUNC_ID,'4','1');

	$add_perms_list    = i_get_user_perms($user,'',VENDOR_MASTER_FUNC_ID,'1','1');



	// Query String Data

	// Nothing



	

	if(isset($_POST["vendor_search_submit"]))

	{

		$search_vendor      = $_POST["hd_vendor_id"];

		$search_vendor_name = $_POST["stxt_vendor"];

	}

	else

	{

		$search_vendor      = "";

		$search_vendor_name = "";

	}

	// Temp data

	//Get Venor Type of Service List

	$stock_type_of_service_search_data = array();

	$type_of_service_list = i_get_type_of_service_list($stock_type_of_service_search_data);

	if($type_of_service_list["status"] == SUCCESS)

	{

		$type_of_service_list_data = $type_of_service_list["data"];

	}

	else

	{

		$alert = $type_of_service_list["data"];

		$alert_type = 0;

	}

	

	// Get Venor modes already added

	$stock_vendor_master_search_data = array("active"=>'1',"vendor_name"=>$search_vendor_name);

	$vendor_master_list = i_get_stock_vendor_master_list($stock_vendor_master_search_data);

	if($vendor_master_list['status'] == SUCCESS)

	{

		$vendor_master_list_data = $vendor_master_list['data'];

	}	

	else

	{

		$alert = $vendor_master_list["data"];

		$alert_type = 0;

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Vendor List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">
	<link href="css/style1.css" rel="stylesheet">
   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Vendor List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="stock_master_add_vendor.php">Add Vendor</a></span><?php } ?>

            </div>

			<div class="widget-header" style="height:50px; padding-top:10px;">               

			  <form method="post" id="file_search_form" action="stock_master_vendor_list.php">

			  <input type="hidden" name="hd_vendor_id" id="hd_vendor_id" value="<?php echo $search_vendor; ?>" />

			  <span style="padding-right:20px;">

			  <input type="text" name="stxt_vendor" placeholder="Search by Vendor Name" autocomplete="off" id="stxt_vendor" onkeyup="return get_vendor_list();" value="<?php echo $search_vendor_name; ?>" />

			  <div id="search_results" class="dropdown-content"></div>

			  </span>

			  <span style="padding-left:20px; padding-right:20px;">

			  <input type="submit" name="vendor_search_submit" />

			  </span>

			  </form>			  

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			

              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

				    <th>SL No</th>	

					<th>Vendor Code</th>

					<th>Vendor Name</th>

				    <th>Address</th>

				    <th>Contact Person</th>

					<th>Contact Number</th>

					<th>Email Id</th>

					<th>Pan No</th>

					<th>Tin No</th>

					<th>Last Updated By</th>		

					<th>Last Updated On</th>	

					<th colspan="2" style="text-align:center;">Actions</th>								

				</tr>

				</thead>

				<tbody>							

				<?php

				if($vendor_master_list["status"] == SUCCESS)

								{	

                                    $sl_no = 0;							

									for($count = 0; $count < count($vendor_master_list_data); $count++)

									{

										$sl_no++;

									?>

									<tr>

									<td><?php echo $sl_no; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_master_list_data[$count]["stock_vendor_code"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_master_list_data[$count]["stock_vendor_name"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_master_list_data[$count]["stock_vendor_address"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_master_list_data[$count]["stock_vendor_contact_person"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_master_list_data[$count]["stock_vendor_contact_number"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_master_list_data[$count]["stock_vendor_email_id"]; ?></td>

						            <td style="word-wrap:break-word;"><?php echo $vendor_master_list_data[$count]["stock_vendor_pan_no"]; ?></td>

                                    <td style="word-wrap:break-word;"><?php echo $vendor_master_list_data[$count]["stock_vendor_service_contact_no"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $vendor_master_list_data[$count]["user_name"]; ?></td>	

									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($vendor_master_list_data[$count]["stock_vendor_added_on"])); ?></td>	

									<td><?php if($edit_perms_list['status'] == SUCCESS){ ?><a href="stock_master_edit_vendor.php?vendor_id=<?php echo $vendor_master_list_data[$count]["stock_vendor_id"]; ?>">Edit</a><?php } ?></td>	

									<td><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($vendor_master_list_data[$count]["stock_vendor_active"] == "1")){?><a href="#" onclick="return delete_vendor(<?php echo $vendor_master_list_data[$count]["stock_vendor_id"]; ?>);">Delete</a><?php } ?><?php } ?></td>

									</tr>

									<?php									

									}

								}

								else

								{

								?>

								<td colspan="5">No Vendor data added yet!</td>

				                <?php

				                }

				               ?>	



                  </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function delete_vendor(vendor_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_master_vendor_list.php";

					}

				}

			}



			xmlhttp.open("POST", "ajax/stock_master_delete_vendor.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("vendor_id=" + vendor_id + "&action=0");

		}

	}	

}

function get_vendor_list()

{ 

	var searchstring = document.getElementById('stxt_vendor').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{	

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/stock_get_vendor.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_vendor(vendor_id,search_vendor)

{

	document.getElementById('hd_vendor_id').value = vendor_id;

	document.getElementById('stxt_vendor').value = search_vendor;

	

	document.getElementById('search_results').style.display = 'none';

}



</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

</body>



</html>