<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 13th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1; // No alert
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}
	else
	{
		$file_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["bd_add_doc_to_file_submit"]))
	{
		$file_id   = $_POST["hd_file_id"];
		$doc_title = $_POST["stxt_doc_title"];
		$remarks   = $_POST["txt_remarks"];		
		$file_path = upload("file_doc",$user);
		
		// Check for mandatory fields
		if(($file_id !="") && ($doc_title !="") && ($file_path !=""))
		{
			$add_bd_doc_file_result = i_add_bd_file_doc($file_id,$doc_title,$file_path,$remarks,$user);
			
			if($add_bd_doc_file_result["status"] == SUCCESS)
			{
				$alert = $add_bd_doc_file_result["data"];
				$alert_type = 1;
			}
			else
			{
				$alert = $add_bd_doc_file_result["data"];
				$alert_type = 0;
			}	
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get list of BD projects
	$bd_project_list = i_get_bd_project_list('','','','');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_project_list_data = $bd_project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_project_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of villages
	$village_list = i_get_village_list('');
	if($village_list["status"] == SUCCESS)
	{
		$village_list_data = $village_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$village_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of owner status
	$owner_status_list = i_get_owner_status_list('');
	if($owner_status_list["status"] == SUCCESS)
	{
		$owner_status_list_data = $owner_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$owner_status_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of process status
	$process_status_list = i_get_process_type_list('','');
	if($process_status_list["status"] == SUCCESS)
	{
		$process_status_list_data = $process_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$process_status_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	

// Functions
function upload($file_id,$user_id) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "bd_files".DIRECTORY_SEPARATOR.$_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Upload Document to file</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Upload File</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Upload document to BD File</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_bd_doc_to_file_form" class="form-horizontal" method="post" action="bd_add_document_to_file.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_file_id" value="<?php echo $file_id; ?>" />
									<fieldset>
									
										<div class="control-group">											
											<label class="control-label" for="stxt_doc_title">Document Title *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_doc_title" placeholder="Dont use spaces. Use only / symbol" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="file_doc">Document *</label>
											<div class="controls">
												<input type="file" class="span6" name="file_doc" required="required" >
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->																														
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks"></textarea>
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="bd_add_doc_to_file_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>