<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');



/*

LIST OF TBDs

1. Wherever there is name search, there should be a wildcard

*/



/*

PURPOSE : To add new customer profile

INPUT 	: Booking ID, Names (max. of 4 names), Relationship (max. of 4 relationship details), Contact No (max. of 4 contact numbers), Email (max. of 4 emails), DOB 9max. of 4 DOBs), Anniversary (max. of 2 anniversaries), Designation, Funding Type, Bank (mandatory if loan taken), Agreement Address, Residential Address, Office Address, PAN No (max. of 4 PAN numbers), GPA Holder Name, GPA Address, GPA email, GPA Remarks, Added By

OUTPUT 	: Customer Profile id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_customer_profile($booking_id,$name_one,$relationship_one,$name_two,$relationship_two,$name_three,$relationship_three,$name_four,$relationship_four,$contact_no_one,$contact_no_two,$contact_no_three,$contact_no_four,$email_one,$email_two,$email_three,$email_four,$dob_one,$dob_two,$dob_three,$dob_four,$anniversary_one,$anniversary_two,$company,$designation,$funding_type,$bank,$agreement_address,$residential_address,$office_address,$pan_no_one,$pan_no_two,$pan_no_three,$pan_no_four,$gpa_holder,$gpa_contact_no,$gpa_address,$gpa_email,$gpa_remarks,$added_by)

{

	// Query

    $customer_profile_iquery = "insert into crm_customer_details (crm_customer_details_booking_id,crm_customer_name_one,crm_customer_relationship_one,crm_customer_name_two,crm_customer_relationship_two,crm_customer_name_three,crm_customer_relationship_three,crm_customer_name_four,crm_customer_relationship_four,crm_customer_contact_no_one,crm_customer_contact_no_two,crm_customer_contact_no_three,crm_customer_contact_no_four,crm_customer_email_id_one,crm_customer_email_id_two,crm_customer_email_id_three,crm_customer_email_id_four,crm_customer_dob_one,crm_customer_dob_two,crm_customer_dob_three,crm_customer_dob_four,crm_customer_anniversary_one,crm_customer_anniversary_two,crm_customer_company,crm_customer_designation,crm_customer_funding_type,crm_customer_loan_bank,crm_customer_agreement_address,crm_customer_residential_address,crm_customer_office_address,crm_customer_pan_no_one,crm_customer_pan_no_two,crm_customer_pan_no_three,crm_customer_pan_no_four,crm_customer_gpa_holder,crm_customer_gpa_contact_no,crm_customer_gpa_address,crm_customer_gpa_email,crm_customer_gpa_remarks,crm_cutomer_added_by,crm_customer_added_on) values (:booking_id,:name_one,:relationship_one,:name_two,:relationship_two,:name_three,:relationship_three,:name_four,:relationship_four,:contact_no_one,:contact_no_two,:contact_no_three,:contact_no_four,:email_one,:email_two,:email_three,:email_four,:dob_one,:dob_two,:dob_three,:dob_four,:anniversary_one,:anniversary_two,:company,:designation,:funding_type,:loan_bank,:agreement_address,:residential_address,:office_address,:pan_no_one,:pan_no_two,:pan_no_three,:pan_no_four,:gpa_holder,:gpa_contact_no,:gpa_address,:gpa_email,:gpa_remarks,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $customer_profile_istatement = $dbConnection->prepare($customer_profile_iquery);

        

        // Data

        $customer_profile_idata = array(':booking_id'=>$booking_id,':name_one'=>$name_one,':relationship_one'=>$relationship_one,':name_two'=>$name_two,':relationship_two'=>$relationship_two,':name_three'=>$name_three,':relationship_three'=>$relationship_three,':name_four'=>$name_four,':relationship_four'=>$relationship_four,':contact_no_one'=>$contact_no_one,':contact_no_two'=>$contact_no_two,':contact_no_three'=>$contact_no_three,':contact_no_four'=>$contact_no_four,':email_one'=>$email_one,':email_two'=>$email_two,':email_three'=>$email_three,':email_four'=>$email_four,':dob_one'=>$dob_one,':dob_two'=>$dob_two,':dob_three'=>$dob_three,':dob_four'=>$dob_four,':anniversary_one'=>$anniversary_one,':anniversary_two'=>$anniversary_two,':company'=>$company,':designation'=>$designation,':funding_type'=>$funding_type,':loan_bank'=>$bank,':agreement_address'=>$agreement_address,':residential_address'=>$residential_address,':office_address'=>$office_address,':pan_no_one'=>$pan_no_one,':pan_no_two'=>$pan_no_two,':pan_no_three'=>$pan_no_three,':pan_no_four'=>$pan_no_four,':gpa_holder'=>$gpa_holder,':gpa_contact_no'=>$gpa_contact_no,':gpa_address'=>$gpa_address,':gpa_email'=>$gpa_email,':gpa_remarks'=>$gpa_remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $customer_profile_istatement->execute($customer_profile_idata);

		$cusomer_profile_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $cusomer_profile_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get customer profile

INPUT 	: Profile ID, Booking ID, Name, Contact No, Email, DOB, Anniversary, Funding Type, PAN No, Added By, Start Date, End Date, Site Status

OUTPUT 	: List of customer profiles

BY 		: Nitin Kashyap

*/

function db_get_customer_profile($profile_id,$booking_id,$name,$contact_no,$email,$dob,$anniversary,$funding_type,$pan_no,$added_by,$start_date,$end_date,$site_status,$project_user)

{

	$get_customer_profile_squery_base = "select * from crm_customer_details CCD inner join crm_booking CB on CB.crm_booking_id = CCD.crm_customer_details_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join crm_dimension_master CDM on CDM.crm_dimension_id = CSM.crm_site_dimension inner join users U on U.user_id = CB.crm_booking_added_by left outer join crm_bank_master CBAM on CBAM.crm_bank_master_id = CCD.crm_customer_loan_bank inner join crm_status_master CSSM on CSSM.status_id = CSM.crm_site_status";		

	

	$get_customer_profile_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_customer_profile_sdata = array();

	

	if($profile_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_details_id=:profile_id";								

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_details_id=:profile_id";				

		}

		

		// Data

		$get_customer_profile_sdata[':profile_id']  = $profile_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_details_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_details_booking_id=:booking_id";				

		}

		

		// Data

		$get_customer_profile_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}		

	

	if($name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_name_one=:name or crm_customer_name_two=:name or crm_customer_name_three=:name or crm_customer_name_four=:name";								

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_name_one=:name or crm_customer_name_two=:name or crm_customer_name_three=:name or crm_customer_name_four=:name";				

		}

		

		// Data

		$get_customer_profile_sdata[':name']  = $name;

		

		$filter_count++;

	}

	

	if($contact_no != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_contact_no_one=:contact_no or crm_customer_contact_no_two=:contact_no or crm_customer_contact_no_three=:contact_no or crm_customer_contact_no_four=:contact_no";								

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_contact_no_one=:contact_no or crm_customer_contact_no_two=:contact_no or crm_customer_contact_no_three=:contact_no or crm_customer_contact_no_four=:contact_no";				

		}

		

		// Data

		$get_customer_profile_sdata[':contact_no']  = $contact_no;

		

		$filter_count++;

	}

	

	if($email != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_email_id_one=:email or crm_customer_email_id_two=:email or crm_customer_email_id_three=:email or crm_customer_email_id_four=:email";								

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_email_id_one=:email or crm_customer_email_id_two=:email or crm_customer_email_id_three=:email or crm_customer_email_id_four=:email";				

		}

		

		// Data

		$get_customer_profile_sdata[':email']  = $email;

		

		$filter_count++;

	}

	

	if($dob != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_dob_one=:dob or crm_customer_dob_two=:dob or crm_customer_dob_three=:dob or crm_customer_dob_four=:dob";								

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_dob_one=:dob or crm_customer_dob_two=:dob or crm_customer_dob_three=:dob or crm_customer_dob_four=:dob";				

		}

		

		// Data

		$get_customer_profile_sdata[':dob']  = $dob;

		

		$filter_count++;

	}

	

	if($anniversary != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_anniversary_one=:anniversary or crm_customer_anniversary_two=:anniversary";								

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_anniversary_one=:anniversary or crm_customer_anniversary_two=:anniversary";				

		}

		

		// Data

		$get_customer_profile_sdata[':anniversary']  = $anniversary;

		

		$filter_count++;

	}

	

	if($funding_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_funding_type=:funding_type";								

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_funding_type=:funding_type";				

		}

		

		// Data

		$get_customer_profile_sdata[':funding_type']  = $funding_type;

		

		$filter_count++;

	}

	

	if($pan_no != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_pan_no=:pan_no";								

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_pan_no=:pan_no";				

		}

		

		// Data

		$get_customer_profile_sdata[':pan_no']  = $pan_no;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_cutomer_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_block_request_added_by=:added_by";				

		}

		

		// Data

		$get_customer_profile_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_added_on >= :start_date";				

		}

		

		// Data

		$get_customer_profile_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where crm_customer_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and crm_customer_added_on <= :end_date";				

		}

		

		// Data

		$get_customer_profile_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	if($site_status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where CSM.crm_site_status = :site_status";						

		}

		else

		{

			// Query

			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and CSM.crm_site_status = :site_status";				

		}

		

		// Data

		$get_customer_profile_sdata[':site_status']  = $site_status;

		

		$filter_count++;

	}
	if($project_user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_customer_profile_squery_where = $get_customer_profile_squery_where." where CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";								
		}
		else
		{
			// Query
			$get_customer_profile_squery_where = $get_customer_profile_squery_where." and CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";				
		}

		// Data
		$get_customer_profile_sdata[':project_user_id']  = $project_user;

		$filter_count++;
	}
	

	$get_customer_profile_squery = $get_customer_profile_squery_base.$get_customer_profile_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_customer_profile_sstatement = $dbConnection->prepare($get_customer_profile_squery);

		

		$get_customer_profile_sstatement -> execute($get_customer_profile_sdata);

		

		$get_customer_profile_sdetails = $get_customer_profile_sstatement -> fetchAll();

		

		if(FALSE === $get_customer_profile_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_customer_profile_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_customer_profile_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update customer details

INPUT 	: Cust Profile ID, Name (1 to 4), Relationship (1 to 4), Contact Number (1 to 4), Email (1 to 4), DOB (1 to 4), Anniversary (1 to 2), Company, Designation, Funding Type, Bank (for loan), Agreement Address, Residential Address, Office Address, PAN No (1 to 4), GPA Holder, GPA Contact No, GPA Address, GPA Email, GPA Remarks

OUTPUT 	: Cust Profile ID; Message of success or failure

BY 		: Nitin Kashyap

*/

function db_update_cust_details($cust_details_id,$name_one,$relationship_one,$name_two,$relationship_two,$name_three,$relationship_three,$name_four,$relationship_four,$contact_one,$contact_two,$contact_three,$contact_four,$email_one,$email_two,$email_three,$email_four,$dob_one,$dob_two,$dob_three,$dob_four,$anniversary_one,$anniversary_two,$company,$designation,$funding_type,$bank,$agreement_address,$residential_address,$office_address,$pan_no_one,$pan_no_two,$pan_no_three,$pan_no_four,$gpa_holder,$gpa_contact_no,$gpa_address,$gpa_email,$gpa_remarks)

{

	// Query

    $cust_details_uquery_base = "update crm_customer_details set";  

	

	$cust_details_uquery_set = "";

	

	$cust_details_uquery_where = " where crm_customer_details_id=:cust_details_id";

	

	$cust_details_udata = array(":cust_details_id"=>$cust_details_id);

	

	$filter_count = 0;

	

	if($name_one != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_name_one=:name_one,";

		$cust_details_udata[":name_one"] = $name_one;

		$filter_count++;

	}

	

	if($relationship_one != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_relationship_one=:relationship_one,";

		$cust_details_udata[":relationship_one"] = $relationship_one;

		$filter_count++;

	}

	

	if($name_two != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_name_two=:name_two,";

		$cust_details_udata[":name_two"] = $name_two;

		$filter_count++;

	}

	

	if($relationship_two != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_relationship_two=:relationship_two,";

		$cust_details_udata[":relationship_two"] = $relationship_two;

		$filter_count++;

	}

	

	if($name_three != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_name_three=:name_three,";

		$cust_details_udata[":name_three"] = $name_three;

		$filter_count++;

	}

	

	if($relationship_three != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_relationship_three=:relationship_three,";

		$cust_details_udata[":relationship_three"] = $relationship_three;

		$filter_count++;

	}

	

	if($name_four != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_name_four=:name_four,";

		$cust_details_udata[":name_four"] = $name_four;

		$filter_count++;

	}

	

	if($relationship_four != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_relationship_four=:relationship_four,";

		$cust_details_udata[":relationship_four"] = $relationship_four;

		$filter_count++;

	}

	

	if($contact_one != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_contact_no_one=:contact_one,";

		$cust_details_udata[":contact_one"] = $contact_one;

		$filter_count++;

	}

	

	if($contact_two != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_contact_no_two=:contact_two,";

		$cust_details_udata[":contact_two"] = $contact_two;

		$filter_count++;

	}

	

	if($contact_three != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_contact_no_three=:contact_three,";

		$cust_details_udata[":contact_three"] = $contact_three;

		$filter_count++;

	}

	

	if($contact_four != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_contact_no_four=:contact_four,";

		$cust_details_udata[":contact_four"] = $contact_four;

		$filter_count++;

	}

	

	if($email_one != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_email_id_one=:email_one,";

		$cust_details_udata[":email_one"] = $email_one;

		$filter_count++;

	}

	

	if($email_two != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_email_id_two=:email_two,";

		$cust_details_udata[":email_two"] = $email_two;

		$filter_count++;

	}

	

	if($email_three != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_email_id_three=:email_three,";

		$cust_details_udata[":email_three"] = $email_three;

		$filter_count++;

	}

	

	if($email_four != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_email_id_four=:email_four,";

		$cust_details_udata[":email_four"] = $email_four;

		$filter_count++;

	}

	

	if($dob_one != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_dob_one=:dob_one,";

		$cust_details_udata[":dob_one"] = $dob_one;

		$filter_count++;

	}

	

	if($dob_two != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_dob_two=:dob_two,";

		$cust_details_udata[":dob_two"] = $dob_two;

		$filter_count++;

	}

	

	if($dob_three != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_dob_three=:dob_three,";

		$cust_details_udata[":dob_three"] = $dob_three;

		$filter_count++;

	}

	

	if($dob_four != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_dob_four=:dob_four,";

		$cust_details_udata[":dob_four"] = $dob_four;

		$filter_count++;

	}

	

	if($anniversary_one != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_anniversary_one=:anniversary_one,";

		$cust_details_udata[":anniversary_one"] = $anniversary_one;

		$filter_count++;

	}

	

	if($anniversary_two != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_anniversary_two=:anniversary_two,";

		$cust_details_udata[":anniversary_two"] = $anniversary_two;

		$filter_count++;

	}

	

	if($company != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_company=:company,";

		$cust_details_udata[":company"] = $company;

		$filter_count++;

	}

	

	if($designation != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_designation=:designation,";

		$cust_details_udata[":designation"] = $designation;

		$filter_count++;

	}

	

	if($funding_type != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_funding_type=:funding_type,";

		$cust_details_udata[":funding_type"] = $funding_type;

		$filter_count++;

	}

	

	if($bank != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_loan_bank=:bank,";

		$cust_details_udata[":bank"] = $bank;

		$filter_count++;

	}

	

	if($agreement_address != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_agreement_address=:agreement_address,";

		$cust_details_udata[":agreement_address"] = $agreement_address;

		$filter_count++;

	}

	

	if($residential_address != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_residential_address=:residential_address,";

		$cust_details_udata[":residential_address"] = $residential_address;

		$filter_count++;

	}

	

	if($office_address != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_office_address=:office_address,";

		$cust_details_udata[":office_address"] = $office_address;

		$filter_count++;

	}

	

	if($pan_no_one != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_pan_no_one=:pan_no_one,";

		$cust_details_udata[":pan_no_one"] = $pan_no_one;

		$filter_count++;

	}

	

	if($pan_no_two != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_pan_no_two=:pan_no_two,";

		$cust_details_udata[":pan_no_two"] = $pan_no_two;

		$filter_count++;

	}

	

	if($pan_no_three != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_pan_no_three=:pan_no_three,";

		$cust_details_udata[":pan_no_three"] = $pan_no_three;

		$filter_count++;

	}

	

	if($pan_no_four != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_pan_no_four=:pan_no_four,";

		$cust_details_udata[":pan_no_four"] = $pan_no_four;

		$filter_count++;

	}

	

	if($gpa_holder != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_gpa_holder=:gpa_holder,";

		$cust_details_udata[":gpa_holder"] = $gpa_holder;

		$filter_count++;

	}

	

	if($gpa_contact_no != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_gpa_contact_no=:gpa_contact_no,";

		$cust_details_udata[":gpa_contact_no"] = $gpa_contact_no;

		$filter_count++;

	}

	

	if($gpa_address != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_gpa_address=:gpa_address,";

		$cust_details_udata[":gpa_address"] = $gpa_address;

		$filter_count++;

	}

	

	if($gpa_email != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_gpa_email=:gpa_email,";

		$cust_details_udata[":gpa_email"] = $gpa_email;

		$filter_count++;

	}

	

	if($gpa_remarks != "")

	{

		$cust_details_uquery_set = $cust_details_uquery_set." crm_customer_gpa_remarks=:gpa_remarks,";

		$cust_details_udata[":gpa_remarks"] = $gpa_remarks;

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$cust_details_uquery_set = trim($cust_details_uquery_set,',');

	}

	

	$cust_details_uquery = $cust_details_uquery_base.$cust_details_uquery_set.$cust_details_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();

        

        $cust_details_ustatement = $dbConnection->prepare($cust_details_uquery);		

        

        $cust_details_ustatement -> execute($cust_details_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $cust_details_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add payment schedule

INPUT 	: Booking ID, Date, Amount, Milestone, Remarks, Added By

OUTPUT 	: Payment Schedule id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_payment_schedule($booking_id,$date,$amount,$milestone,$remarks,$added_by)

{

	// Query

    $pay_schedule_iquery = "insert into crm_payment_schedule (crm_payment_schedule_booking_id,crm_payment_schedule_date,crm_payment_schedule_amount,crm_payment_schedule_milestone,crm_payment_schedule_remarks,crm_payment_schedule_added_by,crm_payment_schedule_added_on) values (:booking_id,:date,:amount,:milestone,:remarks,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $pay_schedule_istatement = $dbConnection->prepare($pay_schedule_iquery);

        

        // Data

        $pay_schedule_idata = array(':booking_id'=>$booking_id,':date'=>$date,':amount'=>$amount,':milestone'=>$milestone,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $pay_schedule_istatement->execute($pay_schedule_idata);

		$pay_schedule_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $pay_schedule_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To add payment follow up

INPUT 	: Booking ID, Date Time, Notes, Type, Added By

OUTPUT 	: Payment Follow Up ID, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_payment_follow_up($booking_id,$date_time,$notes,$type,$added_by)

{

	// Query

    $pay_fup_iquery = "insert into crm_payment_follow_up (crm_payment_follow_up_booking_id,crm_payment_follow_up_date_time,crm_payment_follow_up_notes,crm_payment_follow_up_cust_remarks,crm_payment_follow_up_type,crm_payment_follow_up_added_by,crm_payment_follow_up_added_on,crm_payment_follow_up_updated_by,crm_payment_follow_up_updated_on) values (:booking_id,:date_time,:notes,:remarks,:type,:added_by,:now,:updated_by,:updated_time)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $pay_fup_istatement = $dbConnection->prepare($pay_fup_iquery);

        

        // Data

        $pay_fup_idata = array(':booking_id'=>$booking_id,':date_time'=>$date_time,':notes'=>$notes,':remarks'=>'',':type'=>$type,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"),':updated_by'=>'',':updated_time'=>'');		

		$dbConnection->beginTransaction();

        $pay_fup_istatement->execute($pay_fup_idata);

		$pay_fup_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $pay_fup_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To add payment

INPUT 	: Booking ID, Receipt No, Amount, TDS, Mode, Instrument Date, bank, Branch, receipt Date, Remarks, Added By

OUTPUT 	: Payment id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_payment($booking_id,$receipt_no,$amount,$milestone,$tds,$mode,$instrument_date,$bank,$branch,$receipt_date,$remarks,$added_by)

{

	// Query

    $payment_iquery = "insert into crm_payment (crm_payment_booking_id,crm_payment_receipt_no,crm_payment_amount,crm_payment_milestone,crm_payment_tds_amount,crm_payment_payment_mode,crm_payment_instrument_date,crm_payment_bank,crm_payment_branch,crm_payment_receipt_date,crm_payment_remarks,crm_payment_added_by,crm_payment_added_on) values (:booking_id,:receipt_no,:amount,:milestone,:tds,:mode,:instru_date,:bank,:branch,:receipt_date,:remarks,:added_by,:now)";  

    try

    {

        $dbConnection = get_conn_handle();

        

        $payment_istatement = $dbConnection->prepare($payment_iquery);

        

        // Data

        $payment_idata = array(':booking_id'=>$booking_id,':receipt_no'=>$receipt_no,':amount'=>$amount,':milestone'=>$milestone,':tds'=>$tds,':mode'=>$mode,':instru_date'=>$instrument_date,':bank'=>$bank,':branch'=>$branch,':receipt_date'=>$receipt_date,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $payment_istatement->execute($payment_idata);

		$payment_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $payment_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To add payment track

INPUT 	: Payment ID, Status, Remarks, Added By

OUTPUT 	: Payment Track id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_payment_track($payment_id,$status,$remarks,$added_by)

{

	// Query

    $payment_track_iquery = "insert into crm_payment_track (crm_payment_id,crm_payment_track_status,crm_payment_track_remarks,crm_payment_track_added_by,crm_payment_track_added_on) values (:payment_id,:status,:remarks,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $payment_track_istatement = $dbConnection->prepare($payment_track_iquery);

        

        // Data

        $payment_track_idata = array(':payment_id'=>$payment_id,':status'=>$status,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $payment_track_istatement->execute($payment_track_idata);

		$payment_track_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $payment_track_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get payment schedules

INPUT 	: Booking ID, Payment Start Date, Payment End Date, Added By, Start Date, End Date, Milestone, Order, Schedule ID, Project

OUTPUT 	: List of payment schedules

BY 		: Nitin Kashyap

*/

function db_get_pay_schedule($booking_id,$pay_start_date,$pay_end_date,$added_by,$start_date,$end_date,$milestone='',$order='',$schedule_id='',$project='')

{

	$get_pay_schedule_squery_base = "select * from crm_payment_schedule CPS inner join crm_booking CB on CB.crm_booking_id = CPS.crm_payment_schedule_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id";		

	

	$get_pay_schedule_squery_where = "";



	$get_pay_schedule_squery_order = "";	

	

	$filter_count = 0;

	

	// Data

	$get_pay_schedule_sdata = array();

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." where crm_payment_schedule_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." and crm_payment_schedule_booking_id=:booking_id";				

		}

		

		// Data

		$get_pay_schedule_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}



	if($schedule_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." where crm_payment_schedule_id = :schedule_id";					

		}

		else

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." and crm_payment_schedule_id = :schedule_id";				

		}

		

		// Data

		$get_pay_schedule_sdata[':schedule_id']  = $schedule_id;

		

		$filter_count++;

	}

	

	if($pay_start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." where crm_payment_schedule_date >= :pay_start_date";								

		}

		else

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." and crm_payment_schedule_date >= :pay_start_date";				

		}

		

		// Data

		$get_pay_schedule_sdata[':pay_start_date']  = $pay_start_date;

		

		$filter_count++;

	}

	

	if($pay_end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." where crm_payment_schedule_date <= :pay_end_date";								

		}

		else

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." and crm_payment_schedule_date <= :pay_end_date";				

		}

		

		// Data

		$get_pay_schedule_sdata[':pay_end_date']  = $pay_end_date;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." where crm_payment_schedule_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." and crm_payment_schedule_added_by=:added_by";				

		}

		

		// Data

		$get_pay_schedule_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." where crm_payment_schedule_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." and crm_payment_schedule_added_on >= :start_date";				

		}

		

		// Data

		$get_pay_schedule_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." where crm_payment_schedule_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." and crm_payment_schedule_added_on <= :end_date";				

		}

		

		// Data

		$get_pay_schedule_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	if($milestone != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." where crm_payment_schedule_milestone = :milestone";						

		}

		else

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." and crm_payment_schedule_milestone = :milestone";				

		}

		

		// Data

		$get_pay_schedule_sdata[':milestone']  = $milestone;

		

		$filter_count++;

	}

	if($project != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." where CSM.crm_project_id = :project";						

		}

		else

		{

			// Query

			$get_pay_schedule_squery_where = $get_pay_schedule_squery_where." and CSM.crm_project_id = :project";				

		}

		

		// Data

		$get_pay_schedule_sdata[':project']  = $project;

		

		$filter_count++;

	}
	

	if($order == 'date_desc')

	{

		$get_pay_schedule_squery_order = ' order by crm_payment_schedule_date desc';

	}

	$get_pay_schedule_squery = $get_pay_schedule_squery_base.$get_pay_schedule_squery_where.$get_pay_schedule_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_pay_schedule_sstatement = $dbConnection->prepare($get_pay_schedule_squery);

		

		$get_pay_schedule_sstatement -> execute($get_pay_schedule_sdata);

		

		$get_pay_schedule_sdetails = $get_pay_schedule_sstatement -> fetchAll();

		

		if(FALSE === $get_pay_schedule_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_pay_schedule_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_pay_schedule_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update payment schedule

INPUT 	: Payment Schedule ID, Payment Schedule Data Array

OUTPUT 	: Payment Schedule ID; Message of success or failure

BY 		: Nitin Kashyap

*/

function db_update_payment_schedule_details($payment_schedule_id,$payment_schedule_data)

{

	if(array_key_exists("date",$payment_schedule_data))

	{	

		$date = $payment_schedule_data["date"];

	}

	else

	{

		$date = "";

	}

	

	if(array_key_exists("amount",$payment_schedule_data))

	{	

		$amount = $payment_schedule_data["amount"];

	}

	else

	{

		$amount = "";

	}		

	

	if(array_key_exists("milestone",$payment_schedule_data))

	{	

		$milestone = $payment_schedule_data["milestone"];

	}

	else

	{

		$milestone = "";

	}



	// Query

    $payment_schedule_uquery_base = "update crm_payment_schedule set";  

	

	$payment_schedule_uquery_set = "";

	

	$payment_schedule_uquery_where = " where crm_payment_schedule_id = :payment_schedule_id";

	

	$payment_schedule_udata = array(":payment_schedule_id"=>$payment_schedule_id);

	

	$filter_count = 0;

	

	if($date != "")

	{

		$payment_schedule_uquery_set = $payment_schedule_uquery_set." crm_payment_schedule_date = :date,";

		$payment_schedule_udata[":date"] = $date;

		$filter_count++;

	}

	

	if($amount != "")

	{

		$payment_schedule_uquery_set = $payment_schedule_uquery_set." crm_payment_schedule_amount = :amount,";

		$payment_schedule_udata[":amount"] = $amount;

		$filter_count++;

	}

	

	if($milestone != "")

	{

		$payment_schedule_uquery_set = $payment_schedule_uquery_set." crm_payment_schedule_milestone = :milestone,";

		$payment_schedule_udata[":milestone"] = $milestone;

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$payment_schedule_uquery_set = trim($payment_schedule_uquery_set,',');

	}

	

	$payment_schedule_uquery = $payment_schedule_uquery_base.$payment_schedule_uquery_set.$payment_schedule_uquery_where;	



    try

    {

        $dbConnection = get_conn_handle();

        

        $payment_schedule_ustatement = $dbConnection->prepare($payment_schedule_uquery);		

        

        $payment_schedule_ustatement -> execute($payment_schedule_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $payment_schedule_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add payment terms

INPUT 	: Booking ID, Maintenance Charges, Water Charges, Club House Charges, Documentation Charges, Other Charges, Added By

OUTPUT 	: Payment Terms id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_payment_terms($booking_id,$main_charges,$water_charges,$club_house_charges,$documentation_charges,$other_charges,$added_by)

{

	// Query

    $payment_terms_iquery = "insert into crm_payment_terms (crm_pt_booking_id,crm_pt_layout_maintenance_charges,crm_pt_water_charges,crm_pt_club_house_charges,crm_pt_documentation_charges,crm_pt_other_charges,crm_pt_added_by,crm_pt_added_on) values (:booking_id,:main_charges,:water_charges,:club_house_charges,:documentation_charges,:other_charges,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $payment_terms_istatement = $dbConnection->prepare($payment_terms_iquery);

        

        // Data

        $payment_terms_idata = array(':booking_id'=>$booking_id,':main_charges'=>$main_charges,':water_charges'=>$water_charges,':club_house_charges'=>$club_house_charges,':documentation_charges'=>$documentation_charges,':other_charges'=>$other_charges,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $payment_terms_istatement->execute($payment_terms_idata);

		$payment_terms_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $payment_terms_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get payment terms

INPUT 	: Booking ID, Added By, Start Date, End Date

OUTPUT 	: List of payment terms

BY 		: Nitin Kashyap

*/

function db_get_payment_terms($booking_id,$added_by,$start_date,$end_date)

{

	$get_payment_terms_squery_base = "select * from crm_payment_terms";		

	

	$get_payment_terms_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_payment_terms_sdata = array();

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_terms_squery_where = $get_payment_terms_squery_where." where crm_pt_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_payment_terms_squery_where = $get_payment_terms_squery_where." and crm_pt_booking_id=:booking_id";				

		}

		

		// Data

		$get_payment_terms_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}		

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_terms_squery_where = $get_payment_terms_squery_where." where crm_pt_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_payment_terms_squery_where = $get_payment_terms_squery_where." and crm_pt_added_by=:added_by";				

		}

		

		// Data

		$get_payment_terms_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_terms_squery_where = $get_payment_terms_squery_where." where crm_pt_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_payment_terms_squery_where = $get_payment_terms_squery_where." and crm_pt_added_on >= :start_date";				

		}

		

		// Data

		$get_payment_terms_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_terms_squery_where = $get_payment_terms_squery_where." where crm_pt_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_payment_terms_squery_where = $get_payment_terms_squery_where." and crm_pt_added_on <= :end_date";				

		}

		

		// Data

		$get_payment_terms_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_payment_terms_squery_order = " order by crm_pt_added_on desc";

	$get_payment_terms_squery = $get_payment_terms_squery_base.$get_payment_terms_squery_where.$get_payment_terms_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_payment_terms_sstatement = $dbConnection->prepare($get_payment_terms_squery);

		

		$get_payment_terms_sstatement -> execute($get_payment_terms_sdata);

		

		$get_payment_terms_sdetails = $get_payment_terms_sstatement -> fetchAll();

		

		if(FALSE === $get_payment_terms_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_payment_terms_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_payment_terms_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get payment

INPUT 	: Payment ID, Booking ID, Mode, Received Start Date, Received End Date, Added By, Start Date, End Date, Project

OUTPUT 	: List of payments

BY 		: Nitin Kashyap

*/

function db_get_payment($payment_id,$booking_id,$mode,$rec_start_date,$rec_end_date,$added_by,$start_date,$end_date,$project='')

{

	$get_payments_squery_base = "select * from crm_payment CP inner join crm_booking CB on CB.crm_booking_id = CP.crm_payment_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join payment_mode_master PMM on PMM.payment_mode_id = CP.crm_payment_payment_mode left outer join crm_customer_details CCD on CCD.crm_customer_details_booking_id = CB.crm_booking_id";		

	

	$get_payments_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_payments_sdata = array();

	

	if($payment_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_id=:payment_id";					

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_id=:payment_id";				

		}

		

		// Data

		$get_payments_sdata[':payment_id']  = $payment_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_booking_id=:booking_id";				

		}

		

		// Data

		$get_payments_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}



	if($mode != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_payment_mode=:mode";					

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_payment_mode=:mode";				

		}

		

		// Data

		$get_payments_sdata[':mode']  = $mode;

		

		$filter_count++;

	}

	

	if($rec_start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_receipt_date >= :rec_start_date";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_receipt_date >= :rec_start_date";				

		}

		

		// Data

		$get_payments_sdata[':rec_start_date']  = $rec_start_date;

		

		$filter_count++;

	}



	if($rec_end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_receipt_date <= :rec_end_date";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_receipt_date <= :rec_end_date";				

		}

		

		// Data

		$get_payments_sdata[':rec_end_date']  = $rec_end_date;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_added_by=:added_by";				

		}

		

		// Data

		$get_payments_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_added_on >= :start_date";				

		}

		

		// Data

		$get_payments_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_added_on <= :end_date";				

		}

		

		// Data

		$get_payments_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	if($project != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where CSM.crm_project_id = :project";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and CSM.crm_project_id = :project";				

		}

		

		// Data

		$get_payments_sdata[':project']  = $project;

		

		$filter_count++;

	}
	

	$get_payments_squery_order = " order by cast(crm_payment_receipt_no as signed) desc";

		

	$get_payments_squery = $get_payments_squery_base.$get_payments_squery_where.$get_payments_squery_order;		

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_payments_sstatement = $dbConnection->prepare($get_payments_squery);

		

		$get_payments_sstatement -> execute($get_payments_sdata);

		

		$get_payment_sdetails = $get_payments_sstatement -> fetchAll();

		

		if(FALSE === $get_payment_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_payment_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_payment_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get payment total

INPUT 	: Payment ID, Booking ID, Mode, Received Start Date, Received End Date, Added By, Start Date, End Date

OUTPUT 	: List of payment total

BY 		: Nitin Kashyap

*/

function db_get_payment_total($payment_id,$booking_id,$mode,$rec_start_date,$rec_end_date,$added_by,$start_date,$end_date)

{

	$get_payments_squery_base = "select sum(crm_payment_amount) as paid from crm_payment CP inner join crm_booking CB on CB.crm_booking_id = CP.crm_payment_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join payment_mode_master PMM on PMM.payment_mode_id = CP.crm_payment_payment_mode";		

	

	$get_payments_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_payments_sdata = array();

	

	if($payment_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_id=:booking_id";					

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_id=:booking_id";				

		}

		

		// Data

		$get_payments_sdata[':payment_id']  = $payment_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_booking_id=:booking_id";				

		}

		

		// Data

		$get_payments_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}



	if($mode != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_payment_mode=:mode";					

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_payment_mode=:mode";				

		}

		

		// Data

		$get_payments_sdata[':mode']  = $mode;

		

		$filter_count++;

	}

	

	if($rec_start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_receipt_date >= :rec_start_date";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_receipt_date >= :rec_start_date";				

		}

		

		// Data

		$get_payments_sdata[':rec_start_date']  = $rec_start_date;

		

		$filter_count++;

	}



	if($rec_end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_receipt_date <= :rec_end_date";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_receipt_date <= :rec_end_date";				

		}

		

		// Data

		$get_payments_sdata[':rec_end_date']  = $rec_end_date;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_added_by=:added_by";				

		}

		

		// Data

		$get_payments_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_added_on >= :start_date";				

		}

		

		// Data

		$get_payments_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." where crm_payment_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_payments_squery_where = $get_payments_squery_where." and crm_payment_added_on <= :end_date";				

		}

		

		// Data

		$get_payments_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_payments_squery = $get_payments_squery_base.$get_payments_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_payments_sstatement = $dbConnection->prepare($get_payments_squery);

		

		$get_payments_sstatement -> execute($get_payments_sdata);

		

		$get_payment_sdetails = $get_payments_sstatement -> fetchAll();

		

		if(FALSE === $get_payment_sdetails)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else if(count($get_payment_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_payment_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get payment follow up

INPUT 	: Follow Up ID, Booking ID, FUP Start, FUP End, Added By, Start Date, End Date, Work Start Date, Work End Date (Work date means either added date or updated date), Updated By, Show No Follow Up, Sort Order

OUTPUT 	: List of payment follow up

BY 		: Nitin Kashyap

*/

function db_get_payment_follow_up($fup_id,$booking_id,$fup_start,$fup_end,$added_by,$start_date,$end_date,$work_start_date='',$work_end_date='',$updated_by='',$show_no_fup=false,$order='')

{

	$get_payment_fup_squery_base = "select *,AU.user_name as adder,U.user_name as updater from crm_payment_follow_up CPFU inner join crm_booking CB on CB.crm_booking_id = CPFU.crm_payment_follow_up_booking_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id left outer join users U on U.user_id = CPFU.crm_payment_follow_up_updated_by inner join users AU on AU.user_id = CPFU.crm_payment_follow_up_added_by";		

	

	$get_payment_fup_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_payment_fup_sdata = array();

	

	if($fup_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_id=:fup_id";								

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_id=:fup_id";				

		}

		

		// Data

		$get_payment_fup_sdata[':fup_id']  = $fup_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_booking_id=:booking_id";				

		}

		

		// Data

		$get_payment_fup_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}



	if($fup_start != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_date_time >= :fup_start";						

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_date_time >= :fup_start";				

		}

		

		// Data

		$get_payment_fup_sdata[':fup_start']  = $fup_start;

		

		$filter_count++;

	}



	if($fup_end != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_date_time <= :fup_end";						

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_date_time <= :fup_end";				

		}

		

		// Data

		$get_payment_fup_sdata[':fup_end']  = $fup_end;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_added_by=:added_by";				

		}

		

		// Data

		$get_payment_fup_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_added_on >= :start_date";				

		}

		

		// Data

		$get_payment_fup_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_added_on <= :end_date";				

		}

		

		// Data

		$get_payment_fup_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	if($updated_by != "")

	{

		if($filter_count == 0)

		{

			if($show_no_fup == true)

			{

				// Query

				$get_payment_fup_squery_where = $get_payment_fup_squery_where." where (crm_payment_follow_up_updated_by=:updated_by or crm_payment_follow_up_updated_by='')";						

			}

			else

			{

				$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_updated_by=:updated_by";

			}

		}

		else

		{

			if($show_no_fup == true)

			{

				// Query

				$get_payment_fup_squery_where = $get_payment_fup_squery_where." and (crm_payment_follow_up_updated_by=:updated_by or crm_payment_follow_up_updated_by='')";				

			}

			else

			{

				$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_updated_by=:updated_by";

			}

		}

		

		// Data

		$get_payment_fup_sdata[':updated_by']  = $updated_by;

		

		$filter_count++;

	}

	

	if($show_no_fup == true)

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_updated_by=''";						

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." or crm_payment_follow_up_updated_by=''";				

		}

		

		$filter_count++;

	}

	

	if(($work_start_date != "") && ($work_end_date != ""))

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_updated_on >= :work_start_date and crm_payment_follow_up_updated_on <= :work_end_date";						

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_updated_on >= :work_start_date and crm_payment_follow_up_updated_on <= :work_end_date";				

		}

		

		// Data

		$get_payment_fup_sdata[':work_start_date']  = $work_start_date;

		$get_payment_fup_sdata[':work_end_date']  = $work_end_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." where crm_payment_follow_up_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_payment_fup_squery_where = $get_payment_fup_squery_where." and crm_payment_follow_up_added_on <= :end_date";				

		}

		

		// Data

		$get_payment_fup_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	if($order == 'fup_date_asc')

	{

		$get_payment_fup_squery_order = " order by crm_payment_follow_up_date_time asc";

	}

	else if($order == 'added_date_asc')

	{

		$get_payment_fup_squery_order = " order by crm_payment_follow_up_added_on asc";

	}

	else

	{

		$get_payment_fup_squery_order = " order by crm_payment_follow_up_added_on desc";

	}

	

	$get_payment_fup_squery = $get_payment_fup_squery_base.$get_payment_fup_squery_where.$get_payment_fup_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_payment_fup_sstatement = $dbConnection->prepare($get_payment_fup_squery);

		

		$get_payment_fup_sstatement -> execute($get_payment_fup_sdata);

		

		$get_payment_fup_sdetails = $get_payment_fup_sstatement -> fetchAll();

		

		if(FALSE === $get_payment_fup_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_payment_fup_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_payment_fup_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update payment follow up details

INPUT 	: Follow Up ID, Remarks, Updated By, Date, Notes

OUTPUT 	: Payment FUP ID; Message of success or failure

BY 		: Nitin Kashyap

*/

function db_update_payment_fup_details($fup_id,$remarks,$updated_by,$date='',$notes='')

{

	// Query

    $payment_fup_uquery_base = "update crm_payment_follow_up set";  

	

	$payment_fup_uquery_set = "";

	

	$payment_fup_uquery_where = " where crm_payment_follow_up_id=:payment_fup_id";

	

	$payment_fup_udata = array(":payment_fup_id"=>$fup_id);

	

	$filter_count = 0;

	

	if($remarks != "")

	{

		$payment_fup_uquery_set = $payment_fup_uquery_set." crm_payment_follow_up_cust_remarks=:remarks,";

		$payment_fup_udata[":remarks"] = $remarks;

		$filter_count++;

	}

	

	if($date != "")

	{

		$payment_fup_uquery_set = $payment_fup_uquery_set." crm_payment_follow_up_date_time = :date,";

		$payment_fup_udata[":date"] = $date;

		$filter_count++;

	}

	

	if($notes != "")

	{

		$payment_fup_uquery_set = $payment_fup_uquery_set." crm_payment_follow_up_notes = :notes,";

		$payment_fup_udata[":notes"] = $notes;

		$filter_count++;

	}

	

	if($updated_by != "")

	{

		$payment_fup_uquery_set = $payment_fup_uquery_set." crm_payment_follow_up_updated_by=:updated_by, crm_payment_follow_up_updated_on=:updated_on ";

		$payment_fup_udata[":updated_by"] = $updated_by;

		$payment_fup_udata[":updated_on"] = date("Y-m-d H:i:s");

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$payment_fup_uquery_set = trim($payment_fup_uquery_set,',');

	}

	

	$payment_fup_uquery = $payment_fup_uquery_base.$payment_fup_uquery_set.$payment_fup_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();

        

        $payment_fup_ustatement = $dbConnection->prepare($payment_fup_uquery);		

        

        $payment_fup_ustatement -> execute($payment_fup_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $fup_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To get payment track

INPUT 	: Payment Track ID, Payment ID, Booking ID, Tracking Status, Added By, Start Date, End Date

OUTPUT 	: List of payment track

BY 		: Nitin Kashyap

*/

function db_get_payment_track($pay_track_id,$payment_id,$booking_id,$track_status,$added_by,$start_date,$end_date)

{

	$get_payment_track_squery_base = "select * from crm_payment_track CPT inner join crm_payment CP on CP.crm_payment_id = CPT.crm_payment_id inner join users U on U.user_id = CPT.crm_payment_track_added_by";		

	

	$get_payment_track_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_payment_track_sdata = array();

	

	if($pay_track_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_id=:pay_track_id";					

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_id=:pay_track_id";				

		}

		

		// Data

		$get_payment_track_sdata[':pay_track_id']  = $pay_track_id;

		

		$filter_count++;

	}		

	

	if($payment_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where CPT.crm_payment_id=:payment_id";					

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and CPT.crm_payment_id=:payment_id";				

		}

		

		// Data

		$get_payment_track_sdata[':payment_id']  = $payment_id;

		

		$filter_count++;

	}	

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where CP.crm_payment_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and CP.crm_payment_booking_id=:booking_id";				

		}

		

		// Data

		$get_payment_track_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}		

	

	if($track_status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_status = :track_status";							

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_status = :track_status";				

		}

		

		// Data

		$get_payment_track_sdata[':track_status']  = $track_status;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_added_by=:added_by";				

		}

		

		// Data

		$get_payment_track_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_added_on >= :start_date";				

		}

		

		// Data

		$get_payment_track_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_added_on <= :end_date";				

		}

		

		// Data

		$get_payment_track_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_payment_track_squery = $get_payment_track_squery_base.$get_payment_track_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_payment_track_sstatement = $dbConnection->prepare($get_payment_track_squery);

		

		$get_payment_track_sstatement -> execute($get_payment_track_sdata);

		

		$get_payment_track_sdetails = $get_payment_track_sstatement -> fetchAll();

		

		if(FALSE === $get_payment_track_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_payment_track_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_payment_track_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add cancellation related payment

INPUT 	: Booking ID, Amount, Mode, Instrument Date, Bank, Branch, Added By

OUTPUT 	: Cancellation Payment id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_cancellation_payment($booking_id,$amount,$mode,$instrument_date,$bank,$branch,$added_by)

{

	// Query

    $cancel_payment_iquery = "insert into crm_cancellation_payments (crm_cancelled_booking_id,crm_cancelled_payment_amount,crm_cancelled_payment_mode,crm_cancelled_payment_instrument_date,crm_cancelled_payment_bank,crm_cancelled_payment_branch,crm_cancelled_payment_added_by,crm_cancelled_payment_added_on) values (:booking_id,:amount,:mode,:instru_date,:bank,:branch,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $cancel_payment_istatement = $dbConnection->prepare($cancel_payment_iquery);

        

        // Data

        $cancel_payment_idata = array(':booking_id'=>$booking_id,':amount'=>$amount,':mode'=>$mode,':instru_date'=>$instrument_date,':bank'=>$bank,':branch'=>$branch,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $cancel_payment_istatement->execute($cancel_payment_idata);

		$payment_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $payment_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To add cancellation payment track

INPUT 	: Cancellation Payment ID, Status, Remarks, Added By

OUTPUT 	: Cancellation Payment Track id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_cancel_payment_track($payment_id,$status,$remarks,$added_by)

{

	// Query

    $cancel_payment_track_iquery = "insert into crm_cancel_payment_track (crm_cancel_payment_id,crm_cancel_payment_track_status,crm_cancel_payment_track_remarks,crm_cancel_payment_track_added_by,crm_cancel_payment_track_added_on) values (:payment_id,:status,:remarks,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $cancel_payment_track_istatement = $dbConnection->prepare($cancel_payment_track_iquery);

        

        // Data

        $cancel_payment_track_idata = array(':payment_id'=>$payment_id,':status'=>$status,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $cancel_payment_track_istatement->execute($cancel_payment_track_idata);

		$cancel_payment_track_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $cancel_payment_track_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get cancellation payments

INPUT 	: Payment ID, Booking ID, Mode, Instrument Start Date, Instrument End Date, Added By, Start Date, End Date

OUTPUT 	: List of cancellation payments

BY 		: Nitin Kashyap

*/

function db_get_cancel_payment($payment_id,$booking_id,$mode,$instru_start_date,$instru_end_date,$added_by,$start_date,$end_date)

{

	$get_cancel_payments_squery_base = "select * from crm_cancellation_payments CCP inner join crm_booking CB on CB.crm_booking_id = CCP.crm_cancelled_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join payment_mode_master PMM on PMM.payment_mode_id = CCP.crm_cancelled_payment_mode inner join crm_bank_master CBM on CBM.crm_bank_master_id = CCP.crm_cancelled_payment_bank";		

	

	$get_cancel_payments_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_cancel_payments_sdata = array();

	

	if($payment_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." where crm_cancellation_payment_id=:booking_id";					

		}

		else

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." and crm_cancellation_payment_id=:booking_id";				

		}

		

		// Data

		$get_cancel_payments_sdata[':payment_id']  = $payment_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." where crm_cancelled_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." and crm_cancelled_booking_id=:booking_id";				

		}

		

		// Data

		$get_cancel_payments_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}



	if($mode != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." where crm_cancelled_payment_mode=:mode";					

		}

		else

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." and crm_cancelled_payment_mode=:mode";				

		}

		

		// Data

		$get_cancel_payments_sdata[':mode']  = $mode;

		

		$filter_count++;

	}

	

	if($instru_start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." where crm_cancelled_payment_instrument_date >= :instru_start_date";						

		}

		else

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." and crm_cancelled_payment_instrument_date >= :instru_start_date";				

		}

		

		// Data

		$get_cancel_payments_sdata[':instru_start_date']  = $instru_start_date;

		

		$filter_count++;

	}



	if($instru_end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." where crm_cancelled_payment_instrument_date <= :instru_end_date";						

		}

		else

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." and crm_cancelled_payment_instrument_date <= :instru_end_date";				

		}

		

		// Data

		$get_cancel_payments_sdata[':instru_end_date']  = $instru_end_date;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." where crm_cancelled_payment_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." and crm_cancelled_payment_added_by=:added_by";				

		}

		

		// Data

		$get_cancel_payments_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." where crm_cancelled_payment_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." and crm_cancelled_payment_added_on >= :start_date";				

		}

		

		// Data

		$get_cancel_payments_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." where crm_cancelled_payment_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_cancel_payments_squery_where = $get_cancel_payments_squery_where." and crm_cancelled_payment_added_on <= :end_date";				

		}

		

		// Data

		$get_cancel_payments_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

		

	$get_cancel_payments_squery = $get_cancel_payments_squery_base.$get_cancel_payments_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_cancel_payments_sstatement = $dbConnection->prepare($get_cancel_payments_squery);

		

		$get_cancel_payments_sstatement -> execute($get_cancel_payments_sdata);

		

		$get_cancel_payments_sdetails = $get_cancel_payments_sstatement -> fetchAll();

		

		if(FALSE === $get_cancel_payments_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_cancel_payments_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_cancel_payments_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get cancellation payment track

INPUT 	: Payment Track ID, Payment ID, Booking ID, Tracking Status, Added By, Start Date, End Date

OUTPUT 	: List of payment track

BY 		: Nitin Kashyap

*/

function db_get_cancel_payment_track($pay_track_id,$payment_id,$booking_id,$track_status,$added_by,$start_date,$end_date)

{

	$get_cancel_payment_track_squery_base = "select * from crm_cancel_payment_track CCPT inner join crm_cancellation_payments CCP on CCP.crm_cancellation_payment_id = CCPT.crm_cancel_payment_id inner join users U on U.user_id = CCPT.crm_cancel_payment_track_added_by";		

	

	$get_cancel_payment_track_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_cancel_payment_track_sdata = array();

	

	if($pay_track_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." where crm_cancel_payment_track_id=:pay_track_id";					

		}

		else

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." and crm_cancel_payment_track_id=:pay_track_id";				

		}

		

		// Data

		$get_cancel_payment_track_sdata[':pay_track_id']  = $pay_track_id;

		

		$filter_count++;

	}		

	

	if($payment_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." where CCPT.crm_cancel_payment_id=:payment_id";					

		}

		else

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." and CCPT.crm_cancel_payment_id=:payment_id";				

		}

		

		// Data

		$get_cancel_payment_track_sdata[':payment_id']  = $payment_id;

		

		$filter_count++;

	}	

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." where CCP.crm_cancelled_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." and CCP.crm_cancelled_booking_id=:booking_id";				

		}

		

		// Data

		$get_cancel_payment_track_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}		

	

	if($track_status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." where crm_cancel_payment_track_status = :track_status";							

		}

		else

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." and crm_cancel_payment_track_status = :track_status";				

		}

		

		// Data

		$get_cancel_payment_track_sdata[':track_status']  = $track_status;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." where crm_cancel_payment_track_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." and crm_cancel_payment_track_added_by=:added_by";				

		}

		

		// Data

		$get_cancel_payment_track_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." where crm_cancel_payment_track_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." and crm_cancel_payment_track_added_on >= :start_date";				

		}

		

		// Data

		$get_cancel_payment_track_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." where crm_cancel_payment_track_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_cancel_payment_track_squery_where = $get_cancel_payment_track_squery_where." and crm_cancel_payment_track_added_on <= :end_date";				

		}

		

		// Data

		$get_cancel_payment_track_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_cancel_payment_track_squery = $get_cancel_payment_track_squery_base.$get_cancel_payment_track_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_cancel_payment_track_sstatement = $dbConnection->prepare($get_cancel_payment_track_squery);

		

		$get_cancel_payment_track_sstatement -> execute($get_cancel_payment_track_sdata);

		

		$get_cancel_payment_track_sdetails = $get_cancel_payment_track_sstatement -> fetchAll();

		

		if(FALSE === $get_cancel_payment_track_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_cancel_payment_track_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_cancel_payment_track_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add other payment

INPUT 	: Booking ID, Amount, Type, Mode, Instrument Date, Bank, Branch, Receipt Date, Remarks, Added By

OUTPUT 	: Other Payment ID, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_other_payment($booking_id,$amount,$type,$mode,$instrument_date,$bank,$branch,$receipt_date,$remarks,$added_by)

{

	// Query

    $other_payment_iquery = "insert into crm_other_payment (crm_other_payment_booking_id,crm_other_payment_amount,crm_other_payment_type,crm_other_payment_payment_mode,crm_other_payment_instrument_date,crm_other_payment_bank,crm_other_payment_branch,crm_other_payment_receipt_date,crm_other_payment_remarks,crm_other_payment_added_by,crm_other_payment_added_on) values (:booking_id,:amount,:type,:mode,:instru_date,:bank,:branch,:receipt_date,:remarks,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $other_payment_istatement = $dbConnection->prepare($other_payment_iquery);

        

        // Data

        $other_payment_idata = array(':booking_id'=>$booking_id,':amount'=>$amount,':type'=>$type,':mode'=>$mode,':instru_date'=>$instrument_date,':bank'=>$bank,':branch'=>$branch,':receipt_date'=>$receipt_date,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $other_payment_istatement->execute($other_payment_idata);

		$other_payment_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $other_payment_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get other payments done

INPUT 	: Other Payment ID, Booking ID, Mode, Type, Received Start Date, Received End Date, Added By, Start Date, End Date

OUTPUT 	: List of other payments

BY 		: Nitin Kashyap

*/

function db_get_other_payment($payment_id,$booking_id,$mode,$type,$rec_start_date,$rec_end_date,$added_by,$start_date,$end_date)

{

	$get_other_payments_squery_base = "select * from crm_other_payment COP inner join crm_booking CB on CB.crm_booking_id = COP.crm_other_payment_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join payment_mode_master PMM on PMM.payment_mode_id = COP.crm_other_payment_payment_mode left outer join crm_bank_master CBM on CBM.crm_bank_master_id = COP.crm_other_payment_bank";		

	

	$get_other_payments_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_other_payments_sdata = array();

	

	if($payment_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." where crm_other_payment_id=:payment_id";					

		}

		else

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." and crm_other_payment_id=:payment_id";				

		}

		

		// Data

		$get_other_payments_sdata[':payment_id']  = $payment_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." where crm_other_payment_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." and crm_other_payment_booking_id=:booking_id";				

		}

		

		// Data

		$get_other_payments_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}



	if($mode != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." where crm_other_payment_payment_mode=:mode";					

		}

		else

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." and crm_other_payment_payment_mode=:mode";				

		}

		

		// Data

		$get_other_payments_sdata[':mode']  = $mode;

		

		$filter_count++;

	}

	

	if($type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." where crm_other_payment_type=:type";					

		}

		else

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." and crm_other_payment_type=:type";				

		}

		

		// Data

		$get_other_payments_sdata[':type']  = $type;

		

		$filter_count++;

	}

	

	if($rec_start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." where crm_other_payment_receipt_date >= :rec_start_date";						

		}

		else

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." and crm_other_payment_receipt_date >= :rec_start_date";				

		}

		

		// Data

		$get_other_payments_sdata[':rec_start_date']  = $rec_start_date;

		

		$filter_count++;

	}



	if($rec_end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." where crm_other_payment_receipt_date <= :rec_end_date";						

		}

		else

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." and crm_other_payment_receipt_date <= :rec_end_date";				

		}

		

		// Data

		$get_other_payments_sdata[':rec_end_date']  = $rec_end_date;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." where crm_other_payment_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." and crm_other_payment_added_by=:added_by";				

		}

		

		// Data

		$get_other_payments_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." where crm_other_payment_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." and crm_other_payment_added_on >= :start_date";				

		}

		

		// Data

		$get_other_payments_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." where crm_other_payment_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_other_payments_squery_where = $get_other_payments_squery_where." and crm_other_payment_added_on <= :end_date";				

		}

		

		// Data

		$get_other_payments_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

		

	$get_other_payments_squery = $get_other_payments_squery_base.$get_other_payments_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_other_payments_sstatement = $dbConnection->prepare($get_other_payments_squery);

		

		$get_other_payments_sstatement -> execute($get_other_payments_sdata);

		

		$get_other_payment_sdetails = $get_other_payments_sstatement -> fetchAll();

		

		if(FALSE === $get_other_payment_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_other_payment_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_other_payment_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get total of other payments done

INPUT 	: Other Payment ID, Booking ID, Mode, Type, Received Start Date, Received End Date, Added By, Start Date, End Date

OUTPUT 	: Total of other payments

BY 		: Nitin Kashyap

*/

function db_get_other_payment_total($payment_id,$booking_id,$mode,$type,$rec_start_date,$rec_end_date,$added_by,$start_date,$end_date)

{

	$get_other_total_payments_squery_base = "select sum(crm_other_payment_amount) as other_paid_total from crm_other_payment";		

	

	$get_other_total_payments_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_other_total_payments_sdata = array();

	

	if($payment_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." where crm_other_payment_id=:payment_id";					

		}

		else

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." and crm_other_payment_id=:payment_id";				

		}

		

		// Data

		$get_other_total_payments_sdata[':payment_id']  = $payment_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." where crm_other_payment_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." and crm_other_payment_booking_id=:booking_id";				

		}

		

		// Data

		$get_other_total_payments_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}



	if($mode != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." where crm_other_payment_payment_mode=:mode";					

		}

		else

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." and crm_other_payment_payment_mode=:mode";				

		}

		

		// Data

		$get_other_total_payments_sdata[':mode']  = $mode;

		

		$filter_count++;

	}

	

	if($type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." where crm_other_payment_type=:type";					

		}

		else

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." and crm_other_payment_type=:type";				

		}

		

		// Data

		$get_other_total_payments_sdata[':type']  = $type;

		

		$filter_count++;

	}

	

	if($rec_start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." where crm_other_payment_receipt_date >= :rec_start_date";						

		}

		else

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." and crm_other_payment_receipt_date >= :rec_start_date";				

		}

		

		// Data

		$get_other_total_payments_sdata[':rec_start_date']  = $rec_start_date;

		

		$filter_count++;

	}



	if($rec_end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." where crm_other_payment_receipt_date <= :rec_end_date";						

		}

		else

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." and crm_other_payment_receipt_date <= :rec_end_date";				

		}

		

		// Data

		$get_other_total_payments_sdata[':rec_end_date']  = $rec_end_date;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." where crm_other_payment_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." and crm_other_payment_added_by=:added_by";				

		}

		

		// Data

		$get_other_total_payments_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." where crm_other_payment_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." and crm_other_payment_added_on >= :start_date";				

		}

		

		// Data

		$get_other_total_payments_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." where crm_other_payment_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_other_total_payments_squery_where = $get_other_total_payments_squery_where." and crm_other_payment_added_on <= :end_date";				

		}

		

		// Data

		$get_other_total_payments_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

		

	$get_other_total_payments_squery = $get_other_total_payments_squery_base.$get_other_total_payments_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_other_total_payments_sstatement = $dbConnection->prepare($get_other_total_payments_squery);

		

		$get_other_total_payments_sstatement -> execute($get_other_total_payments_sdata);

		

		$get_other_total_payments_sdetails = $get_other_total_payments_sstatement -> fetchAll();

		

		if(FALSE === $get_other_total_payments_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_other_total_payments_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_other_total_payments_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get other payment track

INPUT 	: Payment Track ID, Payment ID, Booking ID, Tracking Status, Added By, Start Date, End Date

OUTPUT 	: List of payment track

BY 		: Nitin Kashyap

*/

function db_get_other_payment_track($pay_track_id,$payment_id,$booking_id,$track_status,$added_by,$start_date,$end_date)

{

	$get_payment_track_squery_base = "select * from crm_payment_track CPT inner join crm_payment CP on CP.crm_payment_id = CPT.crm_payment_id inner join users U on U.user_id = CPT.crm_payment_track_added_by";		

	

	$get_payment_track_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_payment_track_sdata = array();

	

	if($pay_track_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_id=:pay_track_id";					

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_id=:pay_track_id";				

		}

		

		// Data

		$get_payment_track_sdata[':pay_track_id']  = $pay_track_id;

		

		$filter_count++;

	}		

	

	if($payment_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where CPT.crm_payment_id=:payment_id";					

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and CPT.crm_payment_id=:payment_id";				

		}

		

		// Data

		$get_payment_track_sdata[':payment_id']  = $payment_id;

		

		$filter_count++;

	}	

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where CP.crm_payment_booking_id=:booking_id";					

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and CP.crm_payment_booking_id=:booking_id";				

		}

		

		// Data

		$get_payment_track_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}		

	

	if($track_status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_status = :track_status";							

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_status = :track_status";				

		}

		

		// Data

		$get_payment_track_sdata[':track_status']  = $track_status;

		

		$filter_count++;

	}

	else

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_status IN ('6','7','8','9','10')";							

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_status IN ('6','7','8','9','10')";				

		}				

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_added_by=:added_by";				

		}

		

		// Data

		$get_payment_track_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_added_on >= :start_date";				

		}

		

		// Data

		$get_payment_track_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." where crm_payment_track_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_payment_track_squery_where = $get_payment_track_squery_where." and crm_payment_track_added_on <= :end_date";				

		}

		

		// Data

		$get_payment_track_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_payment_track_squery = $get_payment_track_squery_base.$get_payment_track_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_payment_track_sstatement = $dbConnection->prepare($get_payment_track_squery);

		

		$get_payment_track_sstatement -> execute($get_payment_track_sdata);

		

		$get_payment_track_sdetails = $get_payment_track_sstatement -> fetchAll();

		

		if(FALSE === $get_payment_track_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_payment_track_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_payment_track_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add Katha Transfer transaction between company and respective authority

INPUT 	: Site ID, Transaction Type (Whether applying for KT or received KT), Transaction Date, Remarks, Added By

OUTPUT 	: KT Transaction id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_katha_transfer_transaction($site_id,$transaction_type,$transaction_date,$remarks,$added_by)

{

	// Query

    $kt_transaction_iquery = "insert into crm_katha_transfer_transactions (crm_katha_transfer_transaction_site_id,crm_katha_transfer_transaction_type,crm_katha_transfer_transaction_date,crm_katha_transfer_transaction_remarks,crm_katha_transfer_transaction_added_by,crm_katha_transfer_transaction_added_on) values (:site_id,:type,:date,:remarks,:added_by,:now)";  


    try

    {

        $dbConnection = get_conn_handle();

        

        $kt_transaction_istatement = $dbConnection->prepare($kt_transaction_iquery);

        

        // Data
        $kt_transaction_idata = array(':site_id'=>$site_id,':type'=>$transaction_type,':date'=>$transaction_date,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	


		$dbConnection->beginTransaction();

        $kt_transaction_istatement->execute($kt_transaction_idata);

		$kt_transaction_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $kt_transaction_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Katha transfer transaction

INPUT 	: Katha Transfer Transaction Data Array

OUTPUT 	: List of Katha transfer transactions

BY 		: Nitin Kashyap

*/

function db_get_kt_transactions($kt_transaction_data)

{

	if(array_key_exists("site_id",$kt_transaction_data))

	{	

		$site_id = $kt_transaction_data["site_id"];

	}

	else

	{

		$site_id = "";

	}

	

	if(array_key_exists("type",$kt_transaction_data))

	{	

		$type = $kt_transaction_data["type"];

	}

	else

	{

		$type = "";

	}



	$get_kt_transactions_squery_base = "select * from crm_katha_transfer_transactions CTT inner join crm_site_master CSM on CSM.crm_site_id = CTT.crm_katha_transfer_transaction_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join users U on U.user_id = CTT.crm_katha_transfer_transaction_added_by";		

	

	$get_kt_transactions_squery_where = "";				

	

	$filter_count = 0;

	

	// Data

	$get_kt_transactions_sdata = array();

	

	if($site_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_transactions_squery_where = $get_kt_transactions_squery_where." where crm_katha_transfer_transaction_site_id=:site_id";					

		}

		else

		{

			// Query

			$get_kt_transactions_squery_where = $get_kt_transactions_squery_where." and crm_katha_transfer_transaction_site_id=:site_id";				

		}

		

		// Data

		$get_kt_transactions_sdata[':site_id']  = $site_id;

		

		$filter_count++;

	}	



	if($type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_transactions_squery_where = $get_kt_transactions_squery_where." where crm_katha_transfer_transaction_type=:type";					

		}

		else

		{

			// Query

			$get_kt_transactions_squery_where = $get_kt_transactions_squery_where." and crm_katha_transfer_transaction_type=:type";				

		}

		

		// Data

		$get_kt_transactions_sdata[':type']  = $type;

		

		$filter_count++;

	}		

	

	$get_kt_transactions_squery = $get_kt_transactions_squery_base.$get_kt_transactions_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_kt_transactions_sstatement = $dbConnection->prepare($get_kt_transactions_squery);

		

		$get_kt_transactions_sstatement -> execute($get_kt_transactions_sdata);

		

		$get_kt_transactions_sdetails = $get_kt_transactions_sstatement -> fetchAll();

		

		if(FALSE === $get_kt_transactions_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_kt_transactions_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_kt_transactions_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update payment details

INPUT 	: Payment ID, Payment Data Array

OUTPUT 	: Payment ID; Message of success or failure

BY 		: Nitin Kashyap

*/

function db_update_payment_details($payment_id,$payment_data)

{

	if(array_key_exists("amount",$payment_data))

	{	

		$amount = $payment_data["amount"];

	}

	else

	{

		$amount = "";

	}
	
	if(array_key_exists("milestone",$payment_data))

	{	

		$milestone = $payment_data["milestone"];

	}

	else

	{

		$milestone = "";

	}

	

	if(array_key_exists("mode",$payment_data))

	{	

		$mode = $payment_data["mode"];

	}

	else

	{

		$mode = "";

	}

	

	if(array_key_exists("instru_date",$payment_data))

	{	

		$instru_date = $payment_data["instru_date"];

	}

	else

	{

		$instru_date = "";

	}

	

	if(array_key_exists("bank",$payment_data))

	{	

		$bank = $payment_data["bank"];

	}

	else

	{

		$bank = "";

	}

	

	if(array_key_exists("branch",$payment_data))

	{	

		$branch = $payment_data["branch"];

	}

	else

	{

		$branch = "";

	}

	

	if(array_key_exists("receipt_date",$payment_data))

	{	

		$receipt_date = $payment_data["receipt_date"];

	}

	else

	{

		$receipt_date = "";

	}

	

	if(array_key_exists("remarks",$payment_data))

	{	

		$remarks = $payment_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	// Query

    $payment_uquery_base = "update crm_payment set";  

	

	$payment_uquery_set = "";

	

	$payment_uquery_where = " where crm_payment_id=:payment_id";

	

	$payment_udata = array(":payment_id"=>$payment_id);

	

	$filter_count = 0;

	

	if($amount != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_payment_amount=:amount,";

		$payment_udata[":amount"] = $amount;

		$filter_count++;

	}

	if($milestone != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_payment_milestone=:milestone,";

		$payment_udata[":milestone"] = $milestone;

		$filter_count++;

	}

	

	if($mode != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_payment_payment_mode=:mode,";

		$payment_udata[":mode"] = $mode;		

		$filter_count++;

	}

	

	if($instru_date != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_payment_instrument_date=:instru_date,";

		$payment_udata[":instru_date"] = $instru_date;

		$filter_count++;

	}

	

	if($bank != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_payment_bank=:bank,";

		$payment_udata[":bank"] = $bank;		

		$filter_count++;

	}

	

	if($branch != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_payment_branch=:branch,";

		$payment_udata[":branch"] = $branch;

		$filter_count++;

	}

	

	if($receipt_date != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_payment_receipt_date=:receipt_date,";

		$payment_udata[":receipt_date"] = $receipt_date;		

		$filter_count++;

	}

	

	if($remarks != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_payment_remarks=:remarks,";

		$payment_udata[":remarks"] = $remarks;		

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$payment_uquery_set = trim($payment_uquery_set,',');

	}

	

	$payment_uquery = $payment_uquery_base.$payment_uquery_set.$payment_uquery_where;
	



    try

    {

        $dbConnection = get_conn_handle();

        

        $payment_ustatement = $dbConnection->prepare($payment_uquery);		

        

        $payment_ustatement -> execute($payment_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $payment_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To update other payment details

INPUT 	: Other Payment ID, Other Payment Data Array

OUTPUT 	: Other Payment ID; Message of success or failure

BY 		: Nitin Kashyap

*/

function db_update_other_payment_details($payment_id,$payment_data)

{

	if(array_key_exists("amount",$payment_data))

	{	

		$amount = $payment_data["amount"];

	}

	else

	{

		$amount = "";

	}

	

	if(array_key_exists("type",$payment_data))

	{	

		$type = $payment_data["type"];

	}

	else

	{

		$type = "";

	}

	

	if(array_key_exists("mode",$payment_data))

	{	

		$mode = $payment_data["mode"];

	}

	else

	{

		$mode = "";

	}

	

	if(array_key_exists("instru_date",$payment_data))

	{	

		$instru_date = $payment_data["instru_date"];

	}

	else

	{

		$instru_date = "";

	}

	

	if(array_key_exists("bank",$payment_data))

	{	

		$bank = $payment_data["bank"];

	}

	else

	{

		$bank = "";

	}

	

	if(array_key_exists("branch",$payment_data))

	{	

		$branch = $payment_data["branch"];

	}

	else

	{

		$branch = "";

	}

	

	if(array_key_exists("receipt_date",$payment_data))

	{	

		$receipt_date = $payment_data["receipt_date"];

	}

	else

	{

		$receipt_date = "";

	}

	

	if(array_key_exists("remarks",$payment_data))

	{	

		$remarks = $payment_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	// Query

    $payment_uquery_base = "update crm_other_payment set";  

	

	$payment_uquery_set = "";

	

	$payment_uquery_where = " where crm_other_payment_id=:payment_id";

	

	$payment_udata = array(":payment_id"=>$payment_id);

	

	$filter_count = 0;

	

	if($amount != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_other_payment_amount=:amount,";

		$payment_udata[":amount"] = $amount;

		$filter_count++;

	}

	

	if($type != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_other_payment_type=:type,";

		$payment_udata[":type"] = $type;

		$filter_count++;

	}

	

	if($mode != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_other_payment_payment_mode=:mode,";

		$payment_udata[":mode"] = $mode;		

		$filter_count++;

	}

	

	if($instru_date != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_other_payment_instrument_date=:instru_date,";

		$payment_udata[":instru_date"] = $instru_date;

		$filter_count++;

	}

	

	if($bank != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_other_payment_bank=:bank,";

		$payment_udata[":bank"] = $bank;		

		$filter_count++;

	}

	

	if($branch != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_other_payment_branch=:branch,";

		$payment_udata[":branch"] = $branch;

		$filter_count++;

	}

	

	if($receipt_date != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_other_payment_receipt_date=:receipt_date,";

		$payment_udata[":receipt_date"] = $receipt_date;		

		$filter_count++;

	}

	

	if($remarks != "")

	{

		$payment_uquery_set = $payment_uquery_set." crm_other_payment_remarks=:remarks,";

		$payment_udata[":remarks"] = $remarks;		

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$payment_uquery_set = trim($payment_uquery_set,',');

	}

	

	$payment_uquery = $payment_uquery_base.$payment_uquery_set.$payment_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();

        

        $payment_ustatement = $dbConnection->prepare($payment_uquery);		

        

        $payment_ustatement -> execute($payment_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $payment_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}

?>