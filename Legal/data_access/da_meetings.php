<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs: NONE
*/

/*
PURPOSE : To get meeting list
INPUT 	: Meeting List Data
OUTPUT 	: List of meetings
BY 		: Nitin Kashyap
*/
function db_get_meetings($meeting_data)
{
	// Extract data from data array
	if(array_key_exists("meeting_id",$meeting_data))
	{	
		$meeting_id = $meeting_data["meeting_id"];
	}
	else
	{
		$meeting_id = "";
	}
	
	if(array_key_exists("user",$meeting_data))
	{	
		$user = $meeting_data["user"];
	}
	else
	{
		$user = "";
	}
	
	if(array_key_exists("department",$meeting_data))
	{	
		$department = $meeting_data["department"];
	}
	else
	{
		$department = "";
	}
	
	if(array_key_exists("start_date",$meeting_data))
	{	
		$start_date = $meeting_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$meeting_data))
	{	
		$end_date = $meeting_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	if(array_key_exists("start_time",$meeting_data))
	{	
		$start_time = $meeting_data["start_time"];
	}
	else
	{
		$start_time = "";
	}
	
	if(array_key_exists("end_time",$meeting_data))
	{	
		$end_time = $meeting_data["end_time"];
	}
	else
	{
		$end_time = "";
	}
	
	if(array_key_exists("order",$meeting_data))
	{	
		$order = $meeting_data["order"];
	}
	else
	{
		$order = "";
	}
	
	if(array_key_exists("limit_start",$meeting_data))
	{	
		$limit_start = $meeting_data["limit_start"];
	}
	else
	{
		$limit_start = "";
	}
	
	if(array_key_exists("limit_count",$meeting_data))
	{	
		$limit_count = $meeting_data["limit_count"];
	}
	else
	{
		$limit_count = "";
	}

	$get_meeting_list_squery_base  = "select * from meetings M inner join general_task_department_master GTDM on GTDM.general_task_department_id = M.meeting_department inner join users U on U.user_id = M.meeting_added_by";
	
	$get_meeting_list_squery_where = "";
	
	$get_meeting_list_squery_order = "";
	
	$get_meeting_list_squery_limit = "";
	
	$filter_count = 0;
	
	// Data
	$get_meeting_list_sdata = array();
	
	if($meeting_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." where meeting_id=:meeting_id";								
		}
		else
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." and meeting_id=:meeting_id";				
		}
		
		// Data
		$get_meeting_list_sdata[':meeting_id']  = $meeting_id;
		
		$filter_count++;
	}
	
	if($department != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." where meeting_department=:department";								
		}
		else
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." and meeting_department=:department";				
		}
		
		// Data
		$get_meeting_list_sdata[':department']  = $department;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." where meeting_date >= :start_date";								
		}
		else
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." and meeting_date >= :start_date";				
		}
		
		// Data
		$get_meeting_list_sdata[':start_date'] = $start_date;
		
		$filter_count++;
	}
	
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." where meeting_date <= :end_date";								
		}
		else
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." and meeting_date <= :end_date";				
		}
		
		// Data
		$get_meeting_list_sdata[':end_date'] = $end_date;
		
		$filter_count++;
	}
	
	if($start_time != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." where meeting_time >= :start_time";								
		}
		else
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." and meeting_time >= :start_time";				
		}
		
		// Data
		$get_meeting_list_sdata[':start_time'] = $start_time;
		
		$filter_count++;
	}
	
	if($end_time != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." where meeting_time <= :end_time";								
		}
		else
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." and meeting_time <= :end_time";				
		}
		
		// Data
		$get_meeting_list_sdata[':end_time'] = $end_time;
		
		$filter_count++;
	}
	
	if($user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." where ((meeting_id in (select meeting_id from meeting_attendees where meeting_participant_id=:user)) or meeting_added_by=:user)";								
		}
		else
		{
			// Query
			$get_meeting_list_squery_where = $get_meeting_list_squery_where." and ((meeting_id in (select meeting_id from meeting_attendees where meeting_participant_id=:user)) or meeting_added_by=:user)";				
		}
		
		// Data
		$get_meeting_list_sdata[':user']  = $user;
		
		$filter_count++;
	}
	
	switch($order)
	{
		case "date_time_desc":
		$get_meeting_list_squery_order = " order by meeting_date desc, meeting_time desc";
		break;
		
		case "date_time_asc":
		$get_meeting_list_squery_order = " order by meeting_date asc, meeting_time asc";
		break;
		
		case "added_on_desc":
		$get_meeting_list_squery_order = " order by meeting_added_on desc";
		break;
		
		case "meeting_no":
		$get_meeting_list_squery_order = " order by cast(substring(meeting_no,locate('/',meeting_no,5)+1) as signed) desc";
		break;
		
		default:
		$get_meeting_list_squery_order = "";
		break;
	}
	
	if($limit_count != "")
	{
		if($limit_start == "")
		{
			$limit_start = '0';
		}
		
		$get_meeting_list_squery_limit = " limit ".$limit_start.','.$limit_count;
	}
	
	$get_meeting_list_squery = $get_meeting_list_squery_base.$get_meeting_list_squery_where.$get_meeting_list_squery_order.$get_meeting_list_squery_limit;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_meeting_list_sstatement = $dbConnection->prepare($get_meeting_list_squery);
		
		$get_meeting_list_sstatement -> execute($get_meeting_list_sdata);
		
		$get_meeting_list_sdetails = $get_meeting_list_sstatement -> fetchAll();
		
		if(FALSE === $get_meeting_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_meeting_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_meeting_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new meeting
INPUT 	: Meeting No, Department, Agenda, Doc Path, Date, Time, Venue, Details, Added By
OUTPUT 	: Meeting ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_meeting($meeting_no,$department,$agenda,$doc_path,$date,$time,$venue,$details,$added_by)
{
	// Query
    $meeting_iquery = "insert into meetings (meeting_no,meeting_department,meeting_agenda,meeting_document_path,meeting_date,meeting_time,meeting_venue,meeting_details,meeting_status,meeting_added_by,meeting_added_on) values (:mtng_no,:department,:agenda,:doc_path,:date,:time,:venue,:details,:status,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $meeting_istatement = $dbConnection->prepare($meeting_iquery);
        
        // Data		
        $meeting_idata = array(':mtng_no'=>$meeting_no,':department'=>$department,':agenda'=>$agenda,':doc_path'=>$doc_path,':date'=>$date,':time'=>$time,':venue'=>$venue,':details'=>$details,':status'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		$dbConnection->beginTransaction();
        $meeting_istatement->execute($meeting_idata);
		$meeting_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $meeting_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To add new meeting participant
INPUT 	: Participant ID, Meeting ID, Added By
OUTPUT 	: Participation ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_meeting_participant($participant_id,$meeting_id,$added_by)
{
	// Query
    $meeting_iquery = "insert into meeting_attendees (meeting_participant_id,meeting_id,meeting_participant_confirmation,meeting_participant_added_by,meeting_participant_added_on) values (:participant,:meeting_id,:confirmation,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $meeting_istatement = $dbConnection->prepare($meeting_iquery);
        
        // Data		
        $meeting_idata = array(':participant'=>$participant_id,':meeting_id'=>$meeting_id,':confirmation'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		$dbConnection->beginTransaction();
        $meeting_istatement->execute($meeting_idata);
		$meeting_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $meeting_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To get meeting list
INPUT 	: Meeting List Data
OUTPUT 	: List of meetings
BY 		: Nitin Kashyap
*/
function db_get_meeting_participants($meeting_participant_data)
{
	// Extract data from data array
	if(array_key_exists("meeting_id",$meeting_participant_data))
	{	
		$meeting_id = $meeting_participant_data["meeting_id"];
	}
	else
	{
		$meeting_id = "";
	}

	$get_meeting_particpant_list_squery_base  = "select * from meeting_attendees MA inner join users U on U.user_id = MA.meeting_participant_id";
	
	$get_meeting_particpant_list_squery_where = "";		
	
	$filter_count = 0;
	
	// Data
	$get_meeting_particpant_list_sdata = array();
	
	if($meeting_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_meeting_particpant_list_squery_where = $get_meeting_particpant_list_squery_where." where meeting_id=:meeting_id";								
		}
		else
		{
			// Query
			$get_meeting_particpant_list_squery_where = $get_meeting_particpant_list_squery_where." and meeting_id=:meeting_id";				
		}
		
		// Data
		$get_meeting_particpant_list_sdata[':meeting_id']  = $meeting_id;
		
		$filter_count++;
	}
	
	$get_meeting_particpant_list_squery = $get_meeting_particpant_list_squery_base.$get_meeting_particpant_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_meeting_particpant_list_sstatement = $dbConnection->prepare($get_meeting_particpant_list_squery);
		
		$get_meeting_particpant_list_sstatement -> execute($get_meeting_particpant_list_sdata);
		
		$get_meeting_particpant_list_sdetails = $get_meeting_particpant_list_sstatement -> fetchAll();
		
		if(FALSE === $get_meeting_particpant_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_meeting_particpant_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_meeting_particpant_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new MOM item
INPUT 	: Meeting ID, Item, Description, Target Date, Task ID, Added By
OUTPUT 	: MOM Item ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_mom_item($meeting_id,$mom_item,$description,$target_date,$task_id,$added_by)
{
	// Query
    $mom_iquery = "insert into meeting_minutes (mom_meeting_id,mom_item,mom_description,mom_target_date,mom_task_id,mom_status,mom_added_by,mom_added_on) values (:meeting_id,:item,:description,:target_date,:task_id,:status,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $mom_istatement = $dbConnection->prepare($mom_iquery);
        
        // Data		
        $mom_idata = array(':meeting_id'=>$meeting_id,':item'=>$mom_item,':description'=>$description,':target_date'=>$target_date,':task_id'=>$task_id,':status'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));				
		$dbConnection->beginTransaction();
        $mom_istatement->execute($mom_idata);
		$mom_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $mom_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To get meeting MOM item
INPUT 	: MOM List Data
OUTPUT 	: List of MOMs
BY 		: Nitin Kashyap
*/
function db_get_mom($mom_data)
{
	// Extract data from data array
	if(array_key_exists("meeting_id",$mom_data))
	{	
		$meeting_id = $mom_data["meeting_id"];
	}
	else
	{
		$meeting_id = "";
	}
	
	if(array_key_exists("mom_id",$mom_data))
	{	
		$mom_id = $mom_data["mom_id"];
	}
	else
	{
		$mom_id = "";
	}
	
	if(array_key_exists("status",$mom_data))
	{	
		$status = $mom_data["status"];
	}
	else
	{
		$status = "";
	}

	$mom_list_squery_base  = "select * from meeting_minutes MM left outer join general_tasks GT on GT.general_task_id = MM.mom_task_id left outer join users U on U.user_id = GT.general_task_user";
	
	$mom_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$mom_list_sdata = array();
	
	if($meeting_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$mom_list_squery_where = $mom_list_squery_where." where mom_meeting_id=:meeting_id";								
		}
		else
		{
			// Query
			$mom_list_squery_where = $mom_list_squery_where." and mom_meeting_id=:meeting_id";				
		}
		
		// Data
		$mom_list_sdata[':meeting_id']  = $meeting_id;
		
		$filter_count++;
	}
	
	if($mom_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$mom_list_squery_where = $mom_list_squery_where." where mom_id=:mom_id";								
		}
		else
		{
			// Query
			$mom_list_squery_where = $mom_list_squery_where." and mom_id=:mom_id";				
		}
		
		// Data
		$mom_list_sdata[':mom_id']  = $mom_id;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$mom_list_squery_where = $mom_list_squery_where." where mom_status=:status";								
		}
		else
		{
			// Query
			$mom_list_squery_where = $mom_list_squery_where." and mom_status=:status";				
		}
		
		// Data
		$mom_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	$mom_list_squery = $mom_list_squery_base.$mom_list_squery_where;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$mom_list_sstatement = $dbConnection->prepare($mom_list_squery);
		
		$mom_list_sstatement -> execute($mom_list_sdata);
		
		$mom_list_sdetails = $mom_list_sstatement -> fetchAll();
		
		if(FALSE === $mom_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($mom_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $mom_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update MOM details
INPUT 	: MOM ID, MOM Data
OUTPUT 	: MOM ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_mom($mom_id,$mom_data)
{
	//Extract Data
	if(array_key_exists("item",$mom_data))
	{
		$item = $mom_data["item"];
	}
	else
	{
		$item = "";
	}
	
	if(array_key_exists("desc",$mom_data))
	{
		$desc = $mom_data["desc"];
	}
	else
	{
		$desc = "";
	}
	
	if(array_key_exists("status",$mom_data))
	{
		$status = $mom_data["status"];
	}
	else
	{
		$status = "";
	}
	
	// Query
    $mom_uquery_base = "update meeting_minutes set";  
	
	$mom_uquery_set = "";
	
	$mom_uquery_where = " where mom_id=:mom_id";
	
	$mom_udata = array(":mom_id"=>$mom_id);
	
	$filter_count = 0;
	
	if($item != "")
	{
		$mom_uquery_set = $mom_uquery_set." mom_item=:item,";
		$mom_udata[":item"] = $item;
		$filter_count++;
	}
	
	if($desc != "")
	{
		$mom_uquery_set = $mom_uquery_set." mom_description=:desc,";
		$mom_udata[":desc"] = $desc;
		$filter_count++;
	}
	
	if($status != "")
	{
		$mom_uquery_set = $mom_uquery_set." mom_status=:status,";
		$mom_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$mom_uquery_set = trim($mom_uquery_set,',');
	}
	
	$mom_uquery = $mom_uquery_base.$mom_uquery_set.$mom_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $mom_ustatement = $dbConnection->prepare($mom_uquery);		
        
        $mom_ustatement -> execute($mom_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $mom_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>