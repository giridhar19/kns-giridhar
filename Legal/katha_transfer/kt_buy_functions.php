<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_kt_buy.php');

/*
PURPOSE : To add new KT Buy Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_kt_buy_process_master($name,$remarks,$added_by)
{
	$kt_buy_process_master_search_data = array("name"=>$name,"process_name_check"=>'1',"active"=>'1');
	$kt_buy_process_master_sresult = db_get_kt_buy_process_master($kt_buy_process_master_search_data);
	
	if($kt_buy_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$kt_buy_process_master_iresult = db_add_kt_buy_process_master($name,$remarks,$added_by);
		
		if($kt_buy_process_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "KT Buy Process Master Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "KT Process Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get KT Buy Process Master List
INPUT 	: Process Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Buy Process Master, success or failure message
BY 		: Lakshmi
*/
function i_get_kt_Buy_process_master($kt_buy_process_master_search_data)
{
	$kt_buy_process_master_sresult = db_get_kt_buy_process_master($kt_buy_process_master_search_data);
	
	if($kt_buy_process_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $kt_buy_process_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No KT Buy Process Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update KT Buy Process Master
INPUT 	: Process Master ID, KT Buy Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_kt_buy_process_master($process_master_id,$kt_buy_process_master_update_data)
{
	$kt_buy_process_master_search_data = array("name"=>$kt_buy_process_master_update_data['name'],"process_name_check"=>'1',"active"=>'1');
	$kt_buy_process_master_sresult = db_get_kt_buy_process_master($kt_buy_process_master_search_data);
	
	$allow_update = false;
	if($kt_buy_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($kt_buy_process_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($kt_buy_process_master_sresult['data'][0]['kt_buy_process_master_id'] == $process_master_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$kt_buy_process_master_sresult = db_update_kt_buy_process_master($process_master_id,$kt_buy_process_master_update_data);
		
		if($kt_buy_process_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "KT Buy Process Master Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}


/*
PURPOSE : To Delete KT Buy Process Master 
INPUT 	: Process Master ID, Survey Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_kt_buy_process_master($process_master_id,$kt_buy_process_master_update_data)
{   
    $kt_buy_process_master_update_data = array('active'=>'0');
	$kt_buy_process_master_sresult = db_update_kt_buy_process_master($process_master_id,$kt_buy_process_master_update_data);
	
	if($kt_buy_process_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Buy Process Master  Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new KT Buy Request
INPUT 	: File ID, Request Date, Requested By, Requested On, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_kt_buy_request($project,$site_no,$dimension,$extent,$request_date,$request_for,$remarks,$added_by)
{
	$kt_buy_request_iresult = db_add_kt_buy_request($project,$site_no,$dimension,$extent,$request_date,$request_for,$remarks,$added_by);
	
	if($kt_buy_request_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Buy Request Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get KT Buy Request List
INPUT 	: Request ID, File ID, Booking ID, Request Date, Requested By, Requested On, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Buy Request, success or failure message
BY 		: Lakshmi
*/
function i_get_kt_buy_request($kt_buy_request_search_data)
{
	$kt_buy_request_sresult = db_get_kt_buy_request($kt_buy_request_search_data);
	
	if($kt_buy_request_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$kt_buy_request_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No KT Buy Request Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update KT Buy Request
INPUT 	: Request ID, To update KT Buy Request Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_kt_buy_request($request_id,$kt_buy_request_update_data)
{   
	$kt_buy_request_sresult = db_update_kt_buy_request($request_id,$kt_buy_request_update_data);
	
	if($kt_buy_request_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Buy Request Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new KT Buy KT Process
INPUT 	: Process Master ID, Request ID, Process Start Date, Process End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_kt_buy_kt_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	$kt_buy_kt_process_iresult = db_add_kt_buy_kt_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by);
	
	if($kt_buy_kt_process_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Buy KT Process Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get KT Buy KT Process List
INPUT 	: Process Master ID, Request ID, Process Start Date, Process End Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Buy KT Process, success or failure message
BY 		: Lakshmi
*/
function i_get_kt_buy_kt_process($kt_buy_kt_process_search_data)
{
	$kt_buy_kt_process_sresult = db_get_kt_buy_kt_process($kt_buy_kt_process_search_data);
	
	if($kt_buy_kt_process_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$kt_buy_kt_process_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No KT Buy KT Process Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update KT Buy KT Process
INPUT 	: Process ID, To update KT Buy KT Process Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_kt_buy_kt_process($process_id,$kt_buy_kt_process_update_data)
{   
	$kt_buy_kt_process_sresult = db_update_kt_buy_kt_process($process_id,$kt_buy_kt_process_update_data);
	
	if($kt_buy_kt_process_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Buy KT Process Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new KT Buy Document
INPUT 	: Process ID, File Path ID, Remarks, Added By
OUTPUT 	: Document ID, success or failure message
BY 		: Lakshmi
*/
function i_add_kt_buy_document($process_id,$file_path_id,$remarks,$added_by)
{   
	$kt_buy_document_iresult =  db_add_kt_buy_document($process_id,$file_path_id,$remarks,$added_by);
	
	if($kt_buy_document_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $kt_buy_document_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get KT Buy Document list
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Buy Document
BY 		: Lakshmi
*/
function i_get_kt_buy_document($kt_buy_document_search_data)
{
	$kt_buy_document_sresult = db_get_kt_buy_document($kt_buy_document_search_data);
	
	if($kt_buy_document_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$kt_buy_document_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  KT Buy Document Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

 /*
PURPOSE : To update KT Buy Document 
INPUT 	: Document ID, KT Buy Document Update Array
OUTPUT 	: Document ID; Message of success or failure
BY 		: Lakshmi
*/

function i_update_kt_buy_document($document_id,$kt_buy_document_update_data)
{       
		$kt_buy_document_sresult = db_update_kt_buy_document($document_id,$kt_buy_document_update_data);
		
		if($kt_buy_document_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "KT Buy Document Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To add new KT Buy Project Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_kt_buy_project_master($name,$remarks,$added_by)
{
	$kt_buy_project_master_search_data = array("name"=>$name,"project_name_check"=>'1',"active"=>'1');
	$kt_buy_project_master_sresult = db_get_kt_buy_project_master($kt_buy_project_master_search_data);
	
	if($kt_buy_project_master_sresult["status"] == DB_NO_RECORD)
	{
		$kt_buy_project_master_iresult = db_add_kt_buy_project_master($name,$remarks,$added_by);
		
		if($kt_buy_project_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "KT Buy Project Master Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "KT project Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get KT Buy Project Master List
INPUT 	: Project ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Buy Project Master, success or failure message
BY 		: Lakshmi
*/
function i_get_kt_Buy_project_master($kt_buy_project_master_search_data)
{
	$kt_buy_project_master_sresult = db_get_kt_buy_project_master($kt_buy_project_master_search_data);
	
	if($kt_buy_project_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $kt_buy_project_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No KT Buy Project Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update KT Buy Project Master
INPUT 	: Project ID, KT Buy Project Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_kt_buy_project_master($project_id,$kt_buy_project_master_update_data)
{
	$kt_buy_project_master_search_data = array("name"=>$kt_buy_project_master_update_data['name'],"project_name_check"=>'1',"active"=>'1');
	$kt_buy_project_master_sresult = db_get_kt_buy_project_master($kt_buy_project_master_search_data);
	
	$allow_update = false;
	if($kt_buy_project_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($kt_buy_project_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($kt_buy_project_master_sresult['data'][0]['kt_buy_project_master_id'] == $project_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$kt_buy_project_master_sresult = db_update_kt_buy_project_master($project_id,$kt_buy_project_master_update_data);
		
		if($kt_buy_project_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "KT Buy Project Master Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}


/*
PURPOSE : To Delete KT Buy Project Master 
INPUT 	: project ID, Survey project Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_kt_buy_project_master($project_id,$kt_buy_project_master_update_data)
{   
    $kt_buy_project_master_update_data = array('active'=>'0');
	$kt_buy_project_master_sresult = db_update_kt_buy_project_master($project_id,$kt_buy_project_master_update_data);
	
	if($kt_buy_project_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Buy Project Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}


?>