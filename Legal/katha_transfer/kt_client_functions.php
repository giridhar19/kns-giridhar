<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_kt_client.php');

/*
PURPOSE : To add new KT Client Request
INPUT 	: Client ID, Booking ID, Request Date, Requested By, Requested On, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_kt_client_request($client_id,$booking_id,$request_date,$requested_by,$requested_on,$remarks,$added_by)
{
	$kt_client_request_iresult = db_add_kt_client_request($client_id,$booking_id,$request_date,$requested_by,$requested_on,$remarks,$added_by);
	
	if($kt_client_request_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Client Request Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get KT Client Request List
INPUT 	: Request ID, Client ID, Booking ID, Request Date, Requested By, Requested On, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Client Request, success or failure message
BY 		: Lakshmi
*/
function i_get_kt_client_request($kt_client_request_search_data)
{
	$kt_client_request_sresult = db_get_kt_client_request($kt_client_request_search_data);
	
	if($kt_client_request_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$kt_client_request_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No KT Client Request Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update KT Client Request
INPUT 	: Request ID, To update KT Client Request Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_kt_client_request($request_id,$kt_client_request_update_data)
{   
	$kt_client_request_sresult = db_update_kt_client_request($request_id,$kt_client_request_update_data);
	
	if($kt_client_request_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Client Request Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new KT Client KT Process
INPUT 	: Process Master ID, Request ID, Process Start Date, Process End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_kt_client_kt_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	$kt_client_kt_process_iresult = db_add_kt_client_kt_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by);
	
	if($kt_client_kt_process_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Client KT Process Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get KT Client KT Process List
INPUT 	: Process ID, Client ID, Process Master ID, Request ID, Process Start Date, Process End Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Client KT Process, success or failure message
BY 		: Lakshmi
*/
function i_get_kt_client_kt_process($kt_client_kt_process_search_data)
{
	$kt_client_kt_process_sresult = db_get_kt_client_kt_process($kt_client_kt_process_search_data);
	
	if($kt_client_kt_process_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$kt_client_kt_process_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No KT Client KT Process Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update KT Client KT Process
INPUT 	: Process ID, To update KT Client KT Process Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_kt_client_kt_process($process_id,$kt_client_kt_process_update_data)
{   
	$kt_client_kt_process_sresult = db_update_kt_client_kt_process($process_id,$kt_client_kt_process_update_data);
	
	if($kt_client_kt_process_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Client KT Process Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new KT Client Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_kt_client_process_master($name,$remarks,$added_by)
{
	$kt_client_process_master_search_data = array("name"=>$name,"process_name_check"=>'1',"active"=>'1');
	$kt_client_process_master_sresult = db_get_kt_client_process_master($kt_client_process_master_search_data);
	
	if($kt_client_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$kt_client_process_master_iresult = db_add_kt_client_process_master($name,$remarks,$added_by);
		
		if($kt_client_process_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "KT Client Process Master Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Process Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get KT Client Process Master List
INPUT 	: Process Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Client Process Master, success or failure message
BY 		: Lakshmi
*/
function i_get_kt_client_process_master($kt_client_process_master_search_data)
{
	$kt_client_process_master_sresult = db_get_kt_client_process_master($kt_client_process_master_search_data);
	
	if($kt_client_process_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $kt_client_process_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No KT Client Process Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update KT Client Process Master
INPUT 	: Process Master ID, KT Client Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_kt_client_process_master($process_master_id,$kt_client_process_master_update_data)
{
	$kt_client_process_master_search_data = array("name"=>$kt_client_process_master_update_data['name'],"process_name_check"=>'1',"active"=>'1');
	$kt_client_process_master_sresult = db_get_kt_client_process_master($kt_client_process_master_search_data);
	
	$allow_update = false;
	if($kt_client_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($kt_client_process_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($kt_client_process_master_sresult['data'][0]['kt_client_process_master_id'] == $process_master_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$kt_client_process_master_sresult = db_update_kt_client_process_master($process_master_id,$kt_client_process_master_update_data);
		
		if($kt_client_process_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "KT Client Process Master Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}


/*
PURPOSE : To Delete KT Client Process Master 
INPUT 	: Master ID, Survey Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_kt_client_process_master($process_master_id,$kt_client_process_master_update_data)
{   
    $kt_client_process_master_update_data = array('active'=>'0');
	$kt_client_process_master_sresult = db_update_kt_client_process_master($process_master_id,$kt_client_process_master_update_data);
	
	if($kt_client_process_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "KT Client Process Master  Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new KT Client Document
INPUT 	: Process ID, File Path ID, Remarks, Added By
OUTPUT 	: Document ID, success or failure message
BY 		: Lakshmi
*/
function i_add_kt_client_document($process_id,$file_path_id,$remarks,$added_by)
{   
	$kt_client_document_iresult =  db_add_kt_client_document($process_id,$file_path_id,$remarks,$added_by);
	
	if($kt_client_document_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $kt_client_document_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get KT Client Document list
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Client Document
BY 		: Lakshmi
*/
function i_get_kt_client_document($kt_client_document_search_data)
{
	$kt_client_document_sresult = db_get_kt_client_document($kt_client_document_search_data);
	
	if($kt_client_document_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$kt_client_document_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  KT Client Document Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

 /*
PURPOSE : To update KT Client Document 
INPUT 	: Document ID, KT Client Document Update Array
OUTPUT 	: Document ID; Message of success or failure
BY 		: Lakshmi
*/

function i_update_kt_client_document($document_id,$kt_client_document_update_data)
{       
		$kt_client_document_sresult = db_update_kt_client_document($document_id,$kt_client_document_update_data);
		
		if($kt_client_document_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "KT Client Document Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

?>