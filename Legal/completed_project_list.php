<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: completed_project_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Completed Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["file"]))
	{
		$file = $_GET["file"];
	}
	else
	{
		$file = "";
	}	

	// Temp data
	$alert = "";

	if($role == 1)
	{
		$assigned_to = "";
	}
	else if($role == 2)
	{
		$assigned_to = "";		
	}
	else
	{
		$assigned_to = $user;		
	}

	// Get list of process plans for this module
	$legal_process_plan_list = i_get_legal_process_plan_list('',$file,'','','','','','',$assigned_to,'1','');

	if($legal_process_plan_list["status"] == SUCCESS)
	{
		$legal_process_plan_list_data = $legal_process_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_process_plan_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Completed Processes List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Completed Processes List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>
				    <th>File Number</th>
				    <th>Survey Number</th>
					<th>Process Type</th>
					<th>Start Date</th>
					<th>Land Owner</th>					
					<th>Village</th>
					<th>Extent</th>					
					<th>Assigned To</th>
					<th>Planned Completion Date</th>
					<th>Actual Completion Date</th>
					<th>Variance</th>
					<th>&nbsp;</th>						
					<th>&nbsp;</th>					
				</tr>
				</thead>
				<tbody>
				 <?php
				if($legal_process_plan_list["status"] == SUCCESS)
				{		
					$sl_no = 0;
					for($count = 0; $count < count($legal_process_plan_list_data); $count++)
					{ 
						$sl_no++;
						$task_plan_result = i_get_task_plan_list('','',$legal_process_plan_list_data[$count]["process_plan_legal_id"],'','','','','desc');
						if($task_plan_result["status"] == SUCCESS)
						{
							if(get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00")
							{
								$end_date = date("Y-m-d");
								$actual_date = "0000-00-00";
							}
							else
							{
								$end_date = $task_plan_result["data"][0]["task_plan_actual_end_date"];
								$actual_date = $end_date;
							}
							$start_date = $task_plan_result["data"][0]["task_plan_planned_end_date"];																	
						}
						else
						{
							$start_date = "0000-00-00";
							$end_date   = "0000-00-00";
							
							$actual_date = $end_date;
						}
						
						if($task_plan_result["status"] == SUCCESS)
						{
							$variance = get_date_diff($start_date,$end_date);
							if($variance["status"] == 1)
							{
								if((get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
								{
									$css_class = "#FF0000";
								}
								else						
								{
									$css_class = "#FFA500";
								}
							}
							else
							{
								if((get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
								{
									$css_class = "#000000";
								}
								else						
								{
									$css_class = "#00FF00";
								}
							}
						}
						else
						{
							$css_class = "#0000FF";
						}
					?>
					<tr style="color:<?php echo $css_class; ?>">
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["file_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["file_survey_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["process_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($legal_process_plan_list_data[$count]["process_plan_legal_start_date"],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["file_land_owner"]; ?></td>					
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["file_village"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["file_extent"]; ?></td>					
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($start_date,"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($actual_date,"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo $variance["data"]; ?></td>
						<td style="word-wrap:break-word;"><a href="pending_task_list.php?process=<?php echo $legal_process_plan_list_data[$count]["process_plan_legal_id"]; ?>"><span style="color:black; text-decoration: underline;">View Pending Tasks</span></a><br /><br /><a href="completed_task_list.php?process=<?php echo $legal_process_plan_list_data[$count]["process_plan_legal_id"]; ?>"><span style="color:black; text-decoration: underline;">View Completed Tasks</span></a><br /><br /><a href="na_task_list.php?process=<?php echo $legal_process_plan_list_data[$count]["process_plan_legal_id"]; ?>"><span style="color:black; text-decoration: underline;">View NA Tasks</span></a></td>	
						<td style="word-wrap:break-word;"><a href="download.php?process=<?php echo $legal_process_plan_list_data[$count]["process_plan_legal_id"]; ?>" target="_blank"><span style="color:black; text-decoration: underline;">Download Documents</span></a></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<td colspan="14">No Projects added yet</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
