<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: task_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans for a particular process ID
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["process"]))
	{
		$process_plan_id = $_GET["process"];
	}
	else	
	{
		$process_plan_id = "";
	}
	
	if(isset($_GET["bprocess"]))
	{
		$bprocess_plan_id = $_GET["bprocess"];
	}
	else	
	{
		$bprocess_plan_id = "";
	}

	if(isset($_GET["task_type"]))
	{
		$task_type = $_GET["task_type"];
	}
	else
	{
		$task_type = "";
	}

	// Temp data
	$alert = "";

	if($role == 1)
	{
		$assigned_to = "";
	}
	else
	{
		$assigned_to = $user;
	}

	if(isset($_POST["task_update_button"]))
	{	
		$record_count    = $_POST["count"];
		$process_plan_id = $_POST["process_plan"];
		
		for($count = 0; $count < $record_count; $count++)
		{
			$task                = $_POST["task_data"][$count]['task_id'];
			$process_id          = $_POST["task_data"][$count]['process_id'];
			$bprocess_id         = $_POST["task_data"][$count]['bprocess_id'];
			$planned_end_date    = $_POST["task_data"][$count]['planned_date'];
			$start_date          = $_POST["task_data"][$count]['start_date'];
			$actual_end_date     = $_POST["task_data"][$count]['actual_date'];
			$document            = $_POST["task_data"][$count]['document'];
			$reason				 = $_POST["task_data"][$count]['delay_reason'];
			$approved_by         = $_POST["task_data"][$count]['approved_by'];
			$approved_on         = $_POST["task_data"][$count]['approved_on'];
			
			if($actual_end_date <= date("Y-m-d"))
			{
				$task_plan_update_result = i_update_task_plan($task,$planned_end_date,$start_date,$actual_end_date,$reason,'',$document,$approved_on);		
				
				if($task_plan_update_result["status"] == SUCCESS)
				{
					$alert = $task_plan_update_result["data"];
					$disp_class = "";
					
					if(($actual_end_date == "0000-00-00") || ($actual_end_date == "1969-12-31") || ($actual_end_date != "1970-01-01"))
					{
						if($process_id != "")
						{
							// Get file details
							$file_sdetails = i_get_legal_process_plan_details($process_plan_id);
						
							// Update process and task details in the file
							$update_result = i_update_file_details($task,$process_plan_id,$file_sdetails["data"]["process_plan_legal_file_id"]);
							
							if($update_result["status"] == SUCCESS)
							{
								$alert = "Task Plan Updated Successfully!";
								$disp_class = "green";
							}
							else
							{
								$alert = "Internal Error. please try again later!";
								$disp_class = "red";
							}
						}
						else if($bprocess_id != '0')
						{
							// Update file status in a loop
							$legal_bulk_files_search_data = array("legal_bulk_process_files_process_id"=>$bprocess_id);
							$bulk_process_files = i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data);
							
							if($bulk_process_files['status'] == SUCCESS)
							{
								for($bf_count= 0; $bf_count < count($bulk_process_files['data']); $bf_count++)
								{
									$update_result = i_update_file_bulk_details($task,$bprocess_id,$bulk_process_files['data'][$bf_count]['legal_bulk_process_file_id']);
								}
							}
						}
					}
				}
				else
				{
					$alert = $task_plan_update_result["data"];
					$disp_class = "red";
				}
			}
			else
			{
				$alert = "Actual Date cannot be later than today!";
				$disp_class = "red";
			}
		}
	}

	// Get list of task plans for this process plan
	if($role == 3)
	{
		$access_user = $user;
	}
	else
	{
		$access_user = "";
	}
	$legal_task_plan_list = i_get_task_plan_list('',$task_type,$process_plan_id,'','9999-99-99','',$access_user,'slno',$bprocess_plan_id);
	if($legal_task_plan_list["status"] == SUCCESS)
	{
		$legal_task_plan_list_data = $legal_task_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_task_plan_list["data"];
	}

	if($process_plan_id != "")
	{
		// Get specific project details
		$process_plan_details = i_get_legal_process_plan_details($process_plan_id);
	}
	else
	{
		$process_plan_details = "";
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Completed Task List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Completed Task List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<form action="completed_task_list.php" method="post" id="task_update_form">
			<input type="hidden" name="process_plan" value="<?php echo $process_plan_id; ?>" />
			<input type="hidden" name="bprocess_plan" value="<?php echo $bprocess_plan_id; ?>" />
			<input type="hidden" name="count" value="<?php if($legal_task_plan_list["status"] == SUCCESS)
			{
				echo count($legal_task_plan_list_data);
			}
			else
			{
				echo "0";
			}?>" />
			<span style="padding-left:50px;">
			<?php if($process_plan_details != "")
			{
				if($process_plan_details["status"] == SUCCESS)
				{
			?>
			Survey Number: <strong><?php echo $process_plan_details["data"]["file_survey_number"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Village: <strong><?php echo $process_plan_details["data"]["file_village"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Party Name: <strong><?php echo $process_plan_details["data"]["file_land_owner"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Process: <strong><?php echo $process_plan_details["data"]["process_name"]; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			Start Date: <strong><?php echo date("d-m-Y",strtotime($process_plan_details["data"]["process_plan_legal_start_date"])); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			<?php
				}
				else
				{
				?>
				Invalid Process! You might be looking for tasks of a deleted process
				<?php
				}
			}
			if($alert != "")
			{
				?>
				<span style="color:<?php echo $disp_class; ?>"><?php echo $alert; ?></span>
				<?php
			}
			else
			{
			?>
			All project tasks
			<?php
			}
			?>
			</span>
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>
					<th>Work Description</th>
					<th>Planned End Date</th>
					<th>Actual Start Date</th>
					<th>Actual End Date</th>
					<th>Variance (in days)</th>
					<th>Reason</th>
					<th>Document</th>
					<th>Assigned To</th>										
				</tr>
				</thead>
				<tbody>
				 <?php				
				if($legal_task_plan_list["status"] == SUCCESS)
				{		
					$sl_count = 0;
					for($count = 0; $count < count($legal_task_plan_list_data); $count++)
					{
						if($legal_task_plan_list_data[$count]["task_plan_legal_process_plan_id"] != '0')
						{
							$task_process_data = i_get_legal_process_plan_details($legal_task_plan_list_data[$count]["task_plan_legal_process_plan_id"]);
							$task_process_type = 'normal';
						}
						else if($legal_task_plan_list_data[$count]["task_plan_legal_bulk_process_plan_id"] != '0')
						{
							$legal_bulk_search_data = array("bulk_process_id"=>$legal_task_plan_list_data[$count]["task_plan_legal_bulk_process_plan_id"]);
							$task_process_data = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);
							$task_process_type = 'bulk';
							
							$project_file_number = '';
							$project_survey_number = '';
							$legal_bulk_files_search_data = array("legal_bulk_process_files_process_id"=>$legal_task_plan_list_data[$count]["task_plan_legal_bulk_process_plan_id"]);
							$process_files_result = i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data);			
							for($fcount = 0; $fcount < count($process_files_result['data']); $fcount++)
							{
								$project_file_number   = $project_file_number.$process_files_result['data'][$fcount]["file_number"].',';
								$project_survey_number = $project_survey_number.$process_files_result['data'][$fcount]["file_survey_number"].',';
							}			
							$project_file_number   = trim($project_file_number,',');
							$project_survey_number = trim($project_survey_number,',');
							
							$project_file_number = '<a href="bulk_file_list.php?bprocess='.$legal_task_plan_list_data[$count]["task_plan_legal_bulk_process_plan_id"].'" target="_blank">Click for file no and survey details</a>';
							$project_survey_number = trim($project_survey_number,',');
						}
						else
						{
							// Do nothing
						}
						$sl_count++;
						if(($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] == "0000-00-00") || ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] == "") || ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] == "1969-12-31") || ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] == "1970-01-01"))
						{
							// Do nothing
						}					
						else
						{
						if(get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00")
						{
							$end_date = date("Y-m-d");
						}
						else
						{
							$end_date = $legal_task_plan_list_data[$count]["task_plan_actual_end_date"];
						}
						$start_date = $legal_task_plan_list_data[$count]["task_plan_planned_end_date"];
						
						$variance = get_date_diff($start_date,$end_date);
						if($variance["status"] == 1)
						{
							if((get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#FF0000";								
							}
							else						
							{
								$css_class = "#FFA500";								
							}
						}
						else
						{
							if((get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#000000";								
							}
							else
							{	
								$css_class = "#00FF00";								
							}
						}
					?>
					<input type="hidden" name="task_data[<?php echo $count; ?>][task_id]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_id"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][process_id]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_process_plan_id"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][bprocess_id]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_bulk_process_plan_id"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][document]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_document_path"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][approved_by]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_approved_by"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][approved_on]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_planned_approved_on"]; ?>" />
					<input type="hidden" name="task_data[<?php echo $count; ?>][delay_reason]" value="<?php echo $legal_task_plan_list_data[$count]["task_plan_delay_reason"]; ?>" />
					<tr style="color:<?php echo $css_class; ?>">
						<td style="word-wrap:break-word;"><?php echo $sl_count; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $legal_task_plan_list_data[$count]["task_type_name"]; ?> (Lead Time: <?php echo $legal_task_plan_list_data[$count]["task_type_maximum_expected_duration"]; ?> days)<br /><br /><strong><?php if($task_process_type != 'bulk')
						{
							echo $task_process_data["data"]["file_number"]; ?></strong><br /><br />
							Sy No: <?php echo $task_process_data["data"]["file_survey_number"]; 
						}
						else
						{
							echo $project_file_number; ?></strong><br /><br />
							<?php
						}?></td>
						<td><input type="date" style="width:130px;" name="task_data[<?php echo $count; ?>][planned_date]" value="<?php echo get_formatted_date($legal_task_plan_list_data[$count]["task_plan_planned_end_date"],"Y-m-d"); ?>" <?php if(($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "0000-00-00") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "1969-12-31") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "1970-01-01") && (($role == 3) || ($role == 2))){?> readOnly="true" <?php } ?> /></td>
						<td><input type="date" style="width:130px;" name="task_data[<?php echo $count; ?>][start_date]" value="<?php echo get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_start_date"],"Y-m-d"); ?>" <?php if(($legal_task_plan_list_data[$count]["task_plan_actual_start_date"] != "0000-00-00") && ($legal_task_plan_list_data[$count]["task_plan_actual_start_date"] != "") && ($legal_task_plan_list_data[$count]["task_plan_actual_start_date"] != "1969-12-31") && ($legal_task_plan_list_data[$count]["task_plan_actual_start_date"] != "1970-01-01") && (($role == 3) || ($role == 2))) { ?> readOnly="true" <?php } ?> /></td>
						<td><input type="date" style="width:130px;" name="task_data[<?php echo $count; ?>][actual_date]" value="<?php echo get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d"); ?>" <?php if(($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "0000-00-00") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "1969-12-31") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "1970-01-01") && (($role == 3) || ($role == 2))) { ?> readOnly="true" <?php } ?> /></td>
						<td><?php echo $variance["data"];?></td>
						<td><a href="reason_list.php?task=<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_id"]; ?>"><span style="color:black; text-decoration: underline;"><?php echo $legal_task_plan_list_data[$count]["reason"]; ?></span></a></td>
						<td><?php if($legal_task_plan_list_data[$count]["task_plan_document_path"] != "") { ?><a href="documents/<?php echo $legal_task_plan_list_data[$count]["task_plan_document_path"]; ?>" target="_blank"><span style="color:black; text-decoration: underline;">Download</span></a><?php } ?><br /><br /><a href="upload_document.php?task=<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_id"]; ?>"><span style="color:black; text-decoration: underline;">Upload Document</span></a></td>
						<td><?php echo $legal_task_plan_list_data[$count]["user_name"]; ?></td>									
						
					</tr>
					<?php 
						}
					}
				}
				else
				{
				?>
				<td colspan="9">No tasks added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <br />
			  <?php
			  if($role == '1')
			  {
			  ?>
			<input type="submit" class="btn btn-primary" name="task_update_button" value="Save" />
			<?php
			}
			?>
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>