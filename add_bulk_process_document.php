<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 10th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['process']))
	{
		$bulk_document_process_id = $_GET['process'];
	}
	else
	{
		$bulk_document_process_id = '';
	}

	// Capture the form data
	if(isset($_POST["add_bulk_process"]))
	{
		$bulk_document_process_id = $_POST["hd_process_id"];		
		$bulk_document_type	      = $_POST["stxt_doc_type"];
		$bulk_document_remarks    = $_POST["txt_remarks"];
		$bulk_document_added_by   = $user;
		
		// Check for mandatory fields
		if($bulk_document_type !="")
		{
			$legal_bulk_iresult = i_add_legal_bulk_documents($bulk_document_process_id,$bulk_document_type,$bulk_document_remarks,$bulk_document_added_by);
			
			if($legal_bulk_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = "Document Successfully added";
			}
			else
			{
				$alert_type = 0;
				$alert      = $legal_bulk_iresult["data"];
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get bulk process document list
	$legal_bulk_doc_search_data = array('bulk_document_process_id'=>$bulk_document_process_id);
	$bulk_process_doc_list = i_get_legal_bulk_documents($legal_bulk_doc_search_data);
	if($bulk_process_doc_list['status'] == SUCCESS)
	{
		$bulk_process_doc_list_data = $bulk_process_doc_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Legal Bulk Process Documents</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Document</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Document</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_land_status" class="form-horizontal" method="post" action="add_bulk_process_document.php">
								<input type="hidden" name="hd_process_id" value="<?php echo $bulk_document_process_id; ?>" />
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="stxt_doc_type">Document Type *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_doc_type" placeholder="Name of the document" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Enter Remarks" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_bulk_process" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						<span id="span_msg"></span>
						<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>SL No</th>					
									<th>Document Type</th>	
									<th>Remarks</th>
									<th>Status</th>	
									<th>Added On</th>	
									<th>Last Updated On</th>	
									<th>&nbsp;</th>			
								</tr>
								</thead>
								<tbody>							
								<?php
								$sl_no = 0;								
								if($bulk_process_doc_list["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($bulk_process_doc_list_data); $count++)
									{	
										$sl_no++;
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
									<td style="word-wrap:break-word;"><?php echo $bulk_process_doc_list_data[$count]["bulk_document_type"]; ?></td>		
									<td style="word-wrap:break-word;"><?php echo $bulk_process_doc_list_data[$count]["bulk_document_remarks"]; ?></td>
									<td style="word-wrap:break-word;"><?php if($bulk_process_doc_list_data[$count]["bulk_document_status"] == '0')
									{
										echo 'Pending';
										$action = '1';
										$action_disp = "Mark as complete";
									}
									else
									{
										echo 'Completed';
										$action = '0';
										$action_disp = "Mark as pending";
									}?></td>													
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($bulk_process_doc_list_data[$count]["bulk_document_added_on"])); ?></td>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($bulk_process_doc_list_data[$count]["bulk_document_updated_on"])); ?></td>
									<td style="word-wrap:break-word;"><a href="#" onclick="return change_doc_status(<?php echo $bulk_process_doc_list_data[$count]["bulk_document_id"]; ?>,<?php echo $action; ?>);"><?php echo $action_disp; ?></a></td>
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="5">No documents yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function change_doc_status(doc_id,status)
{
	var ok = confirm("Are you sure you want to change status of document?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{				
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "add_bulk_process_document.php";
					}
				}
			}

			xmlhttp.open("POST", "change_bulk_document_status.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("document=" + doc_id + "&status=" + status);
		}
	}	
}
</script>

  </body>

</html>
