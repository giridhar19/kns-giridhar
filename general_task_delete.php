<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: general_task_delete.php
CREATED ON	: 08-November-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Delete a general task
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["task"]))
	{
		$task_id = $_GET["task"];
	}
	else
	{
		$task_id = "";
	}
	if(isset($_GET["search_user"]))
	{
		$search_user = $_GET["search_user"];
	}
	else
	{
		$search_user = "-1";
	}
	if(isset($_GET["search_project"]))
	{
		$search_project = $_GET["search_project"];
	}
	else
	{
		$search_project = "";
	}
	if(isset($_GET["search_department"]))
	{
		$search_department = $_GET["search_department"];
	}
	else
	{
		$search_department = "";
	}
	// Check whether this user is authorized to do this change
	$general_task_plan_list = i_get_gen_task_plan_list($task_id,'','','','','','','','');
	if($general_task_plan_list["status"] == SUCCESS)
	{
		if($role == "1")
		{
			// Delete the task
			$task_delete_result = i_delete_gen_task_plan($task_id);
			
			if($task_delete_result["status"] == SUCCESS)
			{
				$msg = "Task was successfully deleted";
			}
			else
			{
				$msg = "There was an internal error in deleting the task. Please try after some time";
			}
		}
		else
		{
			$msg = "You are not authorized to delete this task!";
		}
	}
	else
	{
		$msg = "This seems to be an invalid task. Please check!";
	}

	header("location:general_task_summary.php?msg=$msg&search_user=$search_user");
}
else
{
	header("location:login.php");
}	
?>