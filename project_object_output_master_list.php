<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START *///357
define('PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID','');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID,'1','1');

	// Query String Data
	// Nothing
	
	// Temp data
	$alert_type = -1;
	$alert 	    = "";
	
	$search_process = "" ;
	if(isset($_POST["process_search_submit"]))
	{
		$search_process   = $_POST["search_process"];
	}
	elseif(isset($_REQUEST["search_process"]))
	{
		$search_process   	 = $_REQUEST["search_process"];
	}
	else
	{
		$search_process = "" ;
	}
	// Get Already added Object Output
	$project_object_output_search_data = array("active"=>'1',"process_id"=>$search_process);
	$project_object_output_master_list = i_get_project_object_output_task($project_object_output_search_data);
	if($project_object_output_master_list["status"] == SUCCESS)
	{
		$project_object_output_master_list_data = $project_object_output_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_object_output_master_list["data"];
	}
	
	// Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Master - Object Output List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Master - Object Output List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="project_add_object_output.php">Add Project Object Output</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="project_object_output_master_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			 
			  <input type="submit" name="process_search_submit" />
			  </form>			  
            </div>
            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				  
				    <th>SL No</th>					
					<th>Process</th>
					<th>Task</th>
					<th>UOM</th>
					<th>Object Type</th>
					<th>Object Type Refernce</th>
					<th>Object Output</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="2" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_object_output_master_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_object_output_master_list_data); $count++)
					{
						$sl_no++;												
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>					
					<td><?php echo $project_object_output_master_list_data[$count]["project_process_master_name"]; ?></td>	
					<td><?php echo $project_object_output_master_list_data[$count]["project_task_master_name"]; ?></td>	
					<td><?php echo $project_object_output_master_list_data[$count]["project_uom_name"]; ?></td>
					<td><?php echo $project_object_output_master_list_data[$count]["project_object_output_object_type"]; ?></td>
					<td><?php if($project_object_output_master_list_data[$count]["project_object_output_object_type"] == "MC")
					{
						echo $project_object_output_master_list_data[$count]["project_machine_type_master_name"]; 
					}
					else 
					{
						echo 'CW'. $project_object_output_master_list_data[$count]["project_object_output_object_type_reference_id"]; 
					} ?></td>
					<td><?php echo $project_object_output_master_list_data[$count]["project_object_output_obejct_per_hr"]; ?></td>
					<td><?php echo $project_object_output_master_list_data[$count]["project_object_output_remarks"]; ?></td>
					<td><?php echo $project_object_output_master_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_object_output_master_list_data[$count][
					"project_object_output_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_project_object_output_master('<?php echo $project_object_output_master_list_data[$count]["project_object_output_master_id"]; ?>','<?php echo $search_process ;?>');">Edit </a><?php } ?></td>
					<td><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($project_object_output_master_list_data[$count]["project_process_master_active"] == "1")){?><a href="#" onclick="return project_delete_object_output_master(<?php echo $project_object_output_master_list_data[$count]["project_object_output_master_id"]; ?>);">Delete</a><?php } ?><?php } ?></td>
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="8">No Process Master added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			    <?php 
				} 
				?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_object_output_master(output_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
						 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
						 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_object_output_master_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_delete_object_output.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("output_id=" + output_id + "&action=0");
		}
	}	
}
function go_to_project_edit_project_object_output_master(object_output_id,search_process)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_edit_object_output.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","object_output_id");
	hiddenField1.setAttribute("value",object_output_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","search_process");
	hiddenField2.setAttribute("value",search_process);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>