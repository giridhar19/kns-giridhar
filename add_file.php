<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1; // No alert
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["file"]))
	{
		$bd_file_id = $_GET["file"];
	}
	else
	{
		$bd_file_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_file_submit"]))
	{
		$bd_file_id       = $_POST["hd_bd_file_id"];
		$type             = $_POST["type"];
		$survey_no        = $_POST["survey_no"];
		$land_owner       = $_POST["land_owner"];
		$pan              = $_POST["pan_no"];
		$extent           = $_POST["extent"];
		$village          = $_POST["ddl_village"];
		$start_date       = $_POST["start_date"];
		$num_days         = $_POST["num_days"];
		$doc_path         = upload("document");
		
		// Check for mandatory fields
		if(($type !="") && ($survey_no !="") && ($land_owner !="") && ($pan !="") && ($extent !="") && ($village !="") && ($start_date !="") && ($num_days !=""))
		{
			$add_file_result = i_add_file($bd_file_id,$type,$survey_no,$land_owner,$pan,$extent,$village,$start_date,$num_days,$doc_path,'','',$user);
			if($add_file_result["status"] == SUCCESS)
			{
				$alert = $add_file_result["data"];
				$alert_type = 1;
			}
			else
			{
				$alert = $add_file_result["data"];
				$alert_type = 0;
			}	
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get list of file types
	$file_type_list = i_get_file_type_list('','1'); // Get file types for legal module
	if($file_type_list["status"] == SUCCESS)
	{
		$file_type_list_data = $file_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$file_type_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get file details from BD
	$bd_file_details = i_get_bd_files_list($bd_file_id,'','','','','','','');	
	if($bd_file_details["status"] == SUCCESS)
	{
		$bd_file_details_data = $bd_file_details["data"];
	}
	else
	{
		$alert_type = 1;
		$alert      = $bd_file_details["data"];
	}
	
	// Get village list
	$village_list = i_get_village_list('');
	if($village_list["status"] == SUCCESS)
	{
		$village_list_data = $village_list["data"];
	}
	else
	{
		$alert_type = 1;
		$alert      = $village_list["data"];
	}		
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add File</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add File</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add File</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_file_form" class="form-horizontal" method="post" action="add_file.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_bd_file_id" value="<?php echo $bd_file_id; ?>" />
									<fieldset>										
															
										<div class="control-group">											
											<label class="control-label" for="type">Type *</label>
											<div class="controls">
												<select name="type" required>
												<?php
												for($count = 0; $count < count($file_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $file_type_list_data[$count]["legal_file_type_id"]; ?></"><?php echo $file_type_list_data[$count]["legal_file_type_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
												<p class="help-block">File Type</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="survey_no">Survey Number *</label>
											<div class="controls">
												<input type="text" class="span6" name="survey_no" placeholder="Dont use spaces. Use only / symbol" value="<?php echo $bd_file_details_data[0]["bd_file_survey_no"]; ?>" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																														
										<div class="control-group">											
											<label class="control-label" for="land_owner">Land Owner *</label>
											<div class="controls">
												<input type="text" class="span6" name="land_owner" value="<?php echo $bd_file_details_data[0]["bd_file_owner"]; ?>" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="pan_no">PAN Number *</label>
											<div class="controls">
												<input type="text" class="span6" name="pan_no" placeholder="PAN Number of the land owner" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="extent">Extent *</label>
											<div class="controls">
												<input type="number" class="span6" name="extent" value="<?php echo $bd_file_details_data[0]["bd_file_extent"]; ?>" required="required" min="0" step="0.01">
												<p class="help-block">Enter in guntas</p>
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="village">Village *</label>
											<div class="controls">
												<select name="ddl_village" required>
												<?php 
												for($count = 0; $count < count($village_list_data); $count++)
												{?>
												<option value="<?php echo $village_list_data[$count]["village_id"]; ?>" <?php if($bd_file_details_data[0]["bd_file_village"] == $village_list_data[$count]["village_id"]) {?> selected <?php } ?>><?php echo $village_list_data[$count]["village_name"]; ?></option>
												<?php
												}?>
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="start_date">Start Date *</label>
											<div class="controls">
												<input type="date" class="span6" name="start_date" value="<?php echo date('Y-m-d');?>" required="required">
												<p class="help-block">The date on which first payment was made</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_days">No. of Days *</label>
											<div class="controls">
												<input type="text" class="span6" name="num_days" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="document">Document</label>
											<div class="controls">
												<input type="file" class="span6" name="document">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
																																																	<br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_file_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>