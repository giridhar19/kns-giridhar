<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_enquiry_list.php
CREATED ON	: 06-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Enquiries
*/
define('CRM_PROSPECTIVE_CLIENT_LIST_FUNC_ID','105');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];			// Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_PROSPECTIVE_CLIENT_LIST_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_PROSPECTIVE_CLIENT_LIST_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_PROSPECTIVE_CLIENT_LIST_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_PROSPECTIVE_CLIENT_LIST_FUNC_ID,'4','1');

	// Query String / Get form Data
	// Nothing here
	
	if(isset($_POST["prospect_search_submit"]))
	{
		$added_by          = $_POST["ddl_search_user"];
		$filter_start_date = $_POST["dt_start_date"];				
		$filter_end_date   = $_POST["dt_end_date"];	

		if(isset($_POST["cb_unqualifiable"]))
		{
			$unqualifiable = 1;
		}
		else
		{
			$unqualifiable = 0;
		}
	}
	else
	{
		if(($role == 1) || ($role == 5))
		{
			$added_by = "";
		}
		else
		{
			$added_by = $user;
		}
		
		$filter_start_date = "";
		$filter_end_date   = "";
		$unqualifiable     = 0;
	}
	
	// Temp data
	$alert = "";
	
	$prospective_client_list = i_get_prospective_clients('','',$added_by,$filter_start_date,$filter_end_date,$user);
	if($prospective_client_list["status"] == SUCCESS)
	{
		$prospective_client_list_data = $prospective_client_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$prospective_client_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Prospective Profile List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Prospective Profile List (Total Prospects = <span id="total_count_section">
			  <?php
			  if($prospective_client_list["status"] == SUCCESS)
			  {
				$preload_count = "<i>Calculating</i>";			
			  }
			  else
			  {
			    $preload_count = 0;
			  }
			  
			  echo $preload_count;
			  ?></span>)</h3>
            </div>
			
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="prospect_search" action="crm_prospective_profile_list.php">			  	  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_user">
			  <option value="">- - Select User - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{					
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($added_by == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_start_date" value="<?php echo $filter_start_date; ?>" />
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="date" name="dt_end_date" value="<?php echo $filter_end_date; ?>" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Include unqualified&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="cb_unqualifiable" <?php if($unqualifiable == 1){ ?> checked <?php } ?>/>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="prospect_search_submit" />
			  </span>			  
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Name</th>
					<th>Mobile</th>
					<th>Interested In</th>
					<th>Source</th>
					<th>Walk In</th>
					<th>Latest Status</th>
					<th>Enquiry Date</th>
					<th>Prospective Date</th>
					<th>Follow Up Count</th>
					<th>Next FollowUp</th>
					<th>Site Visit Count</th>
					<th>Profile Added By</th>	
					<th>Assignee</th>	
					<th>Tentative Closure Date</th>	
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($prospective_client_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$unq_count = 0;
					$booked_count = 0;
					$int_filter_out_count = 0;
					$unqualifiable_count = 0;
					for($count = 0; $count < count($prospective_client_list_data); $count++)
					{
						// Check if there is an active booking for this customer
						$cp_is_booking_exists = i_get_site_booking('','','',$prospective_client_list_data[$count]["crm_prospective_profile_id"],'','1','','','','','','','','');
						
						if(($cp_is_booking_exists["status"] == SUCCESS) && (($cp_is_booking_exists["data"][0]["crm_booking_status"] == "0") || ($cp_is_booking_exists["data"][0]["crm_booking_status"] == "1")))
						{
							$booked_count++;
						}
						else
						{							
							$latest_follow_up_data = i_get_enquiry_fup_list($prospective_client_list_data[$count]["enquiry_id"],'','','added_date_desc','0','1');						
							$follow_up_data = i_get_enquiry_fup_list($prospective_client_list_data[$count]["enquiry_id"],'','','','','');
							if($follow_up_data["status"] == SUCCESS)
							{
								$fup_count = count($follow_up_data["data"]);
							}
							else
							{
								$fup_count = 0;
							}
							
							// Site Visit Data
							$site_visit_data = i_get_site_travel_plan_list('','',$prospective_client_list_data[$count]["enquiry_id"],'','','','2','','');
							if($site_visit_data["status"] == SUCCESS)
							{
								$sv_count = count($site_visit_data["data"]);
							}
							else
							{
								$sv_count = 0;
							}
							
							// Is the enquiry unqualified
							$unqualified_data = i_get_unqualified_leads('',$prospective_client_list_data[$count]["enquiry_id"],'','','','','');
							if($unqualified_data["status"] != SUCCESS)
							{
								if((($unqualifiable == 0) && ($latest_follow_up_data["data"][0]["crm_cust_interest_status_id"] != "4")) || ($unqualifiable == 1))
								{
								$sl_no++;
							
								$prospective_closure_date_data = i_get_prospective_clients($prospective_client_list_data[$count]["crm_prospective_profile_id"],'','','','');
							?>
							<tr>
							<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["enquiry_number"]; ?></td>					
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["project_name"]; ?><br /><br />
							<a href="crm_prospective_client_profile.php?profile=<?php echo $prospective_client_list_data[$count]["crm_prospective_profile_id"]; ?>">Client Profile</a></td>					
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["cell"]; ?></td>			
							<td style="word-wrap:break-word;"><?php
											$buy_interest = explode(',',$prospective_client_list_data[0]["crm_prospective_profile_buying_interest"]);
											$buy_interest_string = "";
											for($int_count = 0; $int_count < count($buy_interest); $int_count++)
											{
												switch($buy_interest[$int_count])
												{
												case "1";
												$buy_interest_type = "20x30";
												break;
												
												case "2";
												$buy_interest_type = "30x40";
												break;
												
												case "3";
												$buy_interest_type = "40x60";
												break;
												
												case "4";
												$buy_interest_type = "Other";
												break;
												
												case "5";
												$buy_interest_type = "30x50";
												break;
												
												default;
												$buy_interest_type = "";
												break;
												}
												$buy_interest_string = $buy_interest_string.$buy_interest_type." , ";
											}
											echo trim($buy_interest_string,', ');
											?></td>			
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["enquiry_source_master_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php if($prospective_client_list_data[$count]["walk_in"] == "1")
							{
								echo "Yes";
							}
							else
							{
								echo "No";
							}?></td>
							<td style="word-wrap:break-word;"><?php echo $latest_follow_up_data["data"][0]["crm_cust_interest_status_name"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($prospective_client_list_data[$count]["added_on"])); ?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($prospective_client_list_data[$count]["crm_prospective_profile_added_on"])); ?></td>
							<td style="word-wrap:break-word;"><?php echo $fup_count; ?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i",strtotime($latest_follow_up_data["data"][0]["enquiry_follow_up_date"])); ?></td>
							<td style="word-wrap:break-word;"><?php echo $sv_count; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["added_by"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo $prospective_client_list_data[$count]["stm"]; ?></td>
							<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($prospective_closure_date_data["data"][0]["crm_prospective_profile_tentative_closure_date"])); ?>
							<br /><br />
							<a href="crm_update_prospective_profile.php?profile_id=<?php echo $prospective_client_list_data[$count]["crm_prospective_profile_id"]; ?>">Update Tentative Closure Date</a></td>
							<td style="word-wrap:break-word;"><a href="crm_enquiry_fup_list.php?enquiry=<?php echo $prospective_client_list_data[$count]["enquiry_id"]; ?>">Follow Up</a></td>
							
							<td style="word-wrap:break-word;"><a href="crm_site_status_display.php?client=<?php echo $prospective_client_list_data[$count]["crm_prospective_profile_id"]; ?>">Select Site</a></td>
							</tr>
							<?php
								}
								else
								{
									$unqualifiable_count++;
								}
							}
							else
							{
								$unq_count++;
							}
						}	
					}
				}
				else
				{
				?>
				<tr><td colspan="15">No prospective clients yet!</td></tr>
				<?php
				}	
				if($prospective_client_list["status"] == SUCCESS)
				{
					$final_count = count($prospective_client_list_data) - $unq_count - $int_filter_out_count - $booked_count - $unqualifiable_count;			
				}
				else
				{
					$final_count = 0;
				}
				 ?>	
				<script>
				document.getElementById("total_count_section").innerHTML = <?php echo $final_count; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>