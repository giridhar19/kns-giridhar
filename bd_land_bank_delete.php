<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 8th July 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user = $_SESSION["loggedin_user"];
	$role = $_SESSION["loggedin_role"];

	/* QUERY STRING DATA - START */
	$lb_id  = $_POST["land_bank_id"];
	$action = $_POST["action"];
	/* QUERY STRING DATA - END */

	// Enable or disable file from land bank
	$bd_land_bank_update_data = array('active'=>$action);
	$lb_update_result = i_update_bd_land_bank($lb_id,$bd_land_bank_update_data);

	if($lb_update_result["status"] == FAILURE)
	{
		echo $lb_update_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>