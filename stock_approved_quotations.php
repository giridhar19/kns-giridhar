<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: kns_Quotation_Compare_list.php

CREATED ON	: 4th-oct-2016

CREATED BY	: Lakshmi

PURPOSE     : List of quotation Compare withdrawals

*/



/*

TBD: 

*/
$_SESSION['module'] = 'Stock Transactions';



/* DEFINES - START */

define('PO_FUNC_ID','168');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',PO_FUNC_ID,'2','1');

	$add_perms_list    = i_get_user_perms($user,'',PO_FUNC_ID,'1','1');
	
	$delete_perms_list    = i_get_user_perms($user,'',PO_FUNC_ID,'4','1');



	// Query String Data

	if(isset($_REQUEST["indent_item_id"]))

	{

		$indent_item_id = $_REQUEST["indent_item_id"];

	}

	else

	{

		$indent_item_id = "-1";

	}

	if(isset($_POST["vendor_search_submit"]))

	{

		$search_vendor      = $_POST["hd_vendor_id"];

		$search_vendor_name = $_POST["stxt_vendor"];

	}

	else

	{

		$search_vendor      = "";

		$search_vendor_name = "";

	}

	// Temp data

	// Get quotation compare already added

	$stock_quotation_compare_search_data = array("active"=>'1',"status"=>'Approved',"quotation_vendor"=>$search_vendor,"po_not_complete"=>'1');

	$quotation_compare_list = i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

	

	if($quotation_compare_list['status'] == SUCCESS)

	{

		$quotation_compare_list_data = $quotation_compare_list['data'];

		$quotation_id 	= $quotation_compare_list_data[0]["stock_quotation_id"];

		$indent_item_id = $quotation_compare_list_data[0]["stock_quotation_id"];

		$vendor		    = $quotation_compare_list_data[0]["stock_quotation_vendor"];

	}	

	else

	{

		$quotation_id = "";

	}

	

	$stock_purchase_order_search_data = array("quotation_id"=>$quotation_id);

	$purcahse_order_list = i_get_stock_purchase_order_list($stock_purchase_order_search_data);

	if($purcahse_order_list['status'] == SUCCESS)

	{

		$purcahse_order_list_data = $purcahse_order_list['data'];

		$po_id = $purcahse_order_list_data[0]["stock_purchase_order_id"];

	}	

	else

	{

		$po_id ="-1";

	}

	

	//Get Venor Type of Service List

	$stock_vendor_master_search_data = array();

	$vendor_item_list = i_get_stock_vendor_master_list($stock_vendor_master_search_data);

	if($vendor_item_list["status"] == SUCCESS)

	{

		$vendor_item_list_data = $vendor_item_list["data"];

	}

	else

	{

		$alert = $vendor_item_list["data"];

		$alert_type = 0;

	}

	

	if(isset($_POST["items_submit"]))

	{		

		$vendor   = $_POST['hd_vendor_id'];

		$item_nos = $_POST['hd_quote_count'];		

		$purchase_order_iresult = $purchase_order_iresult = i_add_stock_purchase_order('','','',$vendor,'','','','','','','','','','',$user);

		if($purchase_order_iresult["status"] == SUCCESS)

		{

			$purchase_order_id = $purchase_order_iresult["data"];

		}

		for($item_count = 0 ; $item_count < $item_nos; $item_count++)

		{

			if(isset($_POST['cb_item_'.$item_count]))

			{

				$quotation_id = $_POST['cb_item_'.$item_count];

				$stock_quotation_compare_search_data = array("quotation_id"=>$quotation_id);

				$quotation_compare_list = i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

				if($quotation_compare_list["status"] == SUCCESS)

				{

					if(($_POST['hd_qty_'.$item_count] + $quotation_compare_list["data"][0]["stock_quotation_po_qty"]) <= $quotation_compare_list["data"][0]["stock_quotation_quantity"])

					{

						$order_id              = $purchase_order_id;

						$item                  = $quotation_compare_list["data"][0]["stock_quotation_indent_id"];

						$quantity			   = $_POST['hd_qty_'.$item_count];									

						$uom 		           = '';
						
						$excise_duty 		   = '';

						$tax_type	           = "";

						$cost		           = $quotation_compare_list["data"][0]["stock_quotation_amount"];;

						$transport	           = "";

						$vendor 	           = "";

						$delivery_schedule	   = "";

						$remarks 	           = "";

						$quotation_id		   = $quotation_id;

						

						$purchase_item_result = i_add_stock_purchase_order_items($order_id,$item,$quantity,$uom,$excise_duty,$tax_type,$cost,$transport,$delivery_schedule,$remarks,$user);

						if($purchase_item_result["status"] == SUCCESS)

						{

							$quotation_compare_update_data = array("po_qty"=>$quantity);

							if(($_POST['hd_qty_'.$item_count] + $quotation_compare_list["data"][0]["stock_quotation_po_qty"]) == $quotation_compare_list["data"][0]["stock_quotation_quantity"])

							{	

								$quotation_compare_update_data['status'] = 'Po';

							}

							

							$quotation_update_results = i_update_quotation_compare($quotation_id,'',$quotation_compare_update_data);																			

							header("location:stock_add_purchase_order.php?order_id=$order_id&quotation_id=$quotation_id");

							$alert_type = 1;

							$alert      = "Item Succesfully imported from indent";

						}

						else

						{

							$alert_type = 0;

							$alert      = $purchase_item_result["data"];

						}

					}

					else

					{

						$alert_type = 0;

						$alert      = '';

					}

				}

			}

		}

	}

	

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Quotation Compare List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

    <link href="css/style1.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Purchase Order Process</h3><span style="float:right; padding-right:20px;"></span>

			

	  		</div> <!-- /widget-header -->

			<div class="widget-header" style="height:50px; padding-top:10px;">               

			  <form method="post" id="file_search_form" action="stock_approved_quotations.php">

			  <input type="hidden" name="hd_vendor_id" id="hd_vendor_id" value="<?php echo $search_vendor; ?>" />

			  <span style="padding-right:20px;">

			  <input type="text" name="stxt_vendor" placeholder="Search by Vendor Name" autocomplete="off" id="stxt_vendor" onkeyup="return get_vendor_list();" value="<?php echo $search_vendor_name; ?>" />

			  <div id="search_results" class="dropdown-content"></div>

			  </span>

			  <span style="padding-left:20px; padding-right:20px;">

			  <input type="submit" name="vendor_search_submit" />

			  </span>

			  </form>			  

            </div>

			

            <!-- /widget-header -->

            <div class="widget-content">

			

              <table class="table table-bordered">

                <thead>

                  <tr>

				    <th>SL No</th>

					<th>Item</th>
					
					<th>Item Code</th> 
					
					<th>UOM</th> 
					
					<th>Rate</th>

					<th>Quantity</th>

					<th>Value</th>

					<th>Vendor</th>

					<th colspan="4" style="text-align:center;">Action &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if($delete_perms_list['status'] == SUCCESS){ ?><a href="#" onclick="return delete_indent_item_multiple();">Delete Selected</a><?php } ?></th>	</th>	


				</tr>

				</thead>

				<tbody>							

				<form method="post" action="stock_approved_quotations.php">

				<input type="hidden" name="hd_vendor_id" value="<?php echo $search_vendor; ?>" />

				<?php

				if($quotation_compare_list["status"] == SUCCESS)

				{		

					?>

					<input type="hidden" name="hd_quote_count" value="<?php echo count($quotation_compare_list_data); ?>" />

					<?php

					$sl_no = 0;

					for($count = 0; $count < count($quotation_compare_list_data); $count++)

					{

						$sl_no++;

						?>									

							<tr>

							<td><?php echo $sl_no; ?></td>

							<td><?php echo $quotation_compare_list_data[$count]["stock_material_name"]; ?></td>
							
							<td><?php echo $quotation_compare_list_data[$count]["stock_material_code"]; ?></td>
							
							<td><?php echo $quotation_compare_list_data[$count]["stock_unit_name"]; ?></td>

							<td><?php echo $per_unit_amount = $quotation_compare_list_data[$count]["stock_quotation_amount"]; ?></td>

							<td><input type="number" name="hd_qty_<?php echo $count; ?>" id="hd_qty_<?php echo $count; ?>" value="<?php echo $quotation_compare_list_data[$count]["stock_quotation_quantity"] - $quotation_compare_list_data[$count]["stock_quotation_po_qty"]; ?>" onkeyup="return check_qty(<?php echo $quotation_compare_list_data[$count]["stock_quotation_quantity"]; ?>,<?php echo $quotation_compare_list_data[$count]["stock_quotation_po_qty"]; ?>,<?php echo $count; ?>,<?php echo $per_unit_amount; ?>);" /></td>

							<td><span id="span_value_<?php echo $count; ?>"><?php echo $total_value = round(($per_unit_amount * ($quotation_compare_list_data[$count]["stock_quotation_quantity"] - $quotation_compare_list_data[$count]["stock_quotation_po_qty"])),2) ;?></span></td>

							<td style="word-wrap:break-word;"><?php echo $quotation_compare_list_data[$count]["stock_vendor_name"]; ?></td>

							<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_edit_quotation_compare('<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"]; ?>');">Edit </a></div></td>

							<td><?php if(($quotation_compare_list_data[$count]["stock_quotation_active"] == "1")){?><a href="#" onclick="return delete_quotation_compare('<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"]; ?>','<?php echo $indent_item_id; ?>');">Delete</a><?php } ?></td>

							<td><a href="#" onclick="return go_to_purchase_order(<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"]; ?>);">Purchase Order</a></td>-->

							<td><input type="checkbox" name="cb_item_<?php echo $count; ?>" value="<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"] ;?>"><br></td>
							
							<td style="word-wrap:break-word;"><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($quotation_compare_list_data[$count]["stock_quotation_active"] == "1")){?><input type="checkbox" name="cb_multiple_delete" value="<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"]; ?>" /><?php } ?><?php } ?></td>

							</tr>

						<?php							

					//}

					}

				}

				else

				{

				?>

				<td colspan="6">No approved quotations added yet!</td>

				<?php

				}

				 ?>

                </tbody>

              </table>

			  <?php

			  if(($search_vendor != "") && ($add_perms_list['status'] == SUCCESS))

			  {

			  ?>

			  <div class="modal-footer">                              

                 <button type="submit" name="items_submit" class="btn btn-primary">Submit</button>					  

                 </div>

				 <?php

			  }

			  ?>

				 </form>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>



<script>

function delete_quotation_compare(quotation_id,indent_item_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					alert(xmlhttp.responseText);

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "http://localhost/kns/Legal/stock_quotation_compare_list.php?indent_item_id=" + indent_item_id;

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_quotation_compare.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("quotation_id=" + quotation_id + "&indent_item_id=" + indent_item_id + "&action=0");

		}

	}	

}

function go_to_edit_quotation_compare(quotation_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_edit_quotation_compare.php");

	

	var hiddenField1 = document.createElement("input");

	hiddenField1.setAttribute("type","hidden");

	hiddenField1.setAttribute("name","quotation_id");

	hiddenField1.setAttribute("value",quotation_id);

	

	form.appendChild(hiddenField1);

	

	document.body.appendChild(form);

    form.submit();

}

function go_to_purchase_order(quotation_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_purchase_order.php");

	

	var hiddenField1 = document.createElement("input");

	hiddenField1.setAttribute("type","hidden");

	hiddenField1.setAttribute("name","quotation_id");

	hiddenField1.setAttribute("value",quotation_id);

	

	form.appendChild(hiddenField1);

	

	document.body.appendChild(form);

    form.submit();

}

function approve_quotation(quotation_id)

{

	var ok = confirm("Are you sure you want to Approve?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					alert(xmlhttp.responseText);

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "http://localhost/kns/Legal/stock_quotation_compare_list.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_approve_quotation.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("quotation_id=" + quotation_id + "&action=Approved");

		}

	}	

}

function get_vendor_list()

{ 

	var searchstring = document.getElementById('stxt_vendor').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{	

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/stock_get_vendor.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_vendor(vendor_id,search_vendor)

{

	document.getElementById('hd_vendor_id').value = vendor_id;

	document.getElementById('stxt_vendor').value = search_vendor;

	

	document.getElementById('search_results').style.display = 'none';

}



function check_qty(quote_qty,po_qty,count,rate)

{

	var cur_qty = parseInt(document.getElementById('hd_qty_' + count).value);

	

	var quote_qty = parseInt(quote_qty);

	var po_qty    = parseInt(po_qty);	

	

	if((cur_qty + po_qty) > quote_qty)

	{

		document.getElementById('hd_qty_' + count).value = quote_qty - po_qty;

		alert('PO Quantity cannot be greater than approved quantitty');

	}

	else

	{

		document.getElementById('span_value_' + count).innerHTML = rate*cur_qty;

	}

}
function delete_indent_item_multiple()
{
	var quote_items = document.getElementsByName('cb_multiple_delete');
	var selected_items = '';
	for(var count = 0; count < quote_items.length; count++) {
		if (quote_items[count].checked) {
			selected_items = selected_items + quote_items[count].value + ',';
		}
	}
	
	var ok = confirm("Are you sure you want to delete all the selected indent items?")
	{         
		if (ok)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "1")
					{
						alert( xmlhttp.responseText);
						document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
						document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
						window.location = "stock_approved_quotations.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/delete_quotation_items_multiple.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("quote_items=" + selected_items);
		}
	}	
}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>


  </body>



</html>