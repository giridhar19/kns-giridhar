<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing
	
	// Get Unit Measure modes already added
	$project_uom_master_search_data = array("active"=>'1');
	$unit_list =  i_get_project_uom_master($project_uom_master_search_data);
	if($unit_list['status'] == SUCCESS)
	{
		$unit_list_data = $unit_list['data'];
		for($uom_count = 0 ; $uom_count < count($unit_list_data); $uom_count++)
		{
			$result[$uom_count] = $unit_list_data[$uom_count]["project_uom_name"];
		}
	}
	else
	{
		//
	}
	echo json_encode($result);
}
else
{
	header("location:login.php");
}
?>