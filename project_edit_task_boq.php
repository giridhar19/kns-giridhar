<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['boq_id']))
	{
		$boq_id = $_GET['boq_id'];
	}
	else
	{
		$boq_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["edit_project_task_boq_submit"]))
	{
		$boq_id			   = $_POST["hd_boq_id"];
		$uom               = $_POST["ddl_unit"];
		$location          = $_POST["ddl_location"];
		$number            = $_POST["num_number"];
		$contract_task     = $_POST["ddl_task"];
		$length    		   = $_POST["length"];
		$breadth   		   = $_POST["breadth"];
		$depth    		   = $_POST["depth"];
		$total_sqft		   = $_POST["total_sqft"];
		$amount            = $_POST["amount"];
		$remarks 	       = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($uom != ""))
		{
			$project_task_boq_update_data = array("uom"=>$uom,"contract_task"=>$contract_task,"location"=>$location,"number"=>$number,"length"=>$length,"breadth"=>$breadth,"depth"=>$depth,"total_sqft"=>$total_sqft,"amount"=>$amount,"remarks"=>$remarks);
			$project_task_boq_iresult = i_update_project_task_boq($boq_id,'',$project_task_boq_update_data);
			
			if($project_task_boq_iresult["status"] == SUCCESS)
				
			{	
			  $alert_type = 1;
			 header("location:project_task_boq_list.php");
			}
			else
			{
			   $alert = "Project Already Exists";
			  $alert_type = 0;	
			}
			
			$alert = $project_task_boq_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			
		}
	}
	
	// Get Project Contract Rate modes already added
	$project_contract_rate_master_search_data = array("active"=>'1',"process"=>$process);
	$project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
	if($project_contract_rate_master_list['status'] == SUCCESS)
	{
		$project_contract_rate_master_list_data = $project_contract_rate_master_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_contract_rate_master_list["data"];
	}
		
	// Get project Contract Process modes already added
	$project_contract_process_search_data = array("active"=>'1');
	$project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
	if($project_contract_process_list['status'] == SUCCESS)
	{
		$project_contract_process_list_data = $project_contract_process_list['data'];
	}
	else
	{
		$alert = $project_contract_process_list["data"];
		$alert_type = 0;
	}

   // Get Stock master uom modes already added
	$stock_unit_measure_search_data = array("active"=>'1');
	$stock_unit_measure_list = i_get_stock_unit_measure_list($stock_unit_measure_search_data);
	if($stock_unit_measure_list['status'] == SUCCESS)
	{
		$stock_unit_measure_list_data = $stock_unit_measure_list['data'];
	}
	else
	{
		$alert = $stock_unit_measure_list["data"];
		$alert_type = 0;
	}

    // Get Project Task BOQ modes already added
	$project_task_boq_search_data = array("active"=>'1',"boq_id"=>$boq_id);
	$project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
	if($project_task_boq_list['status'] == SUCCESS)
	{
		$project_task_boq_list_data = $project_task_boq_list['data'];
	}

	// Get Project Location already added
	$project_site_location_mapping_master_search_data = array("active"=>'1');
	$project_site_location_list = i_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data);
	if($project_site_location_list['status'] == SUCCESS)
	{
		$project_site_location_list_data = $project_site_location_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_site_location_list["data"];
	}	
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Project Task BOQ</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Project Task BOQ</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Project Task BOQ</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_edit_task_boq_form" class="form-horizontal" method="post" action="project_edit_task_boq.php">
								<input type="hidden" name="hd_boq_id" value="<?php echo $boq_id; ?>" />
									<fieldset>										
										
										
										<div class="control-group">											
											<label class="control-label" for="ddl_process">Contract Process*</label>
											<div class="controls">
												<select name="ddl_process" disabled required>
												<option value="">- - Select Process - -</option>
												<?php
												for($count = 0; $count < count($project_contract_process_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_contract_process_list_data[$count]["project_contract_process_id"]; ?>"<?php if($project_contract_process_list_data[$count]["project_contract_process_id"] == $project_task_boq_list_data
												[0]["project_task_contract_process"]){ ?> selected="selected" <?php } ?>><?php echo $project_contract_process_list_data[$count]["project_contract_process_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->
											
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="ddl_task">Contract Task</label>
											<div class="controls">
												<select name="ddl_task" id="ddl_task" onchange="return get_contract_rate()" required>
												<option value="">- - Select Task - -</option>
												<?php
												for($count = 0; $count < count($project_contract_rate_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_contract_rate_master_list_data[$count]["project_contract_rate_master_id"]; ?>" <?php if($project_contract_rate_master_list_data[$count]["project_contract_rate_master_id"] == $project_task_boq_list_data[0]["project_task_contract_task"]){ ?> selected="selected" <?php } ?>><?php echo $project_contract_rate_master_list_data[$count]["project_contract_rate_master_work_task"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
												<label class="control-label" for="ddl_unit">UOM*</label>
												<div class="controls">
													<select name="ddl_unit" required>
													<option value="">- - Select UOM - -</option>
													<?php
													for($count = 0; $count < count($stock_unit_measure_list_data); $count++)
													{
													?>
													<option value="<?php echo $stock_unit_measure_list_data[$count]["stock_unit_id"]; ?>" <?php if($stock_unit_measure_list_data[$count]["stock_unit_id"] == $project_task_boq_list_data[0]["project_task_boq_uom"]){ ?> selected="selected" <?php } ?>><?php echo $stock_unit_measure_list_data[$count]["stock_unit_name"]; ?></option>
													<?php
													}
													?>
													</select>
												</div> <!-- /controls -->					
											</div> <!-- /control-group -->
											
										<div class="control-group">											
											<label class="control-label" for="ddl_location">Site Location</label>
											<div class="controls">
												<select name="ddl_location" id="ddl_location" required>
												<option value="">- - Select Site Location - -</option>
												<?php
												for($count = 0; $count < count($project_site_location_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_site_location_list_data[$count]["project_site_location_mapping_master_id"]; ?>" <?php if($project_site_location_list_data[$count]["project_site_location_mapping_master_id"] == $project_task_boq_list_data[0]["project_task_boq_location"]){ ?> selected <?php } ?>><?php echo $project_site_location_list_data[$count]["project_site_location_mapping_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_number">Number</label>
											<div class="controls">
												<input type="number" min="1" name="num_number" id="num_number" value="<?php echo $project_task_boq_list_data[0]["project_task_boq_number"]?>" onkeyup="return total_sqft_no();" placeholder="Number">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="length">Length</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="length" id="length" value="<?php echo $project_task_boq_list_data[0]["project_task_boq_length"]?>" onkeyup="return total_sqft_no();" placeholder="Length">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="breadth">Breadth</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="breadth"  id="breadth" value="<?php echo $project_task_boq_list_data[0]["project_task_boq_breadth"]?>" onkeyup="return total_sqft_no();" placeholder="Breadth">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="depth">Depth</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="depth"  id="depth"  value="<?php echo $project_task_boq_list_data[0]["project_task_boq_depth"]?>" value="1"  onkeyup="return total_sqft_no();"  placeholder="Depth">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="total_sqft">Total Sqft</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="total_sqft" value="<?php echo $project_task_boq_list_data[0]["project_task_boq_total_measurement"]?>" id="total_sqft"  placeholder="Total Sqft">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="amount">Contract Rate</label>
											<div class="controls">
												<input type="number" step="0.01" min="0.01" name="amount"  id="amount" value="<?php echo $project_task_boq_list_data[0]["project_task_boq_amount"]?>" placeholder="Contract Rate">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" name="txt_remarks" placeholder="Remarks" value="<?php echo $project_task_boq_list_data[0]["project_task_boq_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_project_task_boq_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function total_sqft_no()
{
	var number = parseFloat(document.getElementById('num_number').value);
	var length = parseFloat(document.getElementById('length').value);
	var breadth = parseFloat(document.getElementById('breadth').value);
	var depth = parseFloat(document.getElementById('depth').value);
	
	var total_sqft = (length * breadth * depth * number);
	
	 document.getElementById("total_sqft").value = parseFloat(total_sqft).toFixed(2);
		
}
function get_contract_rate()
{       
	var contract_rate_id = document.getElementById("ddl_task").value;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			document.getElementById("amount").value = xmlhttp.responseText;
		}
	}

	xmlhttp.open("POST", "ajax/project_get_contract_rate.php");   //
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("contract_rate_id=" + contract_rate_id);
}
</script>


  </body>

</html>
