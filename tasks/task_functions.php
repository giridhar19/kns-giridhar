<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_tasks.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_payments.php');

/*
PURPOSE : To add legal task plan
INPUT 	: Type, Process Plan ID, Bulk Process ID, Planned End Date, Planned Start Date, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_task_plan_legal($type,$process_plan_id,$bprocess_id,$planned_end_date,$planned_start_date,$added_by)
{	
    $task_plan_legal_iresult = db_add_task_plan_legal($type,$process_plan_id,$bprocess_id,$planned_end_date,$planned_start_date,'',$added_by);
	
	if($task_plan_legal_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $task_plan_legal_iresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get task type list
INPUT 	: Task Type Name, Process, Active, Task Type ID
OUTPUT 	: Task Type List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_task_type_list($task_type_name,$process,$active,$task_type_id="")
{
	$task_type_sresult = db_get_task_type_list($task_type_name,$process,$active,'','','',$task_type_id);
	
	if($task_type_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $task_type_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No process type added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add legal task type
INPUT 	: Task Type, Process Type ID, Max. Duration, Is Document
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_task_type_legal($task_type,$process_type_id,$max_duration,$is_document,$added_by)
{
	$existing_task_list = db_get_task_type_list($task_type,$process_type_id,'','','','');
	if($existing_task_list["status"] == DB_NO_RECORD)
	{
		$task_type_legal_iresult = db_add_task_type_legal($task_type,$process_type_id,$max_duration,$is_document,$added_by);
		
		if($task_type_legal_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Task type successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "The task type $task_type already exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To add reason type
INPUT 	: Reason Type, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_reason_master($reason_type,$added_by)
{
	$reason_type_list = db_get_reason_list($reason_type,'','','','');

	if($reason_type_list["status"] == DB_NO_RECORD)
	{
		$reason_type_iresult = db_add_reason_master($reason_type,$added_by);
		
		if($reason_type_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Reason Master successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Reason already exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get task list
INPUT 	: Task ID, Task Type, Process Plan ID, Planned End Date, Actual End Date, Applicable, Added By, Order, Bulk Process Plan ID
OUTPUT 	: Task List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_task_plan_list($task_id,$task_type,$process_plan_id,$planned_end_date,$actual_end_date,$applicable,$added_by,$order,$bprocess_plan_id="")
{
	$task_plan_sresult = db_get_task_plan_list($task_id,$task_type,$process_plan_id,$planned_end_date,$actual_end_date,$applicable,$added_by,'','',$order,'1',$bprocess_plan_id);
	
	if($task_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $task_plan_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get reason list
INPUT 	: Reason, Active, Added By, Start Date, End Date
OUTPUT 	: Reason List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_reason_list($reason,$active,$added_by,$start_date,$end_date)
{
	$reason_sresult = db_get_reason_list($reason,$active,$added_by,$start_date,$end_date);
	
	if($reason_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $reason_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No reason added!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get delay reason list
INPUT 	: Task
OUTPUT 	: Reason List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_delay_reason_list($task,$reason)
{
	$delay_reason_sresult = db_get_delay_reason_list($task,$reason);
	
	if($delay_reason_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $delay_reason_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No reason added!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update task status
INPUT 	: Task Plan ID, Planned End Date, Start Date, Actual End Date, Reason, Remarks, Document, Approved On
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_update_task_plan($task_plan_id,$planned_end_date,$start_date,$actual_end_date,$reason,$remarks,$document,$approved_on)
{
	$task_plan_uresult = db_update_task_plan($task_plan_id,$planned_end_date,$start_date,$actual_end_date,$reason,$remarks,$document,$approved_on);
	
	if($task_plan_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Task successfully updated!"; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!"; 
    }
	
	return $return;
}

function i_delete_task($task_plan_id,$process_plan_id)
{
	$task_plan_uresult = db_enable_disable_task_plan($task_plan_id,$process_plan_id,'0');
	
	if($task_plan_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Task successfully deleted!"; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add delay reason
INPUT 	: Task, Reason, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_delay_reason($task_id,$reason_id,$remarks,$added_by,$sub_task="")
{
    $delay_reason_iresult = db_add_task_delay_reason($task_id,$sub_task,$reason_id,$remarks,$added_by);
	
	if($delay_reason_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Delay Reason added";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To update assignee
INPUT 	: Task, Assignee
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_assignee($task_id,$assigned_to)
{
    $assignee_uresult = db_update_assignee($task_id,$assigned_to);
	
	if($assignee_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Assignee updated";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To enable/disable task type
INPUT 	: Task Type, Action
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_enable_disable_task_type($task_type_id,$action)
{
    $task_type_uresult = db_enable_disable_task_type($task_type_id,$action);
	
	if($task_type_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Task Type status updated";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get request reason list
INPUT 	: Reason ID, Reason, Active, Added By, Start Date, End Date
OUTPUT 	: Reason List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_pay_request_reason_list($reason_id,$reason_name,$active,$added_by,$start_date,$end_date)
{
	$pay_reason_sresult = db_get_payment_reason_list($reason_id,$reason_name,$active,$added_by,$start_date,$end_date);
	
	if($pay_reason_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $pay_reason_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No payment reason added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add request reason
INPUT 	: Reason, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_pay_reason_request($reason,$added_by)
{
	$pay_reason_list = db_get_payment_reason_list('',$reason,'','','','');

	if($pay_reason_list["status"] == DB_NO_RECORD)
	{
		$pay_reason_iresult =  db_add_payment_request_reason($reason,$added_by);
		
		if($pay_reason_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Payment Reason successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "The payment reason $reason already exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To add legal payment request
INPUT 	: Task ID, Amount, Reason, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_pay_request_legal($task_id,$amount,$reason,$remarks,$added_by)
{	
    $pay_request_legal_iresult = db_add_payment_request($task_id,$amount,$reason,$remarks,$added_by);
	
	if($pay_request_legal_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $pay_request_legal_iresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get payment request list
INPUT 	: Payment Request Filter Array
OUTPUT 	: Payment Request List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_pay_request_list($pay_request_data)
{
	$pay_request_sresult = db_get_payment_request_list($pay_request_data);
	
	if($pay_request_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $pay_request_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No payment request added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update payment request details
INPUT 	: Payment Request ID, Payment Request Data
OUTPUT 	: Success or Failure message
BY 		: Nitin Kashyap
*/
function i_update_pay_request($payment_request_id,$payment_request_data)
{
	$pay_request_uresult = db_update_payment_request_status($payment_request_id,$payment_request_data);
	
	if($pay_request_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $pay_request_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No payment request added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add payment request update history
INPUT 	: Payment Request Update ID, Payment Request Data
OUTPUT 	: Payment Request Update ID, Success or Failure message
BY 		: Nitin Kashyap
*/
function i_add_request_history($request_id,$status,$remarks,$added_by)
{
	$pay_request_history_iresult = db_add_payment_request_history($request_id,$status,$remarks,$added_by);
	
	if($pay_request_history_iresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $pay_request_history_iresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later"; 
    }
	
	return $return;
}

/*
PURPOSE : To add payment release data
INPUT 	: Request ID, Amount, Mode, Remarks, Bill Submit Date, Added By
OUTPUT 	: Payment Release ID, Success or Failure message
BY 		: Nitin Kashyap
*/
function i_add_pay_release($request_id,$amount,$mode,$remarks,$bill_sub_date,$added_by)
{
	$pay_release_iresult = db_add_payment_release($request_id,$amount,$mode,$remarks,$bill_sub_date,$added_by);
	
	if($pay_release_iresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $pay_release_iresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later"; 
    }
	
	return $return;
}

/*
PURPOSE : To get payment released list
INPUT 	: Payment Released Filter Array
OUTPUT 	: Payment Released List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_pay_released_list($pay_release_data)
{
	$pay_released_sresult = db_get_payment_released_list($pay_release_data);
	
	if($pay_released_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $pay_released_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No payment released yet!";
    }
	
	return $return;
}

/*
PURPOSE : To get payment request history list
INPUT 	: Payment Request History Filter Array
OUTPUT 	: Payment Request History List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_pay_request_history_list($pay_history_data)
{
	$pay_history_sresult = db_get_payment_request_history($pay_history_data);
	
	if($pay_history_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $pay_history_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No payment update yet!";
    }
	
	return $return;
}

/*
PURPOSE : To update reason master
INPUT 	: Reason ID, Reason Data
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_update_reason_master($reason_id,$reason_data)
{
	$reason_uresult = db_update_reason_master($reason_id,$reason_data);
	
	if($reason_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Reason successfully updated!"; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!"; 
    }
	
	return $return;
}
/*
PURPOSE : To update Reason Subtask
INPUT 	: Reason id,Subtask
OUTPUT 	: success or failure message
BY 		: Soanakshi 
*/
function i_update_reason_task($sub_task,$reason_id)
{
	$task_plan_uresult = db_update_sub_task($sub_task,$reason_id);
	if($task_plan_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Task successfully updated!"; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No task plan added!"; 
    }
	
	return $return;
}

?>