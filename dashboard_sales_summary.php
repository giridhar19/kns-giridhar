<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 16th April 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_marketing'.DIRECTORY_SEPARATOR.'crm_marketing_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert          = "";
	$today          = 0;
	$this_week      = 0;
	$this_month     = 0;
	$this_quarter   = 0;
	/* DATA INITIALIZATION - END */
	
	$total_num_leads   = 0;
	$total_num_sales   = 0;
	
	if(isset($_POST['filter_dashboard_submit']))
	{
		$current_date = $_POST['dt_report_date'];
	}
	else
	{
		$current_date = date("Y-m-d");	
	}
	
	// Start and end date of week	
	$week_start_date = $current_date;
	$week_end_date   = $current_date;
	
	while(date("D",strtotime($week_start_date)) != "Mon")
	{
		$week_start_date = date("Y-m-d",strtotime($week_start_date." -1 days"));
	}
	while(date("D",strtotime($week_end_date)) != "Sun")
	{
		$week_end_date = date("Y-m-d",strtotime($week_end_date." +1 days"));
	}
	
	// Start date and end date of yesterday
	$yesterday_start_date = date('Y-m-d',strtotime($current_date."-1 days"));
	$yesterday_end_date   = date('Y-m-d',strtotime($current_date."-1 days"));
	
	
	// Start and end date of Month
	$current_year  = date("Y",strtotime($current_date));
	$current_month = date("m",strtotime($current_date));
	$number_of_days = cal_days_in_month(CAL_GREGORIAN,$current_month,$current_year);

	$month_start_date = ($current_year."-".$current_month."-"."01");
	$month_end_date   = ($current_year."-".$current_month."-".$number_of_days);

	/* LEADS - START */
	// No of leads generated from yestrday
	$yest_enq_filter = array('start_date'=>$yesterday_start_date.' 00:00:00','end_date'=>$yesterday_end_date.' 23:59:59');
	$yestrdays_number_of_leads = i_get_enquiry_count($yest_enq_filter);
	
	// No of leads generated from this week
	$week_enq_filter = array('start_date'=>$week_start_date.' 00:00:00','end_date'=>$week_end_date.' 23:59:59');
	$this_week_number_of_leads = i_get_enquiry_count($week_enq_filter);
		
	// No of leads generated from this month
	$week_enq_filter = array('start_date'=>$month_start_date.' 00:00:00','end_date'=>$month_end_date.' 23:59:59');
	$this_month_number_of_leads = i_get_enquiry_count($week_enq_filter);
	/* LEADS - END */
	
	/* SITE VISITS - START */
	// No of leads generated from yestrday
	$yest_tbfsv_filter = array('start_date'=>$yesterday_start_date.' 00:00:00','end_date'=>$yesterday_end_date.' 23:59:59');
	$yest_tbf_sv = i_get_sv_count($yest_tbfsv_filter);	
	
	// No of leads generated from this week
	$week_tbfsv_filter = array('start_date'=>$week_start_date.' 00:00:00','end_date'=>$week_end_date.' 23:59:59');
	$week_tbf_sv = i_get_sv_count($week_tbfsv_filter);
		
	// No of leads generated from this month
	$month_tbfsv_filter = array('start_date'=>$month_start_date.' 00:00:00','end_date'=>$month_end_date.' 23:59:59');
	$month_tbf_sv = i_get_sv_count($month_tbfsv_filter);
	/* SITE VISITS - END */
	
	/* COMPLETED SITE VISITS - START */
	// No of site visits completed yestrday
	$yest_comp_sv_result = i_get_site_visit_completed_enquiries('',$yesterday_start_date,$yesterday_end_date);
	if($yest_comp_sv_result['status'] == SUCCESS)
	{
		$yest_sv = count($yest_comp_sv_result['data']);
	}
	else
	{
		$yest_sv = 0;
	}
	
	// No of site visits completed this week
	$week_comp_sv_result = i_get_site_visit_completed_enquiries('',$week_start_date,$week_end_date);
	if($week_comp_sv_result['status'] == SUCCESS)
	{
		$week_sv = count($week_comp_sv_result['data']);
	}
	else
	{
		$week_sv = 0;
	}
		
	// No of site visits completed this month
	$month_comp_sv_result = i_get_site_visit_completed_enquiries('',$month_start_date,$month_end_date);
	if($month_comp_sv_result['status'] == SUCCESS)
	{
		$month_sv = count($month_comp_sv_result['data']);
	}
	else
	{
		$month_sv = 0;
	}
	/* COMPLETED SITE VISITS - END */
	
	/* PROSPECTIVE CLIENTS - START */
	// No of prospective clients added yesterday
	$yest_prosp_result = i_get_prospective_clients('','','',$yesterday_start_date,$yesterday_end_date);	
	if($yest_prosp_result['status'] == SUCCESS)
	{
		$yest_prosp = count($yest_prosp_result['data']);
	}
	else
	{
		$yest_prosp = 0;
	}
	
	// No of prospective clients added this week
	$week_prosp_result = i_get_prospective_clients('','','',$week_start_date,$week_end_date);
	if($week_prosp_result['status'] == SUCCESS)
	{
		$week_prosp = count($week_prosp_result['data']);
	}
	else
	{
		$week_prosp = 0;
	}
		
	// No of prospective clients added this month
	$month_prosp_result = i_get_prospective_clients('','','',$month_start_date,$month_end_date);
	if($month_prosp_result['status'] == SUCCESS)
	{
		$month_prosp = count($month_prosp_result['data']);
	}
	else
	{
		$month_prosp = 0;
	}
	/* PROSPECTIVE CLIENTS - END */
	
	/* SALES - START */
	// No of bookings done yesterday
	$yest_booking_filter_data = array('booking_start_date'=>$yesterday_start_date,'booking_end_date'=>$yesterday_end_date,'status'=>'1');
	$yest_sales_result = i_get_booking_summary_data($yest_booking_filter_data);
	$yest_sales_count = $yest_sales_result['data']['salecount'];
	$yest_sales_area  = $yest_sales_result['data']['salearea'];
	
	// No of bookings done this week
	$week_booking_filter_data = array('booking_start_date'=>$week_start_date,'booking_end_date'=>$week_end_date,'status'=>'1');
	$week_sales_result = i_get_booking_summary_data($week_booking_filter_data);	
	$week_sales_count = $week_sales_result['data']['salecount'];
	$week_sales_area  = $week_sales_result['data']['salearea'];
		
	// No of bookings done this week
	$month_booking_filter_data = array('booking_start_date'=>$month_start_date,'booking_end_date'=>$month_end_date,'status'=>'1');
	$month_sales_result = i_get_booking_summary_data($month_booking_filter_data);	
	$month_sales_count = $month_sales_result['data']['salecount'];
	$month_sales_area  = $month_sales_result['data']['salearea'];
	/* SALES - END */
	
	/* UNQUALIFIED ENQUIRIES - START */
	// No of leads unqualified yesterday
	$yest_unq_list = i_get_unqualified_leads('','','','','',$yesterday_start_date.' 00:00:00',$yesterday_end_date.' 23:59:59','','','','');
	if($yest_unq_list['status'] == SUCCESS)
	{
		$yest_unq = count($yest_unq_list['data']);
	}
	else
	{
		$yest_unq = 0;
	}
	
	// No of leads unqualified this week
	$week_unq_list = i_get_unqualified_leads('','','','','',$week_start_date.' 00:00:00',$week_end_date.' 23:59:59','','','','');
	if($week_unq_list['status'] == SUCCESS)
	{
		$week_unq = count($week_unq_list['data']);
	}
	else
	{
		$week_unq = 0;
	}
		
	// No of leads unqualified this month
	$month_unq_list = i_get_unqualified_leads('','','','','',$month_start_date.' 00:00:00',$month_end_date.' 23:59:59','','','','');
	if($month_unq_list['status'] == SUCCESS)
	{
		$month_unq = count($month_unq_list['data']);
	}
	else
	{
		$month_unq = 0;
	}
	/* UNQUALIFIED ENQUIRIES - END */
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Sales Summary - Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
	
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span6">                        
                    </div>
                    <!-- /span6 -->
                    <div class="span6">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Sales Summary</h3>
                            </div>
                            <!-- /widget-header -->
							<div class="widget-header" style="height:40px; padding-top:10px;">               
							  <form method="post" id="dashboard_filter" action="dashboard_sales_summary.php">			  
							  <span style="padding-left:8px; padding-right:8px;">
							  <input type="date" name="dt_report_date" value="<?php echo $current_date; ?>" />
							  </span>
							  <span style="padding-left:8px; padding-right:8px;">
							  <input type="submit" name="filter_dashboard_submit" />
							  </span>
							</div>
                            <div class="widget-content">
                                <!-- Tables to go here -->
								<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>&nbsp;</th>
									<th>Yesterday</th>
									<th>This Week</th>
									<th>This Month</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
									<td>Enquires</td>
									<td><a href="#" onclick="return go_to_enquiry_list('<?php echo $yesterday_start_date; ?>','<?php echo $yesterday_end_date; ?>');"><?php echo $yestrdays_number_of_leads; ?></a></td>
									<td><a href="#" onclick="return go_to_enquiry_list('<?php echo $week_start_date; ?>','<?php echo $week_end_date; ?>');"><?php echo $this_week_number_of_leads; ?></a></td>
									<td><a href="#" onclick="return go_to_enquiry_list('<?php echo $month_start_date; ?>','<?php echo $month_end_date; ?>');"><?php echo $this_month_number_of_leads; ?></a></td>
								  </tr>	
								  <tr>
									<td>TBF SV</td>
									<td><?php echo $yest_tbf_sv; ?></td>
									<td><?php echo $week_tbf_sv; ?></td>
									<td><?php echo $month_tbf_sv; ?></td>									
								  </tr>
								  <tr>
									<td>Site Visits</td>									
									<td><?php echo $yest_sv; ?></td>
									<td><?php echo $week_sv; ?></td>
									<td><?php echo $month_sv; ?></td>									
								  </tr>
								  <tr>
									<td>Prospective</td>
									<td><?php echo $yest_prosp; ?></td>
									<td><?php echo $week_prosp; ?></td>
									<td><?php echo $month_prosp; ?></td>									
								  </tr>
								  <tr>
									<td>Sales</td>
									<td><a href="#" onclick="return go_to_booking_list('<?php echo $yesterday_start_date; ?>','<?php echo $yesterday_end_date; ?>');"><?php echo $yest_sales_count; ?></a></td>
									<td><a href="#" onclick="return go_to_booking_list('<?php echo $week_start_date; ?>','<?php echo $week_end_date; ?>');"><?php echo $week_sales_count; ?></a></td>
									<td><a href="#" onclick="return go_to_booking_list('<?php echo $month_start_date; ?>','<?php echo $month_end_date; ?>');"><?php echo $month_sales_count; ?></a></td>									
								  </tr>
								  <tr>
									<td>Sales (in sq. ft)</td>
									<td><?php echo $yest_sales_area; ?></td>
									<td><?php echo $week_sales_area; ?></td>
									<td><?php echo $month_sales_area; ?></td>									
								  </tr>								  
								  <tr>
									<td>Unqualified</td>
									<td><?php echo $yest_unq; ?></td>
									<td><?php echo $week_unq; ?></td>
									<td><?php echo $month_unq; ?></td>									
								  </tr>
								</tbody>
								</table>
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
    <div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2016
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/excanvas.min.js"></script>
    <script src="js/chart.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/base.js"></script>
<script>
function go_to_enquiry_list(start_date,end_date)
{	
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "crm_enquiry_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","dt_start_date_entry_form");
	hiddenField1.setAttribute("value",start_date);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","dt_end_date_entry_form");
	hiddenField2.setAttribute("value",end_date);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","enquiry_search_submit");
	hiddenField3.setAttribute("value",'submit');
	
    form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_booking_list(start_date,end_date)
{	
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "crm_overall_report.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","dt_start_date");
	hiddenField1.setAttribute("value",start_date);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","dt_end_date");
	hiddenField2.setAttribute("value",end_date);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","search_report_submit");
	hiddenField3.setAttribute("value",'submit');
	
    form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}
</script>
</body>
</html>