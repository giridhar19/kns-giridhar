<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 05-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('APF_QUERY_RESPONSE_FUNC_ID','');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',APF_QUERY_RESPONSE_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',APF_QUERY_RESPONSE_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',APF_QUERY_RESPONSE_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',APF_QUERY_RESPONSE_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}
	
	if(isset($_GET['apf_process_id']))
	{
		$apf_process_id = $_GET['apf_process_id'];
	}
	else
	{
		$apf_process_id = "";
	}
	
	if(isset($_GET['query_id']))
	{
		$query_id = $_GET['query_id'];
	}
	else
	{
		$query_id = "";
	}

	// Query String
	if(isset($_REQUEST["apf_id"]))
	{
		$apf_id = $_REQUEST["apf_id"];		
	}	
	else
	{
		$apf_id = "";
	}
	
	
	// Get process details
		$apf_process_search_data = array("apf_id"=>$apf_id);
		$apf_process_list = i_get_apf_process($apf_process_search_data);
		if($apf_process_list['status'] == SUCCESS)
		{			
			$project_name   = $apf_process_list["data"][0]["apf_project_master_name"];
			$process_name   = $apf_process_list["data"][0]["apf_process_master_name"];
			$bank_name      = $apf_process_list["data"][0]["apf_bank_master_name"];
		}		
		else
		{
			$project_name  = "<em>PLEASE ADD PROCESS</em>";
			$process_name  = "<em>PLEASE ADD PROCESS</em>";
			$bank_name  = "<em>PLEASE ADD PROCESS</em>";
		}
	// Capture the form data
	if(isset($_POST["add_apf_query_submit"]))
	{
		$query_id         = $_POST["hd_query_id"];
		$process_id       = $_POST["hd_process_id"];
		$query            = $_POST["query"];
		$query_by         = $_POST["query_by"];
		$remarks          = $_POST["txt_remarks"];
		
		$query_date = date('Y-m-d');
		
		// Check for mandatory fields
		if(($query != "") && ($query_by != ""))
		{
			$adf_query_response_iresult = i_add_apf_query($process_id,$query,$query_by,$remarks,$user);
			
			if($adf_query_response_iresult["status"] == SUCCESS)				
			{	
		        header("location:apf_query_list.php?process_id=$process_id");
				$alert_type = 1;
				$alert = " Query Response Successfully Added";
			}
			
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get APF Process modes already added
	$apf_process_search_data = array("active"=>'1');
	$apf_process_list = i_get_apf_process($apf_process_search_data);
	if($apf_process_list['status'] == SUCCESS)
	{
		$apf_process_list_data = $apf_process_list['data'];
	}	
	else
	{
		$alert = $apf_process_list["data"];
		$alert_type = 0;
	}
	
	
	// Get APF Query Response modes already added
	$apf_query_search_data = array("active"=>'1',"query_id"=>$query_id,"process_id"=>$process_id);
	$apf_query_response_list = i_get_apf_query($apf_query_search_data);
	if($apf_query_response_list['status'] == SUCCESS)
	{
		$apf_query_response_list_data = $apf_query_response_list['data'];
	}	
}
else
{
	header("location:login.php");
}	

?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>APF - Add Query Response</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>APF - Add Query</h3><span style="float:right; padding-right:20px;"><a href="apf_query_list.php?process_id=<?php echo $process_id ;?>">APF - Query List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    Project:&nbsp;&nbsp;&nbsp;<?php echo $project_name; ?>&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;<br />Process:&nbsp;&nbsp;&nbsp;<?php echo $process_name; ?>&nbsp;&nbsp;&nbsp;&nbsp;Bank:&nbsp;&nbsp;&nbsp;<?php echo $bank_name; ?>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="apf_add_query_form" class="form-horizontal" method="post" action="apf_add_query.php">
								<input type="hidden" name="hd_query_id" value="<?php echo $query_id; ?>" />
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
									<fieldset>										
										 
										 <div class="control-group">											
											<label class="control-label" for="query">Query*</label>
											<div class="controls">
												<textarea rows="4" cols="50" class="span6" name="query" placeholder="Ex: Legal Opinion, No Response" required="required"></textarea>
											</div> <!-- /controls -->	
										 </div> <!-- /control-group -->
										 
										 <div class="control-group">											
											<label class="control-label" for="query_by">Query By*</label>
											<div class="controls">
												<input type="text" class="span6" name="query_by" placeholder="Query By">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
										<?php if($add_perms_list['status'] == SUCCESS){?>
											<input type="submit" class="btn btn-primary" name="add_apf_query_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
											<?php
											}
											?>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
