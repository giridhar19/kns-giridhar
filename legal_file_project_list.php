<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: legal_file_project_list.php
CREATED ON	: 23-August-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : List of survey numbers under projects
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'post_legal_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here
	
	// Initialization
	$alert_type = -1;
	$alert      = '';
	
	if(isset($_GET['file_project_search_submit']))
	{
		$survey_no = $_GET['survey'];
		$project   = $_GET['project'];
	}
	else
	{
		$survey_no = '';
		$project   = '';
	}

	// Get Survey No - Project Mapping List
	$project_file_list = i_get_site_project_list('',$project,$survey_no,'1','','','','','');
	if($project_file_list["status"] == SUCCESS)
	{
		$project_file_list_data = $project_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_file_list["data"];
	}
	
	// Get CRM project list
	$project_list = i_get_project_list('','');
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = -1;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project File Mapping List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
			<h3>List of Survey Numbers assigned to project</h3>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="get" id="file_project_search" action="legal_file_project_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($count = 0; $count < count($project_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php if($project == $project_list_data[$count]["project_id"]){ ?> selected <?php } ?>><?php echo $project_list_data[$count]["project_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>	
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="text" name="survey" value="<?php echo $survey_no; ?>" placeholder="Survey No" />
			  </span>			  
			  <input type="submit" name="file_project_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project Name</th>						
					<th>Survey Number</th>	
					<th>Extent</th>	
					<th>Account</th>	
					<th>Added On</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$sl_no = 0;
				if($project_file_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($project_file_list_data); $count++)
					{						
						$sl_no++;
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $project_file_list_data[$count]["project_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php if($project_file_list_data[$count]["bd_project_file_id"] != '')
						{
							echo $project_file_list_data[$count]["bd_file_survey_no"];
						}
						else
						{
							echo $project_file_list_data[$count]["file_survey_number"];
						}?></td>						
						<td style="word-wrap:break-word;"><?php echo $project_file_list_data[$count]["bd_file_extent"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $project_file_list_data[$count]["bd_own_account_master_account_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_file_list_data[$count]["site_project_added_on"])); ?></td>		
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="4">No File Project Mapping added!</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
