<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_machine_master_list.php
CREATED ON	: 28-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of machines
*/

/*
TBD: 
*/
$_SESSION['module'] = 'PM Masters';


/* DEFINES - START */
define('PROJECT_MASTER_PROJECT_MACHINE_FUNC_ID','186');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
	
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_FUNC_ID,'1','1');


	// Query String Data
	// Nothing
	
	$alert_type = -1;
	$alert = "";	

	// Temp data
	$project_machine_master_search_data = array("active"=>'1');
	$project_machine_master_list = i_get_project_machine_master($project_machine_master_search_data);
	if($project_machine_master_list["status"] == SUCCESS)
	{
		$project_machine_master_list_data = $project_machine_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_machine_master_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Master - Machine List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
	<link href="css/style1.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Master - Machine List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="project_master_add_machine.php">Add Machine</a></span><?php } ?>
            </div>
            <!-- /widget-header -->

            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Name</th>
					<th>Number</th>
					<th>Vendor</th>
					<th>Type</th>
					<th>Use</th>
					<th>Machine Type</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="2" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_machine_master_list["status"] == SUCCESS)
				{					
					$sl_no = 0;
					for($count = 0; $count < count($project_machine_master_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_machine_master_list_data[$count]["project_machine_master_name"]; ?></td>
					<td><?php echo $project_machine_master_list_data[$count]["project_machine_master_id_number"]; ?></td>
					<td><?php echo $project_machine_master_list_data[$count]["project_machine_vendor_master_name"]; ?></td>
					<td><?php echo $project_machine_master_list_data[$count]["project_machine_type"]; ?></td>
					<td><?php echo $project_machine_master_list_data[$count]["project_machine_master_use"]; ?></td>
					<td><?php echo $project_machine_master_list_data[$count]["project_machine_type_master_name"]; ?></td>
					<td><?php echo $project_machine_master_list_data[$count]["project_machine_master_remarks"]; ?></td>
					<td><?php echo $project_machine_master_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_machine_master_list_data[$count][
					"project_machine_master_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_machine_master('<?php echo $project_machine_master_list_data[$count]["project_machine_master_id"]; ?>');">Edit </a><?php } ?></td>
					<td><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($project_machine_master_list_data[$count]["project_machine_master_active"] == "1")){?><a href="#" onclick="return project_delete_machine_master(<?php echo $project_machine_master_list_data[$count]["project_machine_master_id"]; ?>);">Delete</a><?php } ?><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="10">No Machine details added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			   	<?php 
				} 
				?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_machine_master(machine_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_master_machine_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_master_delete_machine.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("machine_id=" + machine_id + "&action=0");
		}
	}	
}
function go_to_project_edit_machine_master(machine_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_master_edit_machine.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","machine_id");
	hiddenField1.setAttribute("value",machine_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}

function get_machine_list()
{ 
	var searchstring = document.getElementById('stxt_machine').value;
	
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{				
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{		
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results').style.display = 'block';
					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}
			}
		}

		xmlhttp.open("POST", "ajax/project_get_machine.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);				
	}
	else	
	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_machine(machine_id,name)
{
	document.getElementById('hd_machine_id').value 	= machine_id;
	document.getElementById('stxt_machine').value = name;
	
	document.getElementById('search_results').style.display = 'none';
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>