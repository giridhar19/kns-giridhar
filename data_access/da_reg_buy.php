<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
PURPOSE : To add new Reg Buy Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Process Master ID, success or failure message
BY 		: Lakshmi
*/
function db_add_reg_buy_process_master($name,$remarks,$added_by)
{
	// Query
   $reg_buy_process_master_iquery = "insert into reg_buy_process_master (reg_buy_process_master_name,reg_buy_process_master_active,reg_buy_process_master_remarks,reg_buy_process_master_added_by,reg_buy_process_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $reg_buy_process_master_istatement = $dbConnection->prepare($reg_buy_process_master_iquery);
        
        // Data
        $reg_buy_process_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $reg_buy_process_master_istatement->execute($reg_buy_process_master_idata);
		$reg_buy_process_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $reg_buy_process_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Reg Buy Process Master list
INPUT 	: Process Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Buy Process Master
BY 		: Lakshmi
*/
function db_get_reg_buy_process_master($reg_buy_process_master_search_data)
{  
	if(array_key_exists("process_master_id",$reg_buy_process_master_search_data))
	{
		$process_master_id = $reg_buy_process_master_search_data["process_master_id"];
	}
	else
	{
		$process_master_id= "";
	}
	
	if(array_key_exists("name",$reg_buy_process_master_search_data))
	{
		$name = $reg_buy_process_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("process_name_check",$reg_buy_process_master_search_data))
	{
		$process_name_check = $reg_buy_process_master_search_data["process_name_check"];
	}
	else
	{
		$process_name_check = "";
	}
	
	if(array_key_exists("active",$reg_buy_process_master_search_data))
	{
		$active = $reg_buy_process_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_process_master_search_data))
	{
		$added_by = $reg_buy_process_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$reg_buy_process_master_search_data))
	{
		$start_date= $reg_buy_process_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$reg_buy_process_master_search_data))
	{
		$end_date= $reg_buy_process_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_reg_buy_process_master_list_squery_base = "select * from reg_buy_process_master RBPM inner join users U on U.user_id = RBPM.reg_buy_process_master_added_by";
	
	$get_reg_buy_process_master_list_squery_where = "";
	$get_reg_buy_process_master_list_squery_order_by = " order by reg_buy_process_master_name ASC";
	
	$filter_count = 0;
	
	// Data
	$get_reg_buy_process_master_list_sdata = array();
	
	if($process_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." where reg_buy_process_master_id = :process_master_id";
		}
		else
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." and reg_buy_process_master_id = :process_master_id";
		}
		
		// Data
		$get_reg_buy_process_master_list_sdata[':process_master_id'] = $process_master_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($process_name_check == '1')
			{
				$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." where reg_buy_process_master_name = :name";		
				// Data
				$get_reg_buy_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." where reg_buy_process_master_name like :name";						

				// Data
				$get_reg_buy_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." where reg_buy_process_master_name like :name";						

				// Data
				$get_reg_buy_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($process_name_check == '1')
			{
				$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." and reg_buy_process_master_name = :name";	
					
				// Data
				$get_reg_buy_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." or reg_buy_process_master_name like :name";				
				// Data
				$get_reg_buy_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." and reg_buy_process_master_name like :name";
				
				// Data
				$get_reg_buy_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." where reg_buy_process_master_active = :active";
		}
		else
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." and reg_buy_process_master_active = :active";
		}
		
		// Data
		$get_reg_buy_process_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." where reg_buy_process_master_added_by = :added_by";
		}
		else
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." and reg_buy_process_master_added_by = :added_by";
		}
		
		//Data
		$get_reg_buy_process_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." where reg_buy_process_master_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." and reg_buy_process_master_added_on >= :start_date";
		}
		
		//Data
		$get_reg_buy_process_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." where reg_buy_process_master_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_reg_buy_process_master_list_squery_where = $get_reg_buy_process_master_list_squery_where." and reg_buy_process_master_added_on <= :end_date";
		}
		
		//Data
		$get_reg_buy_process_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reg_buy_process_master_list_squery = $get_reg_buy_process_master_list_squery_base.$get_reg_buy_process_master_list_squery_where.$get_reg_buy_process_master_list_squery_order_by;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reg_buy_process_master_list_sstatement = $dbConnection->prepare($get_reg_buy_process_master_list_squery);
		
		$get_reg_buy_process_master_list_sstatement -> execute($get_reg_buy_process_master_list_sdata);
		
		$get_reg_buy_process_master_list_sdetails = $get_reg_buy_process_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reg_buy_process_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reg_buy_process_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reg_buy_process_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
 
/*
PURPOSE : To update Reg Buy Process Master
INPUT 	: Process Master ID, Reg Buy Process Master Update Array
OUTPUT 	: Process Master ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_reg_buy_process_master($process_master_id,$reg_buy_process_master_update_data)
{
	if(array_key_exists("name",$reg_buy_process_master_update_data))
	{	
		$name = $reg_buy_process_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$reg_buy_process_master_update_data))
	{	
		$active = $reg_buy_process_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$reg_buy_process_master_update_data))
	{	
		$remarks = $reg_buy_process_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_process_master_update_data))
	{	
		$added_by = $reg_buy_process_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$reg_buy_process_master_update_data))
	{	
		$added_on = $reg_buy_process_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $reg_buy_process_master_update_uquery_base = "update reg_buy_process_master set ";  
	
	$reg_buy_process_master_update_uquery_set = "";
	
	$reg_buy_process_master_update_uquery_where = " where reg_buy_process_master_id = :process_master_id";
	
	$reg_buy_process_master_update_udata = array(":process_master_id"=>$process_master_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$reg_buy_process_master_update_uquery_set = $reg_buy_process_master_update_uquery_set." reg_buy_process_master_name = :name,";
		$reg_buy_process_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$reg_buy_process_master_update_uquery_set = $reg_buy_process_master_update_uquery_set." reg_buy_process_master_active = :active,";
		$reg_buy_process_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$reg_buy_process_master_update_uquery_set = $reg_buy_process_master_update_uquery_set." reg_buy_process_master_remarks = :remarks,";
		$reg_buy_process_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$reg_buy_process_master_update_uquery_set = $reg_buy_process_master_update_uquery_set." reg_buy_process_master_added_by = :added_by,";
		$reg_buy_process_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$reg_buy_process_master_update_uquery_set = $reg_buy_process_master_update_uquery_set." reg_buy_process_master_added_on = :added_on,";
		$reg_buy_process_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$reg_buy_process_master_update_uquery_set = trim($reg_buy_process_master_update_uquery_set,',');
	}
	
	$reg_buy_process_master_update_uquery = $reg_buy_process_master_update_uquery_base.$reg_buy_process_master_update_uquery_set.$reg_buy_process_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $reg_buy_process_master_update_ustatement = $dbConnection->prepare($reg_buy_process_master_update_uquery);		
        
        $reg_buy_process_master_update_ustatement -> execute($reg_buy_process_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_master_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Reg Buy Request
INPUT 	: Survey No, Village, Land Lard, Extent, Date, Request For, Remarks, Added By
OUTPUT 	: Request ID, success or failure message
BY 		: Lakshmi
*/
function db_add_reg_buy_request($village,$survey_no,$extent,$land_lard,$request_date,$request_for,$remarks,$added_by)
{
	// Query
   $reg_buy_request_iquery = "insert into reg_buy_request 
   (reg_buy_request_village,reg_buy_request_survey_no,reg_buy_request_extent,reg_buy_request_land_lard,reg_buy_request_date,reg_buy_request_for,reg_buy_request_active,
   reg_buy_request_remarks,reg_buy_request_added_by,reg_buy_request_added_on)
   values(:village,:survey_no,:extent,:land_lard,:request_date,:request_for,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $reg_buy_request_istatement = $dbConnection->prepare($reg_buy_request_iquery);
        
        // Data
        $reg_buy_request_idata = array(':village'=>$village,':survey_no'=>$survey_no,':extent'=>$extent,':land_lard'=>$land_lard,':request_date'=>$request_date,
		':request_for'=>$request_for,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $reg_buy_request_istatement->execute($reg_buy_request_idata);
		$reg_buy_request_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $reg_buy_request_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Reg Buy Request List
INPUT 	: Request ID, Survey No, Village, Land Lard, Extent, Request Date, Request For, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Buy Request
BY 		: Lakshmi
*/
function db_get_reg_buy_request($reg_buy_request_search_data)
{  
	if(array_key_exists("request_id",$reg_buy_request_search_data))
	{
		$request_id = $reg_buy_request_search_data["request_id"];
	}
	else
	{
		$request_id= "";
	}
	
	if(array_key_exists("village",$reg_buy_request_search_data))
	{
		$village = $reg_buy_request_search_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("survey_no",$reg_buy_request_search_data))
	{
		$survey_no = $reg_buy_request_search_data["survey_no"];
	}
	else
	{
		$survey_no = "";
	}
	
	if(array_key_exists("extent",$reg_buy_request_search_data))
	{
		$extent = $reg_buy_request_search_data["extent"];
	}
	else
	{
		$extent = "";
	}
	
	if(array_key_exists("land_lard",$reg_buy_request_search_data))
	{
		$land_lard = $reg_buy_request_search_data["land_lard"];
	}
	else
	{
		$land_lard = "";
	}
	
	if(array_key_exists("request_start_date",$reg_buy_request_search_data))
	{
		$request_start_date = $reg_buy_request_search_data["request_start_date"];
	}
	else
	{
		$request_start_date = "";
	}
	
	if(array_key_exists("request_end_date",$reg_buy_request_search_data))
	{
		$request_end_date = $reg_buy_request_search_data["request_end_date"];
	}
	else
	{
		$request_end_date = "";
	}
	
	if(array_key_exists("request_for",$reg_buy_request_search_data))
	{
		$request_for = $reg_buy_request_search_data["request_for"];
	}
	else
	{
		$request_for = "";
	}
	
	if(array_key_exists("active",$reg_buy_request_search_data))
	{
		$active = $reg_buy_request_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_request_search_data))
	{
		$added_by = $reg_buy_request_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$reg_buy_request_search_data))
	{
		$start_date= $reg_buy_request_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$reg_buy_request_search_data))
	{
		$end_date= $reg_buy_request_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_reg_buy_request_list_squery_base = "select * from reg_buy_request RBR inner join users U on U.user_id = RBR.reg_buy_request_added_by inner join bd_own_account_master AM on AM.bd_own_accunt_master_id = RBR.reg_buy_request_for inner join reg_buy_village_master RBVM on RBVM.reg_buy_village_master_id = RBR.reg_buy_request_village";
	
	$get_reg_buy_request_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_reg_buy_request_list_sdata = array();
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_id = :request_id";				
		}
		
		// Data
		$get_reg_buy_request_list_sdata[':request_id'] = $request_id;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_village = :village";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_village = :village";				
		}
		
		// Data
		$get_reg_buy_request_list_sdata[':village'] = $village;
		
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_survey_no = :survey_no";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_survey_no = :survey_no";				
		}
		
		// Data
		$get_reg_buy_request_list_sdata[':survey_no']  = $survey_no;
		
		$filter_count++;
	}
	
	if($extent != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_extent = :extent";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_extent = :extent";				
		}
		
		// Data
		$get_reg_buy_request_list_sdata[':extent']  = $extent;
		
		$filter_count++;
	}
	
	if($land_lard != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_land_lard = :land_lard";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_land_lard = :land_lard";				
		}
		
		// Data
		$get_reg_buy_request_list_sdata[':land_lard']  = $land_lard;
		
		$filter_count++;
	}
	
	if($request_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_date >= :request_start_date";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_date >= :request_start_date";				
		}
		
		// Data
		$get_reg_buy_request_list_sdata[':request_start_date']  = $request_start_date;
		
		$filter_count++;
	}
	
	if($request_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_date <= :request_end_date";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_date <= :request_end_date";				
		}
		
		// Data
		$get_reg_buy_request_list_sdata[':request_end_date']  = $request_end_date;
		
		$filter_count++;
	}
	
	if($request_for != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_for = :request_for";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_for = :request_for";				
		}
		
		// Data
		$get_reg_buy_request_list_sdata[':request_for']  = $request_for;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_active = :active";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_active = :active";				
		}
		
		// Data
		$get_reg_buy_request_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_added_by = :added_by";
		}
		
		//Data
		$get_reg_buy_request_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_added_on >= :start_date";				
		}
		
		//Data
		$get_reg_buy_request_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." where reg_buy_request_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reg_buy_request_list_squery_where = $get_reg_buy_request_list_squery_where." and reg_buy_request_added_on <= :end_date";
		}
		
		//Data
		$get_reg_buy_request_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reg_buy_request_list_squery = $get_reg_buy_request_list_squery_base.$get_reg_buy_request_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reg_buy_request_list_sstatement = $dbConnection->prepare($get_reg_buy_request_list_squery);
		
		$get_reg_buy_request_list_sstatement -> execute($get_reg_buy_request_list_sdata);
		
		$get_reg_buy_request_list_sdetails = $get_reg_buy_request_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reg_buy_request_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reg_buy_request_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reg_buy_request_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Reg Buy Request
INPUT 	: Request ID, Reg Buy Request Update Array
OUTPUT 	: Request ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_reg_buy_request($request_id,$reg_buy_request_update_data)
{
	if(array_key_exists("village",$reg_buy_request_update_data))
	{	
		$village = $reg_buy_request_update_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("survey_no",$reg_buy_request_update_data))
	{	
		$survey_no = $reg_buy_request_update_data["survey_no"];
	}
	else
	{
		$survey_no = "";
	}
	
	if(array_key_exists("extent",$reg_buy_request_update_data))
	{	
		$extent = $reg_buy_request_update_data["extent"];
	}
	else
	{
		$extent = "";
	}
	
	if(array_key_exists("land_lard",$reg_buy_request_update_data))
	{	
		$land_lard = $reg_buy_request_update_data["land_lard"];
	}
	else
	{
		$land_lard = "";
	}
	
	if(array_key_exists("request_date",$reg_buy_request_update_data))
	{	
		$request_date = $reg_buy_request_update_data["request_date"];
	}
	else
	{
		$request_date = "";
	}
	
	if(array_key_exists("request_for",$reg_buy_request_update_data))
	{	
		$request_for = $reg_buy_request_update_data["request_for"];
	}
	else
	{
		$request_for = "";
	}
	
	if(array_key_exists("active",$reg_buy_request_update_data))
	{	
		$active = $reg_buy_request_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$reg_buy_request_update_data))
	{	
		$remarks = $reg_buy_request_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_request_update_data))
	{	
		$added_by = $reg_buy_request_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$reg_buy_request_update_data))
	{	
		$added_on = $reg_buy_request_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $reg_buy_request_update_uquery_base = "update reg_buy_request set";  
	
	$reg_buy_request_update_uquery_set = "";
	
	$reg_buy_request_update_uquery_where = " where reg_buy_request_id = :request_id";
	
	$reg_buy_request_update_udata = array(":request_id"=>$request_id);
	
	$filter_count = 0;
	
	if($village != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_village = :village,";
		$reg_buy_request_update_udata[":village"] = $village;
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_survey_no = :survey_no,";
		$reg_buy_request_update_udata[":survey_no"] = $survey_no;
		$filter_count++;
	}
	
	if($extent != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_extent = :extent,";
		$reg_buy_request_update_udata[":extent"] = $extent;
		$filter_count++;
	}
	
	if($land_lard != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_land_lard = :land_lard,";
		$reg_buy_request_update_udata[":land_lard"] = $land_lard;
		$filter_count++;
	}
	
	if($request_date != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_date = :request_date,";
		$reg_buy_request_update_udata[":request_date"] = $request_date;
		$filter_count++;
	}
	
	if($request_for != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_for = :request_for,";
		$reg_buy_request_update_udata[":request_for"] = $request_for;
		$filter_count++;
	}
	
	if($active != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_active = :active,";
		$reg_buy_request_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_remarks = :remarks,";
		$reg_buy_request_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_added_by = :added_by,";
		$reg_buy_request_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$reg_buy_request_update_uquery_set = $reg_buy_request_update_uquery_set." reg_buy_request_added_on = :added_on,";
		$reg_buy_request_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$reg_buy_request_update_uquery_set = trim($reg_buy_request_update_uquery_set,',');
	}
	
	$reg_buy_request_update_uquery = $reg_buy_request_update_uquery_base.$reg_buy_request_update_uquery_set.$reg_buy_request_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $reg_buy_request_update_ustatement = $dbConnection->prepare($reg_buy_request_update_uquery);		
        
        $reg_buy_request_update_ustatement -> execute($reg_buy_request_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $request_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Reg Buy Reg Process
INPUT 	: Process Master ID, Request ID, Process Start Date, Process End Date, Remarks, Added By
OUTPUT 	: Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_reg_buy_reg_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	// Query
   $reg_buy_reg_process_iquery = "insert into reg_buy_reg_process 
   (reg_buy_reg_process_master_id,reg_buy_reg_process_request_id,reg_buy_reg_process_start_date,reg_buy_reg_process_end_date,reg_buy_reg_process_active,
   reg_buy_reg_process_remarks,reg_buy_reg_process_added_by,reg_buy_reg_process_added_on)
   values(:process_master_id,:request_id,:process_start_date,:process_end_date,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $reg_buy_reg_process_istatement = $dbConnection->prepare($reg_buy_reg_process_iquery);
        
        // Data
        $reg_buy_reg_process_idata = array(':process_master_id'=>$process_master_id,':request_id'=>$request_id,':process_start_date'=>$process_start_date,
		':process_end_date'=>$process_end_date,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $reg_buy_reg_process_istatement->execute($reg_buy_reg_process_idata);
		$reg_buy_reg_process_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $reg_buy_reg_process_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Reg buy Reg Process List
INPUT 	: Process ID, Process Master ID, Request ID, Process Start Date, Process End Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg buy Reg Process
BY 		: Lakshmi
*/
function db_get_reg_buy_reg_process($reg_buy_reg_process_search_data)
{  
	if(array_key_exists("process_id",$reg_buy_reg_process_search_data))
	{
		$process_id = $reg_buy_reg_process_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}
	
	if(array_key_exists("process_master_id",$reg_buy_reg_process_search_data))
	{
		$process_master_id = $reg_buy_reg_process_search_data["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}
	
	if(array_key_exists("request_id",$reg_buy_reg_process_search_data))
	{
		$request_id = $reg_buy_reg_process_search_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("process_start_date",$reg_buy_reg_process_search_data))
	{
		$process_start_date = $reg_buy_reg_process_search_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$reg_buy_reg_process_search_data))
	{
		$process_end_date = $reg_buy_reg_process_search_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$reg_buy_reg_process_search_data))
	{
		$active = $reg_buy_reg_process_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_reg_process_search_data))
	{
		$added_by = $reg_buy_reg_process_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$reg_buy_reg_process_search_data))
	{
		$start_date= $reg_buy_reg_process_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$reg_buy_reg_process_search_data))
	{
		$end_date= $reg_buy_reg_process_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_reg_buy_reg_process_list_squery_base = "select * from reg_buy_reg_process RBRP inner join users U on U.user_id=RBRP.reg_buy_reg_process_added_by inner join reg_buy_process_master RBPM on RBPM.reg_buy_process_master_id=RBRP.reg_buy_reg_process_master_id inner join reg_buy_request RBR on RBR.reg_buy_request_id=RBRP.reg_buy_reg_process_request_id";
	
	$get_reg_buy_reg_process_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_reg_buy_reg_process_list_sdata = array();
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." where reg_buy_reg_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." and reg_buy_reg_process_id = :process_id";				
		}
		
		// Data
		$get_reg_buy_reg_process_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($process_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." where reg_buy_reg_process_master_id = :process_master_id";								
		}
		else
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." and reg_buy_reg_process_master_id = :process_master_id";				
		}
		
		// Data
		$get_reg_buy_reg_process_list_sdata[':process_master_id'] = $process_master_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." where reg_buy_reg_process_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." and reg_buy_reg_process_request_id = :request_id";				
		}
		
		// Data
		$get_reg_buy_reg_process_list_sdata[':request_id']  = $request_id;
		
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." where reg_buy_reg_process_start_date = :process_start_date";								
		}
		else
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." and reg_buy_reg_process_start_date = :process_start_date";				
		}
		
		// Data
		$get_reg_buy_reg_process_list_sdata[':process_start_date']  = $process_start_date;
		
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." where reg_buy_reg_process_end_date = :process_end_date";								
		}
		else
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." and reg_buy_reg_process_end_date = :process_end_date";				
		}
		
		// Data
		$get_reg_buy_reg_process_list_sdata[':process_end_date']  = $process_end_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." where reg_buy_reg_process_active = :active";								
		}
		else
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." and reg_buy_reg_process_active = :active";				
		}
		
		// Data
		$get_reg_buy_reg_process_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." where reg_buy_reg_process_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." and reg_buy_reg_process_added_by = :added_by";
		}
		
		//Data
		$get_reg_buy_reg_process_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." where reg_buy_reg_process_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." and reg_buy_reg_process_added_on >= :start_date";				
		}
		
		//Data
		$get_reg_buy_reg_process_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." where reg_buy_reg_process_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reg_buy_reg_process_list_squery_where = $get_reg_buy_reg_process_list_squery_where." and reg_buy_reg_process_added_on <= :end_date";
		}
		
		//Data
		$get_reg_buy_reg_process_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reg_buy_reg_process_list_squery = $get_reg_buy_reg_process_list_squery_base.$get_reg_buy_reg_process_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reg_buy_reg_process_list_sstatement = $dbConnection->prepare($get_reg_buy_reg_process_list_squery);
		
		$get_reg_buy_reg_process_list_sstatement -> execute($get_reg_buy_reg_process_list_sdata);
		
		$get_reg_buy_reg_process_list_sdetails = $get_reg_buy_reg_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reg_buy_reg_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reg_buy_reg_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reg_buy_reg_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Reg buy Reg Process
INPUT 	: Process ID, Reg buy Reg Process Update Array
OUTPUT 	: Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_reg_buy_reg_process($process_id,$reg_buy_reg_process_update_data)
{
	if(array_key_exists("process_master_id",$reg_buy_reg_process_update_data))
	{	
		$process_master_id = $reg_buy_reg_process_update_data["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}
	
	if(array_key_exists("request_id",$reg_buy_reg_process_update_data))
	{	
		$request_id = $reg_buy_reg_process_update_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("process_start_date",$reg_buy_reg_process_update_data))
	{	
		$process_start_date = $reg_buy_reg_process_update_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$reg_buy_reg_process_update_data))
	{	
		$process_end_date = $reg_buy_reg_process_update_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$reg_buy_reg_process_update_data))
	{	
		$active = $reg_buy_reg_process_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$reg_buy_reg_process_update_data))
	{	
		$remarks = $reg_buy_reg_process_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_reg_process_update_data))
	{	
		$added_by = $reg_buy_reg_process_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$reg_buy_reg_process_update_data))
	{	
		$added_on = $reg_buy_reg_process_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $reg_buy_reg_process_update_uquery_base = "update reg_buy_reg_process set";  
	
	$reg_buy_reg_process_update_uquery_set = "";
	
	$reg_buy_reg_process_update_uquery_where = " where reg_buy_reg_process_id = :process_id";
	
	$reg_buy_reg_process_update_udata = array(":process_id"=>$process_id);
	
	$filter_count = 0;
	
	if($process_master_id != "")
	{
		$reg_buy_reg_process_update_uquery_set = $reg_buy_reg_process_update_uquery_set." reg_buy_reg_process_master_id = :process_master_id,";
		$reg_buy_reg_process_update_udata[":process_master_id"] = $process_master_id;
		$filter_count++;
	}
	
	if($request_id != "")
	{
		$reg_buy_reg_process_update_uquery_set = $reg_buy_reg_process_update_uquery_set." reg_buy_reg_process_request_id = :request_id,";
		$reg_buy_reg_process_update_udata[":request_id"] = $request_id;
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		$reg_buy_reg_process_update_uquery_set = $reg_buy_reg_process_update_uquery_set." reg_buy_reg_process_start_date = :process_start_date,";
		$reg_buy_reg_process_update_udata[":process_start_date"] = $process_start_date;
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		$reg_buy_reg_process_update_uquery_set = $reg_buy_reg_process_update_uquery_set." reg_buy_reg_process_end_date = :process_end_date,";
		$reg_buy_reg_process_update_udata[":process_end_date"] = $process_end_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$reg_buy_reg_process_update_uquery_set = $reg_buy_reg_process_update_uquery_set." reg_buy_reg_process_active = :active,";
		$reg_buy_reg_process_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$reg_buy_reg_process_update_uquery_set = $reg_buy_reg_process_update_uquery_set." reg_buy_reg_process_remarks = :remarks,";
		$reg_buy_reg_process_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$reg_buy_reg_process_update_uquery_set = $reg_buy_reg_process_update_uquery_set." reg_buy_reg_process_added_by = :added_by,";
		$reg_buy_reg_process_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$reg_buy_reg_process_update_uquery_set = $reg_buy_reg_process_update_uquery_set." reg_buy_reg_process_added_on = :added_on,";
		$reg_buy_reg_process_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$reg_buy_reg_process_update_uquery_set = trim($reg_buy_reg_process_update_uquery_set,',');
	}
	
	$reg_buy_reg_process_update_uquery = $reg_buy_reg_process_update_uquery_base.$reg_buy_reg_process_update_uquery_set.$reg_buy_reg_process_update_uquery_where;
    
    try
    {
        $dbConnection = get_conn_handle();
        
        $reg_buy_reg_process_update_ustatement = $dbConnection->prepare($reg_buy_reg_process_update_uquery);		
        
        $reg_buy_reg_process_update_ustatement -> execute($reg_buy_reg_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Reg Buy Document
INPUT 	: Process ID, File Path ID, Remarks, Added By
OUTPUT 	: Document ID, success or failure message
BY 		: Lakshmi
*/
function db_add_reg_buy_document($process_id,$file_path_id,$remarks,$added_by)
{
	// Query
   $reg_buy_document_iquery = "insert into reg_buy_document
   (reg_buy_document_process_id,reg_buy_document_file_path,reg_buy_document_active,reg_buy_document_remarks,reg_buy_document_added_by,reg_buy_document_added_on) values(:process_id,:file_path_id,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $reg_buy_document_istatement = $dbConnection->prepare($reg_buy_document_iquery);
        
        // Data
        $reg_buy_document_idata = array(':process_id'=>$process_id,':file_path_id'=>$file_path_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $reg_buy_document_istatement->execute($reg_buy_document_idata);
		$reg_buy_document_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $reg_buy_document_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Reg Buy Document list
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Buy Document
BY 		: Lakshmi
*/
function db_get_reg_buy_document($reg_buy_document_search_data)
{  
	if(array_key_exists("document_id",$reg_buy_document_search_data))
	{
		$document_id = $reg_buy_document_search_data["document_id"];
	}
	else
	{
		$document_id = "";
	}
	
	if(array_key_exists("process_id",$reg_buy_document_search_data))
	{
		$process_id = $reg_buy_document_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("request_id",$reg_buy_document_search_data))
	{
		$request_id = $reg_buy_document_search_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("file_path_id",$reg_buy_document_search_data))
	{
		$file_path_id = $reg_buy_document_search_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$reg_buy_document_search_data))
	{
		$active = $reg_buy_document_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_document_search_data))
	{
		$added_by = $reg_buy_document_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$reg_buy_document_search_data))
	{
		$start_date = $reg_buy_document_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$reg_buy_document_search_data))
	{
		$end_date = $reg_buy_document_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}

	$get_reg_buy_document_list_squery_base = "select * from reg_buy_document RBD inner join users U on U.user_id = RBD.reg_buy_document_added_by inner join reg_buy_reg_process RBRP on RBRP.reg_buy_reg_process_id=RBD.reg_buy_document_process_id inner join reg_buy_request RBR on RBR.reg_buy_request_id=RBRP.reg_buy_reg_process_request_id inner join reg_buy_process_master RBPM on RBPM.reg_buy_process_master_id = RBRP.reg_buy_reg_process_master_id";
	
	$get_reg_buy_document_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_reg_buy_document_list_sdata = array();
	
	if($document_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." where reg_buy_document_id = :document_id";								
		}
		else
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." and reg_buy_document_id = :document_id";				
		}
		
		// Data
		$get_reg_buy_document_list_sdata[':document_id'] = $document_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." where reg_buy_document_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." and reg_buy_document_process_id = :process_id";				
		}
		
		// Data
		$get_reg_buy_document_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." where RBRP.reg_buy_reg_process_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." and RBRP.reg_buy_reg_process_request_id = :request_id";				
		}
		
		// Data
		$get_reg_buy_document_list_sdata[':request_id'] = $request_id;
		
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." where reg_buy_document_file_path = :file_path_id";								
		}
		else
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." and reg_buy_document_file_path = :file_path_id";				
		}
		
		// Data
		$get_reg_buy_document_list_sdata[':file_path_id'] = $file_path_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." where reg_buy_document_active = :active";								
		}
		else
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." and reg_buy_document_active = :active";				
		}
		
		// Data
		$get_reg_buy_document_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." where reg_buy_document_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." and reg_buy_document_added_by = :added_by";
		}
		
		//Data
		$get_reg_buy_document_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." where reg_buy_document_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." and reg_buy_document_added_on >= :start_date";				
		}
		
		//Data
		$get_reg_buy_document_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." where reg_buy_document_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reg_buy_document_list_squery_where = $get_reg_buy_document_list_squery_where." and reg_buy_document_added_on <= :end_date";
		}
		
		//Data
		$get_reg_buy_document_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reg_buy_document_list_squery = " order by reg_buy_document_added_on DESC";
	$get_reg_buy_document_list_squery = $get_reg_buy_document_list_squery_base.$get_reg_buy_document_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reg_buy_document_list_sstatement = $dbConnection->prepare($get_reg_buy_document_list_squery);
		
		$get_reg_buy_document_list_sstatement -> execute($get_reg_buy_document_list_sdata);
		
		$get_reg_buy_document_list_sdetails = $get_reg_buy_document_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reg_buy_document_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reg_buy_document_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reg_buy_document_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
 /*
PURPOSE : To update Reg Buy Document 
INPUT 	: Document ID, Reg Buy Document Update Array
OUTPUT 	: Document ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_reg_buy_document($document_id,$reg_buy_document_update_data)
{
	
	if(array_key_exists("process_id",$reg_buy_document_update_data))
	{	
		$process_id = $reg_buy_document_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_path_id",$reg_buy_document_update_data))
	{	
		$file_path_id = $reg_buy_document_update_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$reg_buy_document_update_data))
	{	
		$active = $reg_buy_document_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$reg_buy_document_update_data))
	{	
		$remarks = $reg_buy_document_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_document_update_data))
	{	
		$added_by = $reg_buy_document_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$reg_buy_document_update_data))
	{	
		$added_on = $reg_buy_document_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $reg_buy_document_update_uquery_base = "update reg_buy_document set";  
	
	$reg_buy_document_update_uquery_set = "";
	
	$reg_buy_document_update_uquery_where = " where reg_buy_document_id = :document_id";
	
	$reg_buy_document_update_udata = array(":document_id"=>$document_id);
	
	$filter_count = 0;
	
	if($process_id != "")
	{
		$reg_buy_document_update_uquery_set = $reg_buy_document_update_uquery_set." reg_buy_document_process_id = :process_id,";
		$reg_buy_document_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		$reg_buy_document_update_uquery_set = $reg_buy_document_update_uquery_set." reg_buy_document_file_path = :file_path_id,";
		$reg_buy_document_update_udata[":file_path_id"] = $file_path_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$reg_buy_document_update_uquery_set = $reg_buy_document_update_uquery_set." reg_buy_document_active = :active,";
		$reg_buy_document_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$reg_buy_document_update_uquery_set = $reg_buy_document_update_uquery_set." reg_buy_document_remarks = :remarks,";
		$reg_buy_document_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$reg_buy_document_update_uquery_set = $reg_buy_document_update_uquery_set." reg_buy_document_added_by = :added_by,";
		$reg_buy_document_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$reg_buy_document_update_uquery_set = $reg_buy_document_update_uquery_set." reg_buy_document_added_on = :added_on,";
		$reg_buy_document_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$reg_buy_document_update_uquery_set = trim($reg_buy_document_update_uquery_set,',');
	}
	
	$reg_buy_document_update_uquery = $reg_buy_document_update_uquery_base.$reg_buy_document_update_uquery_set.$reg_buy_document_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $reg_buy_document_update_ustatement = $dbConnection->prepare($reg_buy_document_update_uquery);		
        
        $reg_buy_document_update_ustatement -> execute($reg_buy_document_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $document_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add New Reg Buy Village Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Master ID, success or failure message
BY 		: Ashwini
*/
function db_add_reg_buy_village_master($name,$remarks,$added_by)
{
	// Query
   $reg_buy_village_master_iquery = "insert into reg_buy_village_master 
   (reg_buy_village_master_name,reg_buy_village_master_active,reg_buy_village_master_remarks,reg_buy_village_master_added_by,reg_buy_village_master_added_on)
   values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $reg_buy_village_master_istatement = $dbConnection->prepare($reg_buy_village_master_iquery);
        
        // Data
        $reg_buy_village_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
		
		$dbConnection->beginTransaction();
        $reg_buy_village_master_istatement->execute($reg_buy_village_master_idata);
		$reg_buy_village_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $reg_buy_village_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Reg Buy Village Master List
INPUT 	: Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Buy Village Master
BY 		: Ashwini
*/
function db_get_reg_buy_village_master($reg_buy_village_master_search_data)
{  
	if(array_key_exists("village_id",$reg_buy_village_master_search_data))
	{
		$village_id = $reg_buy_village_master_search_data["village_id"];
	}
	else
	{
		$village_id= "";
	}
	
	if(array_key_exists("name",$reg_buy_village_master_search_data))
	{
		$name = $reg_buy_village_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("village_name_check",$reg_buy_village_master_search_data))
	{
		$village_name_check = $reg_buy_village_master_search_data["village_name_check"];
	}
	else
	{
		$village_name_check = "";
	}
	
	if(array_key_exists("active",$reg_buy_village_master_search_data))
	{
		$active = $reg_buy_village_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_village_master_search_data))
	{
		$added_by = $reg_buy_village_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$reg_buy_village_master_search_data))
	{
		$start_date= $reg_buy_village_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$reg_buy_village_master_search_data))
	{
		$end_date= $reg_buy_village_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_reg_buy_village_master_list_squery_base = "select * from reg_buy_village_master RBVM inner join users U on U.user_id = RBVM.reg_buy_village_master_added_by";
	
	$get_reg_buy_village_master_list_squery_where = "";
	$get_reg_buy_village_master_list_squery_order_by = " order by reg_buy_village_master_name ASC";
	$filter_count = 0;
	
	// Data
	$get_reg_buy_village_master_list_sdata = array();
	
	if($village_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." where reg_buy_village_master_id = :village_id";								
		}
		else
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." and reg_buy_village_master_id = :village_id";				
		}
		
		// Data
		$get_reg_buy_village_master_list_sdata[':village_id'] = $village_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($village_name_check == '1')
			{
				$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." where reg_buy_village_master_name = :name";		
				// Data
				$get_reg_buy_village_master_list_sdata[':name']  = $name;
			}
			else if($village_name_check == '2')
			{
				$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." where reg_buy_village_master_name like :name";						

				// Data
				$get_reg_buy_village_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." where reg_buy_village_master_name like :name";						

				// Data
				$get_reg_buy_village_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($village_name_check == '1')
			{
				$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." and reg_buy_village_master_name = :name";	
					
				// Data
				$get_reg_buy_village_master_list_sdata[':name']  = $name;
			}
			else if($village_name_check == '2')
			{
				$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." or reg_buy_village_master_name like :name";				
				// Data
				$get_reg_buy_village_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." and reg_buy_village_master_name like :name";
				
				// Data
				$get_reg_buy_village_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." where reg_buy_village_master_active = :active";								
		}
		else
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." and reg_buy_village_master_active = :active";				
		}
		
		// Data
		$get_reg_buy_village_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." where reg_buy_village_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." and reg_buy_village_master_added_by = :added_by";
		}
		
		//Data
		$get_reg_buy_village_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." where reg_buy_village_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." and reg_buy_village_master_added_on >= :start_date";				
		}
		
		//Data
		$get_reg_buy_village_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." where reg_buy_village_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reg_buy_village_master_list_squery_where = $get_reg_buy_village_master_list_squery_where." and reg_buy_village_master_added_on <= :end_date";
		}
		
		//Data
		$get_reg_buy_village_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reg_buy_village_master_list_squery = $get_reg_buy_village_master_list_squery_base.$get_reg_buy_village_master_list_squery_where.$get_reg_buy_village_master_list_squery_order_by;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reg_buy_village_master_list_sstatement = $dbConnection->prepare($get_reg_buy_village_master_list_squery);
		
		$get_reg_buy_village_master_list_sstatement -> execute($get_reg_buy_village_master_list_sdata);
		
		$get_reg_buy_village_master_list_sdetails = $get_reg_buy_village_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reg_buy_village_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reg_buy_village_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reg_buy_village_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Reg Buy Village Master
INPUT 	: Master ID, Reg Buy Village Master Update Array
OUTPUT 	: Master ID; Message of success or failure
BY 		: Ashwini
*/
function db_update_reg_buy_village_master($village_id,$reg_buy_village_master_update_data)
{
	if(array_key_exists("name",$reg_buy_village_master_update_data))
	{	
		$name = $reg_buy_village_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$reg_buy_village_master_update_data))
	{	
		$active = $reg_buy_village_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$reg_buy_village_master_update_data))
	{	
		$remarks = $reg_buy_village_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$reg_buy_village_master_update_data))
	{	
		$added_by = $reg_buy_village_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$reg_buy_village_master_update_data))
	{	
		$added_on = $reg_buy_village_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $reg_buy_village_master_update_uquery_base = "update reg_buy_village_master set";  
	
	$reg_buy_village_master_update_uquery_set = "";
	
	$reg_buy_village_master_update_uquery_where = " where reg_buy_village_master_id = :village_id";
	
	$reg_buy_village_master_update_udata = array(":village_id"=>$village_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$reg_buy_village_master_update_uquery_set = $reg_buy_village_master_update_uquery_set." reg_buy_village_master_name = :name,";
		$reg_buy_village_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$reg_buy_village_master_update_uquery_set = $reg_buy_village_master_update_uquery_set." reg_buy_village_master_active = :active,";
		$reg_buy_village_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$reg_buy_village_master_update_uquery_set = $reg_buy_village_master_update_uquery_set." reg_buy_village_master_remarks = :remarks,";
		$reg_buy_village_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$reg_buy_village_master_update_uquery_set = $reg_buy_village_master_update_uquery_set." reg_buy_village_master_added_by = :added_by,";
		$reg_buy_village_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$reg_buy_village_master_update_uquery_set = $reg_buy_village_master_update_uquery_set." reg_buy_village_master_added_on = :added_on,";
		$reg_buy_village_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$reg_buy_village_master_update_uquery_set = trim($reg_buy_village_master_update_uquery_set,',');
	}
	
	$reg_buy_village_master_update_uquery = $reg_buy_village_master_update_uquery_base.$reg_buy_village_master_update_uquery_set.$reg_buy_village_master_update_uquery_where;
  
    try
    {
        $dbConnection = get_conn_handle();
        
        $reg_buy_village_master_update_ustatement = $dbConnection->prepare($reg_buy_village_master_update_uquery);		
        
        $reg_buy_village_master_update_ustatement -> execute($reg_buy_village_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $village_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

?>